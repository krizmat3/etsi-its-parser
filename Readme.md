# ETSI-ITS Parser
This is a simplified parser of ETSI-ITS messages into `.json` format, currently it is capable of parsing
- CAM
- DENM
- SPATEM
- MAPEM
- SSEM

In most cases SREM messages have TSB_Multi_Hop header, which is not implemented
in Vanetza, but as it is always 28 bytes long, you can hack it into functioning
by adding `bytes = 28` to the correct line in
`std::size_t Parser::parse_extended(HeaderVariant& extended, HeaderType ht)`
function in `vanetza/vanetza/geonet/parser.cpp`. The compiled version has this
hack added.

## Usage
This executable needs two libraries
- GeographicLib (> 1.37) `sudo apt install libgeographic-dev`
- Crypto++ (> 5.6.1) `sudo apt install libcrypto++-dev`

### Building
If you want to build this from source, you will additionaly need
- Boost (> 1.58) `sudo apt install libboost-all-dev`
- RapidJSON `sudo apt install rapidjson-dev`
- PcapPlusPlus `sudo apt install pcapplusplus`

Vanetza is included as a submodule, you can intialise it using
`git submodule update --init --recursive`.

As we do not need the full scope of this Vanetza fork, go to
[vanetza/CMakeLists.txt](vanetza/CMakeLists.txt) and comment/delete line containing
`add_subdirectory(tools/socktap)` as this functionality adds other hard to come by dependencies
and I cannot be bothered to actually fork that library and change it for good.

## How does it work
I simply put together PcapPlusPlus, Vanetza parser and RapidJSON intergration from
[mobility-networks Vanetza version](https://code.nap.av.it.pt/mobility-networks/vanetza), internally
it is rather simple, apart from geonetworking parsing, which is always messy.

# Disclaimer
There are still some edge cases, but from a sample of 285655 messages
this parsed 285648.
