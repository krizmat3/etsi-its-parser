#ifndef ETSI_ITS_SUBPARSERS_HPP
#define ETSI_ITS_SUBPARSERS_HPP

#include <cstdlib>

#include "headers.hpp"

int parse_radiotap(
    radiotap_header_t** ppRadiotapHeader,

    const uint8_t** ppBuffer_new,
    size_t* pBufLen_new,

    const uint8_t* pBuffer,
    size_t bufLen);

int parse_ieee_qos(
    IEEE_QoS_t** ppIeeeQos,

    const uint8_t** ppBuffer_new,
    size_t* pBufLen_new,

    const uint8_t* pBuffer,
    size_t bufLen);

int parse_logical_link_control(
    logical_link_control_t** ppLogicalLinkControl,

    const uint8_t** ppBuffer_new,
    size_t* pBufLen_new,

    const uint8_t* pBuffer,
    size_t bufLen);

#endif // ETSI_ITS_PARSER_SUBPARSERS_HPP
