/*
*   JSON marshalling and unmarshalling functions for use by RapidJSON
*   Auto-generated from the asn1 directory by asn1json.py on 2024-06-20 15:49:12.960167
*/

#include "asn1json.hpp"
#include <boost/optional.hpp>

Value to_json(const ITS_Container_TimestampIts_t& p, Document::AllocatorType& allocator) {
    long tmp;
    asn_INTEGER2long(&p, &tmp);
    return Value(tmp);
}


Value to_json(const long& p, Document::AllocatorType& allocator) {
    return Value(p);
}


Value to_json(const unsigned long* p, Document::AllocatorType& allocator) {
    return Value(*p);
}


Value to_json(const unsigned long& p, Document::AllocatorType& allocator) {
    return Value(p);
}


Value to_json(const unsigned& p, Document::AllocatorType& allocator) {
    return Value(p);
}


Value to_json(const double& p, Document::AllocatorType& allocator) {
    return Value(p);
}


Value to_json(const bool& p, Document::AllocatorType& allocator) {
    return Value(p);
}


Value to_json(const OCTET_STRING_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    return json;
    // TODO
}


Value to_json(const NULL_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    return json;
    // TODO
}




/*
*   AccelerationControl - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_AccelerationControl(const ETSI_ITS_CDD_AccelerationControl_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("brakePedalEngaged", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("gasPedalEngaged", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("emergencyBrakeEngaged", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("collisionWarningEngaged", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("accEngaged", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("cruiseControlEngaged", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("speedLimiterEngaged", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    return json;
}


/*
*   CountryCode - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_CountryCode(const ETSI_ITS_CDD_CountryCode_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    return json;
}


/*
*   DrivingLaneStatus - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_DrivingLaneStatus(const ETSI_ITS_CDD_DrivingLaneStatus_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    return json;
}


/*
*   EmergencyPriority - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_EmergencyPriority(const ETSI_ITS_CDD_EmergencyPriority_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("requestForRightOfWay", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("requestForFreeCrossingAtATrafficLight", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    return json;
}


/*
*   EnergyStorageType - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_EnergyStorageType(const ETSI_ITS_CDD_EnergyStorageType_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("hydrogenStorage", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("electricEnergyStorage", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("liquidPropaneGas", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("compressedNaturalGas", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("diesel", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("gasoline", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("ammonia", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    return json;
}


/*
*   ExteriorLights - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_ExteriorLights(const ETSI_ITS_CDD_ExteriorLights_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lowBeamHeadlightsOn", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("highBeamHeadlightsOn", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("leftTurnSignalOn", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("rightTurnSignalOn", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("daytimeRunningLightsOn", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("reverseLightOn", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("fogLightOn", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    json.AddMember("parkingLightsOn", (bool) (*(p.buf + (sizeof(uint8_t) * (7 / 8))) & (1 << ((7 * ((7 / 8) + 1))-(7 % 8)))), allocator);
    return json;
}


/*
*   LightBarSirenInUse - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_LightBarSirenInUse(const ETSI_ITS_CDD_LightBarSirenInUse_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lightBarActivated", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("sirenActivated", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    return json;
}


/*
*   MatrixIncludedComponents - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_MatrixIncludedComponents(const MatrixIncludedComponents_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("xPosition", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("yPosition", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("zPosition", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("xVelocityOrVelocityMagnitude", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("yVelocityOrVelocityDirection", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("zSpeed", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("xAccelOrAccelMagnitude", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    json.AddMember("yAccelOrAccelDirection", (bool) (*(p.buf + (sizeof(uint8_t) * (7 / 8))) & (1 << ((7 * ((7 / 8) + 1))-(7 % 8)))), allocator);
    json.AddMember("zAcceleration", (bool) (*(p.buf + (sizeof(uint8_t) * (8 / 8))) & (1 << ((7 * ((8 / 8) + 1))-(8 % 8)))), allocator);
    json.AddMember("zAngle", (bool) (*(p.buf + (sizeof(uint8_t) * (9 / 8))) & (1 << ((7 * ((9 / 8) + 1))-(9 % 8)))), allocator);
    json.AddMember("yAngle", (bool) (*(p.buf + (sizeof(uint8_t) * (10 / 8))) & (1 << ((7 * ((10 / 8) + 1))-(10 % 8)))), allocator);
    json.AddMember("xAngle", (bool) (*(p.buf + (sizeof(uint8_t) * (11 / 8))) & (1 << ((7 * ((11 / 8) + 1))-(11 % 8)))), allocator);
    json.AddMember("zAngularVelocity", (bool) (*(p.buf + (sizeof(uint8_t) * (12 / 8))) & (1 << ((7 * ((12 / 8) + 1))-(12 % 8)))), allocator);
    return json;
}


/*
*   PositionOfOccupants - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_PositionOfOccupants(const ETSI_ITS_CDD_PositionOfOccupants_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("row1LeftOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("row1RightOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("row1MidOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("row1NotDetectable", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("row1NotPresent", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("row2LeftOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("row2RightOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    json.AddMember("row2MidOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (7 / 8))) & (1 << ((7 * ((7 / 8) + 1))-(7 % 8)))), allocator);
    json.AddMember("row2NotDetectable", (bool) (*(p.buf + (sizeof(uint8_t) * (8 / 8))) & (1 << ((7 * ((8 / 8) + 1))-(8 % 8)))), allocator);
    json.AddMember("row2NotPresent", (bool) (*(p.buf + (sizeof(uint8_t) * (9 / 8))) & (1 << ((7 * ((9 / 8) + 1))-(9 % 8)))), allocator);
    json.AddMember("row3LeftOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (10 / 8))) & (1 << ((7 * ((10 / 8) + 1))-(10 % 8)))), allocator);
    json.AddMember("row3RightOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (11 / 8))) & (1 << ((7 * ((11 / 8) + 1))-(11 % 8)))), allocator);
    json.AddMember("row3MidOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (12 / 8))) & (1 << ((7 * ((12 / 8) + 1))-(12 % 8)))), allocator);
    json.AddMember("row3NotDetectable", (bool) (*(p.buf + (sizeof(uint8_t) * (13 / 8))) & (1 << ((7 * ((13 / 8) + 1))-(13 % 8)))), allocator);
    json.AddMember("row3NotPresent", (bool) (*(p.buf + (sizeof(uint8_t) * (14 / 8))) & (1 << ((7 * ((14 / 8) + 1))-(14 % 8)))), allocator);
    json.AddMember("row4LeftOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (15 / 8))) & (1 << ((7 * ((15 / 8) + 1))-(15 % 8)))), allocator);
    json.AddMember("row4RightOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (16 / 8))) & (1 << ((7 * ((16 / 8) + 1))-(16 % 8)))), allocator);
    json.AddMember("row4MidOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (17 / 8))) & (1 << ((7 * ((17 / 8) + 1))-(17 % 8)))), allocator);
    json.AddMember("row4NotDetectable", (bool) (*(p.buf + (sizeof(uint8_t) * (18 / 8))) & (1 << ((7 * ((18 / 8) + 1))-(18 % 8)))), allocator);
    json.AddMember("row4NotPresent", (bool) (*(p.buf + (sizeof(uint8_t) * (19 / 8))) & (1 << ((7 * ((19 / 8) + 1))-(19 % 8)))), allocator);
    return json;
}


/*
*   SensorTypes - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_SensorTypes(const SensorTypes_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("undefined", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("radar", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("lidar", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("monovideo", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("stereovision", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("nightvision", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("ultrasonic", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    json.AddMember("pmd", (bool) (*(p.buf + (sizeof(uint8_t) * (7 / 8))) & (1 << ((7 * ((7 / 8) + 1))-(7 % 8)))), allocator);
    json.AddMember("inductionLoop", (bool) (*(p.buf + (sizeof(uint8_t) * (8 / 8))) & (1 << ((7 * ((8 / 8) + 1))-(8 % 8)))), allocator);
    json.AddMember("sphericalCamera", (bool) (*(p.buf + (sizeof(uint8_t) * (9 / 8))) & (1 << ((7 * ((9 / 8) + 1))-(9 % 8)))), allocator);
    json.AddMember("uwb", (bool) (*(p.buf + (sizeof(uint8_t) * (10 / 8))) & (1 << ((7 * ((10 / 8) + 1))-(10 % 8)))), allocator);
    json.AddMember("acoustic", (bool) (*(p.buf + (sizeof(uint8_t) * (11 / 8))) & (1 << ((7 * ((11 / 8) + 1))-(11 % 8)))), allocator);
    json.AddMember("localAggregation", (bool) (*(p.buf + (sizeof(uint8_t) * (12 / 8))) & (1 << ((7 * ((12 / 8) + 1))-(12 % 8)))), allocator);
    json.AddMember("itsAggregation", (bool) (*(p.buf + (sizeof(uint8_t) * (13 / 8))) & (1 << ((7 * ((13 / 8) + 1))-(13 % 8)))), allocator);
    return json;
}


/*
*   SpecialTransportType - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_SpecialTransportType(const ETSI_ITS_CDD_SpecialTransportType_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("heavyLoad", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("excessWidth", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("excessLength", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("excessHeight", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    return json;
}


/*
*   StoredInformationType - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_StoredInformationType(const StoredInformationType_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("undefined", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("staticDb", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("dynamicDb", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("realTimeDb", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("map", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    return json;
}


/*
*   VruClusterProfiles - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_VruClusterProfiles(const VruClusterProfiles_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("pedestrian", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("bicyclist", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("motorcyclist", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("animal", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    return json;
}


/*
*   VruSpecificExteriorLights - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_VruSpecificExteriorLights(const ETSI_ITS_CDD_VruSpecificExteriorLights_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("unavailable", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("backFlashLight", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("helmetLight", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("armLight", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("legLight", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("wheelLight", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    return json;
}


/*
*   AccelerationComponent - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const AccelerationComponent_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("value", to_json((p.value), allocator), allocator);
    json.AddMember("confidence", to_json(((p.confidence) == 102) ? (p.confidence) : (double)(p.confidence) / 10.0, allocator), allocator);

    return json;
}




/*
*   AccelerationChangeIndication - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_AccelerationChangeIndication_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("accelOrDecel", to_json((p.accelOrDecel), allocator), allocator);
    json.AddMember("actionDeltaTime", to_json((p.actionDeltaTime), allocator), allocator);

    return json;
}




/*
*   AccelerationMagnitude - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const AccelerationMagnitude_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("accelerationMagnitudeValue", to_json((p.accelerationMagnitudeValue), allocator), allocator);
    json.AddMember("accelerationConfidence", to_json(((p.accelerationConfidence) == 102) ? (p.accelerationConfidence) : (double)(p.accelerationConfidence) / 10.0, allocator), allocator);

    return json;
}




/*
*   ActionId - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ActionId_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("originatingStationId", to_json((p.originatingStationId), allocator), allocator);
    json.AddMember("sequenceNumber", to_json((p.sequenceNumber), allocator), allocator);

    return json;
}




/*
*   ActionID - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ActionID_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("originatingStationId", to_json((p.originatingStationId), allocator), allocator);
    json.AddMember("sequenceNumber", to_json((p.sequenceNumber), allocator), allocator);

    return json;
}




/*
*   ActionIdList - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ActionIdList_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ActionId_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   Altitude - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_Altitude_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("altitudeValue", to_json(((p.altitudeValue) == 800001) ? (p.altitudeValue) : (double)(p.altitudeValue) / 100.0, allocator), allocator);
    json.AddMember("altitudeConfidence", to_json((p.altitudeConfidence), allocator), allocator);

    return json;
}




/*
*   BasicLaneInformation - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const BasicLaneInformation_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("laneNumber", to_json((p.laneNumber), allocator), allocator);
    json.AddMember("direction", to_json((p.direction), allocator), allocator);
    if (p.laneWidth != 0) json.AddMember("laneWidth", to_json(*(p.laneWidth), allocator), allocator);
    if (p.connectingLane != 0) json.AddMember("connectingLane", to_json(*(p.connectingLane), allocator), allocator);
    if (p.connectingRoadSection != 0) json.AddMember("connectingRoadSection", to_json(*(p.connectingRoadSection), allocator), allocator);
    return json;
}




/*
*   CartesianAngle - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CartesianAngle_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("value", to_json(((p.value) == 3601) ? (p.value) : (double)(p.value) / 10.0, allocator), allocator);
    json.AddMember("confidence", to_json((p.confidence), allocator), allocator);

    return json;
}




/*
*   CartesianAngularVelocityComponent - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CartesianAngularVelocityComponent_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("value", to_json((p.value), allocator), allocator);
    json.AddMember("confidence", to_json((p.confidence), allocator), allocator);

    return json;
}




/*
*   CartesianAngularAccelerationComponent - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CartesianAngularAccelerationComponent_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("value", to_json((p.value), allocator), allocator);
    json.AddMember("confidence", to_json((p.confidence), allocator), allocator);

    return json;
}




/*
*   CartesianCoordinateWithConfidence - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CartesianCoordinateWithConfidence_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("value", to_json((p.value), allocator), allocator);
    json.AddMember("confidence", to_json((p.confidence), allocator), allocator);

    return json;
}




/*
*   CartesianPosition3d - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CartesianPosition3d_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("xCoordinate", to_json((p.xCoordinate), allocator), allocator);
    json.AddMember("yCoordinate", to_json((p.yCoordinate), allocator), allocator);
    if (p.zCoordinate != 0) json.AddMember("zCoordinate", to_json(*(p.zCoordinate), allocator), allocator);
    return json;
}




/*
*   CartesianPosition3dWithConfidence - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CartesianPosition3dWithConfidence_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("xCoordinate", to_json((p.xCoordinate), allocator), allocator);
    json.AddMember("yCoordinate", to_json((p.yCoordinate), allocator), allocator);
    if (p.zCoordinate != 0) json.AddMember("zCoordinate", to_json(*(p.zCoordinate), allocator), allocator);
    return json;
}




/*
*   CauseCode - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_CauseCode_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("causeCode", to_json((p.causeCode), allocator), allocator);
    json.AddMember("subCauseCode", to_json((p.subCauseCode), allocator), allocator);

    return json;
}




/*
*   CauseCodeChoice - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CauseCodeChoice_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == CauseCodeChoice_PR_reserved0) {
        json.AddMember("reserved0", to_json(p.choice.reserved0, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_trafficCondition1) {
        json.AddMember("trafficCondition1", to_json(p.choice.trafficCondition1, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_accident2) {
        json.AddMember("accident2", to_json(p.choice.accident2, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_roadworks3) {
        json.AddMember("roadworks3", to_json(p.choice.roadworks3, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved4) {
        json.AddMember("reserved4", to_json(p.choice.reserved4, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_impassability5) {
        json.AddMember("impassability5", to_json(p.choice.impassability5, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_adverseWeatherCondition_Adhesion6) {
        json.AddMember("adverseWeatherCondition-Adhesion6", to_json(p.choice.adverseWeatherCondition_Adhesion6, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_aquaplaning7) {
        json.AddMember("aquaplaning7", to_json(p.choice.aquaplaning7, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved8) {
        json.AddMember("reserved8", to_json(p.choice.reserved8, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_hazardousLocation_SurfaceCondition9) {
        json.AddMember("hazardousLocation-SurfaceCondition9", to_json(p.choice.hazardousLocation_SurfaceCondition9, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_hazardousLocation_ObstacleOnTheRoad10) {
        json.AddMember("hazardousLocation-ObstacleOnTheRoad10", to_json(p.choice.hazardousLocation_ObstacleOnTheRoad10, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_hazardousLocation_AnimalOnTheRoad11) {
        json.AddMember("hazardousLocation-AnimalOnTheRoad11", to_json(p.choice.hazardousLocation_AnimalOnTheRoad11, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_humanPresenceOnTheRoad12) {
        json.AddMember("humanPresenceOnTheRoad12", to_json(p.choice.humanPresenceOnTheRoad12, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved13) {
        json.AddMember("reserved13", to_json(p.choice.reserved13, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_wrongWayDriving14) {
        json.AddMember("wrongWayDriving14", to_json(p.choice.wrongWayDriving14, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_rescueAndRecoveryWorkInProgress15) {
        json.AddMember("rescueAndRecoveryWorkInProgress15", to_json(p.choice.rescueAndRecoveryWorkInProgress15, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved16) {
        json.AddMember("reserved16", to_json(p.choice.reserved16, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_adverseWeatherCondition_ExtremeWeatherCondition17) {
        json.AddMember("adverseWeatherCondition-ExtremeWeatherCondition17", to_json(p.choice.adverseWeatherCondition_ExtremeWeatherCondition17, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_adverseWeatherCondition_Visibility18) {
        json.AddMember("adverseWeatherCondition-Visibility18", to_json(p.choice.adverseWeatherCondition_Visibility18, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_adverseWeatherCondition_Precipitation19) {
        json.AddMember("adverseWeatherCondition-Precipitation19", to_json(p.choice.adverseWeatherCondition_Precipitation19, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_violence20) {
        json.AddMember("violence20", to_json(p.choice.violence20, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved21) {
        json.AddMember("reserved21", to_json(p.choice.reserved21, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved22) {
        json.AddMember("reserved22", to_json(p.choice.reserved22, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved23) {
        json.AddMember("reserved23", to_json(p.choice.reserved23, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved24) {
        json.AddMember("reserved24", to_json(p.choice.reserved24, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved25) {
        json.AddMember("reserved25", to_json(p.choice.reserved25, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_slowVehicle26) {
        json.AddMember("slowVehicle26", to_json(p.choice.slowVehicle26, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_dangerousEndOfQueue27) {
        json.AddMember("dangerousEndOfQueue27", to_json(p.choice.dangerousEndOfQueue27, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_publicTransportVehicleApproaching28) {
        json.AddMember("publicTransportVehicleApproaching28", to_json(p.choice.publicTransportVehicleApproaching28, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved29) {
        json.AddMember("reserved29", to_json(p.choice.reserved29, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved30) {
        json.AddMember("reserved30", to_json(p.choice.reserved30, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved31) {
        json.AddMember("reserved31", to_json(p.choice.reserved31, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved32) {
        json.AddMember("reserved32", to_json(p.choice.reserved32, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved33) {
        json.AddMember("reserved33", to_json(p.choice.reserved33, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved34) {
        json.AddMember("reserved34", to_json(p.choice.reserved34, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved35) {
        json.AddMember("reserved35", to_json(p.choice.reserved35, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved36) {
        json.AddMember("reserved36", to_json(p.choice.reserved36, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved37) {
        json.AddMember("reserved37", to_json(p.choice.reserved37, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved38) {
        json.AddMember("reserved38", to_json(p.choice.reserved38, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved39) {
        json.AddMember("reserved39", to_json(p.choice.reserved39, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved40) {
        json.AddMember("reserved40", to_json(p.choice.reserved40, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved41) {
        json.AddMember("reserved41", to_json(p.choice.reserved41, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved42) {
        json.AddMember("reserved42", to_json(p.choice.reserved42, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved43) {
        json.AddMember("reserved43", to_json(p.choice.reserved43, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved44) {
        json.AddMember("reserved44", to_json(p.choice.reserved44, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved45) {
        json.AddMember("reserved45", to_json(p.choice.reserved45, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved46) {
        json.AddMember("reserved46", to_json(p.choice.reserved46, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved47) {
        json.AddMember("reserved47", to_json(p.choice.reserved47, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved48) {
        json.AddMember("reserved48", to_json(p.choice.reserved48, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved49) {
        json.AddMember("reserved49", to_json(p.choice.reserved49, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved50) {
        json.AddMember("reserved50", to_json(p.choice.reserved50, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved51) {
        json.AddMember("reserved51", to_json(p.choice.reserved51, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved52) {
        json.AddMember("reserved52", to_json(p.choice.reserved52, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved53) {
        json.AddMember("reserved53", to_json(p.choice.reserved53, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved54) {
        json.AddMember("reserved54", to_json(p.choice.reserved54, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved55) {
        json.AddMember("reserved55", to_json(p.choice.reserved55, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved56) {
        json.AddMember("reserved56", to_json(p.choice.reserved56, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved57) {
        json.AddMember("reserved57", to_json(p.choice.reserved57, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved58) {
        json.AddMember("reserved58", to_json(p.choice.reserved58, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved59) {
        json.AddMember("reserved59", to_json(p.choice.reserved59, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved60) {
        json.AddMember("reserved60", to_json(p.choice.reserved60, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved61) {
        json.AddMember("reserved61", to_json(p.choice.reserved61, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved62) {
        json.AddMember("reserved62", to_json(p.choice.reserved62, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved63) {
        json.AddMember("reserved63", to_json(p.choice.reserved63, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved64) {
        json.AddMember("reserved64", to_json(p.choice.reserved64, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved65) {
        json.AddMember("reserved65", to_json(p.choice.reserved65, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved66) {
        json.AddMember("reserved66", to_json(p.choice.reserved66, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved67) {
        json.AddMember("reserved67", to_json(p.choice.reserved67, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved68) {
        json.AddMember("reserved68", to_json(p.choice.reserved68, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved69) {
        json.AddMember("reserved69", to_json(p.choice.reserved69, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved70) {
        json.AddMember("reserved70", to_json(p.choice.reserved70, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved71) {
        json.AddMember("reserved71", to_json(p.choice.reserved71, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved72) {
        json.AddMember("reserved72", to_json(p.choice.reserved72, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved73) {
        json.AddMember("reserved73", to_json(p.choice.reserved73, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved74) {
        json.AddMember("reserved74", to_json(p.choice.reserved74, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved75) {
        json.AddMember("reserved75", to_json(p.choice.reserved75, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved76) {
        json.AddMember("reserved76", to_json(p.choice.reserved76, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved77) {
        json.AddMember("reserved77", to_json(p.choice.reserved77, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved78) {
        json.AddMember("reserved78", to_json(p.choice.reserved78, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved79) {
        json.AddMember("reserved79", to_json(p.choice.reserved79, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved80) {
        json.AddMember("reserved80", to_json(p.choice.reserved80, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved81) {
        json.AddMember("reserved81", to_json(p.choice.reserved81, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved82) {
        json.AddMember("reserved82", to_json(p.choice.reserved82, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved83) {
        json.AddMember("reserved83", to_json(p.choice.reserved83, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved84) {
        json.AddMember("reserved84", to_json(p.choice.reserved84, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved85) {
        json.AddMember("reserved85", to_json(p.choice.reserved85, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved86) {
        json.AddMember("reserved86", to_json(p.choice.reserved86, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved87) {
        json.AddMember("reserved87", to_json(p.choice.reserved87, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved88) {
        json.AddMember("reserved88", to_json(p.choice.reserved88, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved89) {
        json.AddMember("reserved89", to_json(p.choice.reserved89, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved90) {
        json.AddMember("reserved90", to_json(p.choice.reserved90, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_vehicleBreakdown91) {
        json.AddMember("vehicleBreakdown91", to_json(p.choice.vehicleBreakdown91, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_postCrash92) {
        json.AddMember("postCrash92", to_json(p.choice.postCrash92, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_humanProblem93) {
        json.AddMember("humanProblem93", to_json(p.choice.humanProblem93, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_stationaryVehicle94) {
        json.AddMember("stationaryVehicle94", to_json(p.choice.stationaryVehicle94, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_emergencyVehicleApproaching95) {
        json.AddMember("emergencyVehicleApproaching95", to_json(p.choice.emergencyVehicleApproaching95, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_hazardousLocation_DangerousCurve96) {
        json.AddMember("hazardousLocation-DangerousCurve96", to_json(p.choice.hazardousLocation_DangerousCurve96, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_collisionRisk97) {
        json.AddMember("collisionRisk97", to_json(p.choice.collisionRisk97, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_signalViolation98) {
        json.AddMember("signalViolation98", to_json(p.choice.signalViolation98, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_dangerousSituation99) {
        json.AddMember("dangerousSituation99", to_json(p.choice.dangerousSituation99, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_railwayLevelCrossing100) {
        json.AddMember("railwayLevelCrossing100", to_json(p.choice.railwayLevelCrossing100, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved101) {
        json.AddMember("reserved101", to_json(p.choice.reserved101, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved102) {
        json.AddMember("reserved102", to_json(p.choice.reserved102, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved103) {
        json.AddMember("reserved103", to_json(p.choice.reserved103, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved104) {
        json.AddMember("reserved104", to_json(p.choice.reserved104, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved105) {
        json.AddMember("reserved105", to_json(p.choice.reserved105, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved106) {
        json.AddMember("reserved106", to_json(p.choice.reserved106, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved107) {
        json.AddMember("reserved107", to_json(p.choice.reserved107, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved108) {
        json.AddMember("reserved108", to_json(p.choice.reserved108, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved109) {
        json.AddMember("reserved109", to_json(p.choice.reserved109, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved110) {
        json.AddMember("reserved110", to_json(p.choice.reserved110, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved111) {
        json.AddMember("reserved111", to_json(p.choice.reserved111, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved112) {
        json.AddMember("reserved112", to_json(p.choice.reserved112, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved113) {
        json.AddMember("reserved113", to_json(p.choice.reserved113, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved114) {
        json.AddMember("reserved114", to_json(p.choice.reserved114, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved115) {
        json.AddMember("reserved115", to_json(p.choice.reserved115, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved116) {
        json.AddMember("reserved116", to_json(p.choice.reserved116, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved117) {
        json.AddMember("reserved117", to_json(p.choice.reserved117, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved118) {
        json.AddMember("reserved118", to_json(p.choice.reserved118, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved119) {
        json.AddMember("reserved119", to_json(p.choice.reserved119, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved120) {
        json.AddMember("reserved120", to_json(p.choice.reserved120, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved121) {
        json.AddMember("reserved121", to_json(p.choice.reserved121, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved122) {
        json.AddMember("reserved122", to_json(p.choice.reserved122, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved123) {
        json.AddMember("reserved123", to_json(p.choice.reserved123, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved124) {
        json.AddMember("reserved124", to_json(p.choice.reserved124, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved125) {
        json.AddMember("reserved125", to_json(p.choice.reserved125, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved126) {
        json.AddMember("reserved126", to_json(p.choice.reserved126, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved127) {
        json.AddMember("reserved127", to_json(p.choice.reserved127, allocator), allocator);
    } else if (p.present == CauseCodeChoice_PR_reserved128) {
        json.AddMember("reserved128", to_json(p.choice.reserved128, allocator), allocator);
    }
    return json;
}


/*
*   CauseCodeV2 - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CauseCodeV2_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("ccAndScc", to_json((p.ccAndScc), allocator), allocator);

    return json;
}




/*
*   CenDsrcTollingZone - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_CenDsrcTollingZone_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("protectedZoneLatitude", to_json(((p.protectedZoneLatitude) == 900000001) ? (p.protectedZoneLatitude) : (double)(p.protectedZoneLatitude) / 10000000.0, allocator), allocator);
    json.AddMember("protectedZoneLongitude", to_json(((p.protectedZoneLongitude) == 1800000001) ? (p.protectedZoneLongitude) : (double)(p.protectedZoneLongitude) / 10000000.0, allocator), allocator);
    if (p.cenDsrcTollingZoneId != 0) json.AddMember("cenDsrcTollingZoneId", to_json(*(p.cenDsrcTollingZoneId), allocator), allocator);
    return json;
}




/*
*   CircularShape - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CircularShape_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("radius", to_json((p.radius), allocator), allocator);
    if (p.shapeReferencePoint != 0) json.AddMember("shapeReferencePoint", to_json(*(p.shapeReferencePoint), allocator), allocator);
    if (p.height != 0) json.AddMember("height", to_json(*(p.height), allocator), allocator);
    return json;
}




/*
*   ClosedLanes - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ClosedLanes_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.innerhardShoulderStatus != 0) json.AddMember("innerhardShoulderStatus", to_json(*(p.innerhardShoulderStatus), allocator), allocator);
    if (p.outerhardShoulderStatus != 0) json.AddMember("outerhardShoulderStatus", to_json(*(p.outerhardShoulderStatus), allocator), allocator);
    if (p.drivingLaneStatus != 0) json.AddMember("drivingLaneStatus", to_json_ETSI_ITS_CDD_DrivingLaneStatus(*(p.drivingLaneStatus), allocator), allocator);
    return json;
}




/*
*   ClusterBreakupInfo - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ClusterBreakupInfo_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("clusterBreakupReason", to_json((p.clusterBreakupReason), allocator), allocator);
    json.AddMember("breakupTime", to_json((p.breakupTime), allocator), allocator);

    return json;
}




/*
*   ClusterJoinInfo - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ClusterJoinInfo_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("clusterId", to_json((p.clusterId), allocator), allocator);
    json.AddMember("joinTime", to_json((p.joinTime), allocator), allocator);

    return json;
}




/*
*   ClusterLeaveInfo - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ClusterLeaveInfo_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("clusterId", to_json((p.clusterId), allocator), allocator);
    json.AddMember("clusterLeaveReason", to_json((p.clusterLeaveReason), allocator), allocator);

    return json;
}




/*
*   CorrelationColumn - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CorrelationColumn_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const CorrelationCellValue_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   Curvature - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_Curvature_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("curvatureValue", to_json((p.curvatureValue), allocator), allocator);
    json.AddMember("curvatureConfidence", to_json((p.curvatureConfidence), allocator), allocator);

    return json;
}




/*
*   DangerousGoodsExtended - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_DangerousGoodsExtended_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("dangerousGoodsType", to_json((p.dangerousGoodsType), allocator), allocator);
    json.AddMember("unNumber", to_json((p.unNumber), allocator), allocator);
    json.AddMember("elevatedTemperature", to_json((p.elevatedTemperature), allocator), allocator);
    json.AddMember("tunnelsRestricted", to_json((p.tunnelsRestricted), allocator), allocator);
    json.AddMember("limitedQuantity", to_json((p.limitedQuantity), allocator), allocator);
    if (p.emergencyActionCode != 0) json.AddMember("emergencyActionCode", to_json(*(p.emergencyActionCode), allocator), allocator);
    if (p.companyName != 0) json.AddMember("companyName", to_json(*(p.companyName), allocator), allocator);
    return json;
}




/*
*   DeltaReferencePosition - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_DeltaReferencePosition_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("deltaLatitude", to_json((p.deltaLatitude), allocator), allocator);
    json.AddMember("deltaLongitude", to_json((p.deltaLongitude), allocator), allocator);
    json.AddMember("deltaAltitude", to_json((p.deltaAltitude), allocator), allocator);

    return json;
}




/*
*   EllipticalShape - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const EllipticalShape_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("semiMajorAxisLength", to_json((p.semiMajorAxisLength), allocator), allocator);
    json.AddMember("semiMinorAxisLength", to_json((p.semiMinorAxisLength), allocator), allocator);
    if (p.shapeReferencePoint != 0) json.AddMember("shapeReferencePoint", to_json(*(p.shapeReferencePoint), allocator), allocator);
    if (p.orientation != 0) json.AddMember("orientation", to_json((*(p.orientation) != 3601) ? (double) *(p.orientation) / 10.0 : *(p.orientation), allocator), allocator);
    if (p.height != 0) json.AddMember("height", to_json(*(p.height), allocator), allocator);
    return json;
}




/*
*   EulerAnglesWithConfidence - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const EulerAnglesWithConfidence_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("zAngle", to_json((p.zAngle), allocator), allocator);
    if (p.yAngle != 0) json.AddMember("yAngle", to_json(*(p.yAngle), allocator), allocator);
    if (p.xAngle != 0) json.AddMember("xAngle", to_json(*(p.xAngle), allocator), allocator);
    return json;
}




/*
*   EuVehicleCategoryCode - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_EuVehicleCategoryCode_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == ETSI_ITS_CDD_EuVehicleCategoryCode_PR_euVehicleCategoryL) {
        json.AddMember("euVehicleCategoryL", to_json(p.choice.euVehicleCategoryL, allocator), allocator);
    } else if (p.present == ETSI_ITS_CDD_EuVehicleCategoryCode_PR_euVehicleCategoryM) {
        json.AddMember("euVehicleCategoryM", to_json(p.choice.euVehicleCategoryM, allocator), allocator);
    } else if (p.present == ETSI_ITS_CDD_EuVehicleCategoryCode_PR_euVehicleCategoryN) {
        json.AddMember("euVehicleCategoryN", to_json(p.choice.euVehicleCategoryN, allocator), allocator);
    } else if (p.present == ETSI_ITS_CDD_EuVehicleCategoryCode_PR_euVehicleCategoryO) {
        json.AddMember("euVehicleCategoryO", to_json(p.choice.euVehicleCategoryO, allocator), allocator);
    } else if (p.present == ETSI_ITS_CDD_EuVehicleCategoryCode_PR_euVehicleCategoryT) {
        json.AddMember("euVehicleCategoryT", to_json(p.choice.euVehicleCategoryT, allocator), allocator);
    } else if (p.present == ETSI_ITS_CDD_EuVehicleCategoryCode_PR_euVehicleCategoryG) {
        json.AddMember("euVehicleCategoryG", to_json(p.choice.euVehicleCategoryG, allocator), allocator);
    }
    return json;
}


/*
*   EventPoint - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_EventPoint_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("eventPosition", to_json((p.eventPosition), allocator), allocator);
    json.AddMember("informationQuality", to_json((p.informationQuality), allocator), allocator);
    if (p.eventDeltaTime != 0) json.AddMember("eventDeltaTime", to_json(*(p.eventDeltaTime), allocator), allocator);
    return json;
}




/*
*   GeoPosition - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const GeoPosition_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("latitude", to_json(((p.latitude) == 900000001) ? (p.latitude) : (double)(p.latitude) / 10000000.0, allocator), allocator);
    json.AddMember("longitude", to_json(((p.longitude) == 1800000001) ? (p.longitude) : (double)(p.longitude) / 10000000.0, allocator), allocator);
    if (p.altitude != 0) json.AddMember("altitude", to_json((*(p.altitude) != 800001) ? (double) *(p.altitude) / 100.0 : *(p.altitude), allocator), allocator);
    return json;
}




/*
*   Heading - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_Heading_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("headingValue", to_json(((p.headingValue) == 3601) ? (p.headingValue) : (double)(p.headingValue) / 10.0, allocator), allocator);
    json.AddMember("headingConfidence", to_json(((p.headingConfidence) == 126 || (p.headingConfidence) == 127) ? (p.headingConfidence) : (double)(p.headingConfidence) / 10.0, allocator), allocator);

    return json;
}




/*
*   HeadingChangeIndication - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_HeadingChangeIndication_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("direction", to_json((p.direction), allocator), allocator);
    json.AddMember("actionDeltaTime", to_json((p.actionDeltaTime), allocator), allocator);

    return json;
}




/*
*   InterferenceManagementChannel - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_InterferenceManagementChannel_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("centreFrequency", to_json((p.centreFrequency), allocator), allocator);
    json.AddMember("channelWidth", to_json((p.channelWidth), allocator), allocator);
    json.AddMember("exponent", to_json((p.exponent), allocator), allocator);

    return json;
}




/*
*   IntersectionReferenceId - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const IntersectionReferenceId_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("id", to_json((p.id), allocator), allocator);
    if (p.region != 0) json.AddMember("region", to_json(*(p.region), allocator), allocator);
    return json;
}




/*
*   ItsPduHeader - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ItsPduHeader_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("protocolVersion", to_json((p.protocolVersion), allocator), allocator);
    json.AddMember("messageId", to_json((p.messageId), allocator), allocator);
    json.AddMember("stationId", to_json((p.stationId), allocator), allocator);

    return json;
}




/*
*   LanePositionAndType - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const LanePositionAndType_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("transversalPosition", to_json((p.transversalPosition), allocator), allocator);
    json.AddMember("laneType", to_json((p.laneType), allocator), allocator);
    json.AddMember("direction", to_json((p.direction), allocator), allocator);

    return json;
}




/*
*   LanePositionWithLateralDetails - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const LanePositionWithLateralDetails_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("transversalPosition", to_json((p.transversalPosition), allocator), allocator);
    json.AddMember("laneType", to_json((p.laneType), allocator), allocator);
    json.AddMember("direction", to_json((p.direction), allocator), allocator);
    json.AddMember("distanceToLeftBorder", to_json((p.distanceToLeftBorder), allocator), allocator);
    json.AddMember("distanceToRightBorder", to_json((p.distanceToRightBorder), allocator), allocator);

    return json;
}




/*
*   LateralAcceleration - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_LateralAcceleration_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lateralAccelerationValue", to_json(((p.lateralAccelerationValue) == 161) ? (p.lateralAccelerationValue) : (double)(p.lateralAccelerationValue) / 10.0, allocator), allocator);
    json.AddMember("lateralAccelerationConfidence", to_json(((p.lateralAccelerationConfidence) == 102) ? (p.lateralAccelerationConfidence) : (double)(p.lateralAccelerationConfidence) / 10.0, allocator), allocator);

    return json;
}




/*
*   LongitudinalAcceleration - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_LongitudinalAcceleration_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("longitudinalAccelerationValue", to_json(((p.longitudinalAccelerationValue) == 161) ? (p.longitudinalAccelerationValue) : (double)(p.longitudinalAccelerationValue) / 10.0, allocator), allocator);
    json.AddMember("longitudinalAccelerationConfidence", to_json(((p.longitudinalAccelerationConfidence) == 102) ? (p.longitudinalAccelerationConfidence) : (double)(p.longitudinalAccelerationConfidence) / 10.0, allocator), allocator);

    return json;
}




/*
*   LongitudinalLanePosition - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const LongitudinalLanePosition_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("longitudinalLanePositionValue", to_json((p.longitudinalLanePositionValue), allocator), allocator);
    json.AddMember("longitudinalLanePositionConfidence", to_json((p.longitudinalLanePositionConfidence), allocator), allocator);

    return json;
}




/*
*   LowerTriangularPositiveSemidefiniteMatrixColumns - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const LowerTriangularPositiveSemidefiniteMatrixColumns_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const CorrelationColumn_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   MapemLaneList - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const MapemLaneList_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const Identifier1B_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   MapemConnectionList - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const MapemConnectionList_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const Identifier1B_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   MessageRateHz - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const MessageRateHz_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("mantissa", to_json((p.mantissa), allocator), allocator);
    json.AddMember("exponent", to_json((p.exponent), allocator), allocator);

    return json;
}




/*
*   MessageSegmentationInfo - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const MessageSegmentationInfo_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("totalMsgNo", to_json((p.totalMsgNo), allocator), allocator);
    json.AddMember("thisMsgNo", to_json((p.thisMsgNo), allocator), allocator);

    return json;
}




/*
*   MetaInformation - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const MetaInformation_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("usedDetectionInformation", to_json_SensorTypes((p.usedDetectionInformation), allocator), allocator);
    json.AddMember("usedStoredInformation", to_json_StoredInformationType((p.usedStoredInformation), allocator), allocator);
    if (p.confidenceValue != 0) json.AddMember("confidenceValue", to_json(*(p.confidenceValue), allocator), allocator);
    return json;
}




/*
*   MitigationPerTechnologyClass - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_MitigationPerTechnologyClass_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("accessTechnologyClass", to_json((p.accessTechnologyClass), allocator), allocator);
    if (p.lowDutyCycle != 0) json.AddMember("lowDutyCycle", to_json(*(p.lowDutyCycle), allocator), allocator);
    if (p.powerReduction != 0) json.AddMember("powerReduction", to_json(*(p.powerReduction), allocator), allocator);
    if (p.dmcToffLimit != 0) json.AddMember("dmcToffLimit", to_json(*(p.dmcToffLimit), allocator), allocator);
    if (p.dmcTonLimit != 0) json.AddMember("dmcTonLimit", to_json(*(p.dmcTonLimit), allocator), allocator);
    return json;
}




/*
*   ObjectDimension - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ObjectDimension_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("value", to_json((double)(p.value) / 10.0, allocator), allocator);
    json.AddMember("confidence", to_json((p.confidence), allocator), allocator);

    return json;
}




/*
*   PathDeltaTimeChoice - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PathDeltaTimeChoice_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == PathDeltaTimeChoice_PR_deltaTimeHighPrecision) {
        json.AddMember("deltaTimeHighPrecision", to_json(p.choice.deltaTimeHighPrecision, allocator), allocator);
    } else if (p.present == PathDeltaTimeChoice_PR_deltaTimeBigRange) {
        json.AddMember("deltaTimeBigRange", to_json(p.choice.deltaTimeBigRange, allocator), allocator);
    }
    return json;
}


/*
*   PathPoint - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_PathPoint_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("pathPosition", to_json((p.pathPosition), allocator), allocator);
    if (p.pathDeltaTime != 0) json.AddMember("pathDeltaTime", to_json(*(p.pathDeltaTime), allocator), allocator);
    return json;
}




/*
*   PathReferences - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PathReferences_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const PathId_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   PosConfidenceEllipse - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_PosConfidenceEllipse_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("semiMajorConfidence", to_json((p.semiMajorConfidence), allocator), allocator);
    json.AddMember("semiMinorConfidence", to_json((p.semiMinorConfidence), allocator), allocator);
    json.AddMember("semiMajorOrientation", to_json(((p.semiMajorOrientation) == 3601) ? (p.semiMajorOrientation) : (double)(p.semiMajorOrientation) / 10.0, allocator), allocator);

    return json;
}




/*
*   PositionConfidenceEllipse - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PositionConfidenceEllipse_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("semiMajorAxisLength", to_json((p.semiMajorAxisLength), allocator), allocator);
    json.AddMember("semiMinorAxisLength", to_json((p.semiMinorAxisLength), allocator), allocator);
    json.AddMember("semiMajorAxisOrientation", to_json((p.semiMajorAxisOrientation), allocator), allocator);

    return json;
}




/*
*   PositionOfPillars - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_PositionOfPillars_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ETSI_ITS_CDD_PosPillar_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   ProtectedCommunicationZone - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ProtectedCommunicationZone_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("protectedZoneType", to_json((p.protectedZoneType), allocator), allocator);
    json.AddMember("protectedZoneLatitude", to_json(((p.protectedZoneLatitude) == 900000001) ? (p.protectedZoneLatitude) : (double)(p.protectedZoneLatitude) / 10000000.0, allocator), allocator);
    json.AddMember("protectedZoneLongitude", to_json(((p.protectedZoneLongitude) == 1800000001) ? (p.protectedZoneLongitude) : (double)(p.protectedZoneLongitude) / 10000000.0, allocator), allocator);
    if (p.protectedZoneRadius != 0) json.AddMember("protectedZoneRadius", to_json(*(p.protectedZoneRadius), allocator), allocator);
    if (p.protectedZoneId != 0) json.AddMember("protectedZoneId", to_json(*(p.protectedZoneId), allocator), allocator);
    return json;
}




/*
*   ProtectedCommunicationZonesRSU - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ProtectedCommunicationZonesRSU_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ETSI_ITS_CDD_ProtectedCommunicationZone_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   Provider - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_Provider_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("countryCode", to_json_AVIAEINumberingAndDataStructures_CountryCode((p.countryCode), allocator), allocator);
    json.AddMember("providerIdentifier", to_json((p.providerIdentifier), allocator), allocator);

    return json;
}




/*
*   PtActivation - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_PtActivation_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("ptActivationType", to_json((p.ptActivationType), allocator), allocator);
    json.AddMember("ptActivationData", to_json((p.ptActivationData), allocator), allocator);

    return json;
}




/*
*   RadialShape - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const RadialShape_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("range", to_json((p.range), allocator), allocator);
    json.AddMember("horizontalOpeningAngleStart", to_json(((p.horizontalOpeningAngleStart) == 3601) ? (p.horizontalOpeningAngleStart) : (double)(p.horizontalOpeningAngleStart) / 10.0, allocator), allocator);
    json.AddMember("horizontalOpeningAngleEnd", to_json(((p.horizontalOpeningAngleEnd) == 3601) ? (p.horizontalOpeningAngleEnd) : (double)(p.horizontalOpeningAngleEnd) / 10.0, allocator), allocator);
    if (p.shapeReferencePoint != 0) json.AddMember("shapeReferencePoint", to_json(*(p.shapeReferencePoint), allocator), allocator);
    if (p.verticalOpeningAngleStart != 0) json.AddMember("verticalOpeningAngleStart", to_json((*(p.verticalOpeningAngleStart) != 3601) ? (double) *(p.verticalOpeningAngleStart) / 10.0 : *(p.verticalOpeningAngleStart), allocator), allocator);
    if (p.verticalOpeningAngleEnd != 0) json.AddMember("verticalOpeningAngleEnd", to_json((*(p.verticalOpeningAngleEnd) != 3601) ? (double) *(p.verticalOpeningAngleEnd) / 10.0 : *(p.verticalOpeningAngleEnd), allocator), allocator);
    return json;
}




/*
*   RadialShapeDetails - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const RadialShapeDetails_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("range", to_json((p.range), allocator), allocator);
    json.AddMember("horizontalOpeningAngleStart", to_json(((p.horizontalOpeningAngleStart) == 3601) ? (p.horizontalOpeningAngleStart) : (double)(p.horizontalOpeningAngleStart) / 10.0, allocator), allocator);
    json.AddMember("horizontalOpeningAngleEnd", to_json(((p.horizontalOpeningAngleEnd) == 3601) ? (p.horizontalOpeningAngleEnd) : (double)(p.horizontalOpeningAngleEnd) / 10.0, allocator), allocator);
    if (p.verticalOpeningAngleStart != 0) json.AddMember("verticalOpeningAngleStart", to_json((*(p.verticalOpeningAngleStart) != 3601) ? (double) *(p.verticalOpeningAngleStart) / 10.0 : *(p.verticalOpeningAngleStart), allocator), allocator);
    if (p.verticalOpeningAngleEnd != 0) json.AddMember("verticalOpeningAngleEnd", to_json((*(p.verticalOpeningAngleEnd) != 3601) ? (double) *(p.verticalOpeningAngleEnd) / 10.0 : *(p.verticalOpeningAngleEnd), allocator), allocator);
    return json;
}




/*
*   RectangularShape - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const RectangularShape_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("semiLength", to_json((p.semiLength), allocator), allocator);
    json.AddMember("semiBreadth", to_json((p.semiBreadth), allocator), allocator);
    if (p.shapeReferencePoint != 0) json.AddMember("shapeReferencePoint", to_json(*(p.shapeReferencePoint), allocator), allocator);
    if (p.orientation != 0) json.AddMember("orientation", to_json((*(p.orientation) != 3601) ? (double) *(p.orientation) / 10.0 : *(p.orientation), allocator), allocator);
    if (p.height != 0) json.AddMember("height", to_json(*(p.height), allocator), allocator);
    return json;
}




/*
*   ReferencePosition - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ReferencePosition_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("latitude", to_json(((p.latitude) == 900000001) ? (p.latitude) : (double)(p.latitude) / 10000000.0, allocator), allocator);
    json.AddMember("longitude", to_json(((p.longitude) == 1800000001) ? (p.longitude) : (double)(p.longitude) / 10000000.0, allocator), allocator);
    json.AddMember("positionConfidenceEllipse", to_json((p.positionConfidenceEllipse), allocator), allocator);
    json.AddMember("altitude", to_json((p.altitude), allocator), allocator);

    return json;
}




/*
*   ReferencePositionWithConfidence - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ReferencePositionWithConfidence_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("latitude", to_json(((p.latitude) == 900000001) ? (p.latitude) : (double)(p.latitude) / 10000000.0, allocator), allocator);
    json.AddMember("longitude", to_json(((p.longitude) == 1800000001) ? (p.longitude) : (double)(p.longitude) / 10000000.0, allocator), allocator);
    json.AddMember("positionConfidenceEllipse", to_json((p.positionConfidenceEllipse), allocator), allocator);
    json.AddMember("altitude", to_json((p.altitude), allocator), allocator);

    return json;
}




/*
*   RestrictedTypes - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_RestrictedTypes_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ETSI_ITS_CDD_StationType_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   RoadSectionDefinition - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const RoadSectionDefinition_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("startingPointSection", to_json((p.startingPointSection), allocator), allocator);
    json.AddMember("connectedPaths", to_json((p.connectedPaths), allocator), allocator);
    json.AddMember("includedPaths", to_json((p.includedPaths), allocator), allocator);
    json.AddMember("isEventZoneIncluded", to_json((p.isEventZoneIncluded), allocator), allocator);
    json.AddMember("isEventZoneConnected", to_json((p.isEventZoneConnected), allocator), allocator);
    if (p.lengthOfSection != 0) json.AddMember("lengthOfSection", to_json(*(p.lengthOfSection), allocator), allocator);
    if (p.endingPointSection != 0) json.AddMember("endingPointSection", to_json(*(p.endingPointSection), allocator), allocator);
    return json;
}




/*
*   RoadSegmentReferenceId - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const RoadSegmentReferenceId_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("id", to_json((p.id), allocator), allocator);
    if (p.region != 0) json.AddMember("region", to_json(*(p.region), allocator), allocator);
    return json;
}




/*
*   SafeDistanceIndication - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const SafeDistanceIndication_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("safeDistanceIndicator", to_json((p.safeDistanceIndicator), allocator), allocator);
    if (p.subjectStation != 0) json.AddMember("subjectStation", to_json(*(p.subjectStation), allocator), allocator);
    if (p.timeToCollision != 0) json.AddMember("timeToCollision", to_json(*(p.timeToCollision), allocator), allocator);
    return json;
}




/*
*   SequenceOfCartesianPosition3d - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const SequenceOfCartesianPosition3d_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const CartesianPosition3d_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   SequenceOfIdentifier1B - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const SequenceOfIdentifier1B_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const Identifier1B_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   SequenceOfSafeDistanceIndication - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const SequenceOfSafeDistanceIndication_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const SafeDistanceIndication_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   Speed - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_Speed_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("speedValue", to_json(((p.speedValue) == 16383) ? (p.speedValue) : (double)(p.speedValue) / 100.0, allocator), allocator);
    json.AddMember("speedConfidence", to_json(((p.speedConfidence) == 126 || (p.speedConfidence) == 127) ? (p.speedConfidence) : (double)(p.speedConfidence) / 100.0, allocator), allocator);

    return json;
}




/*
*   StabilityChangeIndication - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_StabilityChangeIndication_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lossProbability", to_json((p.lossProbability), allocator), allocator);
    json.AddMember("actionDeltaTime", to_json((p.actionDeltaTime), allocator), allocator);

    return json;
}




/*
*   SteeringWheelAngle - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_SteeringWheelAngle_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("steeringWheelAngleValue", to_json((p.steeringWheelAngleValue), allocator), allocator);
    json.AddMember("steeringWheelAngleConfidence", to_json((p.steeringWheelAngleConfidence), allocator), allocator);

    return json;
}




/*
*   TrafficIslandPosition - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_TrafficIslandPosition_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("oneSide", to_json((p.oneSide), allocator), allocator);
    json.AddMember("otherSide", to_json((p.otherSide), allocator), allocator);

    return json;
}




/*
*   TrailerData - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const TrailerData_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("refPointId", to_json((p.refPointId), allocator), allocator);
    json.AddMember("hitchPointOffset", to_json((p.hitchPointOffset), allocator), allocator);
    json.AddMember("hitchAngle", to_json((p.hitchAngle), allocator), allocator);
    if (p.frontOverhang != 0) json.AddMember("frontOverhang", to_json(*(p.frontOverhang), allocator), allocator);
    if (p.rearOverhang != 0) json.AddMember("rearOverhang", to_json(*(p.rearOverhang), allocator), allocator);
    if (p.trailerWidth != 0) json.AddMember("trailerWidth", to_json((*(p.trailerWidth) != 61 && *(p.trailerWidth) != 62) ? (double) *(p.trailerWidth) / 10.0 : *(p.trailerWidth), allocator), allocator);
    return json;
}




/*
*   TrajectoryInterceptionIndication - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_TrajectoryInterceptionIndication_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("trajectoryInterceptionProbability", to_json((p.trajectoryInterceptionProbability), allocator), allocator);
    if (p.subjectStation != 0) json.AddMember("subjectStation", to_json(*(p.subjectStation), allocator), allocator);
    if (p.trajectoryInterceptionConfidence != 0) json.AddMember("trajectoryInterceptionConfidence", to_json(*(p.trajectoryInterceptionConfidence), allocator), allocator);
    return json;
}




/*
*   Ext2 - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_Ext2_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == ETSI_ITS_CDD_Ext2_PR_content) {
        json.AddMember("content", to_json(p.choice.content, allocator), allocator);
    } else if (p.present == ETSI_ITS_CDD_Ext2_PR_extension) {
        json.AddMember("extension", to_json(p.choice.extension, allocator), allocator);
    }
    return json;
}


/*
*   VerticalAcceleration - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_VerticalAcceleration_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("verticalAccelerationValue", to_json(((p.verticalAccelerationValue) == 161) ? (p.verticalAccelerationValue) : (double)(p.verticalAccelerationValue) / 10.0, allocator), allocator);
    json.AddMember("verticalAccelerationConfidence", to_json(((p.verticalAccelerationConfidence) == 102) ? (p.verticalAccelerationConfidence) : (double)(p.verticalAccelerationConfidence) / 10.0, allocator), allocator);

    return json;
}




/*
*   VehicleIdentification - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_VehicleIdentification_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);


    return json;
}




/*
*   VehicleLength - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_VehicleLength_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("vehicleLengthValue", to_json(((p.vehicleLengthValue) == 1023) ? (p.vehicleLengthValue) : (double)(p.vehicleLengthValue) / 10.0, allocator), allocator);
    json.AddMember("vehicleLengthConfidenceIndication", to_json((p.vehicleLengthConfidenceIndication), allocator), allocator);

    return json;
}




/*
*   VehicleLengthV2 - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const VehicleLengthV2_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("vehicleLengthValue", to_json(((p.vehicleLengthValue) == 1023) ? (p.vehicleLengthValue) : (double)(p.vehicleLengthValue) / 10.0, allocator), allocator);
    json.AddMember("trailerPresenceInformation", to_json((p.trailerPresenceInformation), allocator), allocator);

    return json;
}




/*
*   VelocityComponent - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const VelocityComponent_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("value", to_json((p.value), allocator), allocator);
    json.AddMember("confidence", to_json(((p.confidence) == 126 || (p.confidence) == 127) ? (p.confidence) : (double)(p.confidence) / 100.0, allocator), allocator);

    return json;
}




/*
*   VelocityPolarWithZ - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const VelocityPolarWithZ_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("velocityMagnitude", to_json((p.velocityMagnitude), allocator), allocator);
    json.AddMember("velocityDirection", to_json((p.velocityDirection), allocator), allocator);
    if (p.zVelocity != 0) json.AddMember("zVelocity", to_json(*(p.zVelocity), allocator), allocator);
    return json;
}




/*
*   VruExteriorLights - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_VruExteriorLights_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("vehicular", to_json_ITS_Container_ExteriorLights((p.vehicular), allocator), allocator);
    json.AddMember("vruSpecific", to_json_ETSI_ITS_CDD_VruSpecificExteriorLights((p.vruSpecific), allocator), allocator);

    return json;
}




/*
*   VruProfileAndSubprofile - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_VruProfileAndSubprofile_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == ETSI_ITS_CDD_VruProfileAndSubprofile_PR_pedestrian) {
        json.AddMember("pedestrian", to_json(p.choice.pedestrian, allocator), allocator);
    } else if (p.present == ETSI_ITS_CDD_VruProfileAndSubprofile_PR_bicyclistAndLightVruVehicle) {
        json.AddMember("bicyclistAndLightVruVehicle", to_json(p.choice.bicyclistAndLightVruVehicle, allocator), allocator);
    } else if (p.present == ETSI_ITS_CDD_VruProfileAndSubprofile_PR_motorcyclist) {
        json.AddMember("motorcyclist", to_json(p.choice.motorcyclist, allocator), allocator);
    } else if (p.present == ETSI_ITS_CDD_VruProfileAndSubprofile_PR_animal) {
        json.AddMember("animal", to_json(p.choice.animal, allocator), allocator);
    }
    return json;
}


/*
*   Wgs84Angle - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const Wgs84Angle_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("value", to_json((p.value), allocator), allocator);
    json.AddMember("confidence", to_json((p.confidence), allocator), allocator);

    return json;
}




/*
*   YawRate - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_YawRate_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("yawRateValue", to_json(((p.yawRateValue) == 32767) ? (p.yawRateValue) : (double)(p.yawRateValue) / 100.0, allocator), allocator);
    json.AddMember("yawRateConfidence", to_json((p.yawRateConfidence), allocator), allocator);

    return json;
}




/*
*   ItsPduHeader - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_ItsPduHeader_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("protocolVersion", to_json((p.protocolVersion), allocator), allocator);
    json.AddMember("messageID", to_json((p.messageID), allocator), allocator);
    json.AddMember("stationID", to_json((p.stationID), allocator), allocator);

    return json;
}




/*
*   ReferencePosition - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_ReferencePosition_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("latitude", to_json(((p.latitude) == 900000001) ? (p.latitude) : (double)(p.latitude) / 10000000.0, allocator), allocator);
    json.AddMember("longitude", to_json(((p.longitude) == 1800000001) ? (p.longitude) : (double)(p.longitude) / 10000000.0, allocator), allocator);
    json.AddMember("positionConfidenceEllipse", to_json((p.positionConfidenceEllipse), allocator), allocator);
    json.AddMember("altitude", to_json((p.altitude), allocator), allocator);

    return json;
}




/*
*   DeltaReferencePosition - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_DeltaReferencePosition_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("deltaLatitude", to_json((p.deltaLatitude), allocator), allocator);
    json.AddMember("deltaLongitude", to_json((p.deltaLongitude), allocator), allocator);
    json.AddMember("deltaAltitude", to_json((p.deltaAltitude), allocator), allocator);

    return json;
}




/*
*   Altitude - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_Altitude_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("altitudeValue", to_json(((p.altitudeValue) == 800001) ? (p.altitudeValue) : (double)(p.altitudeValue) / 100.0, allocator), allocator);
    json.AddMember("altitudeConfidence", to_json((p.altitudeConfidence), allocator), allocator);

    return json;
}




/*
*   PosConfidenceEllipse - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_PosConfidenceEllipse_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("semiMajorConfidence", to_json((p.semiMajorConfidence), allocator), allocator);
    json.AddMember("semiMinorConfidence", to_json((p.semiMinorConfidence), allocator), allocator);
    json.AddMember("semiMajorOrientation", to_json(((p.semiMajorOrientation) == 3601) ? (p.semiMajorOrientation) : (double)(p.semiMajorOrientation) / 10.0, allocator), allocator);

    return json;
}




/*
*   PathPoint - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_PathPoint_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("pathPosition", to_json((p.pathPosition), allocator), allocator);
    if (p.pathDeltaTime != 0) json.AddMember("pathDeltaTime", to_json(*(p.pathDeltaTime), allocator), allocator);
    return json;
}




/*
*   PtActivation - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_PtActivation_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("ptActivationType", to_json((p.ptActivationType), allocator), allocator);
    json.AddMember("ptActivationData", to_json((p.ptActivationData), allocator), allocator);

    return json;
}




/*
*   AccelerationControl - Type BIT STRING
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json_ITS_Container_AccelerationControl(const ITS_Container_AccelerationControl_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("brakePedalEngaged", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("gasPedalEngaged", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("emergencyBrakeEngaged", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("collisionWarningEngaged", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("accEngaged", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("cruiseControlEngaged", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("speedLimiterEngaged", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    return json;
}


/*
*   CauseCode - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_CauseCode_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("causeCode", to_json((p.causeCode), allocator), allocator);
    json.AddMember("subCauseCode", to_json((p.subCauseCode), allocator), allocator);

    return json;
}




/*
*   Curvature - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_Curvature_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("curvatureValue", to_json((p.curvatureValue), allocator), allocator);
    json.AddMember("curvatureConfidence", to_json((p.curvatureConfidence), allocator), allocator);

    return json;
}




/*
*   Heading - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_Heading_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("headingValue", to_json(((p.headingValue) == 3601) ? (p.headingValue) : (double)(p.headingValue) / 10.0, allocator), allocator);
    json.AddMember("headingConfidence", to_json(((p.headingConfidence) == 126 || (p.headingConfidence) == 127) ? (p.headingConfidence) : (double)(p.headingConfidence) / 10.0, allocator), allocator);

    return json;
}




/*
*   ClosedLanes - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_ClosedLanes_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.innerhardShoulderStatus != 0) json.AddMember("innerhardShoulderStatus", to_json(*(p.innerhardShoulderStatus), allocator), allocator);
    if (p.outerhardShoulderStatus != 0) json.AddMember("outerhardShoulderStatus", to_json(*(p.outerhardShoulderStatus), allocator), allocator);
    if (p.drivingLaneStatus != 0) json.AddMember("drivingLaneStatus", to_json_ITS_Container_DrivingLaneStatus(*(p.drivingLaneStatus), allocator), allocator);
    return json;
}




/*
*   DrivingLaneStatus - Type BIT STRING
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json_ITS_Container_DrivingLaneStatus(const ITS_Container_DrivingLaneStatus_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    return json;
}


/*
*   Speed - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_Speed_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("speedValue", to_json(((p.speedValue) == 16383) ? (p.speedValue) : (double)(p.speedValue) / 100.0, allocator), allocator);
    json.AddMember("speedConfidence", to_json(((p.speedConfidence) == 126 || (p.speedConfidence) == 127) ? (p.speedConfidence) : (double)(p.speedConfidence) / 100.0, allocator), allocator);

    return json;
}




/*
*   LongitudinalAcceleration - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_LongitudinalAcceleration_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("longitudinalAccelerationValue", to_json(((p.longitudinalAccelerationValue) == 161) ? (p.longitudinalAccelerationValue) : (double)(p.longitudinalAccelerationValue) / 10.0, allocator), allocator);
    json.AddMember("longitudinalAccelerationConfidence", to_json(((p.longitudinalAccelerationConfidence) == 102) ? (p.longitudinalAccelerationConfidence) : (double)(p.longitudinalAccelerationConfidence) / 10.0, allocator), allocator);

    return json;
}




/*
*   LateralAcceleration - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_LateralAcceleration_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lateralAccelerationValue", to_json(((p.lateralAccelerationValue) == 161) ? (p.lateralAccelerationValue) : (double)(p.lateralAccelerationValue) / 10.0, allocator), allocator);
    json.AddMember("lateralAccelerationConfidence", to_json(((p.lateralAccelerationConfidence) == 102) ? (p.lateralAccelerationConfidence) : (double)(p.lateralAccelerationConfidence) / 10.0, allocator), allocator);

    return json;
}




/*
*   VerticalAcceleration - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_VerticalAcceleration_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("verticalAccelerationValue", to_json(((p.verticalAccelerationValue) == 161) ? (p.verticalAccelerationValue) : (double)(p.verticalAccelerationValue) / 10.0, allocator), allocator);
    json.AddMember("verticalAccelerationConfidence", to_json(((p.verticalAccelerationConfidence) == 102) ? (p.verticalAccelerationConfidence) : (double)(p.verticalAccelerationConfidence) / 10.0, allocator), allocator);

    return json;
}




/*
*   ExteriorLights - Type BIT STRING
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json_ITS_Container_ExteriorLights(const ITS_Container_ExteriorLights_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lowBeamHeadlightsOn", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("highBeamHeadlightsOn", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("leftTurnSignalOn", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("rightTurnSignalOn", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("daytimeRunningLightsOn", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("reverseLightOn", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("fogLightOn", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    json.AddMember("parkingLightsOn", (bool) (*(p.buf + (sizeof(uint8_t) * (7 / 8))) & (1 << ((7 * ((7 / 8) + 1))-(7 % 8)))), allocator);
    return json;
}


/*
*   DangerousGoodsExtended - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_DangerousGoodsExtended_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("dangerousGoodsType", to_json((p.dangerousGoodsType), allocator), allocator);
    json.AddMember("unNumber", to_json((p.unNumber), allocator), allocator);
    json.AddMember("elevatedTemperature", to_json((p.elevatedTemperature), allocator), allocator);
    json.AddMember("tunnelsRestricted", to_json((p.tunnelsRestricted), allocator), allocator);
    json.AddMember("limitedQuantity", to_json((p.limitedQuantity), allocator), allocator);
    if (p.emergencyActionCode != 0) json.AddMember("emergencyActionCode", to_json(*(p.emergencyActionCode), allocator), allocator);
    if (p.companyName != 0) json.AddMember("companyName", to_json(*(p.companyName), allocator), allocator);
    return json;
}




/*
*   SpecialTransportType - Type BIT STRING
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json_ITS_Container_SpecialTransportType(const ITS_Container_SpecialTransportType_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("heavyLoad", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("excessWidth", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("excessLength", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("excessHeight", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    return json;
}


/*
*   LightBarSirenInUse - Type BIT STRING
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json_ITS_Container_LightBarSirenInUse(const ITS_Container_LightBarSirenInUse_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lightBarActivated", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("sirenActivated", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    return json;
}


/*
*   PositionOfOccupants - Type BIT STRING
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json_ITS_Container_PositionOfOccupants(const ITS_Container_PositionOfOccupants_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("row1LeftOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("row1RightOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("row1MidOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("row1NotDetectable", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("row1NotPresent", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("row2LeftOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("row2RightOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    json.AddMember("row2MidOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (7 / 8))) & (1 << ((7 * ((7 / 8) + 1))-(7 % 8)))), allocator);
    json.AddMember("row2NotDetectable", (bool) (*(p.buf + (sizeof(uint8_t) * (8 / 8))) & (1 << ((7 * ((8 / 8) + 1))-(8 % 8)))), allocator);
    json.AddMember("row2NotPresent", (bool) (*(p.buf + (sizeof(uint8_t) * (9 / 8))) & (1 << ((7 * ((9 / 8) + 1))-(9 % 8)))), allocator);
    json.AddMember("row3LeftOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (10 / 8))) & (1 << ((7 * ((10 / 8) + 1))-(10 % 8)))), allocator);
    json.AddMember("row3RightOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (11 / 8))) & (1 << ((7 * ((11 / 8) + 1))-(11 % 8)))), allocator);
    json.AddMember("row3MidOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (12 / 8))) & (1 << ((7 * ((12 / 8) + 1))-(12 % 8)))), allocator);
    json.AddMember("row3NotDetectable", (bool) (*(p.buf + (sizeof(uint8_t) * (13 / 8))) & (1 << ((7 * ((13 / 8) + 1))-(13 % 8)))), allocator);
    json.AddMember("row3NotPresent", (bool) (*(p.buf + (sizeof(uint8_t) * (14 / 8))) & (1 << ((7 * ((14 / 8) + 1))-(14 % 8)))), allocator);
    json.AddMember("row4LeftOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (15 / 8))) & (1 << ((7 * ((15 / 8) + 1))-(15 % 8)))), allocator);
    json.AddMember("row4RightOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (16 / 8))) & (1 << ((7 * ((16 / 8) + 1))-(16 % 8)))), allocator);
    json.AddMember("row4MidOccupied", (bool) (*(p.buf + (sizeof(uint8_t) * (17 / 8))) & (1 << ((7 * ((17 / 8) + 1))-(17 % 8)))), allocator);
    json.AddMember("row4NotDetectable", (bool) (*(p.buf + (sizeof(uint8_t) * (18 / 8))) & (1 << ((7 * ((18 / 8) + 1))-(18 % 8)))), allocator);
    json.AddMember("row4NotPresent", (bool) (*(p.buf + (sizeof(uint8_t) * (19 / 8))) & (1 << ((7 * ((19 / 8) + 1))-(19 % 8)))), allocator);
    return json;
}


/*
*   VehicleIdentification - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_VehicleIdentification_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);


    return json;
}




/*
*   EnergyStorageType - Type BIT STRING
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json_ITS_Container_EnergyStorageType(const ITS_Container_EnergyStorageType_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("hydrogenStorage", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("electricEnergyStorage", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("liquidPropaneGas", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("compressedNaturalGas", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("diesel", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("gasoline", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("ammonia", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    return json;
}


/*
*   VehicleLength - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_VehicleLength_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("vehicleLengthValue", to_json(((p.vehicleLengthValue) == 1023) ? (p.vehicleLengthValue) : (double)(p.vehicleLengthValue) / 10.0, allocator), allocator);
    json.AddMember("vehicleLengthConfidenceIndication", to_json((p.vehicleLengthConfidenceIndication), allocator), allocator);

    return json;
}




/*
*   PathHistory - Type SEQUENCE OF
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_PathHistory_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ITS_Container_PathPoint_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   EmergencyPriority - Type BIT STRING
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json_ITS_Container_EmergencyPriority(const ITS_Container_EmergencyPriority_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("requestForRightOfWay", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("requestForFreeCrossingAtATrafficLight", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    return json;
}


/*
*   SteeringWheelAngle - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_SteeringWheelAngle_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("steeringWheelAngleValue", to_json((p.steeringWheelAngleValue), allocator), allocator);
    json.AddMember("steeringWheelAngleConfidence", to_json((p.steeringWheelAngleConfidence), allocator), allocator);

    return json;
}




/*
*   YawRate - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_YawRate_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("yawRateValue", to_json(((p.yawRateValue) == 32767) ? (p.yawRateValue) : (double)(p.yawRateValue) / 100.0, allocator), allocator);
    json.AddMember("yawRateConfidence", to_json((p.yawRateConfidence), allocator), allocator);

    return json;
}




/*
*   ActionID - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_ActionID_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("originatingStationID", to_json((p.originatingStationID), allocator), allocator);
    json.AddMember("sequenceNumber", to_json((p.sequenceNumber), allocator), allocator);

    return json;
}




/*
*   ItineraryPath - Type SEQUENCE OF
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_ItineraryPath_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ITS_Container_ReferencePosition_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   ProtectedCommunicationZone - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_ProtectedCommunicationZone_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("protectedZoneType", to_json((p.protectedZoneType), allocator), allocator);
    json.AddMember("protectedZoneLatitude", to_json(((p.protectedZoneLatitude) == 900000001) ? (p.protectedZoneLatitude) : (double)(p.protectedZoneLatitude) / 10000000.0, allocator), allocator);
    json.AddMember("protectedZoneLongitude", to_json(((p.protectedZoneLongitude) == 1800000001) ? (p.protectedZoneLongitude) : (double)(p.protectedZoneLongitude) / 10000000.0, allocator), allocator);
    if (p.protectedZoneRadius != 0) json.AddMember("protectedZoneRadius", to_json(*(p.protectedZoneRadius), allocator), allocator);
    if (p.protectedZoneID != 0) json.AddMember("protectedZoneID", to_json(*(p.protectedZoneID), allocator), allocator);
    return json;
}




/*
*   Traces - Type SEQUENCE OF
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_Traces_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ITS_Container_PathHistory_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   PositionOfPillars - Type SEQUENCE OF
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_PositionOfPillars_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ITS_Container_PosPillar_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   RestrictedTypes - Type SEQUENCE OF
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_RestrictedTypes_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ITS_Container_StationType_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   EventHistory - Type SEQUENCE OF
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_EventHistory_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ITS_Container_EventPoint_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   EventPoint - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_EventPoint_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("eventPosition", to_json((p.eventPosition), allocator), allocator);
    json.AddMember("informationQuality", to_json((p.informationQuality), allocator), allocator);
    if (p.eventDeltaTime != 0) json.AddMember("eventDeltaTime", to_json(*(p.eventDeltaTime), allocator), allocator);
    return json;
}




/*
*   ProtectedCommunicationZonesRSU - Type SEQUENCE OF
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_ProtectedCommunicationZonesRSU_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ITS_Container_ProtectedCommunicationZone_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   CenDsrcTollingZone - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_CenDsrcTollingZone_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("protectedZoneLatitude", to_json(((p.protectedZoneLatitude) == 900000001) ? (p.protectedZoneLatitude) : (double)(p.protectedZoneLatitude) / 10000000.0, allocator), allocator);
    json.AddMember("protectedZoneLongitude", to_json(((p.protectedZoneLongitude) == 1800000001) ? (p.protectedZoneLongitude) : (double)(p.protectedZoneLongitude) / 10000000.0, allocator), allocator);
    if (p.cenDsrcTollingZoneID != 0) json.AddMember("cenDsrcTollingZoneID", to_json(*(p.cenDsrcTollingZoneID), allocator), allocator);
    return json;
}




/*
*   DigitalMap - Type SEQUENCE OF
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_DigitalMap_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ITS_Container_ReferencePosition_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   LaneAttributes-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const LaneAttributes_addGrpC& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.maxVehicleHeight != 0) json.AddMember("maxVehicleHeight", to_json(*(p.maxVehicleHeight), allocator), allocator);
    if (p.maxVehicleWeight != 0) json.AddMember("maxVehicleWeight", to_json(*(p.maxVehicleWeight), allocator), allocator);
    return json;
}




/*
*   MovementEvent-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const MovementEvent_addGrpC& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.stateChangeReason != 0) json.AddMember("stateChangeReason", to_json(*(p.stateChangeReason), allocator), allocator);
    return json;
}




/*
*   Position3D-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const Position3D_addGrpC& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("altitude", to_json((p.altitude), allocator), allocator);

    return json;
}




/*
*   RestrictionUserType-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const RestrictionUserType_addGrpC& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.emission != 0) json.AddMember("emission", to_json(*(p.emission), allocator), allocator);
    if (p.fuel != 0) json.AddMember("fuel", to_json(*(p.fuel), allocator), allocator);
    return json;
}




/*
*   RequestorDescription-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const RequestorDescription_addGrpC& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.fuel != 0) json.AddMember("fuel", to_json(*(p.fuel), allocator), allocator);
    if (p.batteryStatus != 0) json.AddMember("batteryStatus", to_json(*(p.batteryStatus), allocator), allocator);
    return json;
}




/*
*   SignalStatusPackage-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const SignalStatusPackage_addGrpC& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.synchToSchedule != 0) json.AddMember("synchToSchedule", to_json(*(p.synchToSchedule), allocator), allocator);
    if (p.rejectedReason != 0) json.AddMember("rejectedReason", to_json(*(p.rejectedReason), allocator), allocator);
    return json;
}




/*
*   Node - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const Node& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("id", to_json((p.id), allocator), allocator);
    if (p.lane != 0) json.AddMember("lane", to_json(*(p.lane), allocator), allocator);
    if (p.connectionID != 0) json.AddMember("connectionID", to_json(*(p.connectionID), allocator), allocator);
    if (p.intersectionID != 0) json.AddMember("intersectionID", to_json(*(p.intersectionID), allocator), allocator);
    return json;
}




/*
*   NodeLink - Type SEQUENCE OF
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const NodeLink& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const Node_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   PrioritizationResponse - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const PrioritizationResponse& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("stationID", to_json((p.stationID), allocator), allocator);
    json.AddMember("priorState", to_json((p.priorState), allocator), allocator);
    json.AddMember("signalGroup", to_json((p.signalGroup), allocator), allocator);

    return json;
}




/*
*   PrioritizationResponseList - Type SEQUENCE OF
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const PrioritizationResponseList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const PrioritizationResponse_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   AdvisorySpeed - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const AdvisorySpeed& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("type", to_json((p.type), allocator), allocator);
    if (p.speed != 0) json.AddMember("speed", to_json(*(p.speed), allocator), allocator);
    if (p.confidence != 0) json.AddMember("confidence", to_json(*(p.confidence), allocator), allocator);
    if (p.distance != 0) json.AddMember("distance", to_json(*(p.distance), allocator), allocator);
    if (p.Class != 0) json.AddMember("class", to_json(*(p.Class), allocator), allocator);
    return json;
}




/*
*   AdvisorySpeedList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const AdvisorySpeedList_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const AdvisorySpeed_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   AntennaOffsetSet - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const AntennaOffsetSet& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("antOffsetX", to_json((p.antOffsetX), allocator), allocator);
    json.AddMember("antOffsetY", to_json((p.antOffsetY), allocator), allocator);
    json.AddMember("antOffsetZ", to_json((p.antOffsetZ), allocator), allocator);

    return json;
}




/*
*   ComputedLane::ComputedLane__offsetXaxis - Type CHOICE
*   From DSRC - File DSRC.asn
*/

Value to_json(const ComputedLane::ComputedLane__offsetXaxis& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == ComputedLane__offsetXaxis_PR::ComputedLane__offsetXaxis_PR_small) {
        json.AddMember("small", to_json(p.choice.small, allocator), allocator);
    } else if (p.present == ComputedLane__offsetXaxis_PR::ComputedLane__offsetXaxis_PR_large) {
        json.AddMember("large", to_json(p.choice.large, allocator), allocator);
    }
    return json;
}


/*
*   ComputedLane::ComputedLane__offsetYaxis - Type CHOICE
*   From DSRC - File DSRC.asn
*/

Value to_json(const ComputedLane::ComputedLane__offsetYaxis& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == ComputedLane__offsetYaxis_PR::ComputedLane__offsetYaxis_PR_small) {
        json.AddMember("small", to_json(p.choice.small, allocator), allocator);
    } else if (p.present == ComputedLane__offsetYaxis_PR::ComputedLane__offsetYaxis_PR_large) {
        json.AddMember("large", to_json(p.choice.large, allocator), allocator);
    }
    return json;
}


/*
*   ComputedLane - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const ComputedLane_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("referenceLaneId", to_json((p.referenceLaneId), allocator), allocator);
    json.AddMember("offsetXaxis", to_json((p.offsetXaxis), allocator), allocator);
    json.AddMember("offsetYaxis", to_json((p.offsetYaxis), allocator), allocator);
    if (p.rotateXY != 0) json.AddMember("rotateXY", to_json(*(p.rotateXY), allocator), allocator);
    if (p.scaleXaxis != 0) json.AddMember("scaleXaxis", to_json(*(p.scaleXaxis), allocator), allocator);
    if (p.scaleYaxis != 0) json.AddMember("scaleYaxis", to_json(*(p.scaleYaxis), allocator), allocator);
    return json;
}




/*
*   ConnectionManeuverAssist - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const ConnectionManeuverAssist_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("connectionID", to_json((p.connectionID), allocator), allocator);
    if (p.queueLength != 0) json.AddMember("queueLength", to_json(*(p.queueLength), allocator), allocator);
    if (p.availableStorageLength != 0) json.AddMember("availableStorageLength", to_json(*(p.availableStorageLength), allocator), allocator);
    if (p.waitOnStop != 0) json.AddMember("waitOnStop", to_json(*(p.waitOnStop), allocator), allocator);
    if (p.pedBicycleDetect != 0) json.AddMember("pedBicycleDetect", to_json(*(p.pedBicycleDetect), allocator), allocator);
    return json;
}




/*
*   DataParameters - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const DataParameters_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.processMethod != 0) json.AddMember("processMethod", to_json(*(p.processMethod), allocator), allocator);
    if (p.processAgency != 0) json.AddMember("processAgency", to_json(*(p.processAgency), allocator), allocator);
    if (p.lastCheckedDate != 0) json.AddMember("lastCheckedDate", to_json(*(p.lastCheckedDate), allocator), allocator);
    if (p.geoidUsed != 0) json.AddMember("geoidUsed", to_json(*(p.geoidUsed), allocator), allocator);
    return json;
}




/*
*   DDateTime - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const DDateTime& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.year != 0) json.AddMember("year", to_json(*(p.year), allocator), allocator);
    if (p.month != 0) json.AddMember("month", to_json(*(p.month), allocator), allocator);
    if (p.day != 0) json.AddMember("day", to_json(*(p.day), allocator), allocator);
    if (p.hour != 0) json.AddMember("hour", to_json(*(p.hour), allocator), allocator);
    if (p.minute != 0) json.AddMember("minute", to_json(*(p.minute), allocator), allocator);
    if (p.second != 0) json.AddMember("second", to_json(*(p.second), allocator), allocator);
    if (p.offset != 0) json.AddMember("offset", to_json(*(p.offset), allocator), allocator);
    return json;
}




/*
*   EnabledLaneList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const EnabledLaneList_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const LaneID_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   IntersectionAccessPoint - Type CHOICE
*   From DSRC - File DSRC.asn
*/

Value to_json(const IntersectionAccessPoint_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == IntersectionAccessPoint_PR_lane) {
        json.AddMember("lane", to_json(p.choice.lane, allocator), allocator);
    } else if (p.present == IntersectionAccessPoint_PR_approach) {
        json.AddMember("approach", to_json(p.choice.approach, allocator), allocator);
    } else if (p.present == IntersectionAccessPoint_PR_connection) {
        json.AddMember("connection", to_json(p.choice.connection, allocator), allocator);
    }
    return json;
}


/*
*   IntersectionReferenceID - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const IntersectionReferenceID& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("id", to_json((p.id), allocator), allocator);
    if (p.region != 0) json.AddMember("region", to_json(*(p.region), allocator), allocator);
    return json;
}




/*
*   LaneSharing - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneSharing(const LaneSharing_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("overlappingLaneDescriptionProvided", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("multipleLanesTreatedAsOneLane", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("otherNonMotorizedTrafficTypes", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("individualMotorizedVehicleTraffic", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("busVehicleTraffic", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("taxiVehicleTraffic", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("pedestriansTraffic", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    json.AddMember("cyclistVehicleTraffic", (bool) (*(p.buf + (sizeof(uint8_t) * (7 / 8))) & (1 << ((7 * ((7 / 8) + 1))-(7 % 8)))), allocator);
    json.AddMember("trackedVehicleTraffic", (bool) (*(p.buf + (sizeof(uint8_t) * (8 / 8))) & (1 << ((7 * ((8 / 8) + 1))-(8 % 8)))), allocator);
    json.AddMember("pedestrianTraffic", (bool) (*(p.buf + (sizeof(uint8_t) * (9 / 8))) & (1 << ((7 * ((9 / 8) + 1))-(9 % 8)))), allocator);
    return json;
}


/*
*   ManeuverAssistList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const ManeuverAssistList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ConnectionManeuverAssist_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   NodeAttributeXYList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const NodeAttributeXYList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const NodeAttributeXY_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   Node-LLmD-64b - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const Node_LLmD_64b& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lon", to_json(((p.lon) == 1800000001) ? (p.lon) : (double)(p.lon) / 10000000.0, allocator), allocator);
    json.AddMember("lat", to_json(((p.lat) == 900000001) ? (p.lat) : (double)(p.lat) / 10000000.0, allocator), allocator);

    return json;
}




/*
*   Node-XY-20b - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const Node_XY_20b& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("x", to_json((p.x), allocator), allocator);
    json.AddMember("y", to_json((p.y), allocator), allocator);

    return json;
}




/*
*   Node-XY-22b - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const Node_XY_22b& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("x", to_json((p.x), allocator), allocator);
    json.AddMember("y", to_json((p.y), allocator), allocator);

    return json;
}




/*
*   Node-XY-24b - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const Node_XY_24b& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("x", to_json((p.x), allocator), allocator);
    json.AddMember("y", to_json((p.y), allocator), allocator);

    return json;
}




/*
*   Node-XY-26b - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const Node_XY_26b& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("x", to_json((p.x), allocator), allocator);
    json.AddMember("y", to_json((p.y), allocator), allocator);

    return json;
}




/*
*   Node-XY-28b - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const Node_XY_28b& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("x", to_json((p.x), allocator), allocator);
    json.AddMember("y", to_json((p.y), allocator), allocator);

    return json;
}




/*
*   Node-XY-32b - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const Node_XY_32b& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("x", to_json((p.x), allocator), allocator);
    json.AddMember("y", to_json((p.y), allocator), allocator);

    return json;
}




/*
*   NodeOffsetPointXY - Type CHOICE
*   From DSRC - File DSRC.asn
*/

Value to_json(const NodeOffsetPointXY& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == NodeOffsetPointXY_PR_node_XY1) {
        json.AddMember("node-XY1", to_json(p.choice.node_XY1, allocator), allocator);
    } else if (p.present == NodeOffsetPointXY_PR_node_XY2) {
        json.AddMember("node-XY2", to_json(p.choice.node_XY2, allocator), allocator);
    } else if (p.present == NodeOffsetPointXY_PR_node_XY3) {
        json.AddMember("node-XY3", to_json(p.choice.node_XY3, allocator), allocator);
    } else if (p.present == NodeOffsetPointXY_PR_node_XY4) {
        json.AddMember("node-XY4", to_json(p.choice.node_XY4, allocator), allocator);
    } else if (p.present == NodeOffsetPointXY_PR_node_XY5) {
        json.AddMember("node-XY5", to_json(p.choice.node_XY5, allocator), allocator);
    } else if (p.present == NodeOffsetPointXY_PR_node_XY6) {
        json.AddMember("node-XY6", to_json(p.choice.node_XY6, allocator), allocator);
    } else if (p.present == NodeOffsetPointXY_PR_node_LatLon) {
        json.AddMember("node-LatLon", to_json(p.choice.node_LatLon, allocator), allocator);
    }
    return json;
}


/*
*   OverlayLaneList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const OverlayLaneList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const LaneID_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   PositionalAccuracy - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const PositionalAccuracy& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("semiMajor", to_json((p.semiMajor), allocator), allocator);
    json.AddMember("semiMinor", to_json((p.semiMinor), allocator), allocator);
    json.AddMember("orientation", to_json((p.orientation), allocator), allocator);

    return json;
}




/*
*   PositionConfidenceSet - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const PositionConfidenceSet& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("pos", to_json((p.pos), allocator), allocator);
    json.AddMember("elevation", to_json((p.elevation), allocator), allocator);

    return json;
}




/*
*   Position3D - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const Position3D_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lat", to_json(((p.lat) == 900000001) ? (p.lat) : (double)(p.lat) / 10000000.0, allocator), allocator);
    json.AddMember("long", to_json(((p.Long) == 1800000001) ? (p.Long) : (double)(p.Long) / 10000000.0, allocator), allocator);
    if (p.elevation != 0) json.AddMember("elevation", to_json(*(p.elevation), allocator), allocator);
    return json;
}




/*
*   RegulatorySpeedLimit - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RegulatorySpeedLimit& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("type", to_json((p.type), allocator), allocator);
    json.AddMember("speed", to_json((p.speed), allocator), allocator);

    return json;
}




/*
*   RequestorType - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RequestorType& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("role", to_json((p.role), allocator), allocator);
    if (p.subrole != 0) json.AddMember("subrole", to_json(*(p.subrole), allocator), allocator);
    if (p.request != 0) json.AddMember("request", to_json(*(p.request), allocator), allocator);
    if (p.hpmsType != 0) json.AddMember("hpmsType", to_json(*(p.hpmsType), allocator), allocator);
    return json;
}




/*
*   RestrictionUserType - Type CHOICE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RestrictionUserType& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == RestrictionUserType_PR_basicType) {
        json.AddMember("basicType", to_json(p.choice.basicType, allocator), allocator);
    }
    return json;
}


/*
*   RestrictionUserTypeList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const RestrictionUserTypeList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const RestrictionUserType_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   RoadSegmentReferenceID - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RoadSegmentReferenceID& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("id", to_json((p.id), allocator), allocator);
    if (p.region != 0) json.AddMember("region", to_json(*(p.region), allocator), allocator);
    return json;
}




/*
*   SegmentAttributeXYList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const SegmentAttributeXYList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const SegmentAttributeXY_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   SignalControlZone - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalControlZone& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);


    return json;
}




/*
*   SignalRequest - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalRequest& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("id", to_json((p.id), allocator), allocator);
    json.AddMember("requestID", to_json((p.requestID), allocator), allocator);
    json.AddMember("requestType", to_json((p.requestType), allocator), allocator);
    json.AddMember("inBoundLane", to_json((p.inBoundLane), allocator), allocator);
    if (p.outBoundLane != 0) json.AddMember("outBoundLane", to_json(*(p.outBoundLane), allocator), allocator);
    return json;
}




/*
*   SignalRequestPackage - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalRequestPackage& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("request", to_json((p.request), allocator), allocator);
    if (p.minute != 0) json.AddMember("minute", to_json(*(p.minute), allocator), allocator);
    if (p.second != 0) json.AddMember("second", to_json(*(p.second), allocator), allocator);
    if (p.duration != 0) json.AddMember("duration", to_json(*(p.duration), allocator), allocator);
    return json;
}




/*
*   SpeedandHeadingIsoandThrottleConfidence - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SpeedandHeadingIsoandThrottleConfidence& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("heading", to_json((p.heading), allocator), allocator);
    json.AddMember("speed", to_json((p.speed), allocator), allocator);
    json.AddMember("throttle", to_json((p.throttle), allocator), allocator);

    return json;
}




/*
*   SpeedLimitList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const SpeedLimitList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const RegulatorySpeedLimit_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   TimeChangeDetails - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const TimeChangeDetails& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("minEndTime", to_json((p.minEndTime), allocator), allocator);
    if (p.startTime != 0) json.AddMember("startTime", to_json(*(p.startTime), allocator), allocator);
    if (p.maxEndTime != 0) json.AddMember("maxEndTime", to_json(*(p.maxEndTime), allocator), allocator);
    if (p.likelyTime != 0) json.AddMember("likelyTime", to_json(*(p.likelyTime), allocator), allocator);
    if (p.confidence != 0) json.AddMember("confidence", to_json(*(p.confidence), allocator), allocator);
    if (p.nextTime != 0) json.AddMember("nextTime", to_json(*(p.nextTime), allocator), allocator);
    return json;
}




/*
*   TransmissionAndSpeed - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const TransmissionAndSpeed_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("transmisson", to_json((p.transmisson), allocator), allocator);
    json.AddMember("speed", to_json((p.speed), allocator), allocator);

    return json;
}




/*
*   VehicleID - Type CHOICE
*   From DSRC - File DSRC.asn
*/

Value to_json(const VehicleID_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == VehicleID_PR_stationID) {
        json.AddMember("stationID", to_json(p.choice.stationID, allocator), allocator);
    }
    return json;
}


/*
*   AllowedManeuvers - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_AllowedManeuvers(const AllowedManeuvers_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("maneuverStraightAllowed", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("maneuverLeftAllowed", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("maneuverRightAllowed", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("maneuverUTurnAllowed", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("maneuverLeftTurnOnRedAllowed", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("maneuverRightTurnOnRedAllowed", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("maneuverLaneChangeAllowed", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    json.AddMember("maneuverNoStoppingAllowed", (bool) (*(p.buf + (sizeof(uint8_t) * (7 / 8))) & (1 << ((7 * ((7 / 8) + 1))-(7 % 8)))), allocator);
    json.AddMember("yieldAllwaysRequired", (bool) (*(p.buf + (sizeof(uint8_t) * (8 / 8))) & (1 << ((7 * ((8 / 8) + 1))-(8 % 8)))), allocator);
    json.AddMember("goWithHalt", (bool) (*(p.buf + (sizeof(uint8_t) * (9 / 8))) & (1 << ((7 * ((9 / 8) + 1))-(9 % 8)))), allocator);
    json.AddMember("caution", (bool) (*(p.buf + (sizeof(uint8_t) * (10 / 8))) & (1 << ((7 * ((10 / 8) + 1))-(10 % 8)))), allocator);
    json.AddMember("reserved1", (bool) (*(p.buf + (sizeof(uint8_t) * (11 / 8))) & (1 << ((7 * ((11 / 8) + 1))-(11 % 8)))), allocator);
    return json;
}


/*
*   GNSSstatus - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_GNSSstatus(const GNSSstatus_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("unavailable", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("isHealthy", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("isMonitored", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("baseStationType", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("aPDOPofUnder5", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("inViewOfUnder5", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("localCorrectionsPresent", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    json.AddMember("networkCorrectionsPresent", (bool) (*(p.buf + (sizeof(uint8_t) * (7 / 8))) & (1 << ((7 * ((7 / 8) + 1))-(7 % 8)))), allocator);
    return json;
}


/*
*   IntersectionStatusObject - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_IntersectionStatusObject(const IntersectionStatusObject_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("manualControlIsEnabled", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("stopTimeIsActivated", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("failureFlash", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("preemptIsActive", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("signalPriorityIsActive", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("fixedTimeOperation", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("trafficDependentOperation", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    json.AddMember("standbyOperation", (bool) (*(p.buf + (sizeof(uint8_t) * (7 / 8))) & (1 << ((7 * ((7 / 8) + 1))-(7 % 8)))), allocator);
    json.AddMember("failureMode", (bool) (*(p.buf + (sizeof(uint8_t) * (8 / 8))) & (1 << ((7 * ((8 / 8) + 1))-(8 % 8)))), allocator);
    json.AddMember("off", (bool) (*(p.buf + (sizeof(uint8_t) * (9 / 8))) & (1 << ((7 * ((9 / 8) + 1))-(9 % 8)))), allocator);
    json.AddMember("recentMAPmessageUpdate", (bool) (*(p.buf + (sizeof(uint8_t) * (10 / 8))) & (1 << ((7 * ((10 / 8) + 1))-(10 % 8)))), allocator);
    json.AddMember("recentChangeInMAPassignedLanesIDsUsed", (bool) (*(p.buf + (sizeof(uint8_t) * (11 / 8))) & (1 << ((7 * ((11 / 8) + 1))-(11 % 8)))), allocator);
    json.AddMember("noValidMAPisAvailableAtThisTime", (bool) (*(p.buf + (sizeof(uint8_t) * (12 / 8))) & (1 << ((7 * ((12 / 8) + 1))-(12 % 8)))), allocator);
    json.AddMember("noValidSPATisAvailableAtThisTime", (bool) (*(p.buf + (sizeof(uint8_t) * (13 / 8))) & (1 << ((7 * ((13 / 8) + 1))-(13 % 8)))), allocator);
    return json;
}


/*
*   LaneAttributes-Barrier - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneAttributes_Barrier(const LaneAttributes_Barrier_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("median-RevocableLane", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("median", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("whiteLineHashing", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("stripedLines", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("doubleStripedLines", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("trafficCones", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("constructionBarrier", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    json.AddMember("trafficChannels", (bool) (*(p.buf + (sizeof(uint8_t) * (7 / 8))) & (1 << ((7 * ((7 / 8) + 1))-(7 % 8)))), allocator);
    json.AddMember("lowCurbs", (bool) (*(p.buf + (sizeof(uint8_t) * (8 / 8))) & (1 << ((7 * ((8 / 8) + 1))-(8 % 8)))), allocator);
    json.AddMember("highCurbs", (bool) (*(p.buf + (sizeof(uint8_t) * (9 / 8))) & (1 << ((7 * ((9 / 8) + 1))-(9 % 8)))), allocator);
    return json;
}


/*
*   LaneAttributes-Bike - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneAttributes_Bike(const LaneAttributes_Bike_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("bikeRevocableLane", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("pedestrianUseAllowed", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("isBikeFlyOverLane", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("fixedCycleTime", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("biDirectionalCycleTimes", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("isolatedByBarrier", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("unsignalizedSegmentsPresent", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    return json;
}


/*
*   LaneAttributes-Crosswalk - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneAttributes_Crosswalk(const LaneAttributes_Crosswalk_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("crosswalkRevocableLane", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("bicyleUseAllowed", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("isXwalkFlyOverLane", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("fixedCycleTime", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("biDirectionalCycleTimes", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("hasPushToWalkButton", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("audioSupport", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    json.AddMember("rfSignalRequestPresent", (bool) (*(p.buf + (sizeof(uint8_t) * (7 / 8))) & (1 << ((7 * ((7 / 8) + 1))-(7 % 8)))), allocator);
    json.AddMember("unsignalizedSegmentsPresent", (bool) (*(p.buf + (sizeof(uint8_t) * (8 / 8))) & (1 << ((7 * ((8 / 8) + 1))-(8 % 8)))), allocator);
    return json;
}


/*
*   LaneAttributes-Parking - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneAttributes_Parking(const LaneAttributes_Parking_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("parkingRevocableLane", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("parallelParkingInUse", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("headInParkingInUse", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("doNotParkZone", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("parkingForBusUse", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("parkingForTaxiUse", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("noPublicParkingUse", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    return json;
}


/*
*   LaneAttributes-Sidewalk - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneAttributes_Sidewalk(const LaneAttributes_Sidewalk_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("sidewalk-RevocableLane", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("bicyleUseAllowed", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("isSidewalkFlyOverLane", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("walkBikes", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    return json;
}


/*
*   LaneAttributes-Striping - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneAttributes_Striping(const LaneAttributes_Striping_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("stripeToConnectingLanesRevocableLane", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("stripeDrawOnLeft", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("stripeDrawOnRight", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("stripeToConnectingLanesLeft", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("stripeToConnectingLanesRight", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("stripeToConnectingLanesAhead", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    return json;
}


/*
*   LaneAttributes-TrackedVehicle - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneAttributes_TrackedVehicle(const LaneAttributes_TrackedVehicle_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("spec-RevocableLane", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("spec-commuterRailRoadTrack", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("spec-lightRailRoadTrack", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("spec-heavyRailRoadTrack", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("spec-otherRailType", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    return json;
}


/*
*   LaneAttributes-Vehicle - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneAttributes_Vehicle(const LaneAttributes_Vehicle_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("isVehicleRevocableLane", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("isVehicleFlyOverLane", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("hovLaneUseOnly", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("restrictedToBusUse", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("restrictedToTaxiUse", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("restrictedFromPublicUse", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("hasIRbeaconCoverage", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    json.AddMember("permissionOnRequest", (bool) (*(p.buf + (sizeof(uint8_t) * (7 / 8))) & (1 << ((7 * ((7 / 8) + 1))-(7 % 8)))), allocator);
    return json;
}


/*
*   LaneDirection - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneDirection(const LaneDirection_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("ingressPath", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("egressPath", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    return json;
}


/*
*   TransitVehicleStatus - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_TransitVehicleStatus(const TransitVehicleStatus_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("loading", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("anADAuse", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("aBikeLoad", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("doorOpen", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("charging", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("atStopLine", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    return json;
}


/*
*   CS5 - Type SEQUENCE
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json(const CS5& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("vin", to_json((p.vin), allocator), allocator);

    return json;
}




/*
*   FreightContainerData - Type SEQUENCE
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json(const FreightContainerData_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("serialNumber", to_json((p.serialNumber), allocator), allocator);
    json.AddMember("checkDigit", to_json((p.checkDigit), allocator), allocator);
    json.AddMember("length", to_json((p.length), allocator), allocator);
    json.AddMember("height", to_json((p.height), allocator), allocator);
    json.AddMember("width", to_json((p.width), allocator), allocator);
    json.AddMember("containerTypeCode", to_json((p.containerTypeCode), allocator), allocator);
    json.AddMember("maximumGrossMass", to_json((p.maximumGrossMass), allocator), allocator);
    json.AddMember("tareMass", to_json((p.tareMass), allocator), allocator);

    return json;
}




/*
*   CountryCode - Type BIT STRING
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json_AVIAEINumberingAndDataStructures_CountryCode(const AVIAEINumberingAndDataStructures_CountryCode_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    return json;
}


/*
*   ServiceNumber - Type BIT STRING
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json_ServiceNumber(const ServiceNumber_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    return json;
}


/*
*   GeoGraphicalLimit - Type BIT STRING
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json_GeoGraphicalLimit(const GeoGraphicalLimit_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("globalRestriction", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("regionalRestriction", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("nationalRestriction", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("district", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("issuerCoverageRestriction", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("reservedForCEN1", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("reservedForCEN2", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    json.AddMember("issuerSpecificRestriction", (bool) (*(p.buf + (sizeof(uint8_t) * (7 / 8))) & (1 << ((7 * ((7 / 8) + 1))-(7 % 8)))), allocator);
    return json;
}


/*
*   ServiceApplicationLimit - Type BIT STRING
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json_ServiceApplicationLimit(const ServiceApplicationLimit_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("notForPostpayment", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("notForPrepayment", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("notForVehicleaccess", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("notForFleetcontrol", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("issuerSpecificRestriction1", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("issuerSpecificRestriction2", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("issuerSpecificRestriction3", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    json.AddMember("issuerSpecificRestriction4", (bool) (*(p.buf + (sizeof(uint8_t) * (7 / 8))) & (1 << ((7 * ((7 / 8) + 1))-(7 % 8)))), allocator);
    return json;
}


/*
*   GddStructure::GddStructure__pictogramCode::GddStructure__pictogramCode__serviceCategoryCode - Type CHOICE
*   From GDD - File ISO14823.asn
*/

Value to_json(const GddStructure::GddStructure__pictogramCode::GddStructure__pictogramCode__serviceCategoryCode& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == GddStructure__pictogramCode__serviceCategoryCode_PR::GddStructure__pictogramCode__serviceCategoryCode_PR_trafficSignPictogram) {
        json.AddMember("trafficSignPictogram", to_json(p.choice.trafficSignPictogram, allocator), allocator);
    } else if (p.present == GddStructure__pictogramCode__serviceCategoryCode_PR::GddStructure__pictogramCode__serviceCategoryCode_PR_publicFacilitiesPictogram) {
        json.AddMember("publicFacilitiesPictogram", to_json(p.choice.publicFacilitiesPictogram, allocator), allocator);
    } else if (p.present == GddStructure__pictogramCode__serviceCategoryCode_PR::GddStructure__pictogramCode__serviceCategoryCode_PR_ambientOrRoadConditionPictogram) {
        json.AddMember("ambientOrRoadConditionPictogram", to_json(p.choice.ambientOrRoadConditionPictogram, allocator), allocator);
    }
    return json;
}


/*
*   GddStructure::GddStructure__pictogramCode::GddStructure__pictogramCode__pictogramCategoryCode - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const GddStructure::GddStructure__pictogramCode::GddStructure__pictogramCode__pictogramCategoryCode& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("nature", to_json((p.nature), allocator), allocator);
    json.AddMember("serialNumber", to_json((p.serialNumber), allocator), allocator);

    return json;
}




/*
*   GddStructure::GddStructure__pictogramCode - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const GddStructure::GddStructure__pictogramCode& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("serviceCategoryCode", to_json((p.serviceCategoryCode), allocator), allocator);
    json.AddMember("pictogramCategoryCode", to_json((p.pictogramCategoryCode), allocator), allocator);
    if (p.countryCode != 0) json.AddMember("countryCode", to_json(*(p.countryCode), allocator), allocator);
    return json;
}




/*
*   InternationalSign-applicablePeriod::InternationalSign-applicablePeriod__year - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const InternationalSign_applicablePeriod::InternationalSign_applicablePeriod__year& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("yearRangeStartYear", to_json((p.yearRangeStartYear), allocator), allocator);
    json.AddMember("yearRangeEndYear", to_json((p.yearRangeEndYear), allocator), allocator);

    return json;
}




/*
*   InternationalSign-speedLimits - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const InternationalSign_speedLimits& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("unit", to_json((p.unit), allocator), allocator);
    if (p.speedLimitMax != 0) json.AddMember("speedLimitMax", to_json(*(p.speedLimitMax), allocator), allocator);
    if (p.speedLimitMin != 0) json.AddMember("speedLimitMin", to_json(*(p.speedLimitMin), allocator), allocator);
    return json;
}




/*
*   DayOfWeek - Type BIT STRING
*   From GDD - File ISO14823.asn
*/

Value to_json_DayOfWeek(const DayOfWeek_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("unused", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("monday", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("tuesday", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("wednesday", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("thursday", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("friday", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    json.AddMember("saturday", (bool) (*(p.buf + (sizeof(uint8_t) * (6 / 8))) & (1 << ((7 * ((6 / 8) + 1))-(6 % 8)))), allocator);
    json.AddMember("sunday", (bool) (*(p.buf + (sizeof(uint8_t) * (7 / 8))) & (1 << ((7 * ((7 / 8) + 1))-(7 % 8)))), allocator);
    return json;
}


/*
*   DestinationPlace - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const DestinationPlace& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("destType", to_json((p.destType), allocator), allocator);
    if (p.destBlob != 0) json.AddMember("destBlob", to_json(*(p.destBlob), allocator), allocator);
    if (p.placeNameIdentification != 0) json.AddMember("placeNameIdentification", to_json(*(p.placeNameIdentification), allocator), allocator);
    if (p.placeNameText != 0) json.AddMember("placeNameText", to_json(*(p.placeNameText), allocator), allocator);
    return json;
}




/*
*   DestinationPlaces - Type SEQUENCE OF
*   From GDD - File ISO14823.asn
*/

Value to_json(const DestinationPlaces& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const DestinationPlace_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   DestinationRoad - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const DestinationRoad& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("derType", to_json((p.derType), allocator), allocator);
    if (p.roadNumberIdentifier != 0) json.AddMember("roadNumberIdentifier", to_json(*(p.roadNumberIdentifier), allocator), allocator);
    if (p.roadNumberText != 0) json.AddMember("roadNumberText", to_json(*(p.roadNumberText), allocator), allocator);
    return json;
}




/*
*   DestinationRoads - Type SEQUENCE OF
*   From GDD - File ISO14823.asn
*/

Value to_json(const DestinationRoads& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const DestinationRoad_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   Distance - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const Distance& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("value", to_json((p.value), allocator), allocator);
    json.AddMember("unit", to_json((p.unit), allocator), allocator);

    return json;
}




/*
*   DistanceOrDuration - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const DistanceOrDuration& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("value", to_json((p.value), allocator), allocator);
    json.AddMember("unit", to_json((p.unit), allocator), allocator);

    return json;
}




/*
*   HoursMinutes - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const HoursMinutes& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("hours", to_json((p.hours), allocator), allocator);
    json.AddMember("mins", to_json((p.mins), allocator), allocator);

    return json;
}




/*
*   MonthDay - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const MonthDay& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("month", to_json((p.month), allocator), allocator);
    json.AddMember("day", to_json((p.day), allocator), allocator);

    return json;
}




/*
*   RepeatingPeriodDayTypes - Type BIT STRING
*   From GDD - File ISO14823.asn
*/

Value to_json_RepeatingPeriodDayTypes(const RepeatingPeriodDayTypes_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("national-holiday", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("even-days", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("odd-days", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("market-day", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    return json;
}


/*
*   Weight - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const Weight& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("value", to_json((p.value), allocator), allocator);
    json.AddMember("unit", to_json((p.unit), allocator), allocator);

    return json;
}




/*
*   AxleWeightLimits - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const AxleWeightLimits_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("maxLadenweightOnAxle1", to_json((p.maxLadenweightOnAxle1), allocator), allocator);
    json.AddMember("maxLadenweightOnAxle2", to_json((p.maxLadenweightOnAxle2), allocator), allocator);
    json.AddMember("maxLadenweightOnAxle3", to_json((p.maxLadenweightOnAxle3), allocator), allocator);
    json.AddMember("maxLadenweightOnAxle4", to_json((p.maxLadenweightOnAxle4), allocator), allocator);
    json.AddMember("maxLadenweightOnAxle5", to_json((p.maxLadenweightOnAxle5), allocator), allocator);

    return json;
}




/*
*   AddRq - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const AddRq_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("attributeId", to_json((p.attributeId), allocator), allocator);
    json.AddMember("value", to_json((p.value), allocator), allocator);

    return json;
}




/*
*   ChannelRq - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const ChannelRq_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("channelId", to_json((p.channelId), allocator), allocator);
    json.AddMember("apdu", to_json((p.apdu), allocator), allocator);

    return json;
}




/*
*   ChannelRs - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const ChannelRs_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("channelId", to_json((p.channelId), allocator), allocator);
    json.AddMember("apdu", to_json((p.apdu), allocator), allocator);

    return json;
}




/*
*   CopyRq - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const CopyRq_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("destinationEID", to_json((p.destinationEID), allocator), allocator);

    return json;
}




/*
*   GetInstanceRq - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const GetInstanceRq_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("posOfFirstInstance", to_json((p.posOfFirstInstance), allocator), allocator);
    json.AddMember("posOfLastInstance", to_json((p.posOfLastInstance), allocator), allocator);

    return json;
}




/*
*   SubRq - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const SubRq_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("attributeId", to_json((p.attributeId), allocator), allocator);
    json.AddMember("value", to_json((p.value), allocator), allocator);

    return json;
}




/*
*   DateCompact - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const DateCompact_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("year", to_json((p.year), allocator), allocator);
    json.AddMember("month", to_json((p.month), allocator), allocator);
    json.AddMember("day", to_json((p.day), allocator), allocator);

    return json;
}




/*
*   DieselEmissionValues::DieselEmissionValues__particulate - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const DieselEmissionValues::DieselEmissionValues__particulate& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("unitType", to_json((p.unitType), allocator), allocator);
    json.AddMember("value", to_json((p.value), allocator), allocator);

    return json;
}




/*
*   DieselEmissionValues - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const DieselEmissionValues_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("particulate", to_json((p.particulate), allocator), allocator);
    json.AddMember("absorptionCoeff", to_json((p.absorptionCoeff), allocator), allocator);

    return json;
}




/*
*   EfcDsrcApplication_DriverCharacteristics - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const EfcDsrcApplication_DriverCharacteristics_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("driverClass", to_json((p.driverClass), allocator), allocator);
    json.AddMember("tripPurpose", to_json((p.tripPurpose), allocator), allocator);

    return json;
}




/*
*   EFC-ContextMark - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const EFC_ContextMark_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("contractProvider", to_json((p.contractProvider), allocator), allocator);
    json.AddMember("typeOfContract", to_json((p.typeOfContract), allocator), allocator);
    json.AddMember("contextVersion", to_json((p.contextVersion), allocator), allocator);

    return json;
}




/*
*   EnvironmentalCharacteristics - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const EnvironmentalCharacteristics_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("euroValue", to_json((p.euroValue), allocator), allocator);
    json.AddMember("copValue", to_json((p.copValue), allocator), allocator);

    return json;
}




/*
*   Engine - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const Engine_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("engineCapacity", to_json((p.engineCapacity), allocator), allocator);
    json.AddMember("enginePower", to_json((p.enginePower), allocator), allocator);

    return json;
}




/*
*   EquipmentStatus - Type BIT STRING
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json_EquipmentStatus(const EquipmentStatus_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    return json;
}


/*
*   ExhaustEmissionValues - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const ExhaustEmissionValues_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("unitType", to_json((p.unitType), allocator), allocator);
    json.AddMember("emissionCO", to_json((p.emissionCO), allocator), allocator);
    json.AddMember("emissionHC", to_json((p.emissionHC), allocator), allocator);
    json.AddMember("emissionNOX", to_json((p.emissionNOX), allocator), allocator);
    json.AddMember("emissionHCNOX", to_json((p.emissionHCNOX), allocator), allocator);

    return json;
}




/*
*   LPN - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const LPN_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("countryCode", to_json_AVIAEINumberingAndDataStructures_CountryCode((p.countryCode), allocator), allocator);
    json.AddMember("alphabetIndicator", to_json((p.alphabetIndicator), allocator), allocator);
    json.AddMember("licencePlateNumber", to_json((p.licencePlateNumber), allocator), allocator);

    return json;
}




/*
*   PassengerCapacity - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const PassengerCapacity_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("numberOfSeats", to_json((p.numberOfSeats), allocator), allocator);
    json.AddMember("numberOfStandingPlaces", to_json((p.numberOfStandingPlaces), allocator), allocator);

    return json;
}




/*
*   SignedValue - Type CHOICE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const SignedValue_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == SignedValue_PR_positive) {
        json.AddMember("positive", to_json(p.choice.positive, allocator), allocator);
    } else if (p.present == SignedValue_PR_negative) {
        json.AddMember("negative", to_json(p.choice.negative, allocator), allocator);
    }
    return json;
}


/*
*   Provider - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const EfcDsrcApplication_Provider_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("countryCode", to_json_AVIAEINumberingAndDataStructures_CountryCode((p.countryCode), allocator), allocator);
    json.AddMember("providerIdentifier", to_json((p.providerIdentifier), allocator), allocator);

    return json;
}




/*
*   PurseBalance - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const PurseBalance_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("purseValue", to_json((p.purseValue), allocator), allocator);
    json.AddMember("purseUnit", to_json((p.purseUnit), allocator), allocator);

    return json;
}




/*
*   ReceiptContract - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const ReceiptContract_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("sessionContractProvider", to_json((p.sessionContractProvider), allocator), allocator);
    json.AddMember("sessionTypeOfContract", to_json((p.sessionTypeOfContract), allocator), allocator);
    json.AddMember("sessionContractSerialNumber", to_json((p.sessionContractSerialNumber), allocator), allocator);

    return json;
}




/*
*   SessionClass - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const SessionClass_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("sessionTariffClass", to_json((p.sessionTariffClass), allocator), allocator);
    json.AddMember("sessionClaimedClass", to_json((p.sessionClaimedClass), allocator), allocator);

    return json;
}




/*
*   SessionLocation - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const SessionLocation_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("ascendingKilometrage", to_json((p.ascendingKilometrage), allocator), allocator);
    json.AddMember("laneCodeNumber", to_json((p.laneCodeNumber), allocator), allocator);

    return json;
}




/*
*   DateAndTime::DateAndTime__timeCompact - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const DateAndTime::DateAndTime__timeCompact& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("hours", to_json((p.hours), allocator), allocator);
    json.AddMember("mins", to_json((p.mins), allocator), allocator);
    json.AddMember("double-secs", to_json((p.double_secs), allocator), allocator);

    return json;
}




/*
*   DateAndTime - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const DateAndTime_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("timeDate", to_json((p.timeDate), allocator), allocator);
    json.AddMember("timeCompact", to_json((p.timeCompact), allocator), allocator);

    return json;
}




/*
*   SoundLevel - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const SoundLevel_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("soundstationary", to_json((p.soundstationary), allocator), allocator);
    json.AddMember("sounddriveby", to_json((p.sounddriveby), allocator), allocator);

    return json;
}




/*
*   TrailerDetails - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const TrailerDetails_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("trailerType", to_json((p.trailerType), allocator), allocator);
    json.AddMember("trailerAxles", to_json((p.trailerAxles), allocator), allocator);

    return json;
}




/*
*   ValidityOfContract - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const ValidityOfContract_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("issuerRestrictions", to_json((p.issuerRestrictions), allocator), allocator);
    json.AddMember("contractExpiryDate", to_json((p.contractExpiryDate), allocator), allocator);

    return json;
}




/*
*   VehicleAxles::VehicleAxles__vehicleAxlesNumber::VehicleAxles__vehicleAxlesNumber__numberOfAxles - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const VehicleAxles::VehicleAxles__vehicleAxlesNumber::VehicleAxles__vehicleAxlesNumber__numberOfAxles& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("trailerAxles", to_json((p.trailerAxles), allocator), allocator);
    json.AddMember("tractorAxles", to_json((p.tractorAxles), allocator), allocator);

    return json;
}




/*
*   VehicleAxles::VehicleAxles__vehicleAxlesNumber - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const VehicleAxles::VehicleAxles__vehicleAxlesNumber& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("tyreType", to_json((p.tyreType), allocator), allocator);
    json.AddMember("numberOfAxles", to_json((p.numberOfAxles), allocator), allocator);

    return json;
}




/*
*   VehicleAxles - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const VehicleAxles_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("vehicleFirstAxleHeight", to_json((p.vehicleFirstAxleHeight), allocator), allocator);
    json.AddMember("vehicleAxlesNumber", to_json((p.vehicleAxlesNumber), allocator), allocator);

    return json;
}




/*
*   VehicleDimensions - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const VehicleDimensions_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("vehicleLengthOverall", to_json((p.vehicleLengthOverall), allocator), allocator);
    json.AddMember("vehicleHeigthOverall", to_json((p.vehicleHeigthOverall), allocator), allocator);
    json.AddMember("vehicleWidthOverall", to_json((p.vehicleWidthOverall), allocator), allocator);

    return json;
}




/*
*   VehicleSpecificCharacteristics - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const VehicleSpecificCharacteristics_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("environmentalCharacteristics", to_json((p.environmentalCharacteristics), allocator), allocator);
    json.AddMember("engineCharacteristics", to_json((p.engineCharacteristics), allocator), allocator);
    json.AddMember("descriptiveCharacteristics", to_json((p.descriptiveCharacteristics), allocator), allocator);
    json.AddMember("futureCharacteristics", to_json((p.futureCharacteristics), allocator), allocator);

    return json;
}




/*
*   VehicleWeightLimits - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const VehicleWeightLimits_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("vehicleMaxLadenWeight", to_json((p.vehicleMaxLadenWeight), allocator), allocator);
    json.AddMember("vehicleTrainMaximumWeight", to_json((p.vehicleTrainMaximumWeight), allocator), allocator);
    json.AddMember("vehicleWeightUnladen", to_json((p.vehicleWeightUnladen), allocator), allocator);

    return json;
}




/*
*   Ext1 - Type CHOICE
*   From CITSapplMgmtIDs - File ISO17419.asn
*/

Value to_json(const CITSapplMgmtIDs_Ext1_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == CITSapplMgmtIDs_Ext1_PR_content) {
        json.AddMember("content", to_json(p.choice.content, allocator), allocator);
    } else if (p.present == CITSapplMgmtIDs_Ext1_PR_extension) {
        json.AddMember("extension", to_json(p.choice.extension, allocator), allocator);
    }
    return json;
}


/*
*   Ext2 - Type CHOICE
*   From CITSapplMgmtIDs - File ISO17419.asn
*/

Value to_json(const CITSapplMgmtIDs_Ext2_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == CITSapplMgmtIDs_Ext2_PR_content) {
        json.AddMember("content", to_json(p.choice.content, allocator), allocator);
    } else if (p.present == CITSapplMgmtIDs_Ext2_PR_extension) {
        json.AddMember("extension", to_json(p.choice.extension, allocator), allocator);
    }
    return json;
}


/*
*   EuVehicleCategoryCode - Type CHOICE
*   From ElectronicRegistrationIdentificationVehicleDataModule - File ISO24534-3.asn
*/

Value to_json(const ElectronicRegistrationIdentificationVehicleDataModule_EuVehicleCategoryCode_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == ElectronicRegistrationIdentificationVehicleDataModule_EuVehicleCategoryCode_PR_euVehicleCategoryL) {
        json.AddMember("euVehicleCategoryL", to_json(p.choice.euVehicleCategoryL, allocator), allocator);
    } else if (p.present == ElectronicRegistrationIdentificationVehicleDataModule_EuVehicleCategoryCode_PR_euVehicleCategoryM) {
        json.AddMember("euVehicleCategoryM", to_json(p.choice.euVehicleCategoryM, allocator), allocator);
    } else if (p.present == ElectronicRegistrationIdentificationVehicleDataModule_EuVehicleCategoryCode_PR_euVehicleCategoryN) {
        json.AddMember("euVehicleCategoryN", to_json(p.choice.euVehicleCategoryN, allocator), allocator);
    } else if (p.present == ElectronicRegistrationIdentificationVehicleDataModule_EuVehicleCategoryCode_PR_euVehicleCategoryO) {
        json.AddMember("euVehicleCategoryO", to_json(p.choice.euVehicleCategoryO, allocator), allocator);
    }
    return json;
}


/*
*   ConnectedDenms - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ConnectedDenms& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ITS_Container_ActionID_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   DeltaReferencePositions - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const DeltaReferencePositions& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ITS_Container_DeltaReferencePosition_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   IviIdentificationNumbers - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const IviIdentificationNumbers& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const IVI_IviIdentificationNumber_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   LaneIds - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const LaneIds& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const LaneID_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   LanePositions - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const LanePositions& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ITS_Container_LanePosition_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   SaeAutomationLevels - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const SaeAutomationLevels& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const SaeAutomationLevel_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   ZoneIds - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ZoneIds& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const Zid_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   AbsolutePosition - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const AbsolutePosition& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("latitude", to_json(((p.latitude) == 900000001) ? (p.latitude) : (double)(p.latitude) / 10000000.0, allocator), allocator);
    json.AddMember("longitude", to_json(((p.longitude) == 1800000001) ? (p.longitude) : (double)(p.longitude) / 10000000.0, allocator), allocator);

    return json;
}




/*
*   AbsolutePositionWAltitude - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const AbsolutePositionWAltitude& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("latitude", to_json(((p.latitude) == 900000001) ? (p.latitude) : (double)(p.latitude) / 10000000.0, allocator), allocator);
    json.AddMember("longitude", to_json(((p.longitude) == 1800000001) ? (p.longitude) : (double)(p.longitude) / 10000000.0, allocator), allocator);
    json.AddMember("altitude", to_json((p.altitude), allocator), allocator);

    return json;
}




/*
*   ComputedSegment - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ComputedSegment& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("zoneId", to_json((p.zoneId), allocator), allocator);
    json.AddMember("laneNumber", to_json((p.laneNumber), allocator), allocator);
    json.AddMember("laneWidth", to_json((p.laneWidth), allocator), allocator);
    if (p.offsetDistance != 0) json.AddMember("offsetDistance", to_json(*(p.offsetDistance), allocator), allocator);
    if (p.offsetPosition != 0) json.AddMember("offsetPosition", to_json(*(p.offsetPosition), allocator), allocator);
    return json;
}




/*
*   DeltaPosition - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const DeltaPosition& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("deltaLatitude", to_json((p.deltaLatitude), allocator), allocator);
    json.AddMember("deltaLongitude", to_json((p.deltaLongitude), allocator), allocator);

    return json;
}




/*
*   ISO14823Code::ISO14823Code__pictogramCode::ISO14823Code__pictogramCode__serviceCategoryCode - Type CHOICE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ISO14823Code::ISO14823Code__pictogramCode::ISO14823Code__pictogramCode__serviceCategoryCode& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == ISO14823Code__pictogramCode__serviceCategoryCode_PR::ISO14823Code__pictogramCode__serviceCategoryCode_PR_trafficSignPictogram) {
        json.AddMember("trafficSignPictogram", to_json(p.choice.trafficSignPictogram, allocator), allocator);
    } else if (p.present == ISO14823Code__pictogramCode__serviceCategoryCode_PR::ISO14823Code__pictogramCode__serviceCategoryCode_PR_publicFacilitiesPictogram) {
        json.AddMember("publicFacilitiesPictogram", to_json(p.choice.publicFacilitiesPictogram, allocator), allocator);
    } else if (p.present == ISO14823Code__pictogramCode__serviceCategoryCode_PR::ISO14823Code__pictogramCode__serviceCategoryCode_PR_ambientOrRoadConditionPictogram) {
        json.AddMember("ambientOrRoadConditionPictogram", to_json(p.choice.ambientOrRoadConditionPictogram, allocator), allocator);
    }
    return json;
}


/*
*   ISO14823Code::ISO14823Code__pictogramCode::ISO14823Code__pictogramCode__pictogramCategoryCode - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ISO14823Code::ISO14823Code__pictogramCode::ISO14823Code__pictogramCode__pictogramCategoryCode& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("nature", to_json((p.nature), allocator), allocator);
    json.AddMember("serialNumber", to_json((p.serialNumber), allocator), allocator);

    return json;
}




/*
*   ISO14823Code::ISO14823Code__pictogramCode - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ISO14823Code::ISO14823Code__pictogramCode& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("serviceCategoryCode", to_json((p.serviceCategoryCode), allocator), allocator);
    json.AddMember("pictogramCategoryCode", to_json((p.pictogramCategoryCode), allocator), allocator);
    if (p.countryCode != 0) json.AddMember("countryCode", to_json(*(p.countryCode), allocator), allocator);
    return json;
}




/*
*   LaneCharacteristics - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const LaneCharacteristics& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("zoneDefinitionAccuracy", to_json((p.zoneDefinitionAccuracy), allocator), allocator);
    json.AddMember("existinglaneMarkingStatus", to_json((p.existinglaneMarkingStatus), allocator), allocator);
    json.AddMember("newlaneMarkingColour", to_json((p.newlaneMarkingColour), allocator), allocator);
    json.AddMember("laneDelimitationLeft", to_json((p.laneDelimitationLeft), allocator), allocator);
    json.AddMember("laneDelimitationRight", to_json((p.laneDelimitationRight), allocator), allocator);
    json.AddMember("mergingWith", to_json((p.mergingWith), allocator), allocator);

    return json;
}




/*
*   LayoutComponent - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const LayoutComponent& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("layoutComponentId", to_json((p.layoutComponentId), allocator), allocator);
    json.AddMember("height", to_json((p.height), allocator), allocator);
    json.AddMember("width", to_json((p.width), allocator), allocator);
    json.AddMember("x", to_json((p.x), allocator), allocator);
    json.AddMember("y", to_json((p.y), allocator), allocator);
    json.AddMember("textScripting", to_json((p.textScripting), allocator), allocator);

    return json;
}




/*
*   LoadType - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const LoadType& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("goodsType", to_json((p.goodsType), allocator), allocator);
    json.AddMember("dangerousGoodsType", to_json((p.dangerousGoodsType), allocator), allocator);
    json.AddMember("specialTransportType", to_json_ITS_Container_SpecialTransportType((p.specialTransportType), allocator), allocator);

    return json;
}




/*
*   MapReference - Type CHOICE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const IVI_MapReference_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == IVI_MapReference_PR_roadsegment) {
        json.AddMember("roadsegment", to_json(p.choice.roadsegment, allocator), allocator);
    } else if (p.present == IVI_MapReference_PR_intersection) {
        json.AddMember("intersection", to_json(p.choice.intersection, allocator), allocator);
    }
    return json;
}


/*
*   RoadSurfaceDynamicCharacteristics - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const RoadSurfaceDynamicCharacteristics& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("condition", to_json((p.condition), allocator), allocator);
    json.AddMember("temperature", to_json((p.temperature), allocator), allocator);
    json.AddMember("iceOrWaterDepth", to_json((p.iceOrWaterDepth), allocator), allocator);
    json.AddMember("treatment", to_json((p.treatment), allocator), allocator);

    return json;
}




/*
*   RoadSurfaceStaticCharacteristics - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const RoadSurfaceStaticCharacteristics& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("frictionCoefficient", to_json((p.frictionCoefficient), allocator), allocator);
    json.AddMember("material", to_json((p.material), allocator), allocator);
    json.AddMember("wear", to_json((p.wear), allocator), allocator);
    json.AddMember("avBankingAngle", to_json((p.avBankingAngle), allocator), allocator);

    return json;
}




/*
*   Text - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const Text& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("textContent", to_json((p.textContent), allocator), allocator);
    if (p.layoutComponentId != 0) json.AddMember("layoutComponentId", to_json(*(p.layoutComponentId), allocator), allocator);
    return json;
}




/*
*   VehicleCharacteristicsFixValues - Type CHOICE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const VehicleCharacteristicsFixValues& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == VehicleCharacteristicsFixValues_PR_simpleVehicleType) {
        json.AddMember("simpleVehicleType", to_json(p.choice.simpleVehicleType, allocator), allocator);
    } else if (p.present == VehicleCharacteristicsFixValues_PR_euVehicleCategoryCode) {
        json.AddMember("euVehicleCategoryCode", to_json(p.choice.euVehicleCategoryCode, allocator), allocator);
    } else if (p.present == VehicleCharacteristicsFixValues_PR_euroAndCo2value) {
        json.AddMember("euroAndCo2value", to_json(p.choice.euroAndCo2value, allocator), allocator);
    } else if (p.present == VehicleCharacteristicsFixValues_PR_engineCharacteristics) {
        json.AddMember("engineCharacteristics", to_json(p.choice.engineCharacteristics, allocator), allocator);
    } else if (p.present == VehicleCharacteristicsFixValues_PR_loadType) {
        json.AddMember("loadType", to_json(p.choice.loadType, allocator), allocator);
    } else if (p.present == VehicleCharacteristicsFixValues_PR_usage) {
        json.AddMember("usage", to_json(p.choice.usage, allocator), allocator);
    }
    return json;
}


/*
*   VehicleCharacteristicsRanges::VehicleCharacteristicsRanges__limits - Type CHOICE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const VehicleCharacteristicsRanges::VehicleCharacteristicsRanges__limits& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == VehicleCharacteristicsRanges__limits_PR::VehicleCharacteristicsRanges__limits_PR_numberOfAxles) {
        json.AddMember("numberOfAxles", to_json(p.choice.numberOfAxles, allocator), allocator);
    } else if (p.present == VehicleCharacteristicsRanges__limits_PR::VehicleCharacteristicsRanges__limits_PR_vehicleDimensions) {
        json.AddMember("vehicleDimensions", to_json(p.choice.vehicleDimensions, allocator), allocator);
    } else if (p.present == VehicleCharacteristicsRanges__limits_PR::VehicleCharacteristicsRanges__limits_PR_vehicleWeightLimits) {
        json.AddMember("vehicleWeightLimits", to_json(p.choice.vehicleWeightLimits, allocator), allocator);
    } else if (p.present == VehicleCharacteristicsRanges__limits_PR::VehicleCharacteristicsRanges__limits_PR_axleWeightLimits) {
        json.AddMember("axleWeightLimits", to_json(p.choice.axleWeightLimits, allocator), allocator);
    } else if (p.present == VehicleCharacteristicsRanges__limits_PR::VehicleCharacteristicsRanges__limits_PR_passengerCapacity) {
        json.AddMember("passengerCapacity", to_json(p.choice.passengerCapacity, allocator), allocator);
    } else if (p.present == VehicleCharacteristicsRanges__limits_PR::VehicleCharacteristicsRanges__limits_PR_exhaustEmissionValues) {
        json.AddMember("exhaustEmissionValues", to_json(p.choice.exhaustEmissionValues, allocator), allocator);
    } else if (p.present == VehicleCharacteristicsRanges__limits_PR::VehicleCharacteristicsRanges__limits_PR_dieselEmissionValues) {
        json.AddMember("dieselEmissionValues", to_json(p.choice.dieselEmissionValues, allocator), allocator);
    } else if (p.present == VehicleCharacteristicsRanges__limits_PR::VehicleCharacteristicsRanges__limits_PR_soundLevel) {
        json.AddMember("soundLevel", to_json(p.choice.soundLevel, allocator), allocator);
    }
    return json;
}


/*
*   VehicleCharacteristicsRanges - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const VehicleCharacteristicsRanges& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("comparisonOperator", to_json((p.comparisonOperator), allocator), allocator);
    json.AddMember("limits", to_json((p.limits), allocator), allocator);

    return json;
}




/*
*   BasicContainer - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const CAM_PDU_Descriptions_BasicContainer_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("stationType", to_json((p.stationType), allocator), allocator);
    json.AddMember("referencePosition", to_json((p.referencePosition), allocator), allocator);

    return json;
}




/*
*   BasicVehicleContainerHighFrequency - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const BasicVehicleContainerHighFrequency& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("heading", to_json((p.heading), allocator), allocator);
    json.AddMember("speed", to_json((p.speed), allocator), allocator);
    json.AddMember("driveDirection", to_json((p.driveDirection), allocator), allocator);
    json.AddMember("vehicleLength", to_json((p.vehicleLength), allocator), allocator);
    json.AddMember("vehicleWidth", to_json(((p.vehicleWidth) == 61 || (p.vehicleWidth) == 62) ? (p.vehicleWidth) : (double)(p.vehicleWidth) / 10.0, allocator), allocator);
    json.AddMember("longitudinalAcceleration", to_json((p.longitudinalAcceleration), allocator), allocator);
    json.AddMember("curvature", to_json((p.curvature), allocator), allocator);
    json.AddMember("curvatureCalculationMode", to_json((p.curvatureCalculationMode), allocator), allocator);
    json.AddMember("yawRate", to_json((p.yawRate), allocator), allocator);
    if (p.accelerationControl != 0) json.AddMember("accelerationControl", to_json_ITS_Container_AccelerationControl(*(p.accelerationControl), allocator), allocator);
    if (p.lanePosition != 0) json.AddMember("lanePosition", to_json(*(p.lanePosition), allocator), allocator);
    if (p.steeringWheelAngle != 0) json.AddMember("steeringWheelAngle", to_json(*(p.steeringWheelAngle), allocator), allocator);
    if (p.lateralAcceleration != 0) json.AddMember("lateralAcceleration", to_json(*(p.lateralAcceleration), allocator), allocator);
    if (p.verticalAcceleration != 0) json.AddMember("verticalAcceleration", to_json(*(p.verticalAcceleration), allocator), allocator);
    if (p.performanceClass != 0) json.AddMember("performanceClass", to_json(*(p.performanceClass), allocator), allocator);
    if (p.cenDsrcTollingZone != 0) json.AddMember("cenDsrcTollingZone", to_json(*(p.cenDsrcTollingZone), allocator), allocator);
    return json;
}




/*
*   BasicVehicleContainerLowFrequency - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const BasicVehicleContainerLowFrequency& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("vehicleRole", to_json((p.vehicleRole), allocator), allocator);
    json.AddMember("exteriorLights", to_json_ITS_Container_ExteriorLights((p.exteriorLights), allocator), allocator);
    json.AddMember("pathHistory", to_json((p.pathHistory), allocator), allocator);

    return json;
}




/*
*   PublicTransportContainer - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const PublicTransportContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("embarkationStatus", to_json((p.embarkationStatus), allocator), allocator);
    if (p.ptActivation != 0) json.AddMember("ptActivation", to_json(*(p.ptActivation), allocator), allocator);
    return json;
}




/*
*   SpecialTransportContainer - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const SpecialTransportContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("specialTransportType", to_json_ITS_Container_SpecialTransportType((p.specialTransportType), allocator), allocator);
    json.AddMember("lightBarSirenInUse", to_json_ITS_Container_LightBarSirenInUse((p.lightBarSirenInUse), allocator), allocator);

    return json;
}




/*
*   DangerousGoodsContainer - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const DangerousGoodsContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("dangerousGoodsBasic", to_json((p.dangerousGoodsBasic), allocator), allocator);

    return json;
}




/*
*   RoadWorksContainerBasic - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const RoadWorksContainerBasic& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lightBarSirenInUse", to_json_ITS_Container_LightBarSirenInUse((p.lightBarSirenInUse), allocator), allocator);
    if (p.roadworksSubCauseCode != 0) json.AddMember("roadworksSubCauseCode", to_json(*(p.roadworksSubCauseCode), allocator), allocator);
    if (p.closedLanes != 0) json.AddMember("closedLanes", to_json(*(p.closedLanes), allocator), allocator);
    return json;
}




/*
*   RescueContainer - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const RescueContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lightBarSirenInUse", to_json_ITS_Container_LightBarSirenInUse((p.lightBarSirenInUse), allocator), allocator);

    return json;
}




/*
*   EmergencyContainer - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const EmergencyContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lightBarSirenInUse", to_json_ITS_Container_LightBarSirenInUse((p.lightBarSirenInUse), allocator), allocator);
    if (p.incidentIndication != 0) json.AddMember("incidentIndication", to_json(*(p.incidentIndication), allocator), allocator);
    if (p.emergencyPriority != 0) json.AddMember("emergencyPriority", to_json_ITS_Container_EmergencyPriority(*(p.emergencyPriority), allocator), allocator);
    return json;
}




/*
*   SafetyCarContainer - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const SafetyCarContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lightBarSirenInUse", to_json_ITS_Container_LightBarSirenInUse((p.lightBarSirenInUse), allocator), allocator);
    if (p.incidentIndication != 0) json.AddMember("incidentIndication", to_json(*(p.incidentIndication), allocator), allocator);
    if (p.trafficRule != 0) json.AddMember("trafficRule", to_json(*(p.trafficRule), allocator), allocator);
    if (p.speedLimit != 0) json.AddMember("speedLimit", to_json(*(p.speedLimit), allocator), allocator);
    return json;
}




/*
*   RSUContainerHighFrequency - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const RSUContainerHighFrequency& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.protectedCommunicationZonesRSU != 0) json.AddMember("protectedCommunicationZonesRSU", to_json(*(p.protectedCommunicationZonesRSU), allocator), allocator);
    return json;
}




/*
*   ManagementContainer - Type SEQUENCE
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const DENM_PDU_Descriptions_ManagementContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("actionID", to_json((p.actionID), allocator), allocator);
    json.AddMember("detectionTime", to_json((p.detectionTime), allocator), allocator);
    json.AddMember("referenceTime", to_json((p.referenceTime), allocator), allocator);
    json.AddMember("eventPosition", to_json((p.eventPosition), allocator), allocator);
    json.AddMember("stationType", to_json((p.stationType), allocator), allocator);
    if (p.termination != 0) json.AddMember("termination", to_json(*(p.termination), allocator), allocator);
    if (p.relevanceDistance != 0) json.AddMember("relevanceDistance", to_json(*(p.relevanceDistance), allocator), allocator);
    if (p.relevanceTrafficDirection != 0) json.AddMember("relevanceTrafficDirection", to_json(*(p.relevanceTrafficDirection), allocator), allocator);
    if (p.validityDuration != 0) json.AddMember("validityDuration", to_json(*(p.validityDuration), allocator), allocator);
    if (p.transmissionInterval != 0) json.AddMember("transmissionInterval", to_json(*(p.transmissionInterval), allocator), allocator);
    return json;
}




/*
*   SituationContainer - Type SEQUENCE
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const SituationContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("informationQuality", to_json((p.informationQuality), allocator), allocator);
    json.AddMember("eventType", to_json((p.eventType), allocator), allocator);
    if (p.linkedCause != 0) json.AddMember("linkedCause", to_json(*(p.linkedCause), allocator), allocator);
    if (p.eventHistory != 0) json.AddMember("eventHistory", to_json(*(p.eventHistory), allocator), allocator);
    return json;
}




/*
*   LocationContainer - Type SEQUENCE
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const LocationContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("traces", to_json((p.traces), allocator), allocator);
    if (p.eventSpeed != 0) json.AddMember("eventSpeed", to_json(*(p.eventSpeed), allocator), allocator);
    if (p.eventPositionHeading != 0) json.AddMember("eventPositionHeading", to_json(*(p.eventPositionHeading), allocator), allocator);
    if (p.roadType != 0) json.AddMember("roadType", to_json(*(p.roadType), allocator), allocator);
    return json;
}




/*
*   ImpactReductionContainer - Type SEQUENCE
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const ImpactReductionContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("heightLonCarrLeft", to_json((p.heightLonCarrLeft), allocator), allocator);
    json.AddMember("heightLonCarrRight", to_json((p.heightLonCarrRight), allocator), allocator);
    json.AddMember("posLonCarrLeft", to_json((p.posLonCarrLeft), allocator), allocator);
    json.AddMember("posLonCarrRight", to_json((p.posLonCarrRight), allocator), allocator);
    json.AddMember("positionOfPillars", to_json((p.positionOfPillars), allocator), allocator);
    json.AddMember("posCentMass", to_json((p.posCentMass), allocator), allocator);
    json.AddMember("wheelBaseVehicle", to_json((p.wheelBaseVehicle), allocator), allocator);
    json.AddMember("turningRadius", to_json((p.turningRadius), allocator), allocator);
    json.AddMember("posFrontAx", to_json((p.posFrontAx), allocator), allocator);
    json.AddMember("positionOfOccupants", to_json_ITS_Container_PositionOfOccupants((p.positionOfOccupants), allocator), allocator);
    json.AddMember("vehicleMass", to_json((p.vehicleMass), allocator), allocator);
    json.AddMember("requestResponseIndication", to_json((p.requestResponseIndication), allocator), allocator);

    return json;
}




/*
*   StationaryVehicleContainer - Type SEQUENCE
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const StationaryVehicleContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.stationarySince != 0) json.AddMember("stationarySince", to_json(*(p.stationarySince), allocator), allocator);
    if (p.stationaryCause != 0) json.AddMember("stationaryCause", to_json(*(p.stationaryCause), allocator), allocator);
    if (p.carryingDangerousGoods != 0) json.AddMember("carryingDangerousGoods", to_json(*(p.carryingDangerousGoods), allocator), allocator);
    if (p.numberOfOccupants != 0) json.AddMember("numberOfOccupants", to_json(*(p.numberOfOccupants), allocator), allocator);
    if (p.vehicleIdentification != 0) json.AddMember("vehicleIdentification", to_json(*(p.vehicleIdentification), allocator), allocator);
    if (p.energyStorageType != 0) json.AddMember("energyStorageType", to_json_ITS_Container_EnergyStorageType(*(p.energyStorageType), allocator), allocator);
    return json;
}




/*
*   ReferenceDenms - Type SEQUENCE OF
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const ReferenceDenms& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ITS_Container_ActionID_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   MapPosition - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_MapPosition_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("intersectionId", to_json((p.intersectionId), allocator), allocator);
    json.AddMember("lane", to_json((p.lane), allocator), allocator);

    return json;
}




/*
*   VruLowFrequencyContainer - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VruLowFrequencyContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.profileAndSubprofile != 0) json.AddMember("profileAndSubprofile", to_json(*(p.profileAndSubprofile), allocator), allocator);
    if (p.exteriorLights != 0) json.AddMember("exteriorLights", to_json(*(p.exteriorLights), allocator), allocator);
    if (p.sizeClass != 0) json.AddMember("sizeClass", to_json(*(p.sizeClass), allocator), allocator);
    return json;
}




/*
*   VruProfileAndSubprofile - Type CHOICE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_VruProfileAndSubprofile_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == VAM_PDU_Descriptions_VruProfileAndSubprofile_PR_pedestrian) {
        json.AddMember("pedestrian", to_json(p.choice.pedestrian, allocator), allocator);
    } else if (p.present == VAM_PDU_Descriptions_VruProfileAndSubprofile_PR_bicyclist) {
        json.AddMember("bicyclist", to_json(p.choice.bicyclist, allocator), allocator);
    } else if (p.present == VAM_PDU_Descriptions_VruProfileAndSubprofile_PR_motorcylist) {
        json.AddMember("motorcylist", to_json(p.choice.motorcylist, allocator), allocator);
    } else if (p.present == VAM_PDU_Descriptions_VruProfileAndSubprofile_PR_animal) {
        json.AddMember("animal", to_json(p.choice.animal, allocator), allocator);
    }
    return json;
}


/*
*   VruExteriorLights - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_VruExteriorLights_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("vruSpecific", to_json_VAM_PDU_Descriptions_VruSpecificExteriorLights((p.vruSpecific), allocator), allocator);
    json.AddMember("vehicular", to_json_ITS_Container_ExteriorLights((p.vehicular), allocator), allocator);

    return json;
}




/*
*   VruSpecificExteriorLights - Type BIT STRING
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json_VAM_PDU_Descriptions_VruSpecificExteriorLights(const VAM_PDU_Descriptions_VruSpecificExteriorLights_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("unavailable", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("backFlashLight", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("helmetLight", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("armLight", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("legLight", (bool) (*(p.buf + (sizeof(uint8_t) * (4 / 8))) & (1 << ((7 * ((4 / 8) + 1))-(4 % 8)))), allocator);
    json.AddMember("wheelLight", (bool) (*(p.buf + (sizeof(uint8_t) * (5 / 8))) & (1 << ((7 * ((5 / 8) + 1))-(5 % 8)))), allocator);
    return json;
}


/*
*   ClusterProfiles - Type BIT STRING
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json_ClusterProfiles(const ClusterProfiles_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("pedestrian", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("bicyclist", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("motorcyclist", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("animal", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    return json;
}


/*
*   VruClusterOperationContainer - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VruClusterOperationContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.clusterJoinInfo != 0) json.AddMember("clusterJoinInfo", to_json(*(p.clusterJoinInfo), allocator), allocator);
    if (p.clusterLeaveInfo != 0) json.AddMember("clusterLeaveInfo", to_json(*(p.clusterLeaveInfo), allocator), allocator);
    if (p.clusterBreakupInfo != 0) json.AddMember("clusterBreakupInfo", to_json(*(p.clusterBreakupInfo), allocator), allocator);
    if (p.clusterIdChangeTimeInfo != 0) json.AddMember("clusterIdChangeTimeInfo", to_json(*(p.clusterIdChangeTimeInfo), allocator), allocator);
    return json;
}




/*
*   ClusterJoinInfo - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_ClusterJoinInfo_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("clusterId", to_json((p.clusterId), allocator), allocator);
    json.AddMember("joinTime", to_json((p.joinTime), allocator), allocator);

    return json;
}




/*
*   ClusterLeaveInfo - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_ClusterLeaveInfo_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("clusterId", to_json((p.clusterId), allocator), allocator);
    json.AddMember("clusterLeaveReason", to_json((p.clusterLeaveReason), allocator), allocator);

    return json;
}




/*
*   ClusterBreakupInfo - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_ClusterBreakupInfo_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("clusterBreakupReason", to_json((p.clusterBreakupReason), allocator), allocator);
    json.AddMember("breakupTime", to_json((p.breakupTime), allocator), allocator);

    return json;
}




/*
*   VruPathPoint - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VruPathPoint& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("pathPosition", to_json((p.pathPosition), allocator), allocator);
    if (p.pathDeltaTime != 0) json.AddMember("pathDeltaTime", to_json(*(p.pathDeltaTime), allocator), allocator);
    return json;
}




/*
*   VruSafeDistanceIndication - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VruSafeDistanceIndication& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("stationSafeDistanceIndication", to_json((p.stationSafeDistanceIndication), allocator), allocator);
    if (p.subjectStation != 0) json.AddMember("subjectStation", to_json(*(p.subjectStation), allocator), allocator);
    if (p.timeToCollision != 0) json.AddMember("timeToCollision", to_json(*(p.timeToCollision), allocator), allocator);
    return json;
}




/*
*   SequenceOfTrajectoryInterceptionIndication - Type SEQUENCE OF
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_SequenceOfTrajectoryInterceptionIndication_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const VAM_PDU_Descriptions_TrajectoryInterceptionIndication_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   TrajectoryInterceptionIndication - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_TrajectoryInterceptionIndication_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("trajectoryInterceptionProbability", to_json((p.trajectoryInterceptionProbability), allocator), allocator);
    if (p.subjectStation != 0) json.AddMember("subjectStation", to_json(*(p.subjectStation), allocator), allocator);
    if (p.trajectoryInterceptionConfidence != 0) json.AddMember("trajectoryInterceptionConfidence", to_json(*(p.trajectoryInterceptionConfidence), allocator), allocator);
    return json;
}




/*
*   HeadingChangeIndication - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_HeadingChangeIndication_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("direction", to_json((p.direction), allocator), allocator);
    json.AddMember("actionDeltaTime", to_json((p.actionDeltaTime), allocator), allocator);

    return json;
}




/*
*   AccelerationChangeIndication - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_AccelerationChangeIndication_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("accelOrDecel", to_json((p.accelOrDecel), allocator), allocator);
    json.AddMember("actionDeltaTime", to_json((p.actionDeltaTime), allocator), allocator);

    return json;
}




/*
*   StabilityChangeIndication - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_StabilityChangeIndication_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lossProbability", to_json((p.lossProbability), allocator), allocator);
    json.AddMember("actionDeltaTime", to_json((p.actionDeltaTime), allocator), allocator);

    return json;
}




/*
*   NodeOffsetPointZ - Type CHOICE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const NodeOffsetPointZ& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == NodeOffsetPointZ_PR_node_Z1) {
        json.AddMember("node-Z1", to_json(p.choice.node_Z1, allocator), allocator);
    } else if (p.present == NodeOffsetPointZ_PR_node_Z2) {
        json.AddMember("node-Z2", to_json(p.choice.node_Z2, allocator), allocator);
    } else if (p.present == NodeOffsetPointZ_PR_node_Z3) {
        json.AddMember("node-Z3", to_json(p.choice.node_Z3, allocator), allocator);
    } else if (p.present == NodeOffsetPointZ_PR_node_Z4) {
        json.AddMember("node-Z4", to_json(p.choice.node_Z4, allocator), allocator);
    } else if (p.present == NodeOffsetPointZ_PR_node_Z5) {
        json.AddMember("node-Z5", to_json(p.choice.node_Z5, allocator), allocator);
    } else if (p.present == NodeOffsetPointZ_PR_node_Z6) {
        json.AddMember("node-Z6", to_json(p.choice.node_Z6, allocator), allocator);
    }
    return json;
}


/*
*   LaneDataAttribute - Type CHOICE
*   From DSRC-REGION-noCircular - File DSRC_REGION_noCircular.asn
*/

Value to_json(const LaneDataAttribute& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == LaneDataAttribute_PR_pathEndPointAngle) {
        json.AddMember("pathEndPointAngle", to_json(p.choice.pathEndPointAngle, allocator), allocator);
    } else if (p.present == LaneDataAttribute_PR_laneCrownPointCenter) {
        json.AddMember("laneCrownPointCenter", to_json(p.choice.laneCrownPointCenter, allocator), allocator);
    } else if (p.present == LaneDataAttribute_PR_laneCrownPointLeft) {
        json.AddMember("laneCrownPointLeft", to_json(p.choice.laneCrownPointLeft, allocator), allocator);
    } else if (p.present == LaneDataAttribute_PR_laneCrownPointRight) {
        json.AddMember("laneCrownPointRight", to_json(p.choice.laneCrownPointRight, allocator), allocator);
    } else if (p.present == LaneDataAttribute_PR_laneAngle) {
        json.AddMember("laneAngle", to_json(p.choice.laneAngle, allocator), allocator);
    } else if (p.present == LaneDataAttribute_PR_speedLimits) {
        json.AddMember("speedLimits", to_json(p.choice.speedLimits, allocator), allocator);
    }
    return json;
}


/*
*   LaneDataAttributeList - Type SEQUENCE OF
*   From DSRC-REGION-noCircular - File DSRC_REGION_noCircular.asn
*/

Value to_json(const LaneDataAttributeList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const LaneDataAttribute_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   NodeAttributeSetXY - Type SEQUENCE
*   From DSRC-REGION-noCircular - File DSRC_REGION_noCircular.asn
*/

Value to_json(const NodeAttributeSetXY_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.localNode != 0) json.AddMember("localNode", to_json(*(p.localNode), allocator), allocator);
    if (p.disabled != 0) json.AddMember("disabled", to_json(*(p.disabled), allocator), allocator);
    if (p.enabled != 0) json.AddMember("enabled", to_json(*(p.enabled), allocator), allocator);
    if (p.data != 0) json.AddMember("data", to_json(*(p.data), allocator), allocator);
    if (p.dWidth != 0) json.AddMember("dWidth", to_json(*(p.dWidth), allocator), allocator);
    if (p.dElevation != 0) json.AddMember("dElevation", to_json(*(p.dElevation), allocator), allocator);
    return json;
}




/*
*   CpmPayload - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const CpmPayload& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("managementContainer", to_json((p.managementContainer), allocator), allocator);
    json.AddMember("cpmContainers", to_json((p.cpmContainers), allocator), allocator);

    return json;
}




/*
*   MessageRateRange - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const MessageRateRange& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("messageRateMin", to_json((p.messageRateMin), allocator), allocator);
    json.AddMember("messageRateMax", to_json((p.messageRateMax), allocator), allocator);

    return json;
}




/*
*   OriginatingRsuContainer - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const OriginatingRsuContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.mapReference != 0) json.AddMember("mapReference", to_json(*(p.mapReference), allocator), allocator);
    return json;
}




/*
*   TrailerDataSet - Type SEQUENCE OF
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const TrailerDataSet& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const TrailerData_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   PerceivedObjectIds - Type SEQUENCE OF
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const PerceivedObjectIds& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const Identifier2B_t po = *(p.list.array[i]);
        // Value obj = to_json(po, allocator);
        json.PushBack(po, allocator);
    }
    return json;
}


/*
*   ItsPOIHeader - Type SEQUENCE
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const ItsPOIHeader& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("poiType", to_json((p.poiType), allocator), allocator);
    json.AddMember("timeStamp", to_json((p.timeStamp), allocator), allocator);
    json.AddMember("relayCapable", to_json((p.relayCapable), allocator), allocator);

    return json;
}




/*
*   ChargingSpotType - Type BIT STRING
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json_ChargingSpotType(const ChargingSpotType_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("standardChargeMode1", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("standardChargeMode2", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("standardOrFastChargeMode3", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("fastChargeWithExternalCharger", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    json.AddMember("quickDrop", (bool) (*(p.buf + (sizeof(uint8_t) * (8 / 8))) & (1 << ((7 * ((8 / 8) + 1))-(8 % 8)))), allocator);
    json.AddMember("inductiveChargeWhileStationary", (bool) (*(p.buf + (sizeof(uint8_t) * (12 / 8))) & (1 << ((7 * ((12 / 8) + 1))-(12 % 8)))), allocator);
    json.AddMember("inductiveChargeWhileDriving", (bool) (*(p.buf + (sizeof(uint8_t) * (14 / 8))) & (1 << ((7 * ((14 / 8) + 1))-(14 % 8)))), allocator);
    return json;
}


/*
*   TypeOfReceptacle - Type BIT STRING
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json_TypeOfReceptacle(const TypeOfReceptacle_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    return json;
}


/*
*   SpotAvailability - Type SEQUENCE
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const SpotAvailability& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("maxWaitingTimeMinutes", to_json((p.maxWaitingTimeMinutes), allocator), allocator);
    json.AddMember("blocking", to_json((p.blocking), allocator), allocator);

    return json;
}




/*
*   Payment-ID - Type CHOICE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const Payment_ID& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == Payment_ID_PR_contractID) {
        json.AddMember("contractID", to_json(p.choice.contractID, allocator), allocator);
    } else if (p.present == Payment_ID_PR_externalIdentificationMeans) {
        json.AddMember("externalIdentificationMeans", to_json(p.choice.externalIdentificationMeans, allocator), allocator);
    }
    return json;
}


/*
*   RechargingType - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const RechargingType& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("rechargingMode", to_json((p.rechargingMode), allocator), allocator);
    json.AddMember("powerSource", to_json((p.powerSource), allocator), allocator);

    return json;
}




/*
*   SupportedPaymentTypes - Type BIT STRING
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json_SupportedPaymentTypes(const SupportedPaymentTypes_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("contract", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("externalIdentification", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    return json;
}


/*
*   InterferenceManagementChannel - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const IMZM_PDU_Descriptions_InterferenceManagementChannel_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("centreFrequency", to_json((p.centreFrequency), allocator), allocator);
    json.AddMember("channelWidth", to_json((p.channelWidth), allocator), allocator);
    json.AddMember("exponent", to_json((p.exponent), allocator), allocator);

    return json;
}




/*
*   MitigationForTechnologies - Type SEQUENCE OF
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const IMZM_PDU_Descriptions_MitigationForTechnologies_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const IMZM_PDU_Descriptions_MitigationPerTechnologyClass_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   MitigationPerTechnologyClass - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const IMZM_PDU_Descriptions_MitigationPerTechnologyClass_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("accessTechnologyClass", to_json((p.accessTechnologyClass), allocator), allocator);
    if (p.lowDutyCycle != 0) json.AddMember("lowDutyCycle", to_json(*(p.lowDutyCycle), allocator), allocator);
    if (p.powerReduction != 0) json.AddMember("powerReduction", to_json(*(p.powerReduction), allocator), allocator);
    if (p.dmcToffLimit != 0) json.AddMember("dmcToffLimit", to_json(*(p.dmcToffLimit), allocator), allocator);
    if (p.dmcTonLimit != 0) json.AddMember("dmcTonLimit", to_json(*(p.dmcTonLimit), allocator), allocator);
    return json;
}




/*
*   TisTpgDRM-Situation - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgDRM_Situation& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("causeCode", to_json((p.causeCode), allocator), allocator);

    return json;
}




/*
*   TisTpgDRM-Location - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgDRM_Location& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("vehiclePosition", to_json((p.vehiclePosition), allocator), allocator);
    json.AddMember("vehicleSpeed", to_json((p.vehicleSpeed), allocator), allocator);
    json.AddMember("vehicleHeading", to_json((p.vehicleHeading), allocator), allocator);
    if (p.requestedPosition != 0) json.AddMember("requestedPosition", to_json(*(p.requestedPosition), allocator), allocator);
    if (p.searchRange != 0) json.AddMember("searchRange", to_json(*(p.searchRange), allocator), allocator);
    if (p.searchCondition != 0) json.AddMember("searchCondition", to_json(*(p.searchCondition), allocator), allocator);
    return json;
}




/*
*   TisTpgSNM-Management - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgSNM_Management& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("generationTime", to_json((p.generationTime), allocator), allocator);
    json.AddMember("totalTpgStations", to_json((p.totalTpgStations), allocator), allocator);

    return json;
}




/*
*   TisTpgTRM-Situation - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTRM_Situation& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("estArrivalTime", to_json((p.estArrivalTime), allocator), allocator);
    if (p.proposedPairingID != 0) json.AddMember("proposedPairingID", to_json(*(p.proposedPairingID), allocator), allocator);
    return json;
}




/*
*   TisTpgTRM-Location - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTRM_Location& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("vehiclePosition", to_json((p.vehiclePosition), allocator), allocator);
    json.AddMember("vehicleSpeed", to_json((p.vehicleSpeed), allocator), allocator);
    json.AddMember("vehicleHeading", to_json((p.vehicleHeading), allocator), allocator);

    return json;
}




/*
*   TisTpgTCM-Location - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTCM_Location& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.tpgLocation != 0) json.AddMember("tpgLocation", to_json(*(p.tpgLocation), allocator), allocator);
    if (p.address != 0) json.AddMember("address", to_json(*(p.address), allocator), allocator);
    return json;
}




/*
*   TyreData::TyreData__currentTyrePressure - Type CHOICE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TyreData::TyreData__currentTyrePressure& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == TyreData__currentTyrePressure_PR::TyreData__currentTyrePressure_PR_tyrePressureValue) {
        json.AddMember("tyrePressureValue", to_json(p.choice.tyrePressureValue, allocator), allocator);
    } else if (p.present == TyreData__currentTyrePressure_PR::TyreData__currentTyrePressure_PR_unavailable) {
        json.AddMember("unavailable", to_json(p.choice.unavailable, allocator), allocator);
    }
    return json;
}


/*
*   TyreData::TyreData__currentInsideAirTemperature - Type CHOICE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TyreData::TyreData__currentInsideAirTemperature& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == TyreData__currentInsideAirTemperature_PR::TyreData__currentInsideAirTemperature_PR_tyreAirTemperatureValue) {
        json.AddMember("tyreAirTemperatureValue", to_json(p.choice.tyreAirTemperatureValue, allocator), allocator);
    } else if (p.present == TyreData__currentInsideAirTemperature_PR::TyreData__currentInsideAirTemperature_PR_unavailable) {
        json.AddMember("unavailable", to_json(p.choice.unavailable, allocator), allocator);
    }
    return json;
}


/*
*   TyreData::TyreData__recommendedTyrePressure - Type CHOICE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TyreData::TyreData__recommendedTyrePressure& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == TyreData__recommendedTyrePressure_PR::TyreData__recommendedTyrePressure_PR_axlePlacardPressureValue) {
        json.AddMember("axlePlacardPressureValue", to_json(p.choice.axlePlacardPressureValue, allocator), allocator);
    } else if (p.present == TyreData__recommendedTyrePressure_PR::TyreData__recommendedTyrePressure_PR_unavailable) {
        json.AddMember("unavailable", to_json(p.choice.unavailable, allocator), allocator);
    }
    return json;
}


/*
*   TyreData::TyreData__sensorState - Type CHOICE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TyreData::TyreData__sensorState& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == TyreData__sensorState_PR::TyreData__sensorState_PR_sensorStateValue) {
        json.AddMember("sensorStateValue", to_json(p.choice.sensorStateValue, allocator), allocator);
    } else if (p.present == TyreData__sensorState_PR::TyreData__sensorState_PR_unavailable) {
        json.AddMember("unavailable", to_json(p.choice.unavailable, allocator), allocator);
    }
    return json;
}


/*
*   AppliedTyrePressure - Type CHOICE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const AppliedTyrePressure& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == AppliedTyrePressure_PR_tyrePressureValue) {
        json.AddMember("tyrePressureValue", to_json(p.choice.tyrePressureValue, allocator), allocator);
    } else if (p.present == AppliedTyrePressure_PR_unavailable) {
        json.AddMember("unavailable", to_json(p.choice.unavailable, allocator), allocator);
    }
    return json;
}


/*
*   TyreSidewallInformation - Type BIT STRING
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json_TyreSidewallInformation(const TyreSidewallInformation_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    return json;
}


/*
*   CurrentVehicleConfiguration - Type BIT STRING
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json_CurrentVehicleConfiguration(const CurrentVehicleConfiguration_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    return json;
}


/*
*   TIN - Type BIT STRING
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json_TIN(const TIN_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    return json;
}


/*
*   PressureConfiguration - Type BIT STRING
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json_PressureConfiguration(const PressureConfiguration_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    return json;
}


/*
*   AppliedTyrePressures - Type SEQUENCE OF
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const AppliedTyrePressures& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const AppliedTyrePressure_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   TpgAutomation - Type BIT STRING
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json_TpgAutomation(const TpgAutomation_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("fullAutomated", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("semiAutomated", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("manual", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("reserved", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    return json;
}


/*
*   TisProfile - Type BIT STRING
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json_TisProfile(const TisProfile_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("reserved", (bool) (*(p.buf + (sizeof(uint8_t) * (0 / 8))) & (1 << ((7 * ((0 / 8) + 1))-(0 % 8)))), allocator);
    json.AddMember("profileOne", (bool) (*(p.buf + (sizeof(uint8_t) * (1 / 8))) & (1 << ((7 * ((1 / 8) + 1))-(1 % 8)))), allocator);
    json.AddMember("profileTwo", (bool) (*(p.buf + (sizeof(uint8_t) * (2 / 8))) & (1 << ((7 * ((2 / 8) + 1))-(2 % 8)))), allocator);
    json.AddMember("profileThree", (bool) (*(p.buf + (sizeof(uint8_t) * (3 / 8))) & (1 << ((7 * ((3 / 8) + 1))-(3 % 8)))), allocator);
    return json;
}


/*
*   Language - Type BIT STRING
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json_Language(const Language_t p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    return json;
}


/*
*   McmBasicContainer - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const McmBasicContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("generationDeltaTime", to_json((p.generationDeltaTime), allocator), allocator);
    json.AddMember("stationID", to_json((p.stationID), allocator), allocator);
    json.AddMember("stationType", to_json((p.stationType), allocator), allocator);
    json.AddMember("deltaReferencePosition", to_json((p.deltaReferencePosition), allocator), allocator);

    return json;
}




/*
*   ManeuverCoordinationRational - Type CHOICE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const ManeuverCoordinationRational& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == ManeuverCoordinationRational_PR_maneuverCooperationGoal) {
        json.AddMember("maneuverCooperationGoal", to_json(p.choice.maneuverCooperationGoal, allocator), allocator);
    } else if (p.present == ManeuverCoordinationRational_PR_maneuverCooperationCost) {
        json.AddMember("maneuverCooperationCost", to_json(p.choice.maneuverCooperationCost, allocator), allocator);
    }
    return json;
}


/*
*   McmGenericCurrentState - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const McmGenericCurrentState& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("mcmType", to_json((p.mcmType), allocator), allocator);
    json.AddMember("maneuverId", to_json((p.maneuverId), allocator), allocator);
    json.AddMember("maneuverCoordinationConcept", to_json((p.maneuverCoordinationConcept), allocator), allocator);
    json.AddMember("maneuverCoordinationRational", to_json((p.maneuverCoordinationRational), allocator), allocator);
    json.AddMember("maneuverExecutionStatus", to_json((p.maneuverExecutionStatus), allocator), allocator);

    return json;
}




/*
*   VehicleAutomationState - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const VehicleAutomationState& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("humanDrivingLongitudinalAutomated", to_json((p.humanDrivingLongitudinalAutomated), allocator), allocator);
    json.AddMember("humanDrivenLateralAutomated", to_json((p.humanDrivenLateralAutomated), allocator), allocator);
    json.AddMember("automatedDriving", to_json((p.automatedDriving), allocator), allocator);

    return json;
}




/*
*   VehicleSize - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const VehicleSize& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("vehicleLenth", to_json((p.vehicleLenth), allocator), allocator);
    json.AddMember("vehicleWidth", to_json((p.vehicleWidth), allocator), allocator);

    return json;
}




/*
*   ReferenceStartingPosition - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const ReferenceStartingPosition& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("latitude", to_json(((p.latitude) == 900000001) ? (p.latitude) : (double)(p.latitude) / 10000000.0, allocator), allocator);
    json.AddMember("longitude", to_json(((p.longitude) == 1800000001) ? (p.longitude) : (double)(p.longitude) / 10000000.0, allocator), allocator);
    json.AddMember("positionConfidenceEllipse", to_json((p.positionConfidenceEllipse), allocator), allocator);
    json.AddMember("altitude", to_json((p.altitude), allocator), allocator);

    return json;
}




/*
*   IntermediatePointIntersection::IntermediatePointIntersection__exitLane - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const IntermediatePointIntersection::IntermediatePointIntersection__exitLane& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lanePosition", to_json((p.lanePosition), allocator), allocator);
    json.AddMember("laneCount", to_json((p.laneCount), allocator), allocator);

    return json;
}




/*
*   IntermediatePointIntersection - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const IntermediatePointIntersection& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("exitLane", to_json((p.exitLane), allocator), allocator);
    json.AddMember("exitHeading", to_json((p.exitHeading), allocator), allocator);
    json.AddMember("timeOfPosEntry", to_json((p.timeOfPosEntry), allocator), allocator);
    json.AddMember("timeOfPosExit", to_json((p.timeOfPosExit), allocator), allocator);

    return json;
}




/*
*   IntermediatePointOffroad - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const IntermediatePointOffroad& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("referencePosition", to_json((p.referencePosition), allocator), allocator);
    json.AddMember("referenceHeading", to_json((p.referenceHeading), allocator), allocator);
    json.AddMember("timeOfPos", to_json((p.timeOfPos), allocator), allocator);

    return json;
}




/*
*   Lane - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const Lane& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lanePosition", to_json((p.lanePosition), allocator), allocator);
    json.AddMember("laneCount", to_json((p.laneCount), allocator), allocator);

    return json;
}




/*
*   AcknowledgmentContainer - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const AcknowledgmentContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("acknowledgedType", to_json((p.acknowledgedType), allocator), allocator);
    json.AddMember("generationDeltaTime", to_json((p.generationDeltaTime), allocator), allocator);

    return json;
}




/*
*   ManeuverResponse - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const ManeuverResponse_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("response", to_json((p.response), allocator), allocator);
    if (p.declineReason != 0) json.AddMember("declineReason", to_json(*(p.declineReason), allocator), allocator);
    return json;
}




/*
*   AccelerationPolarWithZ - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const AccelerationPolarWithZ_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("accelerationMagnitude", to_json((p.accelerationMagnitude), allocator), allocator);
    json.AddMember("accelerationDirection", to_json((p.accelerationDirection), allocator), allocator);
    if (p.zAcceleration != 0) json.AddMember("zAcceleration", to_json(*(p.zAcceleration), allocator), allocator);
    return json;
}




/*
*   AccelerationCartesian - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const AccelerationCartesian_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("xAcceleration", to_json((p.xAcceleration), allocator), allocator);
    json.AddMember("yAcceleration", to_json((p.yAcceleration), allocator), allocator);
    if (p.zAcceleration != 0) json.AddMember("zAcceleration", to_json(*(p.zAcceleration), allocator), allocator);
    return json;
}




/*
*   BasicContainer - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_BasicContainer_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("stationType", to_json((p.stationType), allocator), allocator);
    json.AddMember("referencePosition", to_json((p.referencePosition), allocator), allocator);

    return json;
}




/*
*   BasicLaneConfiguration - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const BasicLaneConfiguration_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const BasicLaneInformation_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   DigitalMap - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_DigitalMap_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ETSI_ITS_CDD_ReferencePosition_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   EventHistory - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_EventHistory_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ETSI_ITS_CDD_EventPoint_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   InterferenceManagementInfoPerChannel - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_InterferenceManagementInfoPerChannel_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("interferenceManagementChannel", to_json((p.interferenceManagementChannel), allocator), allocator);
    json.AddMember("interferenceManagementZoneType", to_json((p.interferenceManagementZoneType), allocator), allocator);
    if (p.interferenceManagementMitigationType != 0) json.AddMember("interferenceManagementMitigationType", to_json(*(p.interferenceManagementMitigationType), allocator), allocator);
    return json;
}




/*
*   ItineraryPath - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ItineraryPath_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ETSI_ITS_CDD_ReferencePosition_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   IvimReference - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const IvimReference_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("serviceProviderId", to_json((p.serviceProviderId), allocator), allocator);
    json.AddMember("iviIdentificationNumber", to_json((p.iviIdentificationNumber), allocator), allocator);

    return json;
}




/*
*   IvimReferences - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const IvimReferences_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const IvimReference_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   LanePositionOptions - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const LanePositionOptions_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == LanePositionOptions_PR_simplelanePosition) {
        json.AddMember("simplelanePosition", to_json(p.choice.simplelanePosition, allocator), allocator);
    } else if (p.present == LanePositionOptions_PR_simpleLaneType) {
        json.AddMember("simpleLaneType", to_json(p.choice.simpleLaneType, allocator), allocator);
    } else if (p.present == LanePositionOptions_PR_detailedlanePosition) {
        json.AddMember("detailedlanePosition", to_json(p.choice.detailedlanePosition, allocator), allocator);
    } else if (p.present == LanePositionOptions_PR_lanePositionWithLateralDetails) {
        json.AddMember("lanePositionWithLateralDetails", to_json(p.choice.lanePositionWithLateralDetails, allocator), allocator);
    } else if (p.present == LanePositionOptions_PR_trafficIslandPosition) {
        json.AddMember("trafficIslandPosition", to_json(p.choice.trafficIslandPosition, allocator), allocator);
    }
    return json;
}


/*
*   LowerTriangularPositiveSemidefiniteMatrix - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const LowerTriangularPositiveSemidefiniteMatrix_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("componentsIncludedIntheMatrix", to_json_MatrixIncludedComponents((p.componentsIncludedIntheMatrix), allocator), allocator);
    json.AddMember("matrix", to_json((p.matrix), allocator), allocator);

    return json;
}




/*
*   MapemElementReference - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const MapemElementReference_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.mapReference != 0) json.AddMember("mapReference", to_json(*(p.mapReference), allocator), allocator);
    if (p.laneIds != 0) json.AddMember("laneIds", to_json(*(p.laneIds), allocator), allocator);
    if (p.connectionIds != 0) json.AddMember("connectionIds", to_json(*(p.connectionIds), allocator), allocator);
    return json;
}




/*
*   MapPosition - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_MapPosition_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.mapReference != 0) json.AddMember("mapReference", to_json(*(p.mapReference), allocator), allocator);
    if (p.laneId != 0) json.AddMember("laneId", to_json(*(p.laneId), allocator), allocator);
    if (p.connectionId != 0) json.AddMember("connectionId", to_json(*(p.connectionId), allocator), allocator);
    if (p.longitudinalLanePosition != 0) json.AddMember("longitudinalLanePosition", to_json(*(p.longitudinalLanePosition), allocator), allocator);
    return json;
}




/*
*   MapReference - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_MapReference_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == ETSI_ITS_CDD_MapReference_PR_roadsegment) {
        json.AddMember("roadsegment", to_json(p.choice.roadsegment, allocator), allocator);
    } else if (p.present == ETSI_ITS_CDD_MapReference_PR_intersection) {
        json.AddMember("intersection", to_json(p.choice.intersection, allocator), allocator);
    }
    return json;
}


/*
*   MapReferences - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const MapReferences_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ETSI_ITS_CDD_MapReference_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   MitigationForTechnologies - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_MitigationForTechnologies_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ETSI_ITS_CDD_MitigationPerTechnologyClass_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   OccupiedLanesWithConfidence::OccupiedLanesWithConfidence__lanePositionBased - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const OccupiedLanesWithConfidence::OccupiedLanesWithConfidence__lanePositionBased& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const LanePositionOptions_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   OccupiedLanesWithConfidence::OccupiedLanesWithConfidence__mapBased - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const OccupiedLanesWithConfidence::OccupiedLanesWithConfidence__mapBased& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ETSI_ITS_CDD_MapPosition_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   OccupiedLanesWithConfidence - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const OccupiedLanesWithConfidence_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lanePositionBased", to_json((p.lanePositionBased), allocator), allocator);
    json.AddMember("confidence", to_json((p.confidence), allocator), allocator);
    if (p.mapBased != 0) json.AddMember("mapBased", to_json(*(p.mapBased), allocator), allocator);
    return json;
}




/*
*   Path - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const Path_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ETSI_ITS_CDD_PathPoint_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   PathExtended - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PathExtended_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("pointOfEventZone", to_json((p.pointOfEventZone), allocator), allocator);
    json.AddMember("path", to_json((p.path), allocator), allocator);

    return json;
}




/*
*   PathHistory - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_PathHistory_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ETSI_ITS_CDD_PathPoint_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   PathPointPredicted - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PathPointPredicted_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("deltaLatitude", to_json((p.deltaLatitude), allocator), allocator);
    json.AddMember("deltaLongitude", to_json((p.deltaLongitude), allocator), allocator);
    json.AddMember("deltaAltitude", to_json((p.deltaAltitude), allocator), allocator);
    json.AddMember("altitudeConfidence", to_json((p.altitudeConfidence), allocator), allocator);
    if (p.horizontalPositionConfidence != 0) json.AddMember("horizontalPositionConfidence", to_json(*(p.horizontalPositionConfidence), allocator), allocator);
    if (p.pathDeltaTime != 0) json.AddMember("pathDeltaTime", to_json(*(p.pathDeltaTime), allocator), allocator);
    if (p.symmetricAreaOffset != 0) json.AddMember("symmetricAreaOffset", to_json(*(p.symmetricAreaOffset), allocator), allocator);
    if (p.asymmetricAreaOffset != 0) json.AddMember("asymmetricAreaOffset", to_json(*(p.asymmetricAreaOffset), allocator), allocator);
    return json;
}




/*
*   PolygonalShape - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PolygonalShape_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("polygon", to_json((p.polygon), allocator), allocator);
    if (p.shapeReferencePoint != 0) json.AddMember("shapeReferencePoint", to_json(*(p.shapeReferencePoint), allocator), allocator);
    if (p.height != 0) json.AddMember("height", to_json(*(p.height), allocator), allocator);
    return json;
}




/*
*   RadialShapesList - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const RadialShapesList_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const RadialShapeDetails_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   SequenceOfTrajectoryInterceptionIndication - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_SequenceOfTrajectoryInterceptionIndication_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ETSI_ITS_CDD_TrajectoryInterceptionIndication_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   Traces - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_Traces_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const Path_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   TracesExtended - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const TracesExtended_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const PathExtended_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   VarLengthNumber - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_VarLengthNumber_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == ETSI_ITS_CDD_VarLengthNumber_PR_content) {
        json.AddMember("content", to_json(p.choice.content, allocator), allocator);
    } else if (p.present == ETSI_ITS_CDD_VarLengthNumber_PR_extension) {
        json.AddMember("extension", to_json(p.choice.extension, allocator), allocator);
    }
    return json;
}


/*
*   Ext1 - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_Ext1_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == ETSI_ITS_CDD_Ext1_PR_content) {
        json.AddMember("content", to_json(p.choice.content, allocator), allocator);
    } else if (p.present == ETSI_ITS_CDD_Ext1_PR_extension) {
        json.AddMember("extension", to_json(p.choice.extension, allocator), allocator);
    }
    return json;
}


/*
*   VelocityCartesian - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const VelocityCartesian_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("xVelocity", to_json((p.xVelocity), allocator), allocator);
    json.AddMember("yVelocity", to_json((p.yVelocity), allocator), allocator);
    if (p.zVelocity != 0) json.AddMember("zVelocity", to_json(*(p.zVelocity), allocator), allocator);
    return json;
}




/*
*   IntersectionState-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const IntersectionState_addGrpC& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.activePrioritizations != 0) json.AddMember("activePrioritizations", to_json(*(p.activePrioritizations), allocator), allocator);
    return json;
}




/*
*   NodeAttributeSet-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const NodeAttributeSet_addGrpC& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.ptvRequest != 0) json.AddMember("ptvRequest", to_json(*(p.ptvRequest), allocator), allocator);
    if (p.nodeLink != 0) json.AddMember("nodeLink", to_json(*(p.nodeLink), allocator), allocator);
    if (p.node != 0) json.AddMember("node", to_json(*(p.node), allocator), allocator);
    return json;
}




/*
*   ItsStationPosition - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const ItsStationPosition_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("stationID", to_json((p.stationID), allocator), allocator);
    if (p.laneID != 0) json.AddMember("laneID", to_json(*(p.laneID), allocator), allocator);
    if (p.nodeXY != 0) json.AddMember("nodeXY", to_json(*(p.nodeXY), allocator), allocator);
    if (p.timeReference != 0) json.AddMember("timeReference", to_json(*(p.timeReference), allocator), allocator);
    return json;
}




/*
*   ItsStationPositionList - Type SEQUENCE OF
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const ItsStationPositionList_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ItsStationPosition_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   SignalHeadLocation - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const SignalHeadLocation_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("nodeXY", to_json((p.nodeXY), allocator), allocator);
    json.AddMember("nodeZ", to_json((p.nodeZ), allocator), allocator);
    json.AddMember("signalGroupID", to_json((p.signalGroupID), allocator), allocator);

    return json;
}




/*
*   SignalHeadLocationList - Type SEQUENCE OF
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const SignalHeadLocationList_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const SignalHeadLocation_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   ConnectingLane - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const ConnectingLane& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lane", to_json((p.lane), allocator), allocator);
    if (p.maneuver != 0) json.AddMember("maneuver", to_json_AllowedManeuvers(*(p.maneuver), allocator), allocator);
    return json;
}




/*
*   Connection - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const Connection& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("connectingLane", to_json((p.connectingLane), allocator), allocator);
    if (p.remoteIntersection != 0) json.AddMember("remoteIntersection", to_json(*(p.remoteIntersection), allocator), allocator);
    if (p.signalGroup != 0) json.AddMember("signalGroup", to_json(*(p.signalGroup), allocator), allocator);
    if (p.userClass != 0) json.AddMember("userClass", to_json(*(p.userClass), allocator), allocator);
    if (p.connectionID != 0) json.AddMember("connectionID", to_json(*(p.connectionID), allocator), allocator);
    return json;
}




/*
*   FullPositionVector - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const FullPositionVector& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("long", to_json(((p.Long) == 1800000001) ? (p.Long) : (double)(p.Long) / 10000000.0, allocator), allocator);
    json.AddMember("lat", to_json(((p.lat) == 900000001) ? (p.lat) : (double)(p.lat) / 10000000.0, allocator), allocator);
    if (p.utcTime != 0) json.AddMember("utcTime", to_json(*(p.utcTime), allocator), allocator);
    if (p.elevation != 0) json.AddMember("elevation", to_json(*(p.elevation), allocator), allocator);
    if (p.heading != 0) json.AddMember("heading", to_json(*(p.heading), allocator), allocator);
    if (p.speed != 0) json.AddMember("speed", to_json(*(p.speed), allocator), allocator);
    if (p.posAccuracy != 0) json.AddMember("posAccuracy", to_json(*(p.posAccuracy), allocator), allocator);
    if (p.timeConfidence != 0) json.AddMember("timeConfidence", to_json(*(p.timeConfidence), allocator), allocator);
    if (p.posConfidence != 0) json.AddMember("posConfidence", to_json(*(p.posConfidence), allocator), allocator);
    if (p.speedConfidence != 0) json.AddMember("speedConfidence", to_json(*(p.speedConfidence), allocator), allocator);
    return json;
}




/*
*   LaneTypeAttributes - Type CHOICE
*   From DSRC - File DSRC.asn
*/

Value to_json(const LaneTypeAttributes& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == LaneTypeAttributes_PR_vehicle) {
        json.AddMember("vehicle", to_json_LaneAttributes_Vehicle(p.choice.vehicle, allocator), allocator);
    } else if (p.present == LaneTypeAttributes_PR_crosswalk) {
        json.AddMember("crosswalk", to_json_LaneAttributes_Crosswalk(p.choice.crosswalk, allocator), allocator);
    } else if (p.present == LaneTypeAttributes_PR_bikeLane) {
        json.AddMember("bikeLane", to_json_LaneAttributes_Bike(p.choice.bikeLane, allocator), allocator);
    } else if (p.present == LaneTypeAttributes_PR_sidewalk) {
        json.AddMember("sidewalk", to_json_LaneAttributes_Sidewalk(p.choice.sidewalk, allocator), allocator);
    } else if (p.present == LaneTypeAttributes_PR_median) {
        json.AddMember("median", to_json_LaneAttributes_Barrier(p.choice.median, allocator), allocator);
    } else if (p.present == LaneTypeAttributes_PR_striping) {
        json.AddMember("striping", to_json_LaneAttributes_Striping(p.choice.striping, allocator), allocator);
    } else if (p.present == LaneTypeAttributes_PR_trackedVehicle) {
        json.AddMember("trackedVehicle", to_json_LaneAttributes_TrackedVehicle(p.choice.trackedVehicle, allocator), allocator);
    } else if (p.present == LaneTypeAttributes_PR_parking) {
        json.AddMember("parking", to_json_LaneAttributes_Parking(p.choice.parking, allocator), allocator);
    }
    return json;
}


/*
*   MovementEvent - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const MovementEvent& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("eventState", to_json((p.eventState), allocator), allocator);
    if (p.timing != 0) json.AddMember("timing", to_json(*(p.timing), allocator), allocator);
    if (p.speeds != 0) json.AddMember("speeds", to_json(*(p.speeds), allocator), allocator);
    return json;
}




/*
*   MovementEventList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const MovementEventList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const MovementEvent_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   MovementState - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const MovementState& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("signalGroup", to_json((p.signalGroup), allocator), allocator);
    json.AddMember("state-time-speed", to_json((p.state_time_speed), allocator), allocator);
    if (p.maneuverAssistList != 0) json.AddMember("maneuverAssistList", to_json(*(p.maneuverAssistList), allocator), allocator);
    return json;
}




/*
*   NodeXY - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const NodeXY_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("delta", to_json((p.delta), allocator), allocator);
    if (p.attributes != 0) json.AddMember("attributes", to_json(*(p.attributes), allocator), allocator);
    return json;
}




/*
*   NodeSetXY - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const NodeSetXY& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const NodeXY_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   RequestorPositionVector - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RequestorPositionVector& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("position", to_json((p.position), allocator), allocator);
    if (p.heading != 0) json.AddMember("heading", to_json(*(p.heading), allocator), allocator);
    if (p.speed != 0) json.AddMember("speed", to_json(*(p.speed), allocator), allocator);
    return json;
}




/*
*   RestrictionClassAssignment - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RestrictionClassAssignment& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("id", to_json((p.id), allocator), allocator);
    json.AddMember("users", to_json((p.users), allocator), allocator);

    return json;
}




/*
*   RestrictionClassList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const RestrictionClassList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const RestrictionClassAssignment_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   RTCMheader - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RTCMheader& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("status", to_json_GNSSstatus((p.status), allocator), allocator);
    json.AddMember("offsetSet", to_json((p.offsetSet), allocator), allocator);

    return json;
}




/*
*   RTCMmessageList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const RTCMmessageList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const RTCMmessage_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   SignalRequesterInfo - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalRequesterInfo& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("id", to_json((p.id), allocator), allocator);
    json.AddMember("request", to_json((p.request), allocator), allocator);
    json.AddMember("sequenceNumber", to_json((p.sequenceNumber), allocator), allocator);
    if (p.role != 0) json.AddMember("role", to_json(*(p.role), allocator), allocator);
    if (p.typeData != 0) json.AddMember("typeData", to_json(*(p.typeData), allocator), allocator);
    return json;
}




/*
*   SignalRequestList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalRequestList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const SignalRequestPackage_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   SignalStatusPackage - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalStatusPackage& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("inboundOn", to_json((p.inboundOn), allocator), allocator);
    json.AddMember("status", to_json((p.status), allocator), allocator);
    if (p.requester != 0) json.AddMember("requester", to_json(*(p.requester), allocator), allocator);
    if (p.outboundOn != 0) json.AddMember("outboundOn", to_json(*(p.outboundOn), allocator), allocator);
    if (p.minute != 0) json.AddMember("minute", to_json(*(p.minute), allocator), allocator);
    if (p.second != 0) json.AddMember("second", to_json(*(p.second), allocator), allocator);
    if (p.duration != 0) json.AddMember("duration", to_json(*(p.duration), allocator), allocator);
    return json;
}




/*
*   CS1 - Type SEQUENCE
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json(const CS1& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("countryCode", to_json_AVIAEINumberingAndDataStructures_CountryCode((p.countryCode), allocator), allocator);
    json.AddMember("issuerIdentifier", to_json((p.issuerIdentifier), allocator), allocator);
    json.AddMember("serviceNumber", to_json_ServiceNumber((p.serviceNumber), allocator), allocator);

    return json;
}




/*
*   CS2 - Type SEQUENCE
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json(const CS2& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("manufacturerIdentifier", to_json((p.manufacturerIdentifier), allocator), allocator);
    json.AddMember("serviceNumber", to_json_ServiceNumber((p.serviceNumber), allocator), allocator);

    return json;
}




/*
*   CS3 - Type SEQUENCE
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json(const CS3& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("startTime", to_json((p.startTime), allocator), allocator);
    json.AddMember("stopTime", to_json((p.stopTime), allocator), allocator);
    json.AddMember("geographLimit", to_json_GeoGraphicalLimit((p.geographLimit), allocator), allocator);
    json.AddMember("serviceAppLimit", to_json_ServiceApplicationLimit((p.serviceAppLimit), allocator), allocator);

    return json;
}




/*
*   CS4 - Type SEQUENCE
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json(const CS4& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("countryCode", to_json_AVIAEINumberingAndDataStructures_CountryCode((p.countryCode), allocator), allocator);
    json.AddMember("alphabetIndicator", to_json((p.alphabetIndicator), allocator), allocator);
    json.AddMember("licPlateNumber", to_json((p.licPlateNumber), allocator), allocator);

    return json;
}




/*
*   CS8 - Type SEQUENCE
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json(const CS8& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("countryCode", to_json_AVIAEINumberingAndDataStructures_CountryCode((p.countryCode), allocator), allocator);
    json.AddMember("taxCode", to_json((p.taxCode), allocator), allocator);

    return json;
}




/*
*   InternationalSign-applicablePeriod::InternationalSign-applicablePeriod__month-day - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const InternationalSign_applicablePeriod::InternationalSign_applicablePeriod__month_day& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("dateRangeStartMonthDay", to_json((p.dateRangeStartMonthDay), allocator), allocator);
    json.AddMember("dateRangeEndMonthDay", to_json((p.dateRangeEndMonthDay), allocator), allocator);

    return json;
}




/*
*   InternationalSign-applicablePeriod::InternationalSign-applicablePeriod__hourMinutes - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const InternationalSign_applicablePeriod::InternationalSign_applicablePeriod__hourMinutes& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("timeRangeStartTime", to_json((p.timeRangeStartTime), allocator), allocator);
    json.AddMember("timeRangeEndTime", to_json((p.timeRangeEndTime), allocator), allocator);

    return json;
}




/*
*   InternationalSign-applicablePeriod - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const InternationalSign_applicablePeriod& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.year != 0) json.AddMember("year", to_json(*(p.year), allocator), allocator);
    if (p.month_day != 0) json.AddMember("month-day", to_json(*(p.month_day), allocator), allocator);
    if (p.repeatingPeriodDayTypes != 0) json.AddMember("repeatingPeriodDayTypes", to_json_RepeatingPeriodDayTypes(*(p.repeatingPeriodDayTypes), allocator), allocator);
    if (p.hourMinutes != 0) json.AddMember("hourMinutes", to_json(*(p.hourMinutes), allocator), allocator);
    if (p.dateRangeOfWeek != 0) json.AddMember("dateRangeOfWeek", to_json_DayOfWeek(*(p.dateRangeOfWeek), allocator), allocator);
    if (p.durationHourMinute != 0) json.AddMember("durationHourMinute", to_json(*(p.durationHourMinute), allocator), allocator);
    return json;
}




/*
*   InternationalSign-applicableVehicleDimensions - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const InternationalSign_applicableVehicleDimensions& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.vehicleHeight != 0) json.AddMember("vehicleHeight", to_json(*(p.vehicleHeight), allocator), allocator);
    if (p.vehicleWidth != 0) json.AddMember("vehicleWidth", to_json(*(p.vehicleWidth), allocator), allocator);
    if (p.vehicleLength != 0) json.AddMember("vehicleLength", to_json(*(p.vehicleLength), allocator), allocator);
    if (p.vehicleWeight != 0) json.AddMember("vehicleWeight", to_json(*(p.vehicleWeight), allocator), allocator);
    return json;
}




/*
*   InternationalSign-section - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const InternationalSign_section& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.startingPointLength != 0) json.AddMember("startingPointLength", to_json(*(p.startingPointLength), allocator), allocator);
    if (p.continuityLength != 0) json.AddMember("continuityLength", to_json(*(p.continuityLength), allocator), allocator);
    return json;
}




/*
*   DDD-IO - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const DDD_IO& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("arrowDirection", to_json((p.arrowDirection), allocator), allocator);
    if (p.destPlace != 0) json.AddMember("destPlace", to_json(*(p.destPlace), allocator), allocator);
    if (p.destRoad != 0) json.AddMember("destRoad", to_json(*(p.destRoad), allocator), allocator);
    if (p.roadNumberIdentifier != 0) json.AddMember("roadNumberIdentifier", to_json(*(p.roadNumberIdentifier), allocator), allocator);
    if (p.streetName != 0) json.AddMember("streetName", to_json(*(p.streetName), allocator), allocator);
    if (p.streetNameText != 0) json.AddMember("streetNameText", to_json(*(p.streetNameText), allocator), allocator);
    if (p.distanceToDivergingPoint != 0) json.AddMember("distanceToDivergingPoint", to_json(*(p.distanceToDivergingPoint), allocator), allocator);
    if (p.distanceToDestinationPlace != 0) json.AddMember("distanceToDestinationPlace", to_json(*(p.distanceToDestinationPlace), allocator), allocator);
    return json;
}




/*
*   CreditRs - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const CreditRs_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("creditResult", to_json((p.creditResult), allocator), allocator);
    json.AddMember("creditAuthenticator", to_json((p.creditAuthenticator), allocator), allocator);

    return json;
}




/*
*   DebitRs - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const DebitRs_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("debitResult", to_json((p.debitResult), allocator), allocator);
    json.AddMember("debitAuthenticator", to_json((p.debitAuthenticator), allocator), allocator);

    return json;
}




/*
*   ContractValidity - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const ContractValidity_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("contractRestrictions", to_json((p.contractRestrictions), allocator), allocator);
    json.AddMember("contractExpiryDate", to_json((p.contractExpiryDate), allocator), allocator);

    return json;
}




/*
*   PaymentFee - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const PaymentFee_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("paymentFeeAmount", to_json((p.paymentFeeAmount), allocator), allocator);
    json.AddMember("paymentFeeUnit", to_json((p.paymentFeeUnit), allocator), allocator);

    return json;
}




/*
*   PaymentMeans - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const PaymentMeans_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("personalAccountNumber", to_json((p.personalAccountNumber), allocator), allocator);
    json.AddMember("paymentMeansExpiryDate", to_json((p.paymentMeansExpiryDate), allocator), allocator);
    json.AddMember("pamentMeansUsageControl", to_json((p.pamentMeansUsageControl), allocator), allocator);

    return json;
}




/*
*   ReceiptData - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const ReceiptData_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("sessionTime", to_json((p.sessionTime), allocator), allocator);
    json.AddMember("sessionServiceProvider", to_json((p.sessionServiceProvider), allocator), allocator);
    json.AddMember("locationOfStation", to_json((p.locationOfStation), allocator), allocator);
    json.AddMember("sessionType", to_json((p.sessionType), allocator), allocator);
    json.AddMember("sessionResult", to_json((p.sessionResult), allocator), allocator);
    json.AddMember("sessionTariffClass", to_json((p.sessionTariffClass), allocator), allocator);
    json.AddMember("sessionClaimedClass", to_json((p.sessionClaimedClass), allocator), allocator);
    json.AddMember("sessionFee", to_json((p.sessionFee), allocator), allocator);
    json.AddMember("sessionContractProvider", to_json((p.sessionContractProvider), allocator), allocator);
    json.AddMember("sessionTypeOfContract", to_json((p.sessionTypeOfContract), allocator), allocator);
    json.AddMember("sessionContextVersion", to_json((p.sessionContextVersion), allocator), allocator);
    json.AddMember("receiptDataAuthenticator", to_json((p.receiptDataAuthenticator), allocator), allocator);

    return json;
}




/*
*   ReceiptFinancialPart - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const ReceiptFinancialPart_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("personalAccountNumber", to_json((p.personalAccountNumber), allocator), allocator);
    json.AddMember("sessionPaymentFee", to_json((p.sessionPaymentFee), allocator), allocator);
    json.AddMember("sessionCurrentBalance", to_json((p.sessionCurrentBalance), allocator), allocator);
    json.AddMember("receiptFinancialSerialNumber", to_json((p.receiptFinancialSerialNumber), allocator), allocator);

    return json;
}




/*
*   ReceiptServicePart - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const ReceiptServicePart_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("sessionTime", to_json((p.sessionTime), allocator), allocator);
    json.AddMember("sessionServiceProvider", to_json((p.sessionServiceProvider), allocator), allocator);
    json.AddMember("stationLocation", to_json((p.stationLocation), allocator), allocator);
    json.AddMember("typeOfSession", to_json((p.typeOfSession), allocator), allocator);
    json.AddMember("sessionResultOperational", to_json((p.sessionResultOperational), allocator), allocator);
    json.AddMember("sessionResultFinancial", to_json((p.sessionResultFinancial), allocator), allocator);

    return json;
}




/*
*   EfcDsrcApplication_TrailerCharacteristics - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const EfcDsrcApplication_TrailerCharacteristics_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("trailerDetails", to_json((p.trailerDetails), allocator), allocator);
    json.AddMember("trailerMaxLadenWeight", to_json((p.trailerMaxLadenWeight), allocator), allocator);
    json.AddMember("trailerWeightUnladen", to_json((p.trailerWeightUnladen), allocator), allocator);

    return json;
}




/*
*   VarLengthNumber - Type CHOICE
*   From CITSapplMgmtIDs - File ISO17419.asn
*/

Value to_json(const CITSapplMgmtIDs_VarLengthNumber_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == CITSapplMgmtIDs_VarLengthNumber_PR_content) {
        json.AddMember("content", to_json(p.choice.content, allocator), allocator);
    } else if (p.present == CITSapplMgmtIDs_VarLengthNumber_PR_extension) {
        json.AddMember("extension", to_json(p.choice.extension, allocator), allocator);
    }
    return json;
}


/*
*   IviManagementContainer - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const IviManagementContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("serviceProviderId", to_json((p.serviceProviderId), allocator), allocator);
    json.AddMember("iviIdentificationNumber", to_json((p.iviIdentificationNumber), allocator), allocator);
    json.AddMember("iviStatus", to_json((p.iviStatus), allocator), allocator);
    if (p.timeStamp != 0) json.AddMember("timeStamp", to_json(*(p.timeStamp), allocator), allocator);
    if (p.validFrom != 0) json.AddMember("validFrom", to_json(*(p.validFrom), allocator), allocator);
    if (p.validTo != 0) json.AddMember("validTo", to_json(*(p.validTo), allocator), allocator);
    if (p.connectedIviStructures != 0) json.AddMember("connectedIviStructures", to_json(*(p.connectedIviStructures), allocator), allocator);
    if (p.connectedDenms != 0) json.AddMember("connectedDenms", to_json(*(p.connectedDenms), allocator), allocator);
    return json;
}




/*
*   RscPart - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const RscPart& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("relevanceZoneIds", to_json((p.relevanceZoneIds), allocator), allocator);
    if (p.detectionZoneIds != 0) json.AddMember("detectionZoneIds", to_json(*(p.detectionZoneIds), allocator), allocator);
    if (p.direction != 0) json.AddMember("direction", to_json(*(p.direction), allocator), allocator);
    if (p.roadSurfaceStaticCharacteristics != 0) json.AddMember("roadSurfaceStaticCharacteristics", to_json(*(p.roadSurfaceStaticCharacteristics), allocator), allocator);
    if (p.roadSurfaceDynamicCharacteristics != 0) json.AddMember("roadSurfaceDynamicCharacteristics", to_json(*(p.roadSurfaceDynamicCharacteristics), allocator), allocator);
    return json;
}




/*
*   MlcPart - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const MlcPart& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("zoneId", to_json((p.zoneId), allocator), allocator);
    if (p.laneIds != 0) json.AddMember("laneIds", to_json(*(p.laneIds), allocator), allocator);
    return json;
}




/*
*   AbsolutePositions - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const AbsolutePositions& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const AbsolutePosition_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   AbsolutePositionsWAltitude - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const AbsolutePositionsWAltitude& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const AbsolutePositionWAltitude_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   DeltaPositions - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const DeltaPositions& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const DeltaPosition_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   ConstraintTextLines1 - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ConstraintTextLines1& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const Text_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   ConstraintTextLines2 - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ConstraintTextLines2& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const Text_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   LayoutComponents - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const LayoutComponents& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const LayoutComponent_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   TextLines - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const TextLines& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const Text_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   TrailerCharacteristicsFixValuesList - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const TrailerCharacteristicsFixValuesList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const VehicleCharacteristicsFixValues_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   TrailerCharacteristicsRangesList - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const TrailerCharacteristicsRangesList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const VehicleCharacteristicsRanges_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   VehicleCharacteristicsFixValuesList - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const VehicleCharacteristicsFixValuesList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const VehicleCharacteristicsFixValues_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   VehicleCharacteristicsRangesList - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const VehicleCharacteristicsRangesList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const VehicleCharacteristicsRanges_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   ValidityPeriods - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ValidityPeriods& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const InternationalSign_applicablePeriod_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   PolygonalLine - Type CHOICE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const PolygonalLine& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == PolygonalLine_PR_deltaPositions) {
        json.AddMember("deltaPositions", to_json(p.choice.deltaPositions, allocator), allocator);
    } else if (p.present == PolygonalLine_PR_deltaPositionsWithAltitude) {
        json.AddMember("deltaPositionsWithAltitude", to_json(p.choice.deltaPositionsWithAltitude, allocator), allocator);
    } else if (p.present == PolygonalLine_PR_absolutePositions) {
        json.AddMember("absolutePositions", to_json(p.choice.absolutePositions, allocator), allocator);
    } else if (p.present == PolygonalLine_PR_absolutePositionsWithAltitude) {
        json.AddMember("absolutePositionsWithAltitude", to_json(p.choice.absolutePositionsWithAltitude, allocator), allocator);
    }
    return json;
}


/*
*   Segment - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const Segment& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("line", to_json((p.line), allocator), allocator);
    if (p.laneWidth != 0) json.AddMember("laneWidth", to_json(*(p.laneWidth), allocator), allocator);
    return json;
}




/*
*   TractorCharacteristics - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const TractorCharacteristics& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.equalTo != 0) json.AddMember("equalTo", to_json(*(p.equalTo), allocator), allocator);
    if (p.notEqualTo != 0) json.AddMember("notEqualTo", to_json(*(p.notEqualTo), allocator), allocator);
    if (p.ranges != 0) json.AddMember("ranges", to_json(*(p.ranges), allocator), allocator);
    return json;
}




/*
*   IVI_TrailerCharacteristics - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const IVI_TrailerCharacteristics& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.equalTo != 0) json.AddMember("equalTo", to_json(*(p.equalTo), allocator), allocator);
    if (p.notEqualTo != 0) json.AddMember("notEqualTo", to_json(*(p.notEqualTo), allocator), allocator);
    if (p.ranges != 0) json.AddMember("ranges", to_json(*(p.ranges), allocator), allocator);
    return json;
}




/*
*   VcCode - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const VcCode& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("roadSignClass", to_json((p.roadSignClass), allocator), allocator);
    json.AddMember("roadSignCode", to_json((p.roadSignCode), allocator), allocator);
    json.AddMember("vcOption", to_json((p.vcOption), allocator), allocator);
    if (p.validity != 0) json.AddMember("validity", to_json(*(p.validity), allocator), allocator);
    if (p.value != 0) json.AddMember("value", to_json(*(p.value), allocator), allocator);
    if (p.unit != 0) json.AddMember("unit", to_json(*(p.unit), allocator), allocator);
    return json;
}




/*
*   Zone - Type CHOICE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const Zone& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == Zone_PR_segment) {
        json.AddMember("segment", to_json(p.choice.segment, allocator), allocator);
    } else if (p.present == Zone_PR_area) {
        json.AddMember("area", to_json(p.choice.area, allocator), allocator);
    } else if (p.present == Zone_PR_computedSegment) {
        json.AddMember("computedSegment", to_json(p.choice.computedSegment, allocator), allocator);
    }
    return json;
}


/*
*   HighFrequencyContainer - Type CHOICE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const HighFrequencyContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == HighFrequencyContainer_PR_basicVehicleContainerHighFrequency) {
        json.AddMember("basicVehicleContainerHighFrequency", to_json(p.choice.basicVehicleContainerHighFrequency, allocator), allocator);
    } else if (p.present == HighFrequencyContainer_PR_rsuContainerHighFrequency) {
        json.AddMember("rsuContainerHighFrequency", to_json(p.choice.rsuContainerHighFrequency, allocator), allocator);
    }
    return json;
}


/*
*   LowFrequencyContainer - Type CHOICE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const LowFrequencyContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == LowFrequencyContainer_PR_basicVehicleContainerLowFrequency) {
        json.AddMember("basicVehicleContainerLowFrequency", to_json(p.choice.basicVehicleContainerLowFrequency, allocator), allocator);
    }
    return json;
}


/*
*   SpecialVehicleContainer - Type CHOICE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const SpecialVehicleContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == SpecialVehicleContainer_PR_publicTransportContainer) {
        json.AddMember("publicTransportContainer", to_json(p.choice.publicTransportContainer, allocator), allocator);
    } else if (p.present == SpecialVehicleContainer_PR_specialTransportContainer) {
        json.AddMember("specialTransportContainer", to_json(p.choice.specialTransportContainer, allocator), allocator);
    } else if (p.present == SpecialVehicleContainer_PR_dangerousGoodsContainer) {
        json.AddMember("dangerousGoodsContainer", to_json(p.choice.dangerousGoodsContainer, allocator), allocator);
    } else if (p.present == SpecialVehicleContainer_PR_roadWorksContainerBasic) {
        json.AddMember("roadWorksContainerBasic", to_json(p.choice.roadWorksContainerBasic, allocator), allocator);
    } else if (p.present == SpecialVehicleContainer_PR_rescueContainer) {
        json.AddMember("rescueContainer", to_json(p.choice.rescueContainer, allocator), allocator);
    } else if (p.present == SpecialVehicleContainer_PR_emergencyContainer) {
        json.AddMember("emergencyContainer", to_json(p.choice.emergencyContainer, allocator), allocator);
    } else if (p.present == SpecialVehicleContainer_PR_safetyCarContainer) {
        json.AddMember("safetyCarContainer", to_json(p.choice.safetyCarContainer, allocator), allocator);
    }
    return json;
}


/*
*   RoadWorksContainerExtended - Type SEQUENCE
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const RoadWorksContainerExtended& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.lightBarSirenInUse != 0) json.AddMember("lightBarSirenInUse", to_json_ITS_Container_LightBarSirenInUse(*(p.lightBarSirenInUse), allocator), allocator);
    if (p.closedLanes != 0) json.AddMember("closedLanes", to_json(*(p.closedLanes), allocator), allocator);
    if (p.restriction != 0) json.AddMember("restriction", to_json(*(p.restriction), allocator), allocator);
    if (p.speedLimit != 0) json.AddMember("speedLimit", to_json(*(p.speedLimit), allocator), allocator);
    if (p.incidentIndication != 0) json.AddMember("incidentIndication", to_json(*(p.incidentIndication), allocator), allocator);
    if (p.recommendedPath != 0) json.AddMember("recommendedPath", to_json(*(p.recommendedPath), allocator), allocator);
    if (p.startingPointSpeedLimit != 0) json.AddMember("startingPointSpeedLimit", to_json(*(p.startingPointSpeedLimit), allocator), allocator);
    if (p.trafficFlowRule != 0) json.AddMember("trafficFlowRule", to_json(*(p.trafficFlowRule), allocator), allocator);
    if (p.referenceDenms != 0) json.AddMember("referenceDenms", to_json(*(p.referenceDenms), allocator), allocator);
    return json;
}




/*
*   AlacarteContainer - Type SEQUENCE
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const AlacarteContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.lanePosition != 0) json.AddMember("lanePosition", to_json(*(p.lanePosition), allocator), allocator);
    if (p.impactReduction != 0) json.AddMember("impactReduction", to_json(*(p.impactReduction), allocator), allocator);
    if (p.externalTemperature != 0) json.AddMember("externalTemperature", to_json(*(p.externalTemperature), allocator), allocator);
    if (p.roadWorks != 0) json.AddMember("roadWorks", to_json(*(p.roadWorks), allocator), allocator);
    if (p.positioningSolution != 0) json.AddMember("positioningSolution", to_json(*(p.positioningSolution), allocator), allocator);
    if (p.stationaryVehicle != 0) json.AddMember("stationaryVehicle", to_json(*(p.stationaryVehicle), allocator), allocator);
    return json;
}




/*
*   VruLanePosition - Type CHOICE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VruLanePosition& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == VruLanePosition_PR_offRoadLanePosition) {
        json.AddMember("offRoadLanePosition", to_json(p.choice.offRoadLanePosition, allocator), allocator);
    } else if (p.present == VruLanePosition_PR_vehicularLanePosition) {
        json.AddMember("vehicularLanePosition", to_json(p.choice.vehicularLanePosition, allocator), allocator);
    } else if (p.present == VruLanePosition_PR_trafficIslandPosition) {
        json.AddMember("trafficIslandPosition", to_json(p.choice.trafficIslandPosition, allocator), allocator);
    } else if (p.present == VruLanePosition_PR_mapPosition) {
        json.AddMember("mapPosition", to_json(p.choice.mapPosition, allocator), allocator);
    }
    return json;
}


/*
*   NonIslandLanePosition - Type CHOICE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const NonIslandLanePosition& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == NonIslandLanePosition_PR_offRoadLanePosition) {
        json.AddMember("offRoadLanePosition", to_json(p.choice.offRoadLanePosition, allocator), allocator);
    } else if (p.present == NonIslandLanePosition_PR_vehicularLanePosition) {
        json.AddMember("vehicularLanePosition", to_json(p.choice.vehicularLanePosition, allocator), allocator);
    } else if (p.present == NonIslandLanePosition_PR_mapPosition) {
        json.AddMember("mapPosition", to_json(p.choice.mapPosition, allocator), allocator);
    }
    return json;
}


/*
*   SequenceOfVruPathPoint - Type SEQUENCE OF
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const SequenceOfVruPathPoint& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const VruPathPoint_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   SequenceOfVruSafeDistanceIndication - Type SEQUENCE OF
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const SequenceOfVruSafeDistanceIndication& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const VruSafeDistanceIndication_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   OffsetPoint - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const OffsetPoint& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("nodeOffsetPointxy", to_json((p.nodeOffsetPointxy), allocator), allocator);
    if (p.nodeOffsetPointZ != 0) json.AddMember("nodeOffsetPointZ", to_json(*(p.nodeOffsetPointZ), allocator), allocator);
    return json;
}




/*
*   CollectivePerceptionMessage - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const CollectivePerceptionMessage& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("header", to_json((p.header), allocator), allocator);
    json.AddMember("payload", to_json((p.payload), allocator), allocator);

    return json;
}




/*
*   ManagementContainer - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const CPM_PDU_Descriptions_ManagementContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("referenceTime", to_json((p.referenceTime), allocator), allocator);
    json.AddMember("referencePosition", to_json((p.referencePosition), allocator), allocator);
    if (p.segmentationInfo != 0) json.AddMember("segmentationInfo", to_json(*(p.segmentationInfo), allocator), allocator);
    if (p.messageRateRange != 0) json.AddMember("messageRateRange", to_json(*(p.messageRateRange), allocator), allocator);
    return json;
}




/*
*   OriginatingVehicleContainer - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const OriginatingVehicleContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("orientationAngle", to_json((p.orientationAngle), allocator), allocator);
    if (p.pitchAngle != 0) json.AddMember("pitchAngle", to_json(*(p.pitchAngle), allocator), allocator);
    if (p.rollAngle != 0) json.AddMember("rollAngle", to_json(*(p.rollAngle), allocator), allocator);
    if (p.trailerDataSet != 0) json.AddMember("trailerDataSet", to_json(*(p.trailerDataSet), allocator), allocator);
    return json;
}




/*
*   ParkingPlacesData - Type SEQUENCE OF
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const ParkingPlacesData& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const SpotAvailability_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   PreReservationRequestMessage - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const PreReservationRequestMessage& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("evse-ID", to_json((p.evse_ID), allocator), allocator);
    json.AddMember("arrivalTime", to_json((p.arrivalTime), allocator), allocator);
    json.AddMember("rechargingType", to_json((p.rechargingType), allocator), allocator);
    if (p.departureTime != 0) json.AddMember("departureTime", to_json(*(p.departureTime), allocator), allocator);
    if (p.batteryType != 0) json.AddMember("batteryType", to_json(*(p.batteryType), allocator), allocator);
    return json;
}




/*
*   PreReservationResponseMessage - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const PreReservationResponseMessage& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("preReservation-ID", to_json((p.preReservation_ID), allocator), allocator);
    json.AddMember("availabilityStatus", to_json((p.availabilityStatus), allocator), allocator);
    json.AddMember("preReservationExpirationTime", to_json((p.preReservationExpirationTime), allocator), allocator);
    json.AddMember("supportedPaymentTypes", to_json_SupportedPaymentTypes((p.supportedPaymentTypes), allocator), allocator);

    return json;
}




/*
*   ReservationRequestMessage - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const ReservationRequestMessage& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("currentTime", to_json((p.currentTime), allocator), allocator);
    json.AddMember("preReservation-ID", to_json((p.preReservation_ID), allocator), allocator);
    json.AddMember("arrivalTime", to_json((p.arrivalTime), allocator), allocator);
    json.AddMember("eAmount", to_json((p.eAmount), allocator), allocator);
    json.AddMember("eAmountMin", to_json((p.eAmountMin), allocator), allocator);
    json.AddMember("paymentType", to_json((p.paymentType), allocator), allocator);
    json.AddMember("payment-ID", to_json((p.payment_ID), allocator), allocator);
    if (p.departureTime != 0) json.AddMember("departureTime", to_json(*(p.departureTime), allocator), allocator);
    if (p.secondPayment_ID != 0) json.AddMember("secondPayment-ID", to_json(*(p.secondPayment_ID), allocator), allocator);
    if (p.pairing_ID != 0) json.AddMember("pairing-ID", to_json(*(p.pairing_ID), allocator), allocator);
    return json;
}




/*
*   ReservationResponseMessage - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const ReservationResponseMessage& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("reservationResponseCode", to_json((p.reservationResponseCode), allocator), allocator);
    json.AddMember("expirationTime", to_json((p.expirationTime), allocator), allocator);
    if (p.reservation_ID != 0) json.AddMember("reservation-ID", to_json(*(p.reservation_ID), allocator), allocator);
    if (p.reservation_Password != 0) json.AddMember("reservation-Password", to_json(*(p.reservation_Password), allocator), allocator);
    if (p.stationDetails != 0) json.AddMember("stationDetails", to_json(*(p.stationDetails), allocator), allocator);
    if (p.chargingSpotLabel != 0) json.AddMember("chargingSpotLabel", to_json(*(p.chargingSpotLabel), allocator), allocator);
    if (p.freeCancelTimeLimit != 0) json.AddMember("freeCancelTimeLimit", to_json(*(p.freeCancelTimeLimit), allocator), allocator);
    return json;
}




/*
*   CancellationRequestMessage - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const CancellationRequestMessage& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("reservation-ID", to_json((p.reservation_ID), allocator), allocator);
    json.AddMember("reservation-Password", to_json((p.reservation_Password), allocator), allocator);
    json.AddMember("currentTime", to_json((p.currentTime), allocator), allocator);

    return json;
}




/*
*   CancellationResponseMessage - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const CancellationResponseMessage& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("reservation-ID", to_json((p.reservation_ID), allocator), allocator);
    json.AddMember("cancellationResponseCode", to_json((p.cancellationResponseCode), allocator), allocator);

    return json;
}




/*
*   UpdateRequestMessage - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const UpdateRequestMessage& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("reservation-ID", to_json((p.reservation_ID), allocator), allocator);
    json.AddMember("reservation-Password", to_json((p.reservation_Password), allocator), allocator);
    json.AddMember("updatedArrivalTime", to_json((p.updatedArrivalTime), allocator), allocator);
    json.AddMember("updatedDepartureTime", to_json((p.updatedDepartureTime), allocator), allocator);

    return json;
}




/*
*   UpdateResponseMessage - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const UpdateResponseMessage& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("reservation-ID", to_json((p.reservation_ID), allocator), allocator);
    json.AddMember("updateResponseCode", to_json((p.updateResponseCode), allocator), allocator);
    if (p.chargingSpotLabel != 0) json.AddMember("chargingSpotLabel", to_json(*(p.chargingSpotLabel), allocator), allocator);
    return json;
}




/*
*   InterferenceManagementInfo - Type SEQUENCE OF
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const IMZM_PDU_Descriptions_InterferenceManagementInfo_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const IMZM_PDU_Descriptions_InterferenceManagementInfoPerChannel_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   InterferenceManagementMitigationType - Type CHOICE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const InterferenceManagementMitigationType& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == InterferenceManagementMitigationType_PR_unavailable) {
        json.AddMember("unavailable", to_json(p.choice.unavailable, allocator), allocator);
    } else if (p.present == InterferenceManagementMitigationType_PR_mitigationForTechnologies) {
        json.AddMember("mitigationForTechnologies", to_json(p.choice.mitigationForTechnologies, allocator), allocator);
    }
    return json;
}


/*
*   IMZMAreaEllipse - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const IMZMAreaEllipse& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("semiMajorRangeLength", to_json((double)(p.semiMajorRangeLength) / 10.0, allocator), allocator);
    json.AddMember("semiMinorRangeLength", to_json((double)(p.semiMinorRangeLength) / 10.0, allocator), allocator);
    json.AddMember("semiMajorRangeOrientation", to_json(((p.semiMajorRangeOrientation) == 3601) ? (p.semiMajorRangeOrientation) : (double)(p.semiMajorRangeOrientation) / 10.0, allocator), allocator);
    if (p.nodeCenterPoint != 0) json.AddMember("nodeCenterPoint", to_json(*(p.nodeCenterPoint), allocator), allocator);
    return json;
}




/*
*   TisTpgDRM-Management - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgDRM_Management& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("generationTime", to_json((p.generationTime), allocator), allocator);
    json.AddMember("vehicleType", to_json((p.vehicleType), allocator), allocator);
    json.AddMember("tisProfile", to_json_TisProfile((p.tisProfile), allocator), allocator);
    if (p.costumerContract != 0) json.AddMember("costumerContract", to_json(*(p.costumerContract), allocator), allocator);
    return json;
}




/*
*   TisTpgTRM-Management - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTRM_Management& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("generationTime", to_json((p.generationTime), allocator), allocator);
    json.AddMember("vehicleType", to_json((p.vehicleType), allocator), allocator);
    json.AddMember("tpgStationID", to_json((p.tpgStationID), allocator), allocator);
    json.AddMember("reservationStatus", to_json((p.reservationStatus), allocator), allocator);
    if (p.costumercontract != 0) json.AddMember("costumercontract", to_json(*(p.costumercontract), allocator), allocator);
    if (p.reservationID != 0) json.AddMember("reservationID", to_json(*(p.reservationID), allocator), allocator);
    return json;
}




/*
*   TisTpgTCM-Management - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTCM_Management& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("generationTime", to_json((p.generationTime), allocator), allocator);
    json.AddMember("tpgStationID", to_json((p.tpgStationID), allocator), allocator);
    json.AddMember("reservationStatus", to_json((p.reservationStatus), allocator), allocator);
    if (p.reservedTpg != 0) json.AddMember("reservedTpg", to_json(*(p.reservedTpg), allocator), allocator);
    if (p.costumercontract != 0) json.AddMember("costumercontract", to_json(*(p.costumercontract), allocator), allocator);
    if (p.reservationID != 0) json.AddMember("reservationID", to_json(*(p.reservationID), allocator), allocator);
    if (p.tpgAutomationLevel != 0) json.AddMember("tpgAutomationLevel", to_json_TpgAutomation(*(p.tpgAutomationLevel), allocator), allocator);
    return json;
}




/*
*   TisTpgTCM-Situation - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTCM_Situation& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("reservationTimeLimit", to_json((p.reservationTimeLimit), allocator), allocator);
    if (p.pairingID != 0) json.AddMember("pairingID", to_json(*(p.pairingID), allocator), allocator);
    if (p.cancellationCondition != 0) json.AddMember("cancellationCondition", to_json(*(p.cancellationCondition), allocator), allocator);
    return json;
}




/*
*   TisTpgVDRM-Management - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgVDRM_Management& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("generationTime", to_json((p.generationTime), allocator), allocator);
    json.AddMember("fillingStatus", to_json((p.fillingStatus), allocator), allocator);
    json.AddMember("automationLevel", to_json_TpgAutomation((p.automationLevel), allocator), allocator);
    if (p.pairingID != 0) json.AddMember("pairingID", to_json(*(p.pairingID), allocator), allocator);
    return json;
}




/*
*   TisTpgVDPM-Management - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgVDPM_Management& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("generationTime", to_json((p.generationTime), allocator), allocator);
    json.AddMember("vehicleType", to_json((p.vehicleType), allocator), allocator);
    json.AddMember("tyreTempCondition", to_json((p.tyreTempCondition), allocator), allocator);
    json.AddMember("fillingStatus", to_json((p.fillingStatus), allocator), allocator);
    if (p.tisProfile != 0) json.AddMember("tisProfile", to_json_TisProfile(*(p.tisProfile), allocator), allocator);
    if (p.pairingID != 0) json.AddMember("pairingID", to_json(*(p.pairingID), allocator), allocator);
    return json;
}




/*
*   TisTpgEOFM-Management - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgEOFM_Management& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("generationTime", to_json((p.generationTime), allocator), allocator);
    json.AddMember("fillingStatus", to_json((p.fillingStatus), allocator), allocator);
    json.AddMember("numberOfAppliedPressure", to_json((p.numberOfAppliedPressure), allocator), allocator);
    if (p.appliedTyrePressures != 0) json.AddMember("appliedTyrePressures", to_json(*(p.appliedTyrePressures), allocator), allocator);
    if (p.pairingID != 0) json.AddMember("pairingID", to_json(*(p.pairingID), allocator), allocator);
    return json;
}




/*
*   PressureVariant - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const PressureVariant& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("pressureConfiguration", to_json_PressureConfiguration((p.pressureConfiguration), allocator), allocator);
    json.AddMember("frontAxlePressure", to_json((p.frontAxlePressure), allocator), allocator);
    json.AddMember("rearAxlePressure", to_json((p.rearAxlePressure), allocator), allocator);

    return json;
}




/*
*   TyreData::TyreData__tyreSidewallInformation - Type CHOICE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TyreData::TyreData__tyreSidewallInformation& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == TyreData__tyreSidewallInformation_PR::TyreData__tyreSidewallInformation_PR_tyreSidewallInformationValue) {
        json.AddMember("tyreSidewallInformationValue", to_json_TyreSidewallInformation(p.choice.tyreSidewallInformationValue, allocator), allocator);
    } else if (p.present == TyreData__tyreSidewallInformation_PR::TyreData__tyreSidewallInformation_PR_unavailable) {
        json.AddMember("unavailable", to_json(p.choice.unavailable, allocator), allocator);
    }
    return json;
}


/*
*   TyreData::TyreData__tin - Type CHOICE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TyreData::TyreData__tin& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == TyreData__tin_PR::TyreData__tin_PR_tinValue) {
        json.AddMember("tinValue", to_json_TIN(p.choice.tinValue, allocator), allocator);
    } else if (p.present == TyreData__tin_PR::TyreData__tin_PR_unavailable) {
        json.AddMember("unavailable", to_json(p.choice.unavailable, allocator), allocator);
    }
    return json;
}


/*
*   TyreData - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TyreData& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.currentTyrePressure != 0) json.AddMember("currentTyrePressure", to_json(*(p.currentTyrePressure), allocator), allocator);
    if (p.tyreSidewallInformation != 0) json.AddMember("tyreSidewallInformation", to_json(*(p.tyreSidewallInformation), allocator), allocator);
    if (p.currentInsideAirTemperature != 0) json.AddMember("currentInsideAirTemperature", to_json(*(p.currentInsideAirTemperature), allocator), allocator);
    if (p.recommendedTyrePressure != 0) json.AddMember("recommendedTyrePressure", to_json(*(p.recommendedTyrePressure), allocator), allocator);
    if (p.tin != 0) json.AddMember("tin", to_json(*(p.tin), allocator), allocator);
    if (p.sensorState != 0) json.AddMember("sensorState", to_json(*(p.sensorState), allocator), allocator);
    return json;
}




/*
*   TpgStationData - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TpgStationData& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("tpgStationID", to_json((p.tpgStationID), allocator), allocator);
    json.AddMember("tpgAutomationLevel", to_json_TpgAutomation((p.tpgAutomationLevel), allocator), allocator);
    json.AddMember("tpgNumber", to_json((p.tpgNumber), allocator), allocator);
    json.AddMember("tpgProvider", to_json((p.tpgProvider), allocator), allocator);
    json.AddMember("tpgLocation", to_json((p.tpgLocation), allocator), allocator);
    json.AddMember("accessibility", to_json((p.accessibility), allocator), allocator);
    if (p.address != 0) json.AddMember("address", to_json(*(p.address), allocator), allocator);
    if (p.digitalMap != 0) json.AddMember("digitalMap", to_json(*(p.digitalMap), allocator), allocator);
    if (p.bookingInfo != 0) json.AddMember("bookingInfo", to_json(*(p.bookingInfo), allocator), allocator);
    if (p.availableTpgNumber != 0) json.AddMember("availableTpgNumber", to_json(*(p.availableTpgNumber), allocator), allocator);
    if (p.cancellationCondition != 0) json.AddMember("cancellationCondition", to_json(*(p.cancellationCondition), allocator), allocator);
    return json;
}




/*
*   TpgNotifContainer - Type SEQUENCE OF
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TpgNotifContainer& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const TpgStationData_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   VehicleCurrentState - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const VehicleCurrentState& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("mcmGenericCurrentState", to_json((p.mcmGenericCurrentState), allocator), allocator);
    json.AddMember("vehicleAutomationState", to_json((p.vehicleAutomationState), allocator), allocator);
    json.AddMember("speed", to_json((p.speed), allocator), allocator);
    json.AddMember("heading", to_json((p.heading), allocator), allocator);
    json.AddMember("longitudinalAcceleration", to_json((p.longitudinalAcceleration), allocator), allocator);
    json.AddMember("vehicleSize", to_json((p.vehicleSize), allocator), allocator);

    return json;
}




/*
*   IntermediatePointReference - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const IntermediatePointReference& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("referenceStartingPosition", to_json((p.referenceStartingPosition), allocator), allocator);
    json.AddMember("referenceHeading", to_json((p.referenceHeading), allocator), allocator);
    json.AddMember("lane", to_json((p.lane), allocator), allocator);
    json.AddMember("timeOfPos", to_json((p.timeOfPos), allocator), allocator);

    return json;
}




/*
*   IntermediatePointLane - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const IntermediatePointLane& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lane", to_json((p.lane), allocator), allocator);
    json.AddMember("reason", to_json((p.reason), allocator), allocator);
    json.AddMember("timeOfPos", to_json((p.timeOfPos), allocator), allocator);

    return json;
}




/*
*   Acceleration3dWithConfidence - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const Acceleration3dWithConfidence_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == Acceleration3dWithConfidence_PR_polarAcceleration) {
        json.AddMember("polarAcceleration", to_json(p.choice.polarAcceleration, allocator), allocator);
    } else if (p.present == Acceleration3dWithConfidence_PR_cartesianAcceleration) {
        json.AddMember("cartesianAcceleration", to_json(p.choice.cartesianAcceleration, allocator), allocator);
    }
    return json;
}


/*
*   GeneralizedLanePosition - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const GeneralizedLanePosition_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("lanePositionBased", to_json((p.lanePositionBased), allocator), allocator);
    json.AddMember("confidence", to_json((p.confidence), allocator), allocator);
    if (p.mapBased != 0) json.AddMember("mapBased", to_json(*(p.mapBased), allocator), allocator);
    return json;
}




/*
*   GeneralizedLanePositions - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const GeneralizedLanePositions_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const GeneralizedLanePosition_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   InterferenceManagementInfo - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_InterferenceManagementInfo_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ETSI_ITS_CDD_InterferenceManagementInfoPerChannel_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   LowerTriangularPositiveSemidefiniteMatrices - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const LowerTriangularPositiveSemidefiniteMatrices_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const LowerTriangularPositiveSemidefiniteMatrix_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   MapemConfiguration - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const MapemConfiguration_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const MapemElementReference_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   PathPredicted - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PathPredicted_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const PathPointPredicted_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   PathPredicted2 - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PathPredicted2_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("pathPredicted", to_json((p.pathPredicted), allocator), allocator);
    json.AddMember("usageIndication", to_json((p.usageIndication), allocator), allocator);
    json.AddMember("confidenceLevel", to_json((p.confidenceLevel), allocator), allocator);

    return json;
}




/*
*   PathPredictedList - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PathPredictedList_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const PathPredicted2_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   RadialShapes - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const RadialShapes_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("refPointId", to_json((p.refPointId), allocator), allocator);
    json.AddMember("xCoordinate", to_json((p.xCoordinate), allocator), allocator);
    json.AddMember("yCoordinate", to_json((p.yCoordinate), allocator), allocator);
    json.AddMember("radialShapesList", to_json((p.radialShapesList), allocator), allocator);
    if (p.zCoordinate != 0) json.AddMember("zCoordinate", to_json(*(p.zCoordinate), allocator), allocator);
    return json;
}




/*
*   RoadConfigurationSection - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const RoadConfigurationSection_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("roadSectionDefinition", to_json((p.roadSectionDefinition), allocator), allocator);
    if (p.roadType != 0) json.AddMember("roadType", to_json(*(p.roadType), allocator), allocator);
    if (p.laneConfiguration != 0) json.AddMember("laneConfiguration", to_json(*(p.laneConfiguration), allocator), allocator);
    if (p.mapemConfiguration != 0) json.AddMember("mapemConfiguration", to_json(*(p.mapemConfiguration), allocator), allocator);
    return json;
}




/*
*   RoadConfigurationSectionList - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const RoadConfigurationSectionList_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const RoadConfigurationSection_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   Shape - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const Shape_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == Shape_PR_rectangular) {
        json.AddMember("rectangular", to_json(p.choice.rectangular, allocator), allocator);
    } else if (p.present == Shape_PR_circular) {
        json.AddMember("circular", to_json(p.choice.circular, allocator), allocator);
    } else if (p.present == Shape_PR_polygonal) {
        json.AddMember("polygonal", to_json(p.choice.polygonal, allocator), allocator);
    } else if (p.present == Shape_PR_elliptical) {
        json.AddMember("elliptical", to_json(p.choice.elliptical, allocator), allocator);
    } else if (p.present == Shape_PR_radial) {
        json.AddMember("radial", to_json(p.choice.radial, allocator), allocator);
    } else if (p.present == Shape_PR_radialShapes) {
        json.AddMember("radialShapes", to_json(p.choice.radialShapes, allocator), allocator);
    }
    return json;
}


/*
*   Velocity3dWithConfidence - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const Velocity3dWithConfidence_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == Velocity3dWithConfidence_PR_polarVelocity) {
        json.AddMember("polarVelocity", to_json(p.choice.polarVelocity, allocator), allocator);
    } else if (p.present == Velocity3dWithConfidence_PR_cartesianVelocity) {
        json.AddMember("cartesianVelocity", to_json(p.choice.cartesianVelocity, allocator), allocator);
    }
    return json;
}


/*
*   VruClusterInformation - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const VruClusterInformation_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("clusterCardinalitySize", to_json((p.clusterCardinalitySize), allocator), allocator);
    if (p.clusterId != 0) json.AddMember("clusterId", to_json(*(p.clusterId), allocator), allocator);
    if (p.clusterBoundingBoxShape != 0) json.AddMember("clusterBoundingBoxShape", to_json(*(p.clusterBoundingBoxShape), allocator), allocator);
    if (p.clusterProfiles != 0) json.AddMember("clusterProfiles", to_json_VruClusterProfiles(*(p.clusterProfiles), allocator), allocator);
    return json;
}




/*
*   ConnectionManeuverAssist-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const ConnectionManeuverAssist_addGrpC& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.itsStationPosition != 0) json.AddMember("itsStationPosition", to_json(*(p.itsStationPosition), allocator), allocator);
    return json;
}




/*
*   ConnectionTrajectory-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const ConnectionTrajectory_addGrpC& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("nodes", to_json((p.nodes), allocator), allocator);
    json.AddMember("connectionID", to_json((p.connectionID), allocator), allocator);

    return json;
}




/*
*   MapData-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const MapData_addGrpC& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.signalHeadLocations != 0) json.AddMember("signalHeadLocations", to_json(*(p.signalHeadLocations), allocator), allocator);
    return json;
}




/*
*   RTCMcorrections - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RTCMcorrections& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("msgCnt", to_json((p.msgCnt), allocator), allocator);
    json.AddMember("rev", to_json((p.rev), allocator), allocator);
    json.AddMember("msgs", to_json((p.msgs), allocator), allocator);
    if (p.timeStamp != 0) json.AddMember("timeStamp", to_json(*(p.timeStamp), allocator), allocator);
    if (p.anchorPoint != 0) json.AddMember("anchorPoint", to_json(*(p.anchorPoint), allocator), allocator);
    if (p.rtcmHeader != 0) json.AddMember("rtcmHeader", to_json(*(p.rtcmHeader), allocator), allocator);
    return json;
}




/*
*   ConnectsToList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const ConnectsToList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const Connection_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   LaneAttributes - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const LaneAttributes& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("directionalUse", to_json_LaneDirection((p.directionalUse), allocator), allocator);
    json.AddMember("sharedWith", to_json_LaneSharing((p.sharedWith), allocator), allocator);
    json.AddMember("laneType", to_json((p.laneType), allocator), allocator);

    return json;
}




/*
*   MovementList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const MovementList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const MovementState_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   NodeListXY - Type CHOICE
*   From DSRC - File DSRC.asn
*/

Value to_json(const NodeListXY& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == NodeListXY_PR_nodes) {
        json.AddMember("nodes", to_json(p.choice.nodes, allocator), allocator);
    } else if (p.present == NodeListXY_PR_computed) {
        json.AddMember("computed", to_json(p.choice.computed, allocator), allocator);
    }
    return json;
}


/*
*   RequestorDescription - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RequestorDescription& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("id", to_json((p.id), allocator), allocator);
    if (p.type != 0) json.AddMember("type", to_json(*(p.type), allocator), allocator);
    if (p.position != 0) json.AddMember("position", to_json(*(p.position), allocator), allocator);
    if (p.transitStatus != 0) json.AddMember("transitStatus", to_json_TransitVehicleStatus(*(p.transitStatus), allocator), allocator);
    if (p.transitOccupancy != 0) json.AddMember("transitOccupancy", to_json(*(p.transitOccupancy), allocator), allocator);
    if (p.transitSchedule != 0) json.AddMember("transitSchedule", to_json(*(p.transitSchedule), allocator), allocator);
    return json;
}




/*
*   SignalStatusPackageList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalStatusPackageList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const SignalStatusPackage_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   DDD-IO-LIST - Type SEQUENCE OF
*   From GDD - File ISO14823.asn
*/

Value to_json(const DDD_IO_LIST& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const DDD_IO_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   CreditRq - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const CreditRq_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("refund", to_json((p.refund), allocator), allocator);
    json.AddMember("nonce", to_json((p.nonce), allocator), allocator);
    json.AddMember("key", to_json((p.key), allocator), allocator);

    return json;
}




/*
*   DebitRq - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const DebitRq_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("debitPaymentFee", to_json((p.debitPaymentFee), allocator), allocator);
    json.AddMember("nonce", to_json((p.nonce), allocator), allocator);
    json.AddMember("keyRef", to_json((p.keyRef), allocator), allocator);

    return json;
}




/*
*   GlcPart - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const GlcPart& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("zoneId", to_json((p.zoneId), allocator), allocator);
    if (p.laneNumber != 0) json.AddMember("laneNumber", to_json(*(p.laneNumber), allocator), allocator);
    if (p.zoneExtension != 0) json.AddMember("zoneExtension", to_json(*(p.zoneExtension), allocator), allocator);
    if (p.zoneHeading != 0) json.AddMember("zoneHeading", to_json((*(p.zoneHeading) != 3601) ? (double) *(p.zoneHeading) / 10.0 : *(p.zoneHeading), allocator), allocator);
    if (p.zone != 0) json.AddMember("zone", to_json(*(p.zone), allocator), allocator);
    return json;
}




/*
*   RoadSurfaceContainer - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const RoadSurfaceContainer& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const RscPart_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   LayoutContainer - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const LayoutContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("layoutId", to_json((p.layoutId), allocator), allocator);
    json.AddMember("layoutComponents", to_json((p.layoutComponents), allocator), allocator);
    if (p.height != 0) json.AddMember("height", to_json(*(p.height), allocator), allocator);
    if (p.width != 0) json.AddMember("width", to_json(*(p.width), allocator), allocator);
    return json;
}




/*
*   MlcParts - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const MlcParts& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const MlcPart_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   TrailerCharacteristicsList - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const TrailerCharacteristicsList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const IVI_TrailerCharacteristics_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   CompleteVehicleCharacteristics - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const CompleteVehicleCharacteristics& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.tractor != 0) json.AddMember("tractor", to_json(*(p.tractor), allocator), allocator);
    if (p.trailer != 0) json.AddMember("trailer", to_json(*(p.trailer), allocator), allocator);
    return json;
}




/*
*   LaneInformation - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const LaneInformation& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("laneNumber", to_json((p.laneNumber), allocator), allocator);
    json.AddMember("direction", to_json((p.direction), allocator), allocator);
    json.AddMember("laneType", to_json((p.laneType), allocator), allocator);
    json.AddMember("laneStatus", to_json((p.laneStatus), allocator), allocator);
    if (p.validity != 0) json.AddMember("validity", to_json(*(p.validity), allocator), allocator);
    if (p.laneTypeQualifier != 0) json.AddMember("laneTypeQualifier", to_json(*(p.laneTypeQualifier), allocator), allocator);
    if (p.laneWidth != 0) json.AddMember("laneWidth", to_json(*(p.laneWidth), allocator), allocator);
    if (p.ext1->detectionZoneIds != 0) json.AddMember("detectionZoneIds", to_json(*(p.ext1->detectionZoneIds), allocator), allocator);
    if (p.ext1->relevanceZoneIds != 0) json.AddMember("relevanceZoneIds", to_json(*(p.ext1->relevanceZoneIds), allocator), allocator);
    if (p.ext1->laneCharacteristics != 0) json.AddMember("laneCharacteristics", to_json(*(p.ext1->laneCharacteristics), allocator), allocator);
    if (p.ext1->laneSurfaceStaticCharacteristics != 0) json.AddMember("laneSurfaceStaticCharacteristics", to_json(*(p.ext1->laneSurfaceStaticCharacteristics), allocator), allocator);
    if (p.ext1->laneSurfaceDynamicCharacteristics != 0) json.AddMember("laneSurfaceDynamicCharacteristics", to_json(*(p.ext1->laneSurfaceDynamicCharacteristics), allocator), allocator);
    return json;
}




/*
*   CamParameters - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const CamParameters& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("basicContainer", to_json((p.basicContainer), allocator), allocator);
    json.AddMember("highFrequencyContainer", to_json((p.highFrequencyContainer), allocator), allocator);
    if (p.lowFrequencyContainer != 0) json.AddMember("lowFrequencyContainer", to_json(*(p.lowFrequencyContainer), allocator), allocator);
    if (p.specialVehicleContainer != 0) json.AddMember("specialVehicleContainer", to_json(*(p.specialVehicleContainer), allocator), allocator);
    return json;
}




/*
*   DecentralizedEnvironmentalNotificationMessage - Type SEQUENCE
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const DecentralizedEnvironmentalNotificationMessage& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("management", to_json((p.management), allocator), allocator);
    if (p.situation != 0) json.AddMember("situation", to_json(*(p.situation), allocator), allocator);
    if (p.location != 0) json.AddMember("location", to_json(*(p.location), allocator), allocator);
    if (p.alacarte != 0) json.AddMember("alacarte", to_json(*(p.alacarte), allocator), allocator);
    return json;
}




/*
*   VruHighFrequencyContainer - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VruHighFrequencyContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("heading", to_json((p.heading), allocator), allocator);
    json.AddMember("speed", to_json((p.speed), allocator), allocator);
    json.AddMember("longitudinalAcceleration", to_json((p.longitudinalAcceleration), allocator), allocator);
    if (p.curvature != 0) json.AddMember("curvature", to_json(*(p.curvature), allocator), allocator);
    if (p.curvatureCalculationMode != 0) json.AddMember("curvatureCalculationMode", to_json(*(p.curvatureCalculationMode), allocator), allocator);
    if (p.yawRate != 0) json.AddMember("yawRate", to_json(*(p.yawRate), allocator), allocator);
    if (p.lateralAcceleration != 0) json.AddMember("lateralAcceleration", to_json(*(p.lateralAcceleration), allocator), allocator);
    if (p.verticalAcceleration != 0) json.AddMember("verticalAcceleration", to_json(*(p.verticalAcceleration), allocator), allocator);
    if (p.vruLanePosition != 0) json.AddMember("vruLanePosition", to_json(*(p.vruLanePosition), allocator), allocator);
    if (p.environment != 0) json.AddMember("environment", to_json(*(p.environment), allocator), allocator);
    if (p.movementControl != 0) json.AddMember("movementControl", to_json(*(p.movementControl), allocator), allocator);
    if (p.orientation != 0) json.AddMember("orientation", to_json(*(p.orientation), allocator), allocator);
    if (p.rollAngle != 0) json.AddMember("rollAngle", to_json(*(p.rollAngle), allocator), allocator);
    if (p.deviceUsage != 0) json.AddMember("deviceUsage", to_json(*(p.deviceUsage), allocator), allocator);
    return json;
}




/*
*   TrafficIslandPosition - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_TrafficIslandPosition_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("oneSide", to_json((p.oneSide), allocator), allocator);
    json.AddMember("otherSide", to_json((p.otherSide), allocator), allocator);

    return json;
}




/*
*   VruMotionPredictionContainer - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VruMotionPredictionContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.pathHistory != 0) json.AddMember("pathHistory", to_json(*(p.pathHistory), allocator), allocator);
    if (p.pathPrediction != 0) json.AddMember("pathPrediction", to_json(*(p.pathPrediction), allocator), allocator);
    if (p.safeDistance != 0) json.AddMember("safeDistance", to_json(*(p.safeDistance), allocator), allocator);
    if (p.trajectoryInterceptionIndication != 0) json.AddMember("trajectoryInterceptionIndication", to_json(*(p.trajectoryInterceptionIndication), allocator), allocator);
    if (p.accelerationChangeIndication != 0) json.AddMember("accelerationChangeIndication", to_json(*(p.accelerationChangeIndication), allocator), allocator);
    if (p.headingChangeIndication != 0) json.AddMember("headingChangeIndication", to_json(*(p.headingChangeIndication), allocator), allocator);
    if (p.stabilityChangeIndication != 0) json.AddMember("stabilityChangeIndication", to_json(*(p.stabilityChangeIndication), allocator), allocator);
    return json;
}




/*
*   AreaCircular - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const AreaCircular& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("radius", to_json((double)(p.radius) / 10.0, allocator), allocator);
    if (p.nodeCenterPoint != 0) json.AddMember("nodeCenterPoint", to_json(*(p.nodeCenterPoint), allocator), allocator);
    return json;
}




/*
*   AreaRectangle - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const AreaRectangle& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("semiMajorRangeLength", to_json((double)(p.semiMajorRangeLength) / 10.0, allocator), allocator);
    json.AddMember("semiMinorRangeLength", to_json((double)(p.semiMinorRangeLength) / 10.0, allocator), allocator);
    json.AddMember("semiMajorRangeOrientation", to_json(((p.semiMajorRangeOrientation) == 3601) ? (p.semiMajorRangeOrientation) : (double)(p.semiMajorRangeOrientation) / 10.0, allocator), allocator);
    if (p.nodeCenterPoint != 0) json.AddMember("nodeCenterPoint", to_json(*(p.nodeCenterPoint), allocator), allocator);
    if (p.semiHeight != 0) json.AddMember("semiHeight", to_json((double) *(p.semiHeight) / 10.0, allocator), allocator);
    return json;
}




/*
*   PolyPointList - Type SEQUENCE OF
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const PolyPointList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const OffsetPoint_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   PerceptionRegion - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const PerceptionRegion& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("measurementDeltaTime", to_json((p.measurementDeltaTime), allocator), allocator);
    json.AddMember("perceptionRegionConfidence", to_json((p.perceptionRegionConfidence), allocator), allocator);
    json.AddMember("perceptionRegionShape", to_json((p.perceptionRegionShape), allocator), allocator);
    if (p.sensorIdList != 0) json.AddMember("sensorIdList", to_json(*(p.sensorIdList), allocator), allocator);
    if (p.numberOfPerceivedObjects != 0) json.AddMember("numberOfPerceivedObjects", to_json(*(p.numberOfPerceivedObjects), allocator), allocator);
    if (p.perceivedObjectIds != 0) json.AddMember("perceivedObjectIds", to_json(*(p.perceivedObjectIds), allocator), allocator);
    return json;
}




/*
*   SensorInformation - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const SensorInformation& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("sensorId", to_json((p.sensorId), allocator), allocator);
    json.AddMember("sensorType", to_json((p.sensorType), allocator), allocator);
    if (p.perceptionRegionShape != 0) json.AddMember("perceptionRegionShape", to_json(*(p.perceptionRegionShape), allocator), allocator);
    if (p.perceptionRegionConfidence != 0) json.AddMember("perceptionRegionConfidence", to_json(*(p.perceptionRegionConfidence), allocator), allocator);
    return json;
}




/*
*   ItsChargingSpotDataElements - Type SEQUENCE
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const ItsChargingSpotDataElements& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("type", to_json_ChargingSpotType((p.type), allocator), allocator);
    json.AddMember("typeOfReceptacle", to_json_TypeOfReceptacle((p.typeOfReceptacle), allocator), allocator);
    json.AddMember("energyAvailability", to_json((p.energyAvailability), allocator), allocator);
    if (p.evEquipmentID != 0) json.AddMember("evEquipmentID", to_json(*(p.evEquipmentID), allocator), allocator);
    if (p.parkingPlacesData != 0) json.AddMember("parkingPlacesData", to_json(*(p.parkingPlacesData), allocator), allocator);
    return json;
}




/*
*   EV-RSR-MessageBody - Type CHOICE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const EV_RSR_MessageBody& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == EV_RSR_MessageBody_PR_preReservationRequestMessage) {
        json.AddMember("preReservationRequestMessage", to_json(p.choice.preReservationRequestMessage, allocator), allocator);
    } else if (p.present == EV_RSR_MessageBody_PR_preReservationResponseMessage) {
        json.AddMember("preReservationResponseMessage", to_json(p.choice.preReservationResponseMessage, allocator), allocator);
    } else if (p.present == EV_RSR_MessageBody_PR_reservationRequestMessage) {
        json.AddMember("reservationRequestMessage", to_json(p.choice.reservationRequestMessage, allocator), allocator);
    } else if (p.present == EV_RSR_MessageBody_PR_reservationResponseMessage) {
        json.AddMember("reservationResponseMessage", to_json(p.choice.reservationResponseMessage, allocator), allocator);
    } else if (p.present == EV_RSR_MessageBody_PR_cancellationRequestMessage) {
        json.AddMember("cancellationRequestMessage", to_json(p.choice.cancellationRequestMessage, allocator), allocator);
    } else if (p.present == EV_RSR_MessageBody_PR_cancellationResponseMessage) {
        json.AddMember("cancellationResponseMessage", to_json(p.choice.cancellationResponseMessage, allocator), allocator);
    } else if (p.present == EV_RSR_MessageBody_PR_updateRequestMessage) {
        json.AddMember("updateRequestMessage", to_json(p.choice.updateRequestMessage, allocator), allocator);
    } else if (p.present == EV_RSR_MessageBody_PR_updateResponseMessage) {
        json.AddMember("updateResponseMessage", to_json(p.choice.updateResponseMessage, allocator), allocator);
    }
    return json;
}


/*
*   InterferenceManagementInfoPerChannel - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const IMZM_PDU_Descriptions_InterferenceManagementInfoPerChannel_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("interferenceManagementChannel", to_json((p.interferenceManagementChannel), allocator), allocator);
    json.AddMember("interferenceManagementZoneType", to_json((p.interferenceManagementZoneType), allocator), allocator);
    if (p.interferenceManagementMitigationType != 0) json.AddMember("interferenceManagementMitigationType", to_json(*(p.interferenceManagementMitigationType), allocator), allocator);
    return json;
}




/*
*   TisTpgDRM - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgDRM& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("management", to_json((p.management), allocator), allocator);
    json.AddMember("situation", to_json((p.situation), allocator), allocator);
    json.AddMember("location", to_json((p.location), allocator), allocator);

    return json;
}




/*
*   TisTpgSNM - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgSNM& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("management", to_json((p.management), allocator), allocator);
    json.AddMember("tpgContainer", to_json((p.tpgContainer), allocator), allocator);

    return json;
}




/*
*   TisTpgTRM - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTRM& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("management", to_json((p.management), allocator), allocator);
    if (p.situation != 0) json.AddMember("situation", to_json(*(p.situation), allocator), allocator);
    if (p.location != 0) json.AddMember("location", to_json(*(p.location), allocator), allocator);
    return json;
}




/*
*   TisTpgTCM - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTCM& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("management", to_json((p.management), allocator), allocator);
    if (p.situation != 0) json.AddMember("situation", to_json(*(p.situation), allocator), allocator);
    if (p.location != 0) json.AddMember("location", to_json(*(p.location), allocator), allocator);
    return json;
}




/*
*   TisTpgVDRM - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgVDRM& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("management", to_json((p.management), allocator), allocator);

    return json;
}




/*
*   VehicleSpecificData - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const VehicleSpecificData& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("currentVehicleConfiguration", to_json_PressureConfiguration((p.currentVehicleConfiguration), allocator), allocator);
    json.AddMember("frontLeftTyreData", to_json((p.frontLeftTyreData), allocator), allocator);
    json.AddMember("frontRightTyreData", to_json((p.frontRightTyreData), allocator), allocator);
    json.AddMember("rearLeftTyreData", to_json((p.rearLeftTyreData), allocator), allocator);
    json.AddMember("rearRightTyreData", to_json((p.rearRightTyreData), allocator), allocator);
    json.AddMember("spareTyreData", to_json((p.spareTyreData), allocator), allocator);

    return json;
}




/*
*   TisTpgEOFM - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgEOFM& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("management", to_json((p.management), allocator), allocator);

    return json;
}




/*
*   PressureVariantsList - Type SEQUENCE OF
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const PressureVariantsList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const PressureVariant_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   RTCMEM - Type SEQUENCE
*   From RTCMEM-PDU-Descriptions - File TS103301v211-RTCMEM.asn
*/

Value to_json(const RTCMEM& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("header", to_json((p.header), allocator), allocator);
    json.AddMember("rtcmc", to_json((p.rtcmc), allocator), allocator);

    return json;
}




/*
*   IntermediatePoint - Type CHOICE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const IntermediatePoint& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == IntermediatePoint_PR_reference) {
        json.AddMember("reference", to_json(p.choice.reference, allocator), allocator);
    } else if (p.present == IntermediatePoint_PR_lane) {
        json.AddMember("lane", to_json(p.choice.lane, allocator), allocator);
    } else if (p.present == IntermediatePoint_PR_intersection) {
        json.AddMember("intersection", to_json(p.choice.intersection, allocator), allocator);
    } else if (p.present == IntermediatePoint_PR_offroad) {
        json.AddMember("offroad", to_json(p.choice.offroad, allocator), allocator);
    }
    return json;
}


/*
*   Wgs84TrajectoryPoint - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const Wgs84TrajectoryPoint& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("intermediatePoint", to_json((p.intermediatePoint), allocator), allocator);
    json.AddMember("longitudePosition", to_json(((p.longitudePosition) == 1800000001) ? (p.longitudePosition) : (double)(p.longitudePosition) / 10000000.0, allocator), allocator);
    json.AddMember("latitudePosition", to_json(((p.latitudePosition) == 900000001) ? (p.latitudePosition) : (double)(p.latitudePosition) / 10000000.0, allocator), allocator);
    json.AddMember("speed", to_json((p.speed), allocator), allocator);
    if (p.altitudePosition != 0) json.AddMember("altitudePosition", to_json(*(p.altitudePosition), allocator), allocator);
    if (p.headings != 0) json.AddMember("headings", to_json(*(p.headings), allocator), allocator);
    return json;
}




/*
*   TrajectoryList - Type SEQUENCE OF
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const TrajectoryList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const Wgs84TrajectoryPoint_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   Submaneuver - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const Submaneuver& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("submaneuverId", to_json((p.submaneuverId), allocator), allocator);
    json.AddMember("trajectoryPoint", to_json((p.trajectoryPoint), allocator), allocator);

    return json;
}




/*
*   InterferenceManagementZoneDefinition - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const InterferenceManagementZoneDefinition_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("interferenceManagementZoneLatitude", to_json(((p.interferenceManagementZoneLatitude) == 900000001) ? (p.interferenceManagementZoneLatitude) : (double)(p.interferenceManagementZoneLatitude) / 10000000.0, allocator), allocator);
    json.AddMember("interferenceManagementZoneLongitude", to_json(((p.interferenceManagementZoneLongitude) == 1800000001) ? (p.interferenceManagementZoneLongitude) : (double)(p.interferenceManagementZoneLongitude) / 10000000.0, allocator), allocator);
    if (p.interferenceManagementZoneId != 0) json.AddMember("interferenceManagementZoneId", to_json(*(p.interferenceManagementZoneId), allocator), allocator);
    if (p.interferenceManagementZoneShape != 0) json.AddMember("interferenceManagementZoneShape", to_json(*(p.interferenceManagementZoneShape), allocator), allocator);
    return json;
}




/*
*   ObjectClass - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ObjectClass_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == ObjectClass_PR_vehicleSubClass) {
        json.AddMember("vehicleSubClass", to_json(p.choice.vehicleSubClass, allocator), allocator);
    } else if (p.present == ObjectClass_PR_vruSubClass) {
        json.AddMember("vruSubClass", to_json(p.choice.vruSubClass, allocator), allocator);
    } else if (p.present == ObjectClass_PR_groupSubClass) {
        json.AddMember("groupSubClass", to_json(p.choice.groupSubClass, allocator), allocator);
    } else if (p.present == ObjectClass_PR_otherSubClass) {
        json.AddMember("otherSubClass", to_json(p.choice.otherSubClass, allocator), allocator);
    }
    return json;
}


/*
*   ObjectClassWithConfidence - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ObjectClassWithConfidence_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("objectClass", to_json((p.objectClass), allocator), allocator);
    json.AddMember("confidence", to_json((p.confidence), allocator), allocator);

    return json;
}




/*
*   SignalRequestMessage - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalRequestMessage& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("second", to_json((p.second), allocator), allocator);
    json.AddMember("requestor", to_json((p.requestor), allocator), allocator);
    if (p.timeStamp != 0) json.AddMember("timeStamp", to_json(*(p.timeStamp), allocator), allocator);
    if (p.sequenceNumber != 0) json.AddMember("sequenceNumber", to_json(*(p.sequenceNumber), allocator), allocator);
    if (p.requests != 0) json.AddMember("requests", to_json(*(p.requests), allocator), allocator);
    return json;
}




/*
*   GenericLane - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const GenericLane& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("laneID", to_json((p.laneID), allocator), allocator);
    json.AddMember("laneAttributes", to_json((p.laneAttributes), allocator), allocator);
    json.AddMember("nodeList", to_json((p.nodeList), allocator), allocator);
    if (p.ingressApproach != 0) json.AddMember("ingressApproach", to_json(*(p.ingressApproach), allocator), allocator);
    if (p.egressApproach != 0) json.AddMember("egressApproach", to_json(*(p.egressApproach), allocator), allocator);
    if (p.maneuvers != 0) json.AddMember("maneuvers", to_json_AllowedManeuvers(*(p.maneuvers), allocator), allocator);
    if (p.connectsTo != 0) json.AddMember("connectsTo", to_json(*(p.connectsTo), allocator), allocator);
    if (p.overlays != 0) json.AddMember("overlays", to_json(*(p.overlays), allocator), allocator);
    return json;
}




/*
*   IntersectionState - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const IntersectionState& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("id", to_json((p.id), allocator), allocator);
    json.AddMember("revision", to_json((p.revision), allocator), allocator);
    json.AddMember("status", to_json_IntersectionStatusObject((p.status), allocator), allocator);
    json.AddMember("states", to_json((p.states), allocator), allocator);
    if (p.moy != 0) json.AddMember("moy", to_json(*(p.moy), allocator), allocator);
    if (p.timeStamp != 0) json.AddMember("timeStamp", to_json(*(p.timeStamp), allocator), allocator);
    if (p.enabledLanes != 0) json.AddMember("enabledLanes", to_json(*(p.enabledLanes), allocator), allocator);
    if (p.maneuverAssistList != 0) json.AddMember("maneuverAssistList", to_json(*(p.maneuverAssistList), allocator), allocator);
    return json;
}




/*
*   IntersectionStateList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const IntersectionStateList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const IntersectionState_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   LaneList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const LaneList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const GenericLane_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   RoadLaneSetList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const RoadLaneSetList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const GenericLane_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   RoadSegment - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RoadSegment& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("id", to_json((p.id), allocator), allocator);
    json.AddMember("revision", to_json((p.revision), allocator), allocator);
    json.AddMember("refPoint", to_json((p.refPoint), allocator), allocator);
    json.AddMember("roadLaneSet", to_json((p.roadLaneSet), allocator), allocator);
    if (p.laneWidth != 0) json.AddMember("laneWidth", to_json(*(p.laneWidth), allocator), allocator);
    if (p.speedLimits != 0) json.AddMember("speedLimits", to_json(*(p.speedLimits), allocator), allocator);
    return json;
}




/*
*   RoadSegmentList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const RoadSegmentList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const RoadSegment_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   SignalStatus - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalStatus& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("sequenceNumber", to_json((p.sequenceNumber), allocator), allocator);
    json.AddMember("id", to_json((p.id), allocator), allocator);
    json.AddMember("sigStatus", to_json((p.sigStatus), allocator), allocator);

    return json;
}




/*
*   SignalStatusList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalStatusList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const SignalStatus_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   InternationalSign-destinationInformation - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const InternationalSign_destinationInformation& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);

    if (p.junctionDirection != 0) json.AddMember("junctionDirection", to_json(*(p.junctionDirection), allocator), allocator);
    if (p.roundaboutCwDirection != 0) json.AddMember("roundaboutCwDirection", to_json(*(p.roundaboutCwDirection), allocator), allocator);
    if (p.roundaboutCcwDirection != 0) json.AddMember("roundaboutCcwDirection", to_json(*(p.roundaboutCcwDirection), allocator), allocator);
    if (p.ioList != 0) json.AddMember("ioList", to_json(*(p.ioList), allocator), allocator);
    return json;
}




/*
*   GlcParts - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const GlcParts& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const GlcPart_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   MapLocationContainer - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const MapLocationContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("reference", to_json((p.reference), allocator), allocator);
    json.AddMember("parts", to_json((p.parts), allocator), allocator);

    return json;
}




/*
*   LaneConfiguration - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const LaneConfiguration& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const LaneInformation_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   VehicleCharacteristicsList - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const VehicleCharacteristicsList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const CompleteVehicleCharacteristics_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   ISO14823Attribute - Type CHOICE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ISO14823Attribute& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == ISO14823Attribute_PR_dtm) {
        json.AddMember("dtm", to_json(p.choice.dtm, allocator), allocator);
    } else if (p.present == ISO14823Attribute_PR_edt) {
        json.AddMember("edt", to_json(p.choice.edt, allocator), allocator);
    } else if (p.present == ISO14823Attribute_PR_dfl) {
        json.AddMember("dfl", to_json(p.choice.dfl, allocator), allocator);
    } else if (p.present == ISO14823Attribute_PR_ved) {
        json.AddMember("ved", to_json(p.choice.ved, allocator), allocator);
    } else if (p.present == ISO14823Attribute_PR_spe) {
        json.AddMember("spe", to_json(p.choice.spe, allocator), allocator);
    } else if (p.present == ISO14823Attribute_PR_roi) {
        json.AddMember("roi", to_json(p.choice.roi, allocator), allocator);
    } else if (p.present == ISO14823Attribute_PR_dbv) {
        json.AddMember("dbv", to_json(p.choice.dbv, allocator), allocator);
    } else if (p.present == ISO14823Attribute_PR_ddd) {
        json.AddMember("ddd", to_json(p.choice.ddd, allocator), allocator);
    }
    return json;
}


/*
*   CoopAwareness - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const CoopAwareness& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("generationDeltaTime", to_json((p.generationDeltaTime), allocator), allocator);
    json.AddMember("camParameters", to_json((p.camParameters), allocator), allocator);

    return json;
}




/*
*   DENM - Type SEQUENCE
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const DENM& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("header", to_json((p.header), allocator), allocator);
    json.AddMember("denm", to_json((p.denm), allocator), allocator);

    return json;
}




/*
*   AreaPolygon - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const AreaPolygon& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("polyPointList", to_json((p.polyPointList), allocator), allocator);

    return json;
}




/*
*   PerceptionRegionContainer - Type SEQUENCE OF
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const PerceptionRegionContainer& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const PerceptionRegion_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   SensorInformationContainer - Type SEQUENCE OF
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const SensorInformationContainer& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const SensorInformation_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   SREM - Type SEQUENCE
*   From SREM-PDU-Descriptions - File TS103301v211-SREM.asn
*/

Value to_json(const SREM& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("header", to_json((p.header), allocator), allocator);
    json.AddMember("srm", to_json((p.srm), allocator), allocator);

    return json;
}




/*
*   ItsChargingSpots - Type SEQUENCE OF
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const ItsChargingSpots& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ItsChargingSpotDataElements_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   EV-RSR - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const EV_RSR& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("header", to_json((p.header), allocator), allocator);
    json.AddMember("messageBody", to_json((p.messageBody), allocator), allocator);

    return json;
}




/*
*   InterferenceManagementZoneShape - Type CHOICE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const InterferenceManagementZoneShape& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == InterferenceManagementZoneShape_PR_rectangle) {
        json.AddMember("rectangle", to_json(p.choice.rectangle, allocator), allocator);
    } else if (p.present == InterferenceManagementZoneShape_PR_circle) {
        json.AddMember("circle", to_json(p.choice.circle, allocator), allocator);
    } else if (p.present == InterferenceManagementZoneShape_PR_polygon) {
        json.AddMember("polygon", to_json(p.choice.polygon, allocator), allocator);
    } else if (p.present == InterferenceManagementZoneShape_PR_ellipse) {
        json.AddMember("ellipse", to_json(p.choice.ellipse, allocator), allocator);
    }
    return json;
}


/*
*   TyreSetVariant - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TyreSetVariant& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("variantID", to_json((p.variantID), allocator), allocator);
    json.AddMember("pressureVariantsList", to_json((p.pressureVariantsList), allocator), allocator);
    if (p.frontAxleDimension != 0) json.AddMember("frontAxleDimension", to_json_TyreSidewallInformation(*(p.frontAxleDimension), allocator), allocator);
    if (p.rearAxleDimension != 0) json.AddMember("rearAxleDimension", to_json_TyreSidewallInformation(*(p.rearAxleDimension), allocator), allocator);
    return json;
}




/*
*   Wgs84Trajectory - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const Wgs84Trajectory& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("trajectoryPoints", to_json((p.trajectoryPoints), allocator), allocator);

    return json;
}




/*
*   Submaneuvers - Type SEQUENCE OF
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const Submaneuvers& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const Submaneuver_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   InterferenceManagementZone - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_InterferenceManagementZone_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("zoneDefinition", to_json((p.zoneDefinition), allocator), allocator);
    json.AddMember("managementInfo", to_json((p.managementInfo), allocator), allocator);

    return json;
}




/*
*   InterferenceManagementZones - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_InterferenceManagementZones_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ETSI_ITS_CDD_InterferenceManagementZone_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   ObjectClassDescription - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ObjectClassDescription_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ObjectClassWithConfidence_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   PerceivedObject - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PerceivedObject_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("measurementDeltaTime", to_json((p.measurementDeltaTime), allocator), allocator);
    json.AddMember("position", to_json((p.position), allocator), allocator);
    if (p.objectId != 0) json.AddMember("objectId", to_json(*(p.objectId), allocator), allocator);
    if (p.velocity != 0) json.AddMember("velocity", to_json(*(p.velocity), allocator), allocator);
    if (p.acceleration != 0) json.AddMember("acceleration", to_json(*(p.acceleration), allocator), allocator);
    if (p.angles != 0) json.AddMember("angles", to_json(*(p.angles), allocator), allocator);
    if (p.zAngularVelocity != 0) json.AddMember("zAngularVelocity", to_json(*(p.zAngularVelocity), allocator), allocator);
    if (p.lowerTriangularCorrelationMatrices != 0) json.AddMember("lowerTriangularCorrelationMatrices", to_json(*(p.lowerTriangularCorrelationMatrices), allocator), allocator);
    if (p.objectDimensionZ != 0) json.AddMember("objectDimensionZ", to_json(*(p.objectDimensionZ), allocator), allocator);
    if (p.objectDimensionY != 0) json.AddMember("objectDimensionY", to_json(*(p.objectDimensionY), allocator), allocator);
    if (p.objectDimensionX != 0) json.AddMember("objectDimensionX", to_json(*(p.objectDimensionX), allocator), allocator);
    if (p.objectAge != 0) json.AddMember("objectAge", to_json(*(p.objectAge), allocator), allocator);
    if (p.objectPerceptionQuality != 0) json.AddMember("objectPerceptionQuality", to_json(*(p.objectPerceptionQuality), allocator), allocator);
    if (p.sensorIdList != 0) json.AddMember("sensorIdList", to_json(*(p.sensorIdList), allocator), allocator);
    if (p.classification != 0) json.AddMember("classification", to_json(*(p.classification), allocator), allocator);
    if (p.mapPosition != 0) json.AddMember("mapPosition", to_json(*(p.mapPosition), allocator), allocator);
    return json;
}




/*
*   SPAT - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SPAT& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("intersections", to_json((p.intersections), allocator), allocator);
    if (p.timeStamp != 0) json.AddMember("timeStamp", to_json(*(p.timeStamp), allocator), allocator);
    return json;
}




/*
*   SignalStatusMessage - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalStatusMessage& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("second", to_json((p.second), allocator), allocator);
    json.AddMember("status", to_json((p.status), allocator), allocator);
    if (p.timeStamp != 0) json.AddMember("timeStamp", to_json(*(p.timeStamp), allocator), allocator);
    if (p.sequenceNumber != 0) json.AddMember("sequenceNumber", to_json(*(p.sequenceNumber), allocator), allocator);
    return json;
}




/*
*   IntersectionGeometry - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const IntersectionGeometry& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("id", to_json((p.id), allocator), allocator);
    json.AddMember("revision", to_json((p.revision), allocator), allocator);
    json.AddMember("refPoint", to_json((p.refPoint), allocator), allocator);
    json.AddMember("laneSet", to_json((p.laneSet), allocator), allocator);
    if (p.laneWidth != 0) json.AddMember("laneWidth", to_json(*(p.laneWidth), allocator), allocator);
    if (p.speedLimits != 0) json.AddMember("speedLimits", to_json(*(p.speedLimits), allocator), allocator);
    return json;
}




/*
*   IntersectionGeometryList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const IntersectionGeometryList& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const IntersectionGeometry_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   GddAttribute - Type CHOICE
*   From GDD - File ISO14823.asn
*/

Value to_json(const GddAttribute& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == GddAttribute_PR_dtm) {
        json.AddMember("dtm", to_json(p.choice.dtm, allocator), allocator);
    } else if (p.present == GddAttribute_PR_edt) {
        json.AddMember("edt", to_json(p.choice.edt, allocator), allocator);
    } else if (p.present == GddAttribute_PR_dfl) {
        json.AddMember("dfl", to_json(p.choice.dfl, allocator), allocator);
    } else if (p.present == GddAttribute_PR_ved) {
        json.AddMember("ved", to_json(p.choice.ved, allocator), allocator);
    } else if (p.present == GddAttribute_PR_spe) {
        json.AddMember("spe", to_json(p.choice.spe, allocator), allocator);
    } else if (p.present == GddAttribute_PR_roi) {
        json.AddMember("roi", to_json(p.choice.roi, allocator), allocator);
    } else if (p.present == GddAttribute_PR_dbv) {
        json.AddMember("dbv", to_json(p.choice.dbv, allocator), allocator);
    } else if (p.present == GddAttribute_PR_ddd) {
        json.AddMember("ddd", to_json(p.choice.ddd, allocator), allocator);
    } else if (p.present == GddAttribute_PR_set) {
        json.AddMember("set", to_json(p.choice.set, allocator), allocator);
    } else if (p.present == GddAttribute_PR_nol) {
        json.AddMember("nol", to_json(p.choice.nol, allocator), allocator);
    }
    return json;
}


/*
*   GeographicLocationContainer - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const GeographicLocationContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("referencePosition", to_json((p.referencePosition), allocator), allocator);
    json.AddMember("parts", to_json((p.parts), allocator), allocator);
    if (p.referencePositionTime != 0) json.AddMember("referencePositionTime", to_json(*(p.referencePositionTime), allocator), allocator);
    if (p.referencePositionHeading != 0) json.AddMember("referencePositionHeading", to_json(*(p.referencePositionHeading), allocator), allocator);
    if (p.referencePositionSpeed != 0) json.AddMember("referencePositionSpeed", to_json(*(p.referencePositionSpeed), allocator), allocator);
    return json;
}




/*
*   RccPart - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const RccPart& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("relevanceZoneIds", to_json((p.relevanceZoneIds), allocator), allocator);
    json.AddMember("roadType", to_json((p.roadType), allocator), allocator);
    json.AddMember("laneConfiguration", to_json((p.laneConfiguration), allocator), allocator);

    return json;
}




/*
*   TcPart - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const TcPart& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("relevanceZoneIds", to_json((p.relevanceZoneIds), allocator), allocator);
    json.AddMember("data", to_json((p.data), allocator), allocator);
    json.AddMember("iviType", to_json((p.ext1->iviType), allocator), allocator);
    if (p.detectionZoneIds != 0) json.AddMember("detectionZoneIds", to_json(*(p.detectionZoneIds), allocator), allocator);
    if (p.direction != 0) json.AddMember("direction", to_json(*(p.direction), allocator), allocator);
    if (p.driverAwarenessZoneIds != 0) json.AddMember("driverAwarenessZoneIds", to_json(*(p.driverAwarenessZoneIds), allocator), allocator);
    if (p.minimumAwarenessTime != 0) json.AddMember("minimumAwarenessTime", to_json(*(p.minimumAwarenessTime), allocator), allocator);
    if (p.applicableLanes != 0) json.AddMember("applicableLanes", to_json(*(p.applicableLanes), allocator), allocator);
    if (p.layoutId != 0) json.AddMember("layoutId", to_json(*(p.layoutId), allocator), allocator);
    if (p.preStoredlayoutId != 0) json.AddMember("preStoredlayoutId", to_json(*(p.preStoredlayoutId), allocator), allocator);
    if (p.text != 0) json.AddMember("text", to_json(*(p.text), allocator), allocator);
    if (p.ext1->laneStatus != 0) json.AddMember("laneStatus", to_json(*(p.ext1->laneStatus), allocator), allocator);
    if (p.ext1->vehicleCharacteristics != 0) json.AddMember("vehicleCharacteristics", to_json(*(p.ext1->vehicleCharacteristics), allocator), allocator);
    return json;
}




/*
*   ISO14823Attributes - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ISO14823Attributes& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ISO14823Attribute_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   AnyCatalogue - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const AnyCatalogue& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("owner", to_json((p.owner), allocator), allocator);
    json.AddMember("version", to_json((p.version), allocator), allocator);
    json.AddMember("pictogramCode", to_json((p.pictogramCode), allocator), allocator);
    if (p.value != 0) json.AddMember("value", to_json(*(p.value), allocator), allocator);
    if (p.unit != 0) json.AddMember("unit", to_json(*(p.unit), allocator), allocator);
    if (p.attributes != 0) json.AddMember("attributes", to_json(*(p.attributes), allocator), allocator);
    return json;
}




/*
*   ISO14823Code - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ISO14823Code& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("pictogramCode", to_json((p.pictogramCode), allocator), allocator);
    if (p.attributes != 0) json.AddMember("attributes", to_json(*(p.attributes), allocator), allocator);
    return json;
}




/*
*   RSCode::RSCode__code - Type CHOICE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const RSCode::RSCode__code& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == RSCode__code_PR::RSCode__code_PR_viennaConvention) {
        json.AddMember("viennaConvention", to_json(p.choice.viennaConvention, allocator), allocator);
    } else if (p.present == RSCode__code_PR::RSCode__code_PR_iso14823) {
        json.AddMember("iso14823", to_json(p.choice.iso14823, allocator), allocator);
    } else if (p.present == RSCode__code_PR::RSCode__code_PR_itisCodes) {
        json.AddMember("itisCodes", to_json(p.choice.itisCodes, allocator), allocator);
    } else if (p.present == RSCode__code_PR::RSCode__code_PR_anyCatalogue) {
        json.AddMember("anyCatalogue", to_json(p.choice.anyCatalogue, allocator), allocator);
    }
    return json;
}


/*
*   RSCode - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const RSCode& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("code", to_json((p.code), allocator), allocator);
    if (p.layoutComponentId != 0) json.AddMember("layoutComponentId", to_json(*(p.layoutComponentId), allocator), allocator);
    return json;
}




/*
*   CAM - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const CAM& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("header", to_json((p.header), allocator), allocator);
    json.AddMember("cam", to_json((p.cam), allocator), allocator);

    return json;
}




/*
*   ClusterBoundingBoxShape - Type CHOICE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const ClusterBoundingBoxShape& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == ClusterBoundingBoxShape_PR_clusterRectangle) {
        json.AddMember("clusterRectangle", to_json(p.choice.clusterRectangle, allocator), allocator);
    } else if (p.present == ClusterBoundingBoxShape_PR_clusterCircle) {
        json.AddMember("clusterCircle", to_json(p.choice.clusterCircle, allocator), allocator);
    } else if (p.present == ClusterBoundingBoxShape_PR_clusterPolygon) {
        json.AddMember("clusterPolygon", to_json(p.choice.clusterPolygon, allocator), allocator);
    }
    return json;
}


/*
*   PerceivedObjects - Type SEQUENCE OF
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const PerceivedObjects& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const PerceivedObject_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   SPATEM - Type SEQUENCE
*   From SPATEM-PDU-Descriptions - File TS103301v211-SPATEM.asn
*/

Value to_json(const SPATEM& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("header", to_json((p.header), allocator), allocator);
    json.AddMember("spat", to_json((p.spat), allocator), allocator);

    return json;
}




/*
*   SSEM - Type SEQUENCE
*   From SSEM-PDU-Descriptions - File TS103301v211-SSEM.asn
*/

Value to_json(const SSEM& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("header", to_json((p.header), allocator), allocator);
    json.AddMember("ssm", to_json((p.ssm), allocator), allocator);

    return json;
}




/*
*   ItsChargingStationData - Type SEQUENCE
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const ItsChargingStationData& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("chargingStationID", to_json((p.chargingStationID), allocator), allocator);
    json.AddMember("chargingStationLocation", to_json((p.chargingStationLocation), allocator), allocator);
    json.AddMember("accessibility", to_json((p.accessibility), allocator), allocator);
    json.AddMember("openingDaysHours", to_json((p.openingDaysHours), allocator), allocator);
    json.AddMember("pricing", to_json((p.pricing), allocator), allocator);
    json.AddMember("chargingSpotsAvailable", to_json((p.chargingSpotsAvailable), allocator), allocator);
    if (p.utilityDistributorId != 0) json.AddMember("utilityDistributorId", to_json(*(p.utilityDistributorId), allocator), allocator);
    if (p.providerID != 0) json.AddMember("providerID", to_json(*(p.providerID), allocator), allocator);
    if (p.address != 0) json.AddMember("address", to_json(*(p.address), allocator), allocator);
    if (p.phoneNumber != 0) json.AddMember("phoneNumber", to_json(*(p.phoneNumber), allocator), allocator);
    if (p.digitalMap != 0) json.AddMember("digitalMap", to_json(*(p.digitalMap), allocator), allocator);
    if (p.bookingContactInfo != 0) json.AddMember("bookingContactInfo", to_json(*(p.bookingContactInfo), allocator), allocator);
    if (p.payment != 0) json.AddMember("payment", to_json(*(p.payment), allocator), allocator);
    return json;
}




/*
*   ImzmContainer - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const ImzmContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("interferenceManagementZones", to_json((p.interferenceManagementZones), allocator), allocator);

    return json;
}




/*
*   InterferenceManagementZones - Type SEQUENCE OF
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const IMZM_PDU_Descriptions_InterferenceManagementZones_t& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const IMZM_PDU_Descriptions_InterferenceManagementZone_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   ZoneDefinition - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const ZoneDefinition& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("interferenceManagementZoneLatitude", to_json(((p.interferenceManagementZoneLatitude) == 900000001) ? (p.interferenceManagementZoneLatitude) : (double)(p.interferenceManagementZoneLatitude) / 10000000.0, allocator), allocator);
    json.AddMember("interferenceManagementZoneLongitude", to_json(((p.interferenceManagementZoneLongitude) == 1800000001) ? (p.interferenceManagementZoneLongitude) : (double)(p.interferenceManagementZoneLongitude) / 10000000.0, allocator), allocator);
    if (p.interferenceManagementZoneRadius != 0) json.AddMember("interferenceManagementZoneRadius", to_json(*(p.interferenceManagementZoneRadius), allocator), allocator);
    if (p.interferenceManagementZoneID != 0) json.AddMember("interferenceManagementZoneID", to_json(*(p.interferenceManagementZoneID), allocator), allocator);
    if (p.interferenceManagementZoneShape != 0) json.AddMember("interferenceManagementZoneShape", to_json(*(p.interferenceManagementZoneShape), allocator), allocator);
    return json;
}




/*
*   PlacardTable - Type SEQUENCE OF
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const PlacardTable& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const TyreSetVariant_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   VehiclemaneuverContainer::VehiclemaneuverContainer__vehicleTrajectory - Type CHOICE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const VehiclemaneuverContainer::VehiclemaneuverContainer__vehicleTrajectory& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == VehiclemaneuverContainer__vehicleTrajectory_PR::VehiclemaneuverContainer__vehicleTrajectory_PR_wgs84Trajectory) {
        json.AddMember("wgs84Trajectory", to_json(p.choice.wgs84Trajectory, allocator), allocator);
    }
    return json;
}


/*
*   Maneuver - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const Maneuver& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("itsId", to_json((p.itsId), allocator), allocator);
    json.AddMember("currentStateAdvisedChange", to_json((p.currentStateAdvisedChange), allocator), allocator);
    json.AddMember("numberOfSubmaneuvers", to_json((p.numberOfSubmaneuvers), allocator), allocator);
    json.AddMember("submaneuver", to_json((p.submaneuver), allocator), allocator);

    return json;
}




/*
*   MapData - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const MapData_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("msgIssueRevision", to_json((p.msgIssueRevision), allocator), allocator);
    if (p.timeStamp != 0) json.AddMember("timeStamp", to_json(*(p.timeStamp), allocator), allocator);
    if (p.layerType != 0) json.AddMember("layerType", to_json(*(p.layerType), allocator), allocator);
    if (p.layerID != 0) json.AddMember("layerID", to_json(*(p.layerID), allocator), allocator);
    if (p.intersections != 0) json.AddMember("intersections", to_json(*(p.intersections), allocator), allocator);
    if (p.roadSegments != 0) json.AddMember("roadSegments", to_json(*(p.roadSegments), allocator), allocator);
    if (p.dataParameters != 0) json.AddMember("dataParameters", to_json(*(p.dataParameters), allocator), allocator);
    if (p.restrictionList != 0) json.AddMember("restrictionList", to_json(*(p.restrictionList), allocator), allocator);
    return json;
}




/*
*   GddAttributes - Type SEQUENCE OF
*   From GDD - File ISO14823.asn
*/

Value to_json(const GddAttributes& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const GddAttribute_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   RoadConfigurationContainer - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const RoadConfigurationContainer& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const RccPart_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   TextContainer - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const TextContainer& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const TcPart_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   RoadSignCodes - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const RoadSignCodes& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const RSCode_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   AutomatedVehicleRule - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const AutomatedVehicleRule& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("priority", to_json((p.priority), allocator), allocator);
    json.AddMember("allowedSaeAutomationLevels", to_json((p.allowedSaeAutomationLevels), allocator), allocator);
    if (p.minGapBetweenVehicles != 0) json.AddMember("minGapBetweenVehicles", to_json(*(p.minGapBetweenVehicles), allocator), allocator);
    if (p.recGapBetweenVehicles != 0) json.AddMember("recGapBetweenVehicles", to_json(*(p.recGapBetweenVehicles), allocator), allocator);
    if (p.automatedVehicleMaxSpeedLimit != 0) json.AddMember("automatedVehicleMaxSpeedLimit", to_json((*(p.automatedVehicleMaxSpeedLimit) != 16383) ? (double) *(p.automatedVehicleMaxSpeedLimit) / 100.0 : *(p.automatedVehicleMaxSpeedLimit), allocator), allocator);
    if (p.automatedVehicleMinSpeedLimit != 0) json.AddMember("automatedVehicleMinSpeedLimit", to_json((*(p.automatedVehicleMinSpeedLimit) != 16383) ? (double) *(p.automatedVehicleMinSpeedLimit) / 100.0 : *(p.automatedVehicleMinSpeedLimit), allocator), allocator);
    if (p.automatedVehicleSpeedRecommendation != 0) json.AddMember("automatedVehicleSpeedRecommendation", to_json((*(p.automatedVehicleSpeedRecommendation) != 16383) ? (double) *(p.automatedVehicleSpeedRecommendation) / 100.0 : *(p.automatedVehicleSpeedRecommendation), allocator), allocator);
    if (p.roadSignCodes != 0) json.AddMember("roadSignCodes", to_json(*(p.roadSignCodes), allocator), allocator);
    if (p.extraText != 0) json.AddMember("extraText", to_json(*(p.extraText), allocator), allocator);
    return json;
}




/*
*   PlatooningRule - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const PlatooningRule& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("priority", to_json((p.priority), allocator), allocator);
    json.AddMember("allowedSaeAutomationLevels", to_json((p.allowedSaeAutomationLevels), allocator), allocator);
    if (p.maxNoOfVehicles != 0) json.AddMember("maxNoOfVehicles", to_json(*(p.maxNoOfVehicles), allocator), allocator);
    if (p.maxLenghtOfPlatoon != 0) json.AddMember("maxLenghtOfPlatoon", to_json(*(p.maxLenghtOfPlatoon), allocator), allocator);
    if (p.minGapBetweenVehicles != 0) json.AddMember("minGapBetweenVehicles", to_json(*(p.minGapBetweenVehicles), allocator), allocator);
    if (p.platoonMaxSpeedLimit != 0) json.AddMember("platoonMaxSpeedLimit", to_json((*(p.platoonMaxSpeedLimit) != 16383) ? (double) *(p.platoonMaxSpeedLimit) / 100.0 : *(p.platoonMaxSpeedLimit), allocator), allocator);
    if (p.platoonMinSpeedLimit != 0) json.AddMember("platoonMinSpeedLimit", to_json((*(p.platoonMinSpeedLimit) != 16383) ? (double) *(p.platoonMinSpeedLimit) / 100.0 : *(p.platoonMinSpeedLimit), allocator), allocator);
    if (p.platoonSpeedRecommendation != 0) json.AddMember("platoonSpeedRecommendation", to_json((*(p.platoonSpeedRecommendation) != 16383) ? (double) *(p.platoonSpeedRecommendation) / 100.0 : *(p.platoonSpeedRecommendation), allocator), allocator);
    if (p.roadSignCodes != 0) json.AddMember("roadSignCodes", to_json(*(p.roadSignCodes), allocator), allocator);
    if (p.extraText != 0) json.AddMember("extraText", to_json(*(p.extraText), allocator), allocator);
    return json;
}




/*
*   VruClusterInformationContainer - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VruClusterInformationContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("clusterId", to_json((p.clusterId), allocator), allocator);
    json.AddMember("clusterBoundingBoxShape", to_json((p.clusterBoundingBoxShape), allocator), allocator);
    json.AddMember("clusterCardinalitySize", to_json((p.clusterCardinalitySize), allocator), allocator);
    json.AddMember("clusterProfiles", to_json_ClusterProfiles((p.clusterProfiles), allocator), allocator);

    return json;
}




/*
*   PerceivedObjectContainer - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const PerceivedObjectContainer_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("numberOfPerceivedObjects", to_json((p.numberOfPerceivedObjects), allocator), allocator);
    json.AddMember("perceivedObjects", to_json((p.perceivedObjects), allocator), allocator);

    return json;
}




/*
*   MAPEM - Type SEQUENCE
*   From MAPEM-PDU-Descriptions - File TS103301v211-MAPEM.asn
*/

Value to_json(const MAPEM& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("header", to_json((p.header), allocator), allocator);
    json.AddMember("map", to_json((p.map), allocator), allocator);

    return json;
}




/*
*   ItsEVCSNData::ItsEVCSNData__chargingStationsData - Type SEQUENCE OF
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const ItsEVCSNData::ItsEVCSNData__chargingStationsData& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const ItsChargingStationData_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   ItsEVCSNData - Type SEQUENCE
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const ItsEVCSNData& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("totalNumberOfStations", to_json((p.totalNumberOfStations), allocator), allocator);
    json.AddMember("chargingStationsData", to_json((p.chargingStationsData), allocator), allocator);

    return json;
}




/*
*   ImzmParameters - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const ImzmParameters& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("basicContainer", to_json((p.basicContainer), allocator), allocator);
    json.AddMember("imzmContainer", to_json((p.imzmContainer), allocator), allocator);

    return json;
}




/*
*   InterferenceManagementZone - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const IMZM_PDU_Descriptions_InterferenceManagementZone_t& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("zoneDefinition", to_json((p.zoneDefinition), allocator), allocator);
    json.AddMember("interferenceManagementInfo", to_json((p.interferenceManagementInfo), allocator), allocator);

    return json;
}




/*
*   TisTpgVDPM - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgVDPM& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("management", to_json((p.management), allocator), allocator);
    json.AddMember("placardTable", to_json((p.placardTable), allocator), allocator);
    if (p.vehicleSpecificData != 0) json.AddMember("vehicleSpecificData", to_json(*(p.vehicleSpecificData), allocator), allocator);
    return json;
}




/*
*   Maneuvers - Type SEQUENCE OF
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const Maneuvers& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const Maneuver_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   GddStructure - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const GddStructure& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("pictogramCode", to_json((p.pictogramCode), allocator), allocator);
    if (p.attributes != 0) json.AddMember("attributes", to_json(*(p.attributes), allocator), allocator);
    return json;
}




/*
*   GicPart - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const GicPart& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("iviType", to_json((p.iviType), allocator), allocator);
    json.AddMember("roadSignCodes", to_json((p.roadSignCodes), allocator), allocator);
    if (p.detectionZoneIds != 0) json.AddMember("detectionZoneIds", to_json(*(p.detectionZoneIds), allocator), allocator);
    if (p.its_Rrid != 0) json.AddMember("its-Rrid", to_json(*(p.its_Rrid), allocator), allocator);
    if (p.relevanceZoneIds != 0) json.AddMember("relevanceZoneIds", to_json(*(p.relevanceZoneIds), allocator), allocator);
    if (p.direction != 0) json.AddMember("direction", to_json(*(p.direction), allocator), allocator);
    if (p.driverAwarenessZoneIds != 0) json.AddMember("driverAwarenessZoneIds", to_json(*(p.driverAwarenessZoneIds), allocator), allocator);
    if (p.minimumAwarenessTime != 0) json.AddMember("minimumAwarenessTime", to_json(*(p.minimumAwarenessTime), allocator), allocator);
    if (p.applicableLanes != 0) json.AddMember("applicableLanes", to_json(*(p.applicableLanes), allocator), allocator);
    if (p.iviPurpose != 0) json.AddMember("iviPurpose", to_json(*(p.iviPurpose), allocator), allocator);
    if (p.laneStatus != 0) json.AddMember("laneStatus", to_json(*(p.laneStatus), allocator), allocator);
    if (p.vehicleCharacteristics != 0) json.AddMember("vehicleCharacteristics", to_json(*(p.vehicleCharacteristics), allocator), allocator);
    if (p.driverCharacteristics != 0) json.AddMember("driverCharacteristics", to_json(*(p.driverCharacteristics), allocator), allocator);
    if (p.layoutId != 0) json.AddMember("layoutId", to_json(*(p.layoutId), allocator), allocator);
    if (p.preStoredlayoutId != 0) json.AddMember("preStoredlayoutId", to_json(*(p.preStoredlayoutId), allocator), allocator);
    if (p.extraText != 0) json.AddMember("extraText", to_json(*(p.extraText), allocator), allocator);
    return json;
}




/*
*   AutomatedVehicleRules - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const AutomatedVehicleRules& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const AutomatedVehicleRule_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   PlatooningRules - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const PlatooningRules& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const PlatooningRule_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   VamParameters - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VamParameters& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("basicContainer", to_json((p.basicContainer), allocator), allocator);
    if (p.vruHighFrequencyContainer != 0) json.AddMember("vruHighFrequencyContainer", to_json(*(p.vruHighFrequencyContainer), allocator), allocator);
    if (p.vruLowFrequencyContainer != 0) json.AddMember("vruLowFrequencyContainer", to_json(*(p.vruLowFrequencyContainer), allocator), allocator);
    if (p.vruClusterInformationContainer != 0) json.AddMember("vruClusterInformationContainer", to_json(*(p.vruClusterInformationContainer), allocator), allocator);
    if (p.vruClusterOperationContainer != 0) json.AddMember("vruClusterOperationContainer", to_json(*(p.vruClusterOperationContainer), allocator), allocator);
    if (p.vruMotionPredictionContainer != 0) json.AddMember("vruMotionPredictionContainer", to_json(*(p.vruMotionPredictionContainer), allocator), allocator);
    return json;
}




/*
*   WrappedCpmContainer::WrappedCpmContainer__containerData - Type CHOICE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const WrappedCpmContainer::WrappedCpmContainer__containerData& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == WrappedCpmContainer__containerData_PR::WrappedCpmContainer__containerData_PR_OriginatingVehicleContainer) {
        json = to_json(p.choice.OriginatingVehicleContainer, allocator);
    } else if (p.present == WrappedCpmContainer__containerData_PR::WrappedCpmContainer__containerData_PR_OriginatingRsuContainer) {
        json = to_json(p.choice.OriginatingRsuContainer, allocator);
    } else if (p.present == WrappedCpmContainer__containerData_PR::WrappedCpmContainer__containerData_PR_SensorInformationContainer) {
        json = to_json(p.choice.SensorInformationContainer, allocator);
    } else if (p.present == WrappedCpmContainer__containerData_PR::WrappedCpmContainer__containerData_PR_PerceptionRegionContainer) {
        json = to_json(p.choice.PerceptionRegionContainer, allocator);
    } else if (p.present == WrappedCpmContainer__containerData_PR::WrappedCpmContainer__containerData_PR_PerceivedObjectContainer) {
        json = to_json(p.choice.PerceivedObjectContainer, allocator);
    }
    return json;
}


/*
*   WrappedCpmContainer - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const WrappedCpmContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("containerId", to_json((p.containerId), allocator), allocator);
    json.AddMember("containerData", to_json((p.containerData), allocator), allocator);

    return json;
}




/*
*   WrappedCpmContainers - Type SEQUENCE OF
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const WrappedCpmContainers& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const WrappedCpmContainer_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   EVChargingSpotNotificationPOIMessage - Type SEQUENCE
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const EVChargingSpotNotificationPOIMessage& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("poiHeader", to_json((p.poiHeader), allocator), allocator);
    json.AddMember("evcsnData", to_json((p.evcsnData), allocator), allocator);

    return json;
}




/*
*   InterferenceManagementZoneMessage - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const InterferenceManagementZoneMessage& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("generationDeltaTime", to_json((p.generationDeltaTime), allocator), allocator);
    json.AddMember("imzmParameters", to_json((p.imzmParameters), allocator), allocator);

    return json;
}




/*
*   TisTpgTransaction - Type CHOICE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTransaction& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == TisTpgTransaction_PR_drm) {
        json.AddMember("drm", to_json(p.choice.drm, allocator), allocator);
    } else if (p.present == TisTpgTransaction_PR_snm) {
        json.AddMember("snm", to_json(p.choice.snm, allocator), allocator);
    } else if (p.present == TisTpgTransaction_PR_trm) {
        json.AddMember("trm", to_json(p.choice.trm, allocator), allocator);
    } else if (p.present == TisTpgTransaction_PR_tcm) {
        json.AddMember("tcm", to_json(p.choice.tcm, allocator), allocator);
    } else if (p.present == TisTpgTransaction_PR_vdrm) {
        json.AddMember("vdrm", to_json(p.choice.vdrm, allocator), allocator);
    } else if (p.present == TisTpgTransaction_PR_vdpm) {
        json.AddMember("vdpm", to_json(p.choice.vdpm, allocator), allocator);
    } else if (p.present == TisTpgTransaction_PR_eofm) {
        json.AddMember("eofm", to_json(p.choice.eofm, allocator), allocator);
    }
    return json;
}


/*
*   ManeuverAdviceContainer - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const ManeuverAdviceContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("numberOfExecutants", to_json((p.numberOfExecutants), allocator), allocator);
    json.AddMember("maneuver", to_json((p.maneuver), allocator), allocator);

    return json;
}




/*
*   GeneralIviContainer - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const GeneralIviContainer& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const GicPart_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   AvcPart - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const AvcPart& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("relevanceZoneIds", to_json((p.relevanceZoneIds), allocator), allocator);
    if (p.detectionZoneIds != 0) json.AddMember("detectionZoneIds", to_json(*(p.detectionZoneIds), allocator), allocator);
    if (p.direction != 0) json.AddMember("direction", to_json(*(p.direction), allocator), allocator);
    if (p.applicableLanes != 0) json.AddMember("applicableLanes", to_json(*(p.applicableLanes), allocator), allocator);
    if (p.vehicleCharacteristics != 0) json.AddMember("vehicleCharacteristics", to_json(*(p.vehicleCharacteristics), allocator), allocator);
    if (p.automatedVehicleRules != 0) json.AddMember("automatedVehicleRules", to_json(*(p.automatedVehicleRules), allocator), allocator);
    if (p.platooningRules != 0) json.AddMember("platooningRules", to_json(*(p.platooningRules), allocator), allocator);
    return json;
}




/*
*   VruAwareness - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VruAwareness& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("generationDeltaTime", to_json((p.generationDeltaTime), allocator), allocator);
    json.AddMember("vamParameters", to_json((p.vamParameters), allocator), allocator);

    return json;
}




/*
*   EvcsnPdu - Type SEQUENCE
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const EvcsnPdu& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("header", to_json((p.header), allocator), allocator);
    json.AddMember("evcsn", to_json((p.evcsn), allocator), allocator);

    return json;
}




/*
*   IMZM - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const IMZM& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("header", to_json((p.header), allocator), allocator);
    json.AddMember("imzm", to_json((p.imzm), allocator), allocator);

    return json;
}




/*
*   TisTpgTransactionsPdu - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTransactionsPdu& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("header", to_json((p.header), allocator), allocator);
    json.AddMember("tisTpgTransaction", to_json((p.tisTpgTransaction), allocator), allocator);

    return json;
}




/*
*   VehiclemaneuverContainer - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const VehiclemaneuverContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("mcmType", to_json((p.mcmType), allocator), allocator);
    json.AddMember("maneuverId", to_json((p.maneuverId), allocator), allocator);
    json.AddMember("maneuverCoordinationConcept", to_json((p.maneuverCoordinationConcept), allocator), allocator);
    json.AddMember("maneuverCoordinationRational", to_json((p.maneuverCoordinationRational), allocator), allocator);
    json.AddMember("vehicleCurrentState", to_json((p.vehicleCurrentState), allocator), allocator);
    json.AddMember("trajectoryId", to_json((p.trajectoryId), allocator), allocator);
    json.AddMember("vehicleTrajectory", to_json((p.vehicleTrajectory), allocator), allocator);
    if (p.maneuverExecutionStatus != 0) json.AddMember("maneuverExecutionStatus", to_json(*(p.maneuverExecutionStatus), allocator), allocator);
    if (p.maneuverAdviceContainer != 0) json.AddMember("maneuverAdviceContainer", to_json(*(p.maneuverAdviceContainer), allocator), allocator);
    if (p.maneuverResponse != 0) json.AddMember("maneuverResponse", to_json(*(p.maneuverResponse), allocator), allocator);
    return json;
}




/*
*   IviContainer - Type CHOICE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const IviContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == IviContainer_PR_glc) {
        json.AddMember("glc", to_json(p.choice.glc, allocator), allocator);
    } else if (p.present == IviContainer_PR_giv) {
        json.AddMember("giv", to_json(p.choice.giv, allocator), allocator);
    } else if (p.present == IviContainer_PR_rcc) {
        json.AddMember("rcc", to_json(p.choice.rcc, allocator), allocator);
    } else if (p.present == IviContainer_PR_tc) {
        json.AddMember("tc", to_json(p.choice.tc, allocator), allocator);
    } else if (p.present == IviContainer_PR_lac) {
        json.AddMember("lac", to_json(p.choice.lac, allocator), allocator);
    }
    return json;
}


/*
*   AutomatedVehicleContainer - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const AutomatedVehicleContainer& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const AvcPart_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   VAM - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("header", to_json((p.header), allocator), allocator);
    json.AddMember("vam", to_json((p.vam), allocator), allocator);

    return json;
}




/*
*   McmContainer - Type CHOICE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const McmContainer& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    if (p.present == McmContainer_PR_vehiclemaneuverContainer) {
        json.AddMember("vehiclemaneuverContainer", to_json(p.choice.vehiclemaneuverContainer, allocator), allocator);
    } else if (p.present == McmContainer_PR_acknowledgmentContainer) {
        json.AddMember("acknowledgmentContainer", to_json(p.choice.acknowledgmentContainer, allocator), allocator);
    }
    return json;
}


/*
*   IviContainers - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const IviContainers& p, Document::AllocatorType& allocator) {
    Value json(kArrayType);
    for(int i = 0; i < p.list.count; i++) {
        const IviContainer_t po = *(p.list.array[i]);
        Value obj = to_json(po, allocator);
        json.PushBack(obj, allocator);
    }
    return json;
}


/*
*   WrappedMcmInformationBlocks - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const WrappedMcmInformationBlocks& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("basicContainer", to_json((p.basicContainer), allocator), allocator);
    json.AddMember("mcmContainer", to_json((p.mcmContainer), allocator), allocator);

    return json;
}




/*
*   IviStructure - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const IviStructure& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("mandatory", to_json((p.mandatory), allocator), allocator);
    if (p.optional != 0) json.AddMember("optional", to_json(*(p.optional), allocator), allocator);
    return json;
}




/*
*   IVIM - Type SEQUENCE
*   From IVIM-PDU-Descriptions - File TS103301v211-IVIM.asn
*/

Value to_json(const IVIM& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("header", to_json((p.header), allocator), allocator);
    json.AddMember("ivi", to_json((p.ivi), allocator), allocator);

    return json;
}




/*
*   MCM - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const MCM& p, Document::AllocatorType& allocator) {
    Value json(kObjectType);
    json.AddMember("header", to_json((p.header), allocator), allocator);
    json.AddMember("payload", to_json((p.payload), allocator), allocator);

    return json;
}



