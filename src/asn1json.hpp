/*
*   JSON marshalling and unmarshalling functions for use by RapidJSON
*   Auto-generated from the asn1 directory by asn1json.py on 2024-06-20 15:49:19.745352
*/

#ifndef ASN1_JSON_HPP
#define ASN1_JSON_HPP

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include <iostream>
#include <vanetza/asn1/cam.hpp>
#include <vanetza/asn1/denm.hpp>
#include <vanetza/asn1/cpm.hpp>
#include <vanetza/asn1/vam.hpp>
#include <vanetza/asn1/spatem.hpp>
#include <vanetza/asn1/mapem.hpp>
#include <vanetza/asn1/srem.hpp>
#include <vanetza/asn1/ssem.hpp>
#include <vanetza/asn1/ivim.hpp>
#include <vanetza/asn1/rtcmem.hpp>
#include <vanetza/asn1/evcsnm.hpp>
#include <vanetza/asn1/evrsrm.hpp>
#include <vanetza/asn1/imzm.hpp>
#include <vanetza/asn1/tistpgm.hpp>
#include <vanetza/asn1/mcm.hpp>

#include <vanetza/asn1/its/NodeXY.h>
#include <vanetza/asn1/its/VehicleID.h>
#include <vanetza/asn1/its/TransitVehicleStatus.h>
#include <vanetza/asn1/its/TransmissionAndSpeed.h>
#include <vanetza/asn1/its/ITS-Container_DigitalMap.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_DigitalMap.h>
#include <vanetza/asn1/its/Position3D.h>
#include <vanetza/asn1/its/IntersectionAccessPoint.h>
#include <vanetza/asn1/its/ComputedLane.h>
#include <vanetza/asn1/its/AdvisorySpeedList.h>
#include <vanetza/asn1/its/ConnectionManeuverAssist.h>
#include <vanetza/asn1/its/DataParameters.h>
#include <vanetza/asn1/its/EnabledLaneList.h>
#include <vanetza/asn1/its/CS1.h>
#include <vanetza/asn1/its/CS2.h>
#include <vanetza/asn1/its/CS3.h>
#include <vanetza/asn1/its/CS4.h>
#include <vanetza/asn1/its/CS5.h>
#include <vanetza/asn1/its/CS7.h>
#include <vanetza/asn1/its/CS8.h>
#include <vanetza/asn1/its/ServiceNumber.h>
#include <vanetza/asn1/its/GeoGraphicalLimit.h>
#include <vanetza/asn1/its/LicPlateNumber.h>
#include <vanetza/asn1/its/TaxCode.h>
#include <vanetza/asn1/its/AviEriDateTime.h>
#include <vanetza/asn1/its/ServiceApplicationLimit.h>
#include <vanetza/asn1/its/FreightContainerData.h>
#include <vanetza/asn1/its/AddRq.h>
#include <vanetza/asn1/its/ChannelRq.h>
#include <vanetza/asn1/its/ChannelRs.h>
#include <vanetza/asn1/its/SubRq.h>
#include <vanetza/asn1/its/ContractAuthenticator.h>
#include <vanetza/asn1/its/DateCompact.h>
#include <vanetza/asn1/its/Engine.h>
#include <vanetza/asn1/its/EquipmentOBUId.h>
#include <vanetza/asn1/its/EquipmentStatus.h>
#include <vanetza/asn1/its/ICC-Id.h>
#include <vanetza/asn1/its/LPN.h>
#include <vanetza/asn1/its/SignedValue.h>
#include <vanetza/asn1/its/PaymentSecurityData.h>
#include <vanetza/asn1/its/PayUnit.h>
#include <vanetza/asn1/its/PersonalAccountNumber.h>
#include <vanetza/asn1/its/PurseBalance.h>
#include <vanetza/asn1/its/ReceiptOBUId.h>
#include <vanetza/asn1/its/ReceiptAuthenticator.h>
#include <vanetza/asn1/its/ReceiptText.h>
#include <vanetza/asn1/its/ResultFin.h>
#include <vanetza/asn1/its/SessionClass.h>
#include <vanetza/asn1/its/ReceiptContract.h>
#include <vanetza/asn1/its/SessionLocation.h>
#include <vanetza/asn1/its/DateAndTime.h>
#include <vanetza/asn1/its/ItsStationPosition.h>
#include <vanetza/asn1/its/SignalHeadLocation.h>
#include <vanetza/asn1/its/ItsStationPositionList.h>
#include <vanetza/asn1/its/SignalHeadLocationList.h>
#include <vanetza/asn1/its/CurrentVehicleConfiguration.h>
#include <vanetza/asn1/its/ManeuverResponse.h>
#include <vanetza/asn1/its/AccelerationChange.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_AccelerationConfidence.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_AccelerationControl.h>
#include <vanetza/asn1/its/AccelerationMagnitudeValue.h>
#include <vanetza/asn1/its/AccelerationValue.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_AccessTechnologyClass.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_AccidentSubCauseCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_AdverseWeatherCondition-AdhesionSubCauseCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_AdverseWeatherCondition-ExtremeWeatherConditionSubCauseCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_AdverseWeatherCondition-PrecipitationSubCauseCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_AdverseWeatherCondition-VisibilitySubCauseCode.h>
#include <vanetza/asn1/its/AirHumidity.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_AltitudeConfidence.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_AltitudeValue.h>
#include <vanetza/asn1/its/AngleConfidence.h>
#include <vanetza/asn1/its/AngularSpeedConfidence.h>
#include <vanetza/asn1/its/AngularAccelerationConfidence.h>
#include <vanetza/asn1/its/AxlesCount.h>
#include <vanetza/asn1/its/BarometricPressure.h>
#include <vanetza/asn1/its/BogiesCount.h>
#include <vanetza/asn1/its/CardinalNumber1B.h>
#include <vanetza/asn1/its/CardinalNumber3b.h>
#include <vanetza/asn1/its/CartesianAngleValue.h>
#include <vanetza/asn1/its/CartesianAngularAccelerationComponentValue.h>
#include <vanetza/asn1/its/CartesianAngularVelocityComponentValue.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_CauseCodeType.h>
#include <vanetza/asn1/its/CartesianCoordinateSmall.h>
#include <vanetza/asn1/its/CartesianCoordinate.h>
#include <vanetza/asn1/its/CartesianCoordinateLarge.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_CenDsrcTollingZoneID.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_ClusterBreakupReason.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_ClusterLeaveReason.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_CollisionRiskSubCauseCode.h>
#include <vanetza/asn1/its/ConfidenceLevel.h>
#include <vanetza/asn1/its/CoordinateConfidence.h>
#include <vanetza/asn1/its/CorrelationCellValue.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_CountryCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_CurvatureCalculationMode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_CurvatureConfidence.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_CurvatureValue.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_DangerousEndOfQueueSubCauseCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_DangerousGoodsBasic.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_DangerousSituationSubCauseCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_DeltaAltitude.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_DeltaLatitude.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_DeltaLongitude.h>
#include <vanetza/asn1/its/DeltaTimeMilliSecondPositive.h>
#include <vanetza/asn1/its/DeltaTimeMilliSecondSigned.h>
#include <vanetza/asn1/its/DeltaTimeQuarterSecond.h>
#include <vanetza/asn1/its/DeltaTimeTenthOfSecond.h>
#include <vanetza/asn1/its/DeltaTimeSecond.h>
#include <vanetza/asn1/its/DeltaTimeTenSeconds.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_Direction.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_DriveDirection.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_DrivingLaneStatus.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_EmbarkationStatus.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_EmergencyPriority.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_EmergencyVehicleApproachingSubCauseCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_EnergyStorageType.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_EuVehicleCategoryL.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_EuVehicleCategoryM.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_EuVehicleCategoryN.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_EuVehicleCategoryO.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_ExteriorLights.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_GenerationDeltaTime.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_HardShoulderStatus.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_HazardousLocation-AnimalOnTheRoadSubCauseCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_HazardousLocation-DangerousCurveSubCauseCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_HazardousLocation-ObstacleOnTheRoadSubCauseCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_HazardousLocation-SurfaceConditionSubCauseCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_HeadingConfidence.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_HeadingValue.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_HeightLonCarr.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_HumanPresenceOnTheRoadSubCauseCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_HumanProblemSubCauseCode.h>
#include <vanetza/asn1/its/Identifier1B.h>
#include <vanetza/asn1/its/Identifier2B.h>
#include <vanetza/asn1/its/ImpassabilitySubCauseCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_InformationQuality.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_InterferenceManagementZoneType.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_Iso3833VehicleType.h>
#include <vanetza/asn1/its/IssuerIdentifier.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_IviIdentificationNumber.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_LanePosition.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_LaneType.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_LaneWidth.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_Latitude.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_LateralAccelerationValue.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_LightBarSirenInUse.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_Longitude.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_LongitudinalAccelerationValue.h>
#include <vanetza/asn1/its/LongitudinalLanePositionValue.h>
#include <vanetza/asn1/its/LongitudinalLanePositionConfidence.h>
#include <vanetza/asn1/its/MatrixIncludedComponents.h>
#include <vanetza/asn1/its/MessageId.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_NumberOfOccupants.h>
#include <vanetza/asn1/its/ObjectPerceptionQuality.h>
#include <vanetza/asn1/its/ObjectDimensionValue.h>
#include <vanetza/asn1/its/ObjectDimensionConfidence.h>
#include <vanetza/asn1/its/ObjectFace.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_OpeningDaysHours.h>
#include <vanetza/asn1/its/OrdinalNumber1B.h>
#include <vanetza/asn1/its/OrdinalNumber3b.h>
#include <vanetza/asn1/its/OtherSubClass.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_PathDeltaTime.h>
#include <vanetza/asn1/its/PathId.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_PerformanceClass.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_PhoneNumber.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_PosCentMass.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_PositioningSolutionType.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_PositionOfOccupants.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_PosFrontAx.h>
#include <vanetza/asn1/its/Position1d.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_PosLonCarr.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_PosPillar.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_PostCrashSubCauseCode.h>
#include <vanetza/asn1/its/PrecipitationIntensity.h>
#include <vanetza/asn1/its/ProtectedZoneId.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_ProtectedZoneRadius.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_ProtectedZoneType.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_PtActivationData.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_PtActivationType.h>
#include <vanetza/asn1/its/RailwayLevelCrossingSubCauseCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_RelevanceDistance.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_RelevanceTrafficDirection.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_RequestResponseIndication.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_RescueAndRecoveryWorkInProgressSubCauseCode.h>
#include <vanetza/asn1/its/RoadSectionId.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_RoadType.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_RoadworksSubCauseCode.h>
#include <vanetza/asn1/its/SafeDistanceIndicator.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_SemiAxisLength.h>
#include <vanetza/asn1/its/SensorType.h>
#include <vanetza/asn1/its/SensorTypes.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_SequenceNumber.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_SignalViolationSubCauseCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_SlowVehicleSubCauseCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_SpecialTransportType.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_SpeedConfidence.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_SpeedLimit.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_SpeedValue.h>
#include <vanetza/asn1/its/StoredInformationType.h>
#include <vanetza/asn1/its/VelocityComponentValue.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_StabilityLossProbability.h>
#include <vanetza/asn1/its/StandardLength12b.h>
#include <vanetza/asn1/its/StandardLength3b.h>
#include <vanetza/asn1/its/StandardLength9b.h>
#include <vanetza/asn1/its/StandardLength1B.h>
#include <vanetza/asn1/its/StandardLength2B.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_StationarySince.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_StationaryVehicleSubCauseCode.h>
#include <vanetza/asn1/its/StationId.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_StationID.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_StationType.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_SteeringWheelAngleConfidence.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_SteeringWheelAngleValue.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_SubCauseCodeType.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_Temperature.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_TimestampIts.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_TrafficConditionSubCauseCode.h>
#include <vanetza/asn1/its/TrafficDirection.h>
#include <vanetza/asn1/its/TrafficParticipantType.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_TrafficRule.h>
#include <vanetza/asn1/its/TrailerPresenceInformation.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_TrajectoryInterceptionProbability.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_TrajectoryInterceptionConfidence.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_TransmissionInterval.h>
#include <vanetza/asn1/its/TurningDirection.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_TurningRadius.h>
#include <vanetza/asn1/its/UsageIndication.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_ValidityDuration.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VDS.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VehicleBreakdownSubCauseCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VehicleHeight.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VehicleLengthConfidenceIndication.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VehicleLengthValue.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VehicleMass.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VehicleRole.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VehicleWidth.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VerticalAccelerationValue.h>
#include <vanetza/asn1/its/VruClusterProfiles.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VruDeviceUsage.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VruEnvironment.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VruMovementControl.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VruSubProfilePedestrian.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VruSubProfileBicyclist.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VruSubProfileMotorcyclist.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VruSubProfileAnimal.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VruSizeClass.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VruSpecificExteriorLights.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_WheelBaseVehicle.h>
#include <vanetza/asn1/its/Wgs84AngleConfidence.h>
#include <vanetza/asn1/its/Wgs84AngleValue.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_WMInumber.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_WrongWayDrivingSubCauseCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_YawRateConfidence.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_YawRateValue.h>
#include <vanetza/asn1/its/Acceleration3dWithConfidence.h>
#include <vanetza/asn1/its/AccelerationPolarWithZ.h>
#include <vanetza/asn1/its/AccelerationCartesian.h>
#include <vanetza/asn1/its/AccelerationComponent.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_AccelerationChangeIndication.h>
#include <vanetza/asn1/its/AccelerationMagnitude.h>
#include <vanetza/asn1/its/ActionId.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_ActionID.h>
#include <vanetza/asn1/its/ActionIdList.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_Altitude.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_BasicContainer.h>
#include <vanetza/asn1/its/BasicLaneConfiguration.h>
#include <vanetza/asn1/its/BasicLaneInformation.h>
#include <vanetza/asn1/its/CartesianAngle.h>
#include <vanetza/asn1/its/CartesianAngularVelocityComponent.h>
#include <vanetza/asn1/its/CartesianAngularAccelerationComponent.h>
#include <vanetza/asn1/its/CartesianCoordinateWithConfidence.h>
#include <vanetza/asn1/its/CartesianPosition3d.h>
#include <vanetza/asn1/its/CartesianPosition3dWithConfidence.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_CauseCode.h>
#include <vanetza/asn1/its/CauseCodeChoice.h>
#include <vanetza/asn1/its/CauseCodeV2.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_CenDsrcTollingZone.h>
#include <vanetza/asn1/its/CircularShape.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_ClosedLanes.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_ClusterBreakupInfo.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_ClusterJoinInfo.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_ClusterLeaveInfo.h>
#include <vanetza/asn1/its/CorrelationColumn.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_Curvature.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_DangerousGoodsExtended.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_DeltaReferencePosition.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_DigitalMap.h>
#include <vanetza/asn1/its/EllipticalShape.h>
#include <vanetza/asn1/its/EulerAnglesWithConfidence.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_EuVehicleCategoryCode.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_EventHistory.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_EventPoint.h>
#include <vanetza/asn1/its/EventZone.h>
#include <vanetza/asn1/its/GeoPosition.h>
#include <vanetza/asn1/its/GeneralizedLanePosition.h>
#include <vanetza/asn1/its/GeneralizedLanePositions.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_Heading.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_HeadingChangeIndication.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_InterferenceManagementChannel.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_InterferenceManagementZone.h>
#include <vanetza/asn1/its/InterferenceManagementZoneDefinition.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_InterferenceManagementInfo.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_InterferenceManagementInfoPerChannel.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_InterferenceManagementZones.h>
#include <vanetza/asn1/its/IntersectionReferenceId.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_ItineraryPath.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_ItsPduHeader.h>
#include <vanetza/asn1/its/IvimReference.h>
#include <vanetza/asn1/its/IvimReferences.h>
#include <vanetza/asn1/its/LanePositionAndType.h>
#include <vanetza/asn1/its/LanePositionOptions.h>
#include <vanetza/asn1/its/LanePositionWithLateralDetails.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_LateralAcceleration.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_LongitudinalAcceleration.h>
#include <vanetza/asn1/its/LongitudinalLanePosition.h>
#include <vanetza/asn1/its/LowerTriangularPositiveSemidefiniteMatrices.h>
#include <vanetza/asn1/its/LowerTriangularPositiveSemidefiniteMatrix.h>
#include <vanetza/asn1/its/LowerTriangularPositiveSemidefiniteMatrixColumns.h>
#include <vanetza/asn1/its/MapemConfiguration.h>
#include <vanetza/asn1/its/MapemElementReference.h>
#include <vanetza/asn1/its/MapemLaneList.h>
#include <vanetza/asn1/its/MapemConnectionList.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_MapPosition.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_MapReference.h>
#include <vanetza/asn1/its/MapReferences.h>
#include <vanetza/asn1/its/MessageRateHz.h>
#include <vanetza/asn1/its/MessageSegmentationInfo.h>
#include <vanetza/asn1/its/MetaInformation.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_MitigationForTechnologies.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_MitigationPerTechnologyClass.h>
#include <vanetza/asn1/its/ObjectClass.h>
#include <vanetza/asn1/its/ObjectClassDescription.h>
#include <vanetza/asn1/its/ObjectClassWithConfidence.h>
#include <vanetza/asn1/its/ObjectDimension.h>
#include <vanetza/asn1/its/OccupiedLanesWithConfidence.h>
#include <vanetza/asn1/its/Path.h>
#include <vanetza/asn1/its/PathDeltaTimeChoice.h>
#include <vanetza/asn1/its/PathExtended.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_PathHistory.h>
#include <vanetza/asn1/its/PathPredicted.h>
#include <vanetza/asn1/its/PathPredicted2.h>
#include <vanetza/asn1/its/PathPredictedList.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_PathPoint.h>
#include <vanetza/asn1/its/PathPointPredicted.h>
#include <vanetza/asn1/its/PathReferences.h>
#include <vanetza/asn1/its/PerceivedObject.h>
#include <vanetza/asn1/its/PolygonalShape.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_PosConfidenceEllipse.h>
#include <vanetza/asn1/its/PositionConfidenceEllipse.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_PositionOfPillars.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_ProtectedCommunicationZone.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_ProtectedCommunicationZonesRSU.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_Provider.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_PtActivation.h>
#include <vanetza/asn1/its/RadialShape.h>
#include <vanetza/asn1/its/RadialShapes.h>
#include <vanetza/asn1/its/RadialShapesList.h>
#include <vanetza/asn1/its/RadialShapeDetails.h>
#include <vanetza/asn1/its/RectangularShape.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_ReferencePosition.h>
#include <vanetza/asn1/its/ReferencePositionWithConfidence.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_RestrictedTypes.h>
#include <vanetza/asn1/its/RoadConfigurationSection.h>
#include <vanetza/asn1/its/RoadConfigurationSectionList.h>
#include <vanetza/asn1/its/RoadSectionDefinition.h>
#include <vanetza/asn1/its/RoadSegmentReferenceId.h>
#include <vanetza/asn1/its/SafeDistanceIndication.h>
#include <vanetza/asn1/its/SequenceOfCartesianPosition3d.h>
#include <vanetza/asn1/its/SequenceOfIdentifier1B.h>
#include <vanetza/asn1/its/SequenceOfSafeDistanceIndication.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_SequenceOfTrajectoryInterceptionIndication.h>
#include <vanetza/asn1/its/Shape.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_Speed.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_StabilityChangeIndication.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_SteeringWheelAngle.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_Traces.h>
#include <vanetza/asn1/its/TracesExtended.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_TrafficIslandPosition.h>
#include <vanetza/asn1/its/TrailerData.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_TrajectoryInterceptionIndication.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VarLengthNumber.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_Ext1.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_Ext2.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_Ext3.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VerticalAcceleration.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VehicleIdentification.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VehicleLength.h>
#include <vanetza/asn1/its/VehicleLengthV2.h>
#include <vanetza/asn1/its/Velocity3dWithConfidence.h>
#include <vanetza/asn1/its/VelocityCartesian.h>
#include <vanetza/asn1/its/VelocityComponent.h>
#include <vanetza/asn1/its/VelocityPolarWithZ.h>
#include <vanetza/asn1/its/VruClusterInformation.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VruExteriorLights.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_VruProfileAndSubprofile.h>
#include <vanetza/asn1/its/Wgs84Angle.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_YawRate.h>
#include <vanetza/asn1/its/ActualNumberOfPassengers.h>
#include <vanetza/asn1/its/AxleWeightLimits.h>
#include <vanetza/asn1/its/AddRq.h>
#include <vanetza/asn1/its/ChannelId.h>
#include <vanetza/asn1/its/ChannelRq.h>
#include <vanetza/asn1/its/ChannelRs.h>
#include <vanetza/asn1/its/CopyRq.h>
#include <vanetza/asn1/its/CreditRq.h>
#include <vanetza/asn1/its/CreditRs.h>
#include <vanetza/asn1/its/DebitRq.h>
#include <vanetza/asn1/its/DebitRs.h>
#include <vanetza/asn1/its/GetInstanceRq.h>
#include <vanetza/asn1/its/GetStampedRq.h>
#include <vanetza/asn1/its/GetStampedRs.h>
#include <vanetza/asn1/its/SetInstanceRq.h>
#include <vanetza/asn1/its/SetMMIRq.h>
#include <vanetza/asn1/its/SetStampedRq.h>
#include <vanetza/asn1/its/SubRq.h>
#include <vanetza/asn1/its/CO2EmissionValue.h>
#include <vanetza/asn1/its/ContractSerialNumber.h>
#include <vanetza/asn1/its/ContractAuthenticator.h>
#include <vanetza/asn1/its/ContractValidity.h>
#include <vanetza/asn1/its/ContractVehicle.h>
#include <vanetza/asn1/its/DateCompact.h>
#include <vanetza/asn1/its/DescriptiveCharacteristics.h>
#include <vanetza/asn1/its/DieselEmissionValues.h>
#include <vanetza/asn1/its/EfcDsrcApplication_DriverCharacteristics.h>
#include <vanetza/asn1/its/EFC-ContextMark.h>
#include <vanetza/asn1/its/EnvironmentalCharacteristics.h>
#include <vanetza/asn1/its/EuroValue.h>
#include <vanetza/asn1/its/CopValue.h>
#include <vanetza/asn1/its/EngineCharacteristics.h>
#include <vanetza/asn1/its/Engine.h>
#include <vanetza/asn1/its/EquipmentOBUId.h>
#include <vanetza/asn1/its/EquipmentStatus.h>
#include <vanetza/asn1/its/ExhaustEmissionValues.h>
#include <vanetza/asn1/its/FutureCharacteristics.h>
#include <vanetza/asn1/its/ICC-Id.h>
#include <vanetza/asn1/its/Int1.h>
#include <vanetza/asn1/its/Int2.h>
#include <vanetza/asn1/its/Int3.h>
#include <vanetza/asn1/its/Int4.h>
#include <vanetza/asn1/its/LPN.h>
#include <vanetza/asn1/its/PassengerCapacity.h>
#include <vanetza/asn1/its/PaymentFee.h>
#include <vanetza/asn1/its/PaymentMeans.h>
#include <vanetza/asn1/its/PaymentMeansBalance.h>
#include <vanetza/asn1/its/SignedValue.h>
#include <vanetza/asn1/its/PaymentMeansUnit.h>
#include <vanetza/asn1/its/PaymentSecurityData.h>
#include <vanetza/asn1/its/PayUnit.h>
#include <vanetza/asn1/its/PersonalAccountNumber.h>
#include <vanetza/asn1/its/ETSI-ITS-CDD_Provider.h>
#include <vanetza/asn1/its/EfcDsrcApplication_Provider.h>
#include <vanetza/asn1/its/PurseBalance.h>
#include <vanetza/asn1/its/ReceiptContract.h>
#include <vanetza/asn1/its/ReceiptData1.h>
#include <vanetza/asn1/its/ReceiptData2.h>
#include <vanetza/asn1/its/ReceiptData.h>
#include <vanetza/asn1/its/ReceiptDistance.h>
#include <vanetza/asn1/its/ReceiptFinancialPart.h>
#include <vanetza/asn1/its/ReceiptICC-Id.h>
#include <vanetza/asn1/its/ReceiptOBUId.h>
#include <vanetza/asn1/its/ReceiptServicePart.h>
#include <vanetza/asn1/its/ReceiptServiceSerialNumber.h>
#include <vanetza/asn1/its/ReceiptAuthenticator.h>
#include <vanetza/asn1/its/ReceiptText.h>
#include <vanetza/asn1/its/ResultFin.h>
#include <vanetza/asn1/its/ResultOp.h>
#include <vanetza/asn1/its/SessionClass.h>
#include <vanetza/asn1/its/SessionLocation.h>
#include <vanetza/asn1/its/StationTypeIso.h>
#include <vanetza/asn1/its/DateAndTime.h>
#include <vanetza/asn1/its/SoundLevel.h>
#include <vanetza/asn1/its/EfcDsrcApplication_TrailerCharacteristics.h>
#include <vanetza/asn1/its/TrailerDetails.h>
#include <vanetza/asn1/its/TrailerLicencePlateNumber.h>
#include <vanetza/asn1/its/UnitType.h>
#include <vanetza/asn1/its/ValidityOfContract.h>
#include <vanetza/asn1/its/VehicleAuthenticator.h>
#include <vanetza/asn1/its/VehicleAxles.h>
#include <vanetza/asn1/its/TrailerAxles.h>
#include <vanetza/asn1/its/TractorAxles.h>
#include <vanetza/asn1/its/VehicleClass.h>
#include <vanetza/asn1/its/VehicleDimensions.h>
#include <vanetza/asn1/its/VehicleLicencePlateNumber.h>
#include <vanetza/asn1/its/VehicleIdentificationNumber.h>
#include <vanetza/asn1/its/VehicleSpecificCharacteristics.h>
#include <vanetza/asn1/its/VehicleTotalDistance.h>
#include <vanetza/asn1/its/VehicleWeightLaden.h>
#include <vanetza/asn1/its/VehicleCurrentMaxTrainWeight.h>
#include <vanetza/asn1/its/VehicleWeightLimits.h>
#include <vanetza/asn1/its/AttributeIdList.h>
#include <vanetza/asn1/its/AttributeList.h>
#include <vanetza/asn1/its/Attributes.h>

using namespace rapidjson;

Value to_json(const ETSI_ITS_CDD_TimestampIts_t& p, Document::AllocatorType& allocator);
Value to_json(const ITS_Container_TimestampIts_t& p, Document::AllocatorType& allocator);
Value to_json(const long& p, Document::AllocatorType& allocator);
Value to_json(const unsigned long& p, Document::AllocatorType& allocator);
Value to_json(const unsigned long* p, Document::AllocatorType& allocator);
Value to_json(const unsigned& p, Document::AllocatorType& allocator);
Value to_json(const double& p, Document::AllocatorType& allocator);
Value to_json(const bool& p, Document::AllocatorType& allocator);
Value to_json(const OCTET_STRING_t& p, Document::AllocatorType& allocator);
Value to_json(const NULL_t& p, Document::AllocatorType& allocator);


/*
*   AccelerationControl - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_AccelerationControl(const ETSI_ITS_CDD_AccelerationControl_t p, Document::AllocatorType& allocator);



/*
*   CountryCode - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_CountryCode(const ETSI_ITS_CDD_CountryCode_t p, Document::AllocatorType& allocator);



/*
*   DrivingLaneStatus - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_DrivingLaneStatus(const ETSI_ITS_CDD_DrivingLaneStatus_t p, Document::AllocatorType& allocator);



/*
*   EmergencyPriority - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_EmergencyPriority(const ETSI_ITS_CDD_EmergencyPriority_t p, Document::AllocatorType& allocator);



/*
*   EnergyStorageType - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_EnergyStorageType(const ETSI_ITS_CDD_EnergyStorageType_t p, Document::AllocatorType& allocator);



/*
*   ExteriorLights - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_ExteriorLights(const ETSI_ITS_CDD_ExteriorLights_t p, Document::AllocatorType& allocator);



/*
*   LightBarSirenInUse - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_LightBarSirenInUse(const ETSI_ITS_CDD_LightBarSirenInUse_t p, Document::AllocatorType& allocator);



/*
*   MatrixIncludedComponents - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_MatrixIncludedComponents(const MatrixIncludedComponents_t p, Document::AllocatorType& allocator);



/*
*   PositionOfOccupants - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_PositionOfOccupants(const ETSI_ITS_CDD_PositionOfOccupants_t p, Document::AllocatorType& allocator);



/*
*   SensorTypes - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_SensorTypes(const SensorTypes_t p, Document::AllocatorType& allocator);



/*
*   SpecialTransportType - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_SpecialTransportType(const ETSI_ITS_CDD_SpecialTransportType_t p, Document::AllocatorType& allocator);



/*
*   StoredInformationType - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_StoredInformationType(const StoredInformationType_t p, Document::AllocatorType& allocator);



/*
*   VruClusterProfiles - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_VruClusterProfiles(const VruClusterProfiles_t p, Document::AllocatorType& allocator);



/*
*   VruSpecificExteriorLights - Type BIT STRING
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json_ETSI_ITS_CDD_VruSpecificExteriorLights(const ETSI_ITS_CDD_VruSpecificExteriorLights_t p, Document::AllocatorType& allocator);



/*
*   AccelerationComponent - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const AccelerationComponent_t& p, Document::AllocatorType& allocator);



/*
*   AccelerationChangeIndication - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_AccelerationChangeIndication_t& p, Document::AllocatorType& allocator);



/*
*   AccelerationMagnitude - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const AccelerationMagnitude_t& p, Document::AllocatorType& allocator);



/*
*   ActionId - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ActionId_t& p, Document::AllocatorType& allocator);



/*
*   ActionID - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ActionID_t& p, Document::AllocatorType& allocator);



/*
*   ActionIdList - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ActionIdList_t& p, Document::AllocatorType& allocator);



/*
*   Altitude - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_Altitude_t& p, Document::AllocatorType& allocator);



/*
*   BasicLaneInformation - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const BasicLaneInformation_t& p, Document::AllocatorType& allocator);



/*
*   CartesianAngle - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CartesianAngle_t& p, Document::AllocatorType& allocator);



/*
*   CartesianAngularVelocityComponent - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CartesianAngularVelocityComponent_t& p, Document::AllocatorType& allocator);



/*
*   CartesianAngularAccelerationComponent - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CartesianAngularAccelerationComponent_t& p, Document::AllocatorType& allocator);



/*
*   CartesianCoordinateWithConfidence - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CartesianCoordinateWithConfidence_t& p, Document::AllocatorType& allocator);



/*
*   CartesianPosition3d - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CartesianPosition3d_t& p, Document::AllocatorType& allocator);



/*
*   CartesianPosition3dWithConfidence - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CartesianPosition3dWithConfidence_t& p, Document::AllocatorType& allocator);



/*
*   CauseCode - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_CauseCode_t& p, Document::AllocatorType& allocator);



/*
*   CauseCodeChoice - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CauseCodeChoice_t& p, Document::AllocatorType& allocator);



/*
*   CauseCodeV2 - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CauseCodeV2_t& p, Document::AllocatorType& allocator);



/*
*   CenDsrcTollingZone - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_CenDsrcTollingZone_t& p, Document::AllocatorType& allocator);



/*
*   CircularShape - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CircularShape_t& p, Document::AllocatorType& allocator);



/*
*   ClosedLanes - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ClosedLanes_t& p, Document::AllocatorType& allocator);



/*
*   ClusterBreakupInfo - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ClusterBreakupInfo_t& p, Document::AllocatorType& allocator);



/*
*   ClusterJoinInfo - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ClusterJoinInfo_t& p, Document::AllocatorType& allocator);



/*
*   ClusterLeaveInfo - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ClusterLeaveInfo_t& p, Document::AllocatorType& allocator);



/*
*   CorrelationColumn - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const CorrelationColumn_t& p, Document::AllocatorType& allocator);



/*
*   Curvature - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_Curvature_t& p, Document::AllocatorType& allocator);



/*
*   DangerousGoodsExtended - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_DangerousGoodsExtended_t& p, Document::AllocatorType& allocator);



/*
*   DeltaReferencePosition - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_DeltaReferencePosition_t& p, Document::AllocatorType& allocator);



/*
*   EllipticalShape - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const EllipticalShape_t& p, Document::AllocatorType& allocator);



/*
*   EulerAnglesWithConfidence - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const EulerAnglesWithConfidence_t& p, Document::AllocatorType& allocator);



/*
*   EuVehicleCategoryCode - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_EuVehicleCategoryCode_t& p, Document::AllocatorType& allocator);



/*
*   EventPoint - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_EventPoint_t& p, Document::AllocatorType& allocator);



/*
*   GeoPosition - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const GeoPosition_t& p, Document::AllocatorType& allocator);



/*
*   Heading - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_Heading_t& p, Document::AllocatorType& allocator);



/*
*   HeadingChangeIndication - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_HeadingChangeIndication_t& p, Document::AllocatorType& allocator);



/*
*   InterferenceManagementChannel - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_InterferenceManagementChannel_t& p, Document::AllocatorType& allocator);



/*
*   IntersectionReferenceId - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const IntersectionReferenceId_t& p, Document::AllocatorType& allocator);



/*
*   ItsPduHeader - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ItsPduHeader_t& p, Document::AllocatorType& allocator);



/*
*   LanePositionAndType - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const LanePositionAndType_t& p, Document::AllocatorType& allocator);



/*
*   LanePositionWithLateralDetails - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const LanePositionWithLateralDetails_t& p, Document::AllocatorType& allocator);



/*
*   LateralAcceleration - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_LateralAcceleration_t& p, Document::AllocatorType& allocator);



/*
*   LongitudinalAcceleration - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_LongitudinalAcceleration_t& p, Document::AllocatorType& allocator);



/*
*   LongitudinalLanePosition - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const LongitudinalLanePosition_t& p, Document::AllocatorType& allocator);



/*
*   LowerTriangularPositiveSemidefiniteMatrixColumns - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const LowerTriangularPositiveSemidefiniteMatrixColumns_t& p, Document::AllocatorType& allocator);



/*
*   MapemLaneList - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const MapemLaneList_t& p, Document::AllocatorType& allocator);



/*
*   MapemConnectionList - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const MapemConnectionList_t& p, Document::AllocatorType& allocator);



/*
*   MessageRateHz - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const MessageRateHz_t& p, Document::AllocatorType& allocator);



/*
*   MessageSegmentationInfo - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const MessageSegmentationInfo_t& p, Document::AllocatorType& allocator);



/*
*   MetaInformation - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const MetaInformation_t& p, Document::AllocatorType& allocator);



/*
*   MitigationPerTechnologyClass - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_MitigationPerTechnologyClass_t& p, Document::AllocatorType& allocator);



/*
*   ObjectDimension - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ObjectDimension_t& p, Document::AllocatorType& allocator);



/*
*   PathDeltaTimeChoice - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PathDeltaTimeChoice_t& p, Document::AllocatorType& allocator);



/*
*   PathPoint - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_PathPoint_t& p, Document::AllocatorType& allocator);



/*
*   PathReferences - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PathReferences_t& p, Document::AllocatorType& allocator);



/*
*   PosConfidenceEllipse - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_PosConfidenceEllipse_t& p, Document::AllocatorType& allocator);



/*
*   PositionConfidenceEllipse - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PositionConfidenceEllipse_t& p, Document::AllocatorType& allocator);



/*
*   PositionOfPillars - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_PositionOfPillars_t& p, Document::AllocatorType& allocator);



/*
*   ProtectedCommunicationZone - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ProtectedCommunicationZone_t& p, Document::AllocatorType& allocator);



/*
*   ProtectedCommunicationZonesRSU - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ProtectedCommunicationZonesRSU_t& p, Document::AllocatorType& allocator);



/*
*   Provider - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_Provider_t& p, Document::AllocatorType& allocator);



/*
*   PtActivation - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_PtActivation_t& p, Document::AllocatorType& allocator);



/*
*   RadialShape - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const RadialShape_t& p, Document::AllocatorType& allocator);



/*
*   RadialShapeDetails - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const RadialShapeDetails_t& p, Document::AllocatorType& allocator);



/*
*   RectangularShape - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const RectangularShape_t& p, Document::AllocatorType& allocator);



/*
*   ReferencePosition - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ReferencePosition_t& p, Document::AllocatorType& allocator);



/*
*   ReferencePositionWithConfidence - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ReferencePositionWithConfidence_t& p, Document::AllocatorType& allocator);



/*
*   RestrictedTypes - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_RestrictedTypes_t& p, Document::AllocatorType& allocator);



/*
*   RoadSectionDefinition - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const RoadSectionDefinition_t& p, Document::AllocatorType& allocator);



/*
*   RoadSegmentReferenceId - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const RoadSegmentReferenceId_t& p, Document::AllocatorType& allocator);



/*
*   SafeDistanceIndication - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const SafeDistanceIndication_t& p, Document::AllocatorType& allocator);



/*
*   SequenceOfCartesianPosition3d - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const SequenceOfCartesianPosition3d_t& p, Document::AllocatorType& allocator);



/*
*   SequenceOfIdentifier1B - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const SequenceOfIdentifier1B_t& p, Document::AllocatorType& allocator);



/*
*   SequenceOfSafeDistanceIndication - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const SequenceOfSafeDistanceIndication_t& p, Document::AllocatorType& allocator);



/*
*   Speed - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_Speed_t& p, Document::AllocatorType& allocator);



/*
*   StabilityChangeIndication - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_StabilityChangeIndication_t& p, Document::AllocatorType& allocator);



/*
*   SteeringWheelAngle - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_SteeringWheelAngle_t& p, Document::AllocatorType& allocator);



/*
*   TrafficIslandPosition - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_TrafficIslandPosition_t& p, Document::AllocatorType& allocator);



/*
*   TrailerData - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const TrailerData_t& p, Document::AllocatorType& allocator);



/*
*   TrajectoryInterceptionIndication - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_TrajectoryInterceptionIndication_t& p, Document::AllocatorType& allocator);



/*
*   Ext2 - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_Ext2_t& p, Document::AllocatorType& allocator);



/*
*   VerticalAcceleration - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_VerticalAcceleration_t& p, Document::AllocatorType& allocator);



/*
*   VehicleIdentification - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_VehicleIdentification_t& p, Document::AllocatorType& allocator);



/*
*   VehicleLength - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_VehicleLength_t& p, Document::AllocatorType& allocator);



/*
*   VehicleLengthV2 - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const VehicleLengthV2_t& p, Document::AllocatorType& allocator);



/*
*   VelocityComponent - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const VelocityComponent_t& p, Document::AllocatorType& allocator);



/*
*   VelocityPolarWithZ - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const VelocityPolarWithZ_t& p, Document::AllocatorType& allocator);



/*
*   VruExteriorLights - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_VruExteriorLights_t& p, Document::AllocatorType& allocator);



/*
*   VruProfileAndSubprofile - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_VruProfileAndSubprofile_t& p, Document::AllocatorType& allocator);



/*
*   Wgs84Angle - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const Wgs84Angle_t& p, Document::AllocatorType& allocator);



/*
*   YawRate - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_YawRate_t& p, Document::AllocatorType& allocator);



/*
*   ItsPduHeader - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_ItsPduHeader_t& p, Document::AllocatorType& allocator);



/*
*   ReferencePosition - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_ReferencePosition_t& p, Document::AllocatorType& allocator);



/*
*   DeltaReferencePosition - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_DeltaReferencePosition_t& p, Document::AllocatorType& allocator);



/*
*   Altitude - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_Altitude_t& p, Document::AllocatorType& allocator);



/*
*   PosConfidenceEllipse - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_PosConfidenceEllipse_t& p, Document::AllocatorType& allocator);



/*
*   PathPoint - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_PathPoint_t& p, Document::AllocatorType& allocator);



/*
*   PtActivation - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_PtActivation_t& p, Document::AllocatorType& allocator);



/*
*   AccelerationControl - Type BIT STRING
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json_ITS_Container_AccelerationControl(const ITS_Container_AccelerationControl_t p, Document::AllocatorType& allocator);



/*
*   CauseCode - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_CauseCode_t& p, Document::AllocatorType& allocator);



/*
*   Curvature - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_Curvature_t& p, Document::AllocatorType& allocator);



/*
*   Heading - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_Heading_t& p, Document::AllocatorType& allocator);



/*
*   ClosedLanes - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_ClosedLanes_t& p, Document::AllocatorType& allocator);



/*
*   DrivingLaneStatus - Type BIT STRING
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json_ITS_Container_DrivingLaneStatus(const ITS_Container_DrivingLaneStatus_t p, Document::AllocatorType& allocator);



/*
*   Speed - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_Speed_t& p, Document::AllocatorType& allocator);



/*
*   LongitudinalAcceleration - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_LongitudinalAcceleration_t& p, Document::AllocatorType& allocator);



/*
*   LateralAcceleration - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_LateralAcceleration_t& p, Document::AllocatorType& allocator);



/*
*   VerticalAcceleration - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_VerticalAcceleration_t& p, Document::AllocatorType& allocator);



/*
*   ExteriorLights - Type BIT STRING
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json_ITS_Container_ExteriorLights(const ITS_Container_ExteriorLights_t p, Document::AllocatorType& allocator);



/*
*   DangerousGoodsExtended - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_DangerousGoodsExtended_t& p, Document::AllocatorType& allocator);



/*
*   SpecialTransportType - Type BIT STRING
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json_ITS_Container_SpecialTransportType(const ITS_Container_SpecialTransportType_t p, Document::AllocatorType& allocator);



/*
*   LightBarSirenInUse - Type BIT STRING
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json_ITS_Container_LightBarSirenInUse(const ITS_Container_LightBarSirenInUse_t p, Document::AllocatorType& allocator);



/*
*   PositionOfOccupants - Type BIT STRING
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json_ITS_Container_PositionOfOccupants(const ITS_Container_PositionOfOccupants_t p, Document::AllocatorType& allocator);



/*
*   VehicleIdentification - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_VehicleIdentification_t& p, Document::AllocatorType& allocator);



/*
*   EnergyStorageType - Type BIT STRING
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json_ITS_Container_EnergyStorageType(const ITS_Container_EnergyStorageType_t p, Document::AllocatorType& allocator);



/*
*   VehicleLength - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_VehicleLength_t& p, Document::AllocatorType& allocator);



/*
*   PathHistory - Type SEQUENCE OF
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_PathHistory_t& p, Document::AllocatorType& allocator);



/*
*   EmergencyPriority - Type BIT STRING
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json_ITS_Container_EmergencyPriority(const ITS_Container_EmergencyPriority_t p, Document::AllocatorType& allocator);



/*
*   SteeringWheelAngle - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_SteeringWheelAngle_t& p, Document::AllocatorType& allocator);



/*
*   YawRate - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_YawRate_t& p, Document::AllocatorType& allocator);



/*
*   ActionID - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_ActionID_t& p, Document::AllocatorType& allocator);



/*
*   ItineraryPath - Type SEQUENCE OF
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_ItineraryPath_t& p, Document::AllocatorType& allocator);



/*
*   ProtectedCommunicationZone - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_ProtectedCommunicationZone_t& p, Document::AllocatorType& allocator);



/*
*   Traces - Type SEQUENCE OF
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_Traces_t& p, Document::AllocatorType& allocator);



/*
*   PositionOfPillars - Type SEQUENCE OF
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_PositionOfPillars_t& p, Document::AllocatorType& allocator);



/*
*   RestrictedTypes - Type SEQUENCE OF
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_RestrictedTypes_t& p, Document::AllocatorType& allocator);



/*
*   EventHistory - Type SEQUENCE OF
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_EventHistory_t& p, Document::AllocatorType& allocator);



/*
*   EventPoint - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_EventPoint_t& p, Document::AllocatorType& allocator);



/*
*   ProtectedCommunicationZonesRSU - Type SEQUENCE OF
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_ProtectedCommunicationZonesRSU_t& p, Document::AllocatorType& allocator);



/*
*   CenDsrcTollingZone - Type SEQUENCE
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_CenDsrcTollingZone_t& p, Document::AllocatorType& allocator);



/*
*   DigitalMap - Type SEQUENCE OF
*   From ITS-Container - File TS102894-2v131-CDD.asn
*/

Value to_json(const ITS_Container_DigitalMap_t& p, Document::AllocatorType& allocator);



/*
*   LaneAttributes-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const LaneAttributes_addGrpC& p, Document::AllocatorType& allocator);



/*
*   MovementEvent-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const MovementEvent_addGrpC& p, Document::AllocatorType& allocator);



/*
*   Position3D-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const Position3D_addGrpC& p, Document::AllocatorType& allocator);



/*
*   RestrictionUserType-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const RestrictionUserType_addGrpC& p, Document::AllocatorType& allocator);



/*
*   RequestorDescription-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const RequestorDescription_addGrpC& p, Document::AllocatorType& allocator);



/*
*   SignalStatusPackage-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const SignalStatusPackage_addGrpC& p, Document::AllocatorType& allocator);



/*
*   Node - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const Node& p, Document::AllocatorType& allocator);



/*
*   NodeLink - Type SEQUENCE OF
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const NodeLink& p, Document::AllocatorType& allocator);



/*
*   PrioritizationResponse - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const PrioritizationResponse& p, Document::AllocatorType& allocator);



/*
*   PrioritizationResponseList - Type SEQUENCE OF
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const PrioritizationResponseList& p, Document::AllocatorType& allocator);



/*
*   AdvisorySpeed - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const AdvisorySpeed& p, Document::AllocatorType& allocator);



/*
*   AdvisorySpeedList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const AdvisorySpeedList_t& p, Document::AllocatorType& allocator);



/*
*   AntennaOffsetSet - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const AntennaOffsetSet& p, Document::AllocatorType& allocator);



/*
*   ComputedLane::ComputedLane__offsetXaxis - Type CHOICE
*   From DSRC - File DSRC.asn
*/

Value to_json(const ComputedLane::ComputedLane__offsetXaxis& p, Document::AllocatorType& allocator);



/*
*   ComputedLane::ComputedLane__offsetYaxis - Type CHOICE
*   From DSRC - File DSRC.asn
*/

Value to_json(const ComputedLane::ComputedLane__offsetYaxis& p, Document::AllocatorType& allocator);



/*
*   ComputedLane - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const ComputedLane_t& p, Document::AllocatorType& allocator);



/*
*   ConnectionManeuverAssist - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const ConnectionManeuverAssist_t& p, Document::AllocatorType& allocator);



/*
*   DataParameters - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const DataParameters_t& p, Document::AllocatorType& allocator);



/*
*   DDateTime - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const DDateTime& p, Document::AllocatorType& allocator);



/*
*   EnabledLaneList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const EnabledLaneList_t& p, Document::AllocatorType& allocator);



/*
*   IntersectionAccessPoint - Type CHOICE
*   From DSRC - File DSRC.asn
*/

Value to_json(const IntersectionAccessPoint_t& p, Document::AllocatorType& allocator);



/*
*   IntersectionReferenceID - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const IntersectionReferenceID& p, Document::AllocatorType& allocator);



/*
*   LaneSharing - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneSharing(const LaneSharing_t p, Document::AllocatorType& allocator);



/*
*   ManeuverAssistList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const ManeuverAssistList& p, Document::AllocatorType& allocator);



/*
*   NodeAttributeXYList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const NodeAttributeXYList& p, Document::AllocatorType& allocator);



/*
*   Node-LLmD-64b - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const Node_LLmD_64b& p, Document::AllocatorType& allocator);



/*
*   Node-XY-20b - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const Node_XY_20b& p, Document::AllocatorType& allocator);



/*
*   Node-XY-22b - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const Node_XY_22b& p, Document::AllocatorType& allocator);



/*
*   Node-XY-24b - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const Node_XY_24b& p, Document::AllocatorType& allocator);



/*
*   Node-XY-26b - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const Node_XY_26b& p, Document::AllocatorType& allocator);



/*
*   Node-XY-28b - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const Node_XY_28b& p, Document::AllocatorType& allocator);



/*
*   Node-XY-32b - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const Node_XY_32b& p, Document::AllocatorType& allocator);



/*
*   NodeOffsetPointXY - Type CHOICE
*   From DSRC - File DSRC.asn
*/

Value to_json(const NodeOffsetPointXY& p, Document::AllocatorType& allocator);



/*
*   OverlayLaneList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const OverlayLaneList& p, Document::AllocatorType& allocator);



/*
*   PositionalAccuracy - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const PositionalAccuracy& p, Document::AllocatorType& allocator);



/*
*   PositionConfidenceSet - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const PositionConfidenceSet& p, Document::AllocatorType& allocator);



/*
*   Position3D - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const Position3D_t& p, Document::AllocatorType& allocator);



/*
*   RegulatorySpeedLimit - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RegulatorySpeedLimit& p, Document::AllocatorType& allocator);



/*
*   RequestorType - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RequestorType& p, Document::AllocatorType& allocator);



/*
*   RestrictionUserType - Type CHOICE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RestrictionUserType& p, Document::AllocatorType& allocator);



/*
*   RestrictionUserTypeList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const RestrictionUserTypeList& p, Document::AllocatorType& allocator);



/*
*   RoadSegmentReferenceID - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RoadSegmentReferenceID& p, Document::AllocatorType& allocator);



/*
*   SegmentAttributeXYList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const SegmentAttributeXYList& p, Document::AllocatorType& allocator);



/*
*   SignalControlZone - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalControlZone& p, Document::AllocatorType& allocator);



/*
*   SignalRequest - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalRequest& p, Document::AllocatorType& allocator);



/*
*   SignalRequestPackage - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalRequestPackage& p, Document::AllocatorType& allocator);



/*
*   SpeedandHeadingIsoandThrottleConfidence - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SpeedandHeadingIsoandThrottleConfidence& p, Document::AllocatorType& allocator);



/*
*   SpeedLimitList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const SpeedLimitList& p, Document::AllocatorType& allocator);



/*
*   TimeChangeDetails - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const TimeChangeDetails& p, Document::AllocatorType& allocator);



/*
*   TransmissionAndSpeed - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const TransmissionAndSpeed_t& p, Document::AllocatorType& allocator);



/*
*   VehicleID - Type CHOICE
*   From DSRC - File DSRC.asn
*/

Value to_json(const VehicleID_t& p, Document::AllocatorType& allocator);



/*
*   AllowedManeuvers - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_AllowedManeuvers(const AllowedManeuvers_t p, Document::AllocatorType& allocator);



/*
*   GNSSstatus - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_GNSSstatus(const GNSSstatus_t p, Document::AllocatorType& allocator);



/*
*   IntersectionStatusObject - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_IntersectionStatusObject(const IntersectionStatusObject_t p, Document::AllocatorType& allocator);



/*
*   LaneAttributes-Barrier - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneAttributes_Barrier(const LaneAttributes_Barrier_t p, Document::AllocatorType& allocator);



/*
*   LaneAttributes-Bike - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneAttributes_Bike(const LaneAttributes_Bike_t p, Document::AllocatorType& allocator);



/*
*   LaneAttributes-Crosswalk - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneAttributes_Crosswalk(const LaneAttributes_Crosswalk_t p, Document::AllocatorType& allocator);



/*
*   LaneAttributes-Parking - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneAttributes_Parking(const LaneAttributes_Parking_t p, Document::AllocatorType& allocator);



/*
*   LaneAttributes-Sidewalk - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneAttributes_Sidewalk(const LaneAttributes_Sidewalk_t p, Document::AllocatorType& allocator);



/*
*   LaneAttributes-Striping - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneAttributes_Striping(const LaneAttributes_Striping_t p, Document::AllocatorType& allocator);



/*
*   LaneAttributes-TrackedVehicle - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneAttributes_TrackedVehicle(const LaneAttributes_TrackedVehicle_t p, Document::AllocatorType& allocator);



/*
*   LaneAttributes-Vehicle - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneAttributes_Vehicle(const LaneAttributes_Vehicle_t p, Document::AllocatorType& allocator);



/*
*   LaneDirection - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_LaneDirection(const LaneDirection_t p, Document::AllocatorType& allocator);



/*
*   TransitVehicleStatus - Type BIT STRING
*   From DSRC - File DSRC.asn
*/

Value to_json_TransitVehicleStatus(const TransitVehicleStatus_t p, Document::AllocatorType& allocator);



/*
*   CS5 - Type SEQUENCE
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json(const CS5& p, Document::AllocatorType& allocator);



/*
*   FreightContainerData - Type SEQUENCE
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json(const FreightContainerData_t& p, Document::AllocatorType& allocator);



/*
*   CountryCode - Type BIT STRING
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json_AVIAEINumberingAndDataStructures_CountryCode(const AVIAEINumberingAndDataStructures_CountryCode_t p, Document::AllocatorType& allocator);



/*
*   ServiceNumber - Type BIT STRING
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json_ServiceNumber(const ServiceNumber_t p, Document::AllocatorType& allocator);



/*
*   GeoGraphicalLimit - Type BIT STRING
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json_GeoGraphicalLimit(const GeoGraphicalLimit_t p, Document::AllocatorType& allocator);



/*
*   ServiceApplicationLimit - Type BIT STRING
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json_ServiceApplicationLimit(const ServiceApplicationLimit_t p, Document::AllocatorType& allocator);



/*
*   GddStructure::GddStructure__pictogramCode::GddStructure__pictogramCode__serviceCategoryCode - Type CHOICE
*   From GDD - File ISO14823.asn
*/

Value to_json(const GddStructure::GddStructure__pictogramCode::GddStructure__pictogramCode__serviceCategoryCode& p, Document::AllocatorType& allocator);



/*
*   GddStructure::GddStructure__pictogramCode::GddStructure__pictogramCode__pictogramCategoryCode - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const GddStructure::GddStructure__pictogramCode::GddStructure__pictogramCode__pictogramCategoryCode& p, Document::AllocatorType& allocator);



/*
*   GddStructure::GddStructure__pictogramCode - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const GddStructure::GddStructure__pictogramCode& p, Document::AllocatorType& allocator);



/*
*   InternationalSign-applicablePeriod::InternationalSign-applicablePeriod__year - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const InternationalSign_applicablePeriod::InternationalSign_applicablePeriod__year& p, Document::AllocatorType& allocator);



/*
*   InternationalSign-speedLimits - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const InternationalSign_speedLimits& p, Document::AllocatorType& allocator);



/*
*   DayOfWeek - Type BIT STRING
*   From GDD - File ISO14823.asn
*/

Value to_json_DayOfWeek(const DayOfWeek_t p, Document::AllocatorType& allocator);



/*
*   DestinationPlace - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const DestinationPlace& p, Document::AllocatorType& allocator);



/*
*   DestinationPlaces - Type SEQUENCE OF
*   From GDD - File ISO14823.asn
*/

Value to_json(const DestinationPlaces& p, Document::AllocatorType& allocator);



/*
*   DestinationRoad - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const DestinationRoad& p, Document::AllocatorType& allocator);



/*
*   DestinationRoads - Type SEQUENCE OF
*   From GDD - File ISO14823.asn
*/

Value to_json(const DestinationRoads& p, Document::AllocatorType& allocator);



/*
*   Distance - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const Distance& p, Document::AllocatorType& allocator);



/*
*   DistanceOrDuration - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const DistanceOrDuration& p, Document::AllocatorType& allocator);



/*
*   HoursMinutes - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const HoursMinutes& p, Document::AllocatorType& allocator);



/*
*   MonthDay - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const MonthDay& p, Document::AllocatorType& allocator);



/*
*   RepeatingPeriodDayTypes - Type BIT STRING
*   From GDD - File ISO14823.asn
*/

Value to_json_RepeatingPeriodDayTypes(const RepeatingPeriodDayTypes_t p, Document::AllocatorType& allocator);



/*
*   Weight - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const Weight& p, Document::AllocatorType& allocator);



/*
*   AxleWeightLimits - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const AxleWeightLimits_t& p, Document::AllocatorType& allocator);



/*
*   AddRq - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const AddRq_t& p, Document::AllocatorType& allocator);



/*
*   ChannelRq - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const ChannelRq_t& p, Document::AllocatorType& allocator);



/*
*   ChannelRs - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const ChannelRs_t& p, Document::AllocatorType& allocator);



/*
*   CopyRq - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const CopyRq_t& p, Document::AllocatorType& allocator);



/*
*   GetInstanceRq - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const GetInstanceRq_t& p, Document::AllocatorType& allocator);



/*
*   SubRq - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const SubRq_t& p, Document::AllocatorType& allocator);



/*
*   DateCompact - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const DateCompact_t& p, Document::AllocatorType& allocator);



/*
*   DieselEmissionValues::DieselEmissionValues__particulate - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const DieselEmissionValues::DieselEmissionValues__particulate& p, Document::AllocatorType& allocator);



/*
*   DieselEmissionValues - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const DieselEmissionValues_t& p, Document::AllocatorType& allocator);



/*
*   EfcDsrcApplication_DriverCharacteristics - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const EfcDsrcApplication_DriverCharacteristics_t& p, Document::AllocatorType& allocator);



/*
*   EFC-ContextMark - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const EFC_ContextMark_t& p, Document::AllocatorType& allocator);



/*
*   EnvironmentalCharacteristics - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const EnvironmentalCharacteristics_t& p, Document::AllocatorType& allocator);



/*
*   Engine - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const Engine_t& p, Document::AllocatorType& allocator);



/*
*   EquipmentStatus - Type BIT STRING
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json_EquipmentStatus(const EquipmentStatus_t p, Document::AllocatorType& allocator);



/*
*   ExhaustEmissionValues - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const ExhaustEmissionValues_t& p, Document::AllocatorType& allocator);



/*
*   LPN - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const LPN_t& p, Document::AllocatorType& allocator);



/*
*   PassengerCapacity - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const PassengerCapacity_t& p, Document::AllocatorType& allocator);



/*
*   SignedValue - Type CHOICE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const SignedValue_t& p, Document::AllocatorType& allocator);



/*
*   Provider - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const EfcDsrcApplication_Provider_t& p, Document::AllocatorType& allocator);



/*
*   PurseBalance - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const PurseBalance_t& p, Document::AllocatorType& allocator);



/*
*   ReceiptContract - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const ReceiptContract_t& p, Document::AllocatorType& allocator);



/*
*   SessionClass - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const SessionClass_t& p, Document::AllocatorType& allocator);



/*
*   SessionLocation - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const SessionLocation_t& p, Document::AllocatorType& allocator);



/*
*   DateAndTime::DateAndTime__timeCompact - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const DateAndTime::DateAndTime__timeCompact& p, Document::AllocatorType& allocator);



/*
*   DateAndTime - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const DateAndTime_t& p, Document::AllocatorType& allocator);



/*
*   SoundLevel - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const SoundLevel_t& p, Document::AllocatorType& allocator);



/*
*   TrailerDetails - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const TrailerDetails_t& p, Document::AllocatorType& allocator);



/*
*   ValidityOfContract - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const ValidityOfContract_t& p, Document::AllocatorType& allocator);



/*
*   VehicleAxles::VehicleAxles__vehicleAxlesNumber::VehicleAxles__vehicleAxlesNumber__numberOfAxles - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const VehicleAxles::VehicleAxles__vehicleAxlesNumber::VehicleAxles__vehicleAxlesNumber__numberOfAxles& p, Document::AllocatorType& allocator);



/*
*   VehicleAxles::VehicleAxles__vehicleAxlesNumber - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const VehicleAxles::VehicleAxles__vehicleAxlesNumber& p, Document::AllocatorType& allocator);



/*
*   VehicleAxles - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const VehicleAxles_t& p, Document::AllocatorType& allocator);



/*
*   VehicleDimensions - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const VehicleDimensions_t& p, Document::AllocatorType& allocator);



/*
*   VehicleSpecificCharacteristics - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const VehicleSpecificCharacteristics_t& p, Document::AllocatorType& allocator);



/*
*   VehicleWeightLimits - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const VehicleWeightLimits_t& p, Document::AllocatorType& allocator);



/*
*   Ext1 - Type CHOICE
*   From CITSapplMgmtIDs - File ISO17419.asn
*/

Value to_json(const CITSapplMgmtIDs_Ext1_t& p, Document::AllocatorType& allocator);



/*
*   Ext2 - Type CHOICE
*   From CITSapplMgmtIDs - File ISO17419.asn
*/

Value to_json(const CITSapplMgmtIDs_Ext2_t& p, Document::AllocatorType& allocator);



/*
*   EuVehicleCategoryCode - Type CHOICE
*   From ElectronicRegistrationIdentificationVehicleDataModule - File ISO24534-3.asn
*/

Value to_json(const ElectronicRegistrationIdentificationVehicleDataModule_EuVehicleCategoryCode_t& p, Document::AllocatorType& allocator);



/*
*   ConnectedDenms - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ConnectedDenms& p, Document::AllocatorType& allocator);



/*
*   DeltaReferencePositions - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const DeltaReferencePositions& p, Document::AllocatorType& allocator);



/*
*   IviIdentificationNumbers - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const IviIdentificationNumbers& p, Document::AllocatorType& allocator);



/*
*   LaneIds - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const LaneIds& p, Document::AllocatorType& allocator);



/*
*   LanePositions - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const LanePositions& p, Document::AllocatorType& allocator);



/*
*   SaeAutomationLevels - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const SaeAutomationLevels& p, Document::AllocatorType& allocator);



/*
*   ZoneIds - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ZoneIds& p, Document::AllocatorType& allocator);



/*
*   AbsolutePosition - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const AbsolutePosition& p, Document::AllocatorType& allocator);



/*
*   AbsolutePositionWAltitude - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const AbsolutePositionWAltitude& p, Document::AllocatorType& allocator);



/*
*   ComputedSegment - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ComputedSegment& p, Document::AllocatorType& allocator);



/*
*   DeltaPosition - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const DeltaPosition& p, Document::AllocatorType& allocator);



/*
*   ISO14823Code::ISO14823Code__pictogramCode::ISO14823Code__pictogramCode__serviceCategoryCode - Type CHOICE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ISO14823Code::ISO14823Code__pictogramCode::ISO14823Code__pictogramCode__serviceCategoryCode& p, Document::AllocatorType& allocator);



/*
*   ISO14823Code::ISO14823Code__pictogramCode::ISO14823Code__pictogramCode__pictogramCategoryCode - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ISO14823Code::ISO14823Code__pictogramCode::ISO14823Code__pictogramCode__pictogramCategoryCode& p, Document::AllocatorType& allocator);



/*
*   ISO14823Code::ISO14823Code__pictogramCode - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ISO14823Code::ISO14823Code__pictogramCode& p, Document::AllocatorType& allocator);



/*
*   LaneCharacteristics - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const LaneCharacteristics& p, Document::AllocatorType& allocator);



/*
*   LayoutComponent - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const LayoutComponent& p, Document::AllocatorType& allocator);



/*
*   LoadType - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const LoadType& p, Document::AllocatorType& allocator);



/*
*   MapReference - Type CHOICE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const IVI_MapReference_t& p, Document::AllocatorType& allocator);



/*
*   RoadSurfaceDynamicCharacteristics - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const RoadSurfaceDynamicCharacteristics& p, Document::AllocatorType& allocator);



/*
*   RoadSurfaceStaticCharacteristics - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const RoadSurfaceStaticCharacteristics& p, Document::AllocatorType& allocator);



/*
*   Text - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const Text& p, Document::AllocatorType& allocator);



/*
*   VehicleCharacteristicsFixValues - Type CHOICE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const VehicleCharacteristicsFixValues& p, Document::AllocatorType& allocator);



/*
*   VehicleCharacteristicsRanges::VehicleCharacteristicsRanges__limits - Type CHOICE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const VehicleCharacteristicsRanges::VehicleCharacteristicsRanges__limits& p, Document::AllocatorType& allocator);



/*
*   VehicleCharacteristicsRanges - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const VehicleCharacteristicsRanges& p, Document::AllocatorType& allocator);



/*
*   BasicContainer - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const CAM_PDU_Descriptions_BasicContainer_t& p, Document::AllocatorType& allocator);



/*
*   BasicVehicleContainerHighFrequency - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const BasicVehicleContainerHighFrequency& p, Document::AllocatorType& allocator);



/*
*   BasicVehicleContainerLowFrequency - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const BasicVehicleContainerLowFrequency& p, Document::AllocatorType& allocator);



/*
*   PublicTransportContainer - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const PublicTransportContainer& p, Document::AllocatorType& allocator);



/*
*   SpecialTransportContainer - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const SpecialTransportContainer& p, Document::AllocatorType& allocator);



/*
*   DangerousGoodsContainer - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const DangerousGoodsContainer& p, Document::AllocatorType& allocator);



/*
*   RoadWorksContainerBasic - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const RoadWorksContainerBasic& p, Document::AllocatorType& allocator);



/*
*   RescueContainer - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const RescueContainer& p, Document::AllocatorType& allocator);



/*
*   EmergencyContainer - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const EmergencyContainer& p, Document::AllocatorType& allocator);



/*
*   SafetyCarContainer - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const SafetyCarContainer& p, Document::AllocatorType& allocator);



/*
*   RSUContainerHighFrequency - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const RSUContainerHighFrequency& p, Document::AllocatorType& allocator);



/*
*   ManagementContainer - Type SEQUENCE
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const DENM_PDU_Descriptions_ManagementContainer& p, Document::AllocatorType& allocator);



/*
*   SituationContainer - Type SEQUENCE
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const SituationContainer& p, Document::AllocatorType& allocator);



/*
*   LocationContainer - Type SEQUENCE
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const LocationContainer& p, Document::AllocatorType& allocator);



/*
*   ImpactReductionContainer - Type SEQUENCE
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const ImpactReductionContainer& p, Document::AllocatorType& allocator);



/*
*   StationaryVehicleContainer - Type SEQUENCE
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const StationaryVehicleContainer& p, Document::AllocatorType& allocator);



/*
*   ReferenceDenms - Type SEQUENCE OF
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const ReferenceDenms& p, Document::AllocatorType& allocator);



/*
*   MapPosition - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_MapPosition_t& p, Document::AllocatorType& allocator);



/*
*   VruLowFrequencyContainer - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VruLowFrequencyContainer& p, Document::AllocatorType& allocator);



/*
*   VruProfileAndSubprofile - Type CHOICE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_VruProfileAndSubprofile_t& p, Document::AllocatorType& allocator);



/*
*   VruExteriorLights - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_VruExteriorLights_t& p, Document::AllocatorType& allocator);



/*
*   VruSpecificExteriorLights - Type BIT STRING
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json_VAM_PDU_Descriptions_VruSpecificExteriorLights(const VAM_PDU_Descriptions_VruSpecificExteriorLights_t p, Document::AllocatorType& allocator);



/*
*   ClusterProfiles - Type BIT STRING
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json_ClusterProfiles(const ClusterProfiles_t p, Document::AllocatorType& allocator);



/*
*   VruClusterOperationContainer - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VruClusterOperationContainer& p, Document::AllocatorType& allocator);



/*
*   ClusterJoinInfo - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_ClusterJoinInfo_t& p, Document::AllocatorType& allocator);



/*
*   ClusterLeaveInfo - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_ClusterLeaveInfo_t& p, Document::AllocatorType& allocator);



/*
*   ClusterBreakupInfo - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_ClusterBreakupInfo_t& p, Document::AllocatorType& allocator);



/*
*   VruPathPoint - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VruPathPoint& p, Document::AllocatorType& allocator);



/*
*   VruSafeDistanceIndication - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VruSafeDistanceIndication& p, Document::AllocatorType& allocator);



/*
*   SequenceOfTrajectoryInterceptionIndication - Type SEQUENCE OF
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_SequenceOfTrajectoryInterceptionIndication_t& p, Document::AllocatorType& allocator);



/*
*   TrajectoryInterceptionIndication - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_TrajectoryInterceptionIndication_t& p, Document::AllocatorType& allocator);



/*
*   HeadingChangeIndication - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_HeadingChangeIndication_t& p, Document::AllocatorType& allocator);



/*
*   AccelerationChangeIndication - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_AccelerationChangeIndication_t& p, Document::AllocatorType& allocator);



/*
*   StabilityChangeIndication - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_StabilityChangeIndication_t& p, Document::AllocatorType& allocator);



/*
*   NodeOffsetPointZ - Type CHOICE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const NodeOffsetPointZ& p, Document::AllocatorType& allocator);



/*
*   LaneDataAttribute - Type CHOICE
*   From DSRC-REGION-noCircular - File DSRC_REGION_noCircular.asn
*/

Value to_json(const LaneDataAttribute& p, Document::AllocatorType& allocator);



/*
*   LaneDataAttributeList - Type SEQUENCE OF
*   From DSRC-REGION-noCircular - File DSRC_REGION_noCircular.asn
*/

Value to_json(const LaneDataAttributeList& p, Document::AllocatorType& allocator);



/*
*   NodeAttributeSetXY - Type SEQUENCE
*   From DSRC-REGION-noCircular - File DSRC_REGION_noCircular.asn
*/

Value to_json(const NodeAttributeSetXY_t& p, Document::AllocatorType& allocator);



/*
*   CpmPayload - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const CpmPayload& p, Document::AllocatorType& allocator);



/*
*   MessageRateRange - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const MessageRateRange& p, Document::AllocatorType& allocator);



/*
*   OriginatingRsuContainer - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const OriginatingRsuContainer& p, Document::AllocatorType& allocator);



/*
*   TrailerDataSet - Type SEQUENCE OF
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const TrailerDataSet& p, Document::AllocatorType& allocator);



/*
*   PerceivedObjectIds - Type SEQUENCE OF
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const PerceivedObjectIds& p, Document::AllocatorType& allocator);



/*
*   ItsPOIHeader - Type SEQUENCE
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const ItsPOIHeader& p, Document::AllocatorType& allocator);



/*
*   ChargingSpotType - Type BIT STRING
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json_ChargingSpotType(const ChargingSpotType_t p, Document::AllocatorType& allocator);



/*
*   TypeOfReceptacle - Type BIT STRING
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json_TypeOfReceptacle(const TypeOfReceptacle_t p, Document::AllocatorType& allocator);



/*
*   SpotAvailability - Type SEQUENCE
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const SpotAvailability& p, Document::AllocatorType& allocator);



/*
*   Payment-ID - Type CHOICE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const Payment_ID& p, Document::AllocatorType& allocator);



/*
*   RechargingType - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const RechargingType& p, Document::AllocatorType& allocator);



/*
*   SupportedPaymentTypes - Type BIT STRING
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json_SupportedPaymentTypes(const SupportedPaymentTypes_t p, Document::AllocatorType& allocator);



/*
*   InterferenceManagementChannel - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const IMZM_PDU_Descriptions_InterferenceManagementChannel_t& p, Document::AllocatorType& allocator);



/*
*   MitigationForTechnologies - Type SEQUENCE OF
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const IMZM_PDU_Descriptions_MitigationForTechnologies_t& p, Document::AllocatorType& allocator);



/*
*   MitigationPerTechnologyClass - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const IMZM_PDU_Descriptions_MitigationPerTechnologyClass_t& p, Document::AllocatorType& allocator);



/*
*   TisTpgDRM-Situation - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgDRM_Situation& p, Document::AllocatorType& allocator);



/*
*   TisTpgDRM-Location - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgDRM_Location& p, Document::AllocatorType& allocator);



/*
*   TisTpgSNM-Management - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgSNM_Management& p, Document::AllocatorType& allocator);



/*
*   TisTpgTRM-Situation - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTRM_Situation& p, Document::AllocatorType& allocator);



/*
*   TisTpgTRM-Location - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTRM_Location& p, Document::AllocatorType& allocator);



/*
*   TisTpgTCM-Location - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTCM_Location& p, Document::AllocatorType& allocator);



/*
*   TyreData::TyreData__currentTyrePressure - Type CHOICE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TyreData::TyreData__currentTyrePressure& p, Document::AllocatorType& allocator);



/*
*   TyreData::TyreData__currentInsideAirTemperature - Type CHOICE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TyreData::TyreData__currentInsideAirTemperature& p, Document::AllocatorType& allocator);



/*
*   TyreData::TyreData__recommendedTyrePressure - Type CHOICE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TyreData::TyreData__recommendedTyrePressure& p, Document::AllocatorType& allocator);



/*
*   TyreData::TyreData__sensorState - Type CHOICE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TyreData::TyreData__sensorState& p, Document::AllocatorType& allocator);



/*
*   AppliedTyrePressure - Type CHOICE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const AppliedTyrePressure& p, Document::AllocatorType& allocator);



/*
*   TyreSidewallInformation - Type BIT STRING
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json_TyreSidewallInformation(const TyreSidewallInformation_t p, Document::AllocatorType& allocator);



/*
*   CurrentVehicleConfiguration - Type BIT STRING
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json_CurrentVehicleConfiguration(const CurrentVehicleConfiguration_t p, Document::AllocatorType& allocator);



/*
*   TIN - Type BIT STRING
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json_TIN(const TIN_t p, Document::AllocatorType& allocator);



/*
*   PressureConfiguration - Type BIT STRING
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json_PressureConfiguration(const PressureConfiguration_t p, Document::AllocatorType& allocator);



/*
*   AppliedTyrePressures - Type SEQUENCE OF
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const AppliedTyrePressures& p, Document::AllocatorType& allocator);



/*
*   TpgAutomation - Type BIT STRING
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json_TpgAutomation(const TpgAutomation_t p, Document::AllocatorType& allocator);



/*
*   TisProfile - Type BIT STRING
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json_TisProfile(const TisProfile_t p, Document::AllocatorType& allocator);



/*
*   Language - Type BIT STRING
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json_Language(const Language_t p, Document::AllocatorType& allocator);



/*
*   McmBasicContainer - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const McmBasicContainer& p, Document::AllocatorType& allocator);



/*
*   ManeuverCoordinationRational - Type CHOICE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const ManeuverCoordinationRational& p, Document::AllocatorType& allocator);



/*
*   McmGenericCurrentState - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const McmGenericCurrentState& p, Document::AllocatorType& allocator);



/*
*   VehicleAutomationState - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const VehicleAutomationState& p, Document::AllocatorType& allocator);



/*
*   VehicleSize - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const VehicleSize& p, Document::AllocatorType& allocator);



/*
*   ReferenceStartingPosition - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const ReferenceStartingPosition& p, Document::AllocatorType& allocator);



/*
*   IntermediatePointIntersection::IntermediatePointIntersection__exitLane - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const IntermediatePointIntersection::IntermediatePointIntersection__exitLane& p, Document::AllocatorType& allocator);



/*
*   IntermediatePointIntersection - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const IntermediatePointIntersection& p, Document::AllocatorType& allocator);



/*
*   IntermediatePointOffroad - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const IntermediatePointOffroad& p, Document::AllocatorType& allocator);



/*
*   Lane - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const Lane& p, Document::AllocatorType& allocator);



/*
*   AcknowledgmentContainer - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const AcknowledgmentContainer& p, Document::AllocatorType& allocator);



/*
*   ManeuverResponse - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const ManeuverResponse_t& p, Document::AllocatorType& allocator);



/*
*   AccelerationPolarWithZ - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const AccelerationPolarWithZ_t& p, Document::AllocatorType& allocator);



/*
*   AccelerationCartesian - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const AccelerationCartesian_t& p, Document::AllocatorType& allocator);



/*
*   BasicContainer - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_BasicContainer_t& p, Document::AllocatorType& allocator);



/*
*   BasicLaneConfiguration - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const BasicLaneConfiguration_t& p, Document::AllocatorType& allocator);



/*
*   DigitalMap - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_DigitalMap_t& p, Document::AllocatorType& allocator);



/*
*   EventHistory - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_EventHistory_t& p, Document::AllocatorType& allocator);



/*
*   InterferenceManagementInfoPerChannel - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_InterferenceManagementInfoPerChannel_t& p, Document::AllocatorType& allocator);



/*
*   ItineraryPath - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_ItineraryPath_t& p, Document::AllocatorType& allocator);



/*
*   IvimReference - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const IvimReference_t& p, Document::AllocatorType& allocator);



/*
*   IvimReferences - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const IvimReferences_t& p, Document::AllocatorType& allocator);



/*
*   LanePositionOptions - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const LanePositionOptions_t& p, Document::AllocatorType& allocator);



/*
*   LowerTriangularPositiveSemidefiniteMatrix - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const LowerTriangularPositiveSemidefiniteMatrix_t& p, Document::AllocatorType& allocator);



/*
*   MapemElementReference - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const MapemElementReference_t& p, Document::AllocatorType& allocator);



/*
*   MapPosition - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_MapPosition_t& p, Document::AllocatorType& allocator);



/*
*   MapReference - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_MapReference_t& p, Document::AllocatorType& allocator);



/*
*   MapReferences - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const MapReferences_t& p, Document::AllocatorType& allocator);



/*
*   MitigationForTechnologies - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_MitigationForTechnologies_t& p, Document::AllocatorType& allocator);



/*
*   OccupiedLanesWithConfidence::OccupiedLanesWithConfidence__lanePositionBased - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const OccupiedLanesWithConfidence::OccupiedLanesWithConfidence__lanePositionBased& p, Document::AllocatorType& allocator);



/*
*   OccupiedLanesWithConfidence::OccupiedLanesWithConfidence__mapBased - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const OccupiedLanesWithConfidence::OccupiedLanesWithConfidence__mapBased& p, Document::AllocatorType& allocator);



/*
*   OccupiedLanesWithConfidence - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const OccupiedLanesWithConfidence_t& p, Document::AllocatorType& allocator);



/*
*   Path - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const Path_t& p, Document::AllocatorType& allocator);



/*
*   PathExtended - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PathExtended_t& p, Document::AllocatorType& allocator);



/*
*   PathHistory - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_PathHistory_t& p, Document::AllocatorType& allocator);



/*
*   PathPointPredicted - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PathPointPredicted_t& p, Document::AllocatorType& allocator);



/*
*   PolygonalShape - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PolygonalShape_t& p, Document::AllocatorType& allocator);



/*
*   RadialShapesList - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const RadialShapesList_t& p, Document::AllocatorType& allocator);



/*
*   SequenceOfTrajectoryInterceptionIndication - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_SequenceOfTrajectoryInterceptionIndication_t& p, Document::AllocatorType& allocator);



/*
*   Traces - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_Traces_t& p, Document::AllocatorType& allocator);



/*
*   TracesExtended - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const TracesExtended_t& p, Document::AllocatorType& allocator);



/*
*   VarLengthNumber - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_VarLengthNumber_t& p, Document::AllocatorType& allocator);



/*
*   Ext1 - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_Ext1_t& p, Document::AllocatorType& allocator);



/*
*   VelocityCartesian - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const VelocityCartesian_t& p, Document::AllocatorType& allocator);



/*
*   IntersectionState-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const IntersectionState_addGrpC& p, Document::AllocatorType& allocator);



/*
*   NodeAttributeSet-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const NodeAttributeSet_addGrpC& p, Document::AllocatorType& allocator);



/*
*   ItsStationPosition - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const ItsStationPosition_t& p, Document::AllocatorType& allocator);



/*
*   ItsStationPositionList - Type SEQUENCE OF
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const ItsStationPositionList_t& p, Document::AllocatorType& allocator);



/*
*   SignalHeadLocation - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const SignalHeadLocation_t& p, Document::AllocatorType& allocator);



/*
*   SignalHeadLocationList - Type SEQUENCE OF
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const SignalHeadLocationList_t& p, Document::AllocatorType& allocator);



/*
*   ConnectingLane - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const ConnectingLane& p, Document::AllocatorType& allocator);



/*
*   Connection - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const Connection& p, Document::AllocatorType& allocator);



/*
*   FullPositionVector - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const FullPositionVector& p, Document::AllocatorType& allocator);



/*
*   LaneTypeAttributes - Type CHOICE
*   From DSRC - File DSRC.asn
*/

Value to_json(const LaneTypeAttributes& p, Document::AllocatorType& allocator);



/*
*   MovementEvent - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const MovementEvent& p, Document::AllocatorType& allocator);



/*
*   MovementEventList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const MovementEventList& p, Document::AllocatorType& allocator);



/*
*   MovementState - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const MovementState& p, Document::AllocatorType& allocator);



/*
*   NodeXY - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const NodeXY_t& p, Document::AllocatorType& allocator);



/*
*   NodeSetXY - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const NodeSetXY& p, Document::AllocatorType& allocator);



/*
*   RequestorPositionVector - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RequestorPositionVector& p, Document::AllocatorType& allocator);



/*
*   RestrictionClassAssignment - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RestrictionClassAssignment& p, Document::AllocatorType& allocator);



/*
*   RestrictionClassList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const RestrictionClassList& p, Document::AllocatorType& allocator);



/*
*   RTCMheader - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RTCMheader& p, Document::AllocatorType& allocator);



/*
*   RTCMmessageList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const RTCMmessageList& p, Document::AllocatorType& allocator);



/*
*   SignalRequesterInfo - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalRequesterInfo& p, Document::AllocatorType& allocator);



/*
*   SignalRequestList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalRequestList& p, Document::AllocatorType& allocator);



/*
*   SignalStatusPackage - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalStatusPackage& p, Document::AllocatorType& allocator);



/*
*   CS1 - Type SEQUENCE
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json(const CS1& p, Document::AllocatorType& allocator);



/*
*   CS2 - Type SEQUENCE
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json(const CS2& p, Document::AllocatorType& allocator);



/*
*   CS3 - Type SEQUENCE
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json(const CS3& p, Document::AllocatorType& allocator);



/*
*   CS4 - Type SEQUENCE
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json(const CS4& p, Document::AllocatorType& allocator);



/*
*   CS8 - Type SEQUENCE
*   From AVIAEINumberingAndDataStructures - File ISO14816.asn
*/

Value to_json(const CS8& p, Document::AllocatorType& allocator);



/*
*   InternationalSign-applicablePeriod::InternationalSign-applicablePeriod__month-day - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const InternationalSign_applicablePeriod::InternationalSign_applicablePeriod__month_day& p, Document::AllocatorType& allocator);



/*
*   InternationalSign-applicablePeriod::InternationalSign-applicablePeriod__hourMinutes - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const InternationalSign_applicablePeriod::InternationalSign_applicablePeriod__hourMinutes& p, Document::AllocatorType& allocator);



/*
*   InternationalSign-applicablePeriod - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const InternationalSign_applicablePeriod& p, Document::AllocatorType& allocator);



/*
*   InternationalSign-applicableVehicleDimensions - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const InternationalSign_applicableVehicleDimensions& p, Document::AllocatorType& allocator);



/*
*   InternationalSign-section - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const InternationalSign_section& p, Document::AllocatorType& allocator);



/*
*   DDD-IO - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const DDD_IO& p, Document::AllocatorType& allocator);



/*
*   CreditRs - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const CreditRs_t& p, Document::AllocatorType& allocator);



/*
*   DebitRs - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const DebitRs_t& p, Document::AllocatorType& allocator);



/*
*   ContractValidity - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const ContractValidity_t& p, Document::AllocatorType& allocator);



/*
*   PaymentFee - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const PaymentFee_t& p, Document::AllocatorType& allocator);



/*
*   PaymentMeans - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const PaymentMeans_t& p, Document::AllocatorType& allocator);



/*
*   ReceiptData - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const ReceiptData_t& p, Document::AllocatorType& allocator);



/*
*   ReceiptFinancialPart - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const ReceiptFinancialPart_t& p, Document::AllocatorType& allocator);



/*
*   ReceiptServicePart - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const ReceiptServicePart_t& p, Document::AllocatorType& allocator);



/*
*   EfcDsrcApplication_TrailerCharacteristics - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const EfcDsrcApplication_TrailerCharacteristics_t& p, Document::AllocatorType& allocator);



/*
*   VarLengthNumber - Type CHOICE
*   From CITSapplMgmtIDs - File ISO17419.asn
*/

Value to_json(const CITSapplMgmtIDs_VarLengthNumber_t& p, Document::AllocatorType& allocator);



/*
*   IviManagementContainer - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const IviManagementContainer& p, Document::AllocatorType& allocator);



/*
*   RscPart - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const RscPart& p, Document::AllocatorType& allocator);



/*
*   MlcPart - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const MlcPart& p, Document::AllocatorType& allocator);



/*
*   AbsolutePositions - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const AbsolutePositions& p, Document::AllocatorType& allocator);



/*
*   AbsolutePositionsWAltitude - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const AbsolutePositionsWAltitude& p, Document::AllocatorType& allocator);



/*
*   DeltaPositions - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const DeltaPositions& p, Document::AllocatorType& allocator);



/*
*   ConstraintTextLines1 - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ConstraintTextLines1& p, Document::AllocatorType& allocator);



/*
*   ConstraintTextLines2 - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ConstraintTextLines2& p, Document::AllocatorType& allocator);



/*
*   LayoutComponents - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const LayoutComponents& p, Document::AllocatorType& allocator);



/*
*   TextLines - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const TextLines& p, Document::AllocatorType& allocator);



/*
*   TrailerCharacteristicsFixValuesList - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const TrailerCharacteristicsFixValuesList& p, Document::AllocatorType& allocator);



/*
*   TrailerCharacteristicsRangesList - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const TrailerCharacteristicsRangesList& p, Document::AllocatorType& allocator);



/*
*   VehicleCharacteristicsFixValuesList - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const VehicleCharacteristicsFixValuesList& p, Document::AllocatorType& allocator);



/*
*   VehicleCharacteristicsRangesList - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const VehicleCharacteristicsRangesList& p, Document::AllocatorType& allocator);



/*
*   ValidityPeriods - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ValidityPeriods& p, Document::AllocatorType& allocator);



/*
*   PolygonalLine - Type CHOICE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const PolygonalLine& p, Document::AllocatorType& allocator);



/*
*   Segment - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const Segment& p, Document::AllocatorType& allocator);



/*
*   TractorCharacteristics - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const TractorCharacteristics& p, Document::AllocatorType& allocator);



/*
*   IVI_TrailerCharacteristics - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const IVI_TrailerCharacteristics& p, Document::AllocatorType& allocator);



/*
*   VcCode - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const VcCode& p, Document::AllocatorType& allocator);



/*
*   Zone - Type CHOICE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const Zone& p, Document::AllocatorType& allocator);



/*
*   HighFrequencyContainer - Type CHOICE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const HighFrequencyContainer& p, Document::AllocatorType& allocator);



/*
*   LowFrequencyContainer - Type CHOICE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const LowFrequencyContainer& p, Document::AllocatorType& allocator);



/*
*   SpecialVehicleContainer - Type CHOICE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const SpecialVehicleContainer& p, Document::AllocatorType& allocator);



/*
*   RoadWorksContainerExtended - Type SEQUENCE
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const RoadWorksContainerExtended& p, Document::AllocatorType& allocator);



/*
*   AlacarteContainer - Type SEQUENCE
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const AlacarteContainer& p, Document::AllocatorType& allocator);



/*
*   VruLanePosition - Type CHOICE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VruLanePosition& p, Document::AllocatorType& allocator);



/*
*   NonIslandLanePosition - Type CHOICE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const NonIslandLanePosition& p, Document::AllocatorType& allocator);



/*
*   SequenceOfVruPathPoint - Type SEQUENCE OF
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const SequenceOfVruPathPoint& p, Document::AllocatorType& allocator);



/*
*   SequenceOfVruSafeDistanceIndication - Type SEQUENCE OF
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const SequenceOfVruSafeDistanceIndication& p, Document::AllocatorType& allocator);



/*
*   OffsetPoint - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const OffsetPoint& p, Document::AllocatorType& allocator);



/*
*   CollectivePerceptionMessage - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const CollectivePerceptionMessage& p, Document::AllocatorType& allocator);



/*
*   ManagementContainer - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const CPM_PDU_Descriptions_ManagementContainer& p, Document::AllocatorType& allocator);



/*
*   OriginatingVehicleContainer - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const OriginatingVehicleContainer& p, Document::AllocatorType& allocator);



/*
*   ParkingPlacesData - Type SEQUENCE OF
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const ParkingPlacesData& p, Document::AllocatorType& allocator);



/*
*   PreReservationRequestMessage - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const PreReservationRequestMessage& p, Document::AllocatorType& allocator);



/*
*   PreReservationResponseMessage - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const PreReservationResponseMessage& p, Document::AllocatorType& allocator);



/*
*   ReservationRequestMessage - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const ReservationRequestMessage& p, Document::AllocatorType& allocator);



/*
*   ReservationResponseMessage - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const ReservationResponseMessage& p, Document::AllocatorType& allocator);



/*
*   CancellationRequestMessage - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const CancellationRequestMessage& p, Document::AllocatorType& allocator);



/*
*   CancellationResponseMessage - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const CancellationResponseMessage& p, Document::AllocatorType& allocator);



/*
*   UpdateRequestMessage - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const UpdateRequestMessage& p, Document::AllocatorType& allocator);



/*
*   UpdateResponseMessage - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const UpdateResponseMessage& p, Document::AllocatorType& allocator);



/*
*   InterferenceManagementInfo - Type SEQUENCE OF
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const IMZM_PDU_Descriptions_InterferenceManagementInfo_t& p, Document::AllocatorType& allocator);



/*
*   InterferenceManagementMitigationType - Type CHOICE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const InterferenceManagementMitigationType& p, Document::AllocatorType& allocator);



/*
*   IMZMAreaEllipse - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const IMZMAreaEllipse& p, Document::AllocatorType& allocator);



/*
*   TisTpgDRM-Management - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgDRM_Management& p, Document::AllocatorType& allocator);



/*
*   TisTpgTRM-Management - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTRM_Management& p, Document::AllocatorType& allocator);



/*
*   TisTpgTCM-Management - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTCM_Management& p, Document::AllocatorType& allocator);



/*
*   TisTpgTCM-Situation - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTCM_Situation& p, Document::AllocatorType& allocator);



/*
*   TisTpgVDRM-Management - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgVDRM_Management& p, Document::AllocatorType& allocator);



/*
*   TisTpgVDPM-Management - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgVDPM_Management& p, Document::AllocatorType& allocator);



/*
*   TisTpgEOFM-Management - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgEOFM_Management& p, Document::AllocatorType& allocator);



/*
*   PressureVariant - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const PressureVariant& p, Document::AllocatorType& allocator);



/*
*   TyreData::TyreData__tyreSidewallInformation - Type CHOICE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TyreData::TyreData__tyreSidewallInformation& p, Document::AllocatorType& allocator);



/*
*   TyreData::TyreData__tin - Type CHOICE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TyreData::TyreData__tin& p, Document::AllocatorType& allocator);



/*
*   TyreData - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TyreData& p, Document::AllocatorType& allocator);



/*
*   TpgStationData - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TpgStationData& p, Document::AllocatorType& allocator);



/*
*   TpgNotifContainer - Type SEQUENCE OF
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TpgNotifContainer& p, Document::AllocatorType& allocator);



/*
*   VehicleCurrentState - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const VehicleCurrentState& p, Document::AllocatorType& allocator);



/*
*   IntermediatePointReference - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const IntermediatePointReference& p, Document::AllocatorType& allocator);



/*
*   IntermediatePointLane - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const IntermediatePointLane& p, Document::AllocatorType& allocator);



/*
*   Acceleration3dWithConfidence - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const Acceleration3dWithConfidence_t& p, Document::AllocatorType& allocator);



/*
*   GeneralizedLanePosition - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const GeneralizedLanePosition_t& p, Document::AllocatorType& allocator);



/*
*   GeneralizedLanePositions - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const GeneralizedLanePositions_t& p, Document::AllocatorType& allocator);



/*
*   InterferenceManagementInfo - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_InterferenceManagementInfo_t& p, Document::AllocatorType& allocator);



/*
*   LowerTriangularPositiveSemidefiniteMatrices - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const LowerTriangularPositiveSemidefiniteMatrices_t& p, Document::AllocatorType& allocator);



/*
*   MapemConfiguration - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const MapemConfiguration_t& p, Document::AllocatorType& allocator);



/*
*   PathPredicted - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PathPredicted_t& p, Document::AllocatorType& allocator);



/*
*   PathPredicted2 - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PathPredicted2_t& p, Document::AllocatorType& allocator);



/*
*   PathPredictedList - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PathPredictedList_t& p, Document::AllocatorType& allocator);



/*
*   RadialShapes - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const RadialShapes_t& p, Document::AllocatorType& allocator);



/*
*   RoadConfigurationSection - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const RoadConfigurationSection_t& p, Document::AllocatorType& allocator);



/*
*   RoadConfigurationSectionList - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const RoadConfigurationSectionList_t& p, Document::AllocatorType& allocator);



/*
*   Shape - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const Shape_t& p, Document::AllocatorType& allocator);



/*
*   Velocity3dWithConfidence - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const Velocity3dWithConfidence_t& p, Document::AllocatorType& allocator);



/*
*   VruClusterInformation - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const VruClusterInformation_t& p, Document::AllocatorType& allocator);



/*
*   ConnectionManeuverAssist-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const ConnectionManeuverAssist_addGrpC& p, Document::AllocatorType& allocator);



/*
*   ConnectionTrajectory-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const ConnectionTrajectory_addGrpC& p, Document::AllocatorType& allocator);



/*
*   MapData-addGrpC - Type SEQUENCE
*   From AddGrpC - File DSRC.asn
*/

Value to_json(const MapData_addGrpC& p, Document::AllocatorType& allocator);



/*
*   RTCMcorrections - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RTCMcorrections& p, Document::AllocatorType& allocator);



/*
*   ConnectsToList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const ConnectsToList& p, Document::AllocatorType& allocator);



/*
*   LaneAttributes - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const LaneAttributes& p, Document::AllocatorType& allocator);



/*
*   MovementList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const MovementList& p, Document::AllocatorType& allocator);



/*
*   NodeListXY - Type CHOICE
*   From DSRC - File DSRC.asn
*/

Value to_json(const NodeListXY& p, Document::AllocatorType& allocator);



/*
*   RequestorDescription - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RequestorDescription& p, Document::AllocatorType& allocator);



/*
*   SignalStatusPackageList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalStatusPackageList& p, Document::AllocatorType& allocator);



/*
*   DDD-IO-LIST - Type SEQUENCE OF
*   From GDD - File ISO14823.asn
*/

Value to_json(const DDD_IO_LIST& p, Document::AllocatorType& allocator);



/*
*   CreditRq - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const CreditRq_t& p, Document::AllocatorType& allocator);



/*
*   DebitRq - Type SEQUENCE
*   From EfcDsrcApplication - File ISO14906-0-6.asn
*/

Value to_json(const DebitRq_t& p, Document::AllocatorType& allocator);



/*
*   GlcPart - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const GlcPart& p, Document::AllocatorType& allocator);



/*
*   RoadSurfaceContainer - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const RoadSurfaceContainer& p, Document::AllocatorType& allocator);



/*
*   LayoutContainer - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const LayoutContainer& p, Document::AllocatorType& allocator);



/*
*   MlcParts - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const MlcParts& p, Document::AllocatorType& allocator);



/*
*   TrailerCharacteristicsList - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const TrailerCharacteristicsList& p, Document::AllocatorType& allocator);



/*
*   CompleteVehicleCharacteristics - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const CompleteVehicleCharacteristics& p, Document::AllocatorType& allocator);



/*
*   LaneInformation - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const LaneInformation& p, Document::AllocatorType& allocator);



/*
*   CamParameters - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const CamParameters& p, Document::AllocatorType& allocator);



/*
*   DecentralizedEnvironmentalNotificationMessage - Type SEQUENCE
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const DecentralizedEnvironmentalNotificationMessage& p, Document::AllocatorType& allocator);



/*
*   VruHighFrequencyContainer - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VruHighFrequencyContainer& p, Document::AllocatorType& allocator);



/*
*   TrafficIslandPosition - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM_PDU_Descriptions_TrafficIslandPosition_t& p, Document::AllocatorType& allocator);



/*
*   VruMotionPredictionContainer - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VruMotionPredictionContainer& p, Document::AllocatorType& allocator);



/*
*   AreaCircular - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const AreaCircular& p, Document::AllocatorType& allocator);



/*
*   AreaRectangle - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const AreaRectangle& p, Document::AllocatorType& allocator);



/*
*   PolyPointList - Type SEQUENCE OF
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const PolyPointList& p, Document::AllocatorType& allocator);



/*
*   PerceptionRegion - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const PerceptionRegion& p, Document::AllocatorType& allocator);



/*
*   SensorInformation - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const SensorInformation& p, Document::AllocatorType& allocator);



/*
*   ItsChargingSpotDataElements - Type SEQUENCE
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const ItsChargingSpotDataElements& p, Document::AllocatorType& allocator);



/*
*   EV-RSR-MessageBody - Type CHOICE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const EV_RSR_MessageBody& p, Document::AllocatorType& allocator);



/*
*   InterferenceManagementInfoPerChannel - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const IMZM_PDU_Descriptions_InterferenceManagementInfoPerChannel_t& p, Document::AllocatorType& allocator);



/*
*   TisTpgDRM - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgDRM& p, Document::AllocatorType& allocator);



/*
*   TisTpgSNM - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgSNM& p, Document::AllocatorType& allocator);



/*
*   TisTpgTRM - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTRM& p, Document::AllocatorType& allocator);



/*
*   TisTpgTCM - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTCM& p, Document::AllocatorType& allocator);



/*
*   TisTpgVDRM - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgVDRM& p, Document::AllocatorType& allocator);



/*
*   VehicleSpecificData - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const VehicleSpecificData& p, Document::AllocatorType& allocator);



/*
*   TisTpgEOFM - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgEOFM& p, Document::AllocatorType& allocator);



/*
*   PressureVariantsList - Type SEQUENCE OF
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const PressureVariantsList& p, Document::AllocatorType& allocator);



/*
*   RTCMEM - Type SEQUENCE
*   From RTCMEM-PDU-Descriptions - File TS103301v211-RTCMEM.asn
*/

Value to_json(const RTCMEM& p, Document::AllocatorType& allocator);



/*
*   IntermediatePoint - Type CHOICE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const IntermediatePoint& p, Document::AllocatorType& allocator);



/*
*   Wgs84TrajectoryPoint - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const Wgs84TrajectoryPoint& p, Document::AllocatorType& allocator);



/*
*   TrajectoryList - Type SEQUENCE OF
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const TrajectoryList& p, Document::AllocatorType& allocator);



/*
*   Submaneuver - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const Submaneuver& p, Document::AllocatorType& allocator);



/*
*   InterferenceManagementZoneDefinition - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const InterferenceManagementZoneDefinition_t& p, Document::AllocatorType& allocator);



/*
*   ObjectClass - Type CHOICE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ObjectClass_t& p, Document::AllocatorType& allocator);



/*
*   ObjectClassWithConfidence - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ObjectClassWithConfidence_t& p, Document::AllocatorType& allocator);



/*
*   SignalRequestMessage - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalRequestMessage& p, Document::AllocatorType& allocator);



/*
*   GenericLane - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const GenericLane& p, Document::AllocatorType& allocator);



/*
*   IntersectionState - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const IntersectionState& p, Document::AllocatorType& allocator);



/*
*   IntersectionStateList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const IntersectionStateList& p, Document::AllocatorType& allocator);



/*
*   LaneList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const LaneList& p, Document::AllocatorType& allocator);



/*
*   RoadLaneSetList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const RoadLaneSetList& p, Document::AllocatorType& allocator);



/*
*   RoadSegment - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const RoadSegment& p, Document::AllocatorType& allocator);



/*
*   RoadSegmentList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const RoadSegmentList& p, Document::AllocatorType& allocator);



/*
*   SignalStatus - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalStatus& p, Document::AllocatorType& allocator);



/*
*   SignalStatusList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalStatusList& p, Document::AllocatorType& allocator);



/*
*   InternationalSign-destinationInformation - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const InternationalSign_destinationInformation& p, Document::AllocatorType& allocator);



/*
*   GlcParts - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const GlcParts& p, Document::AllocatorType& allocator);



/*
*   MapLocationContainer - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const MapLocationContainer& p, Document::AllocatorType& allocator);



/*
*   LaneConfiguration - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const LaneConfiguration& p, Document::AllocatorType& allocator);



/*
*   VehicleCharacteristicsList - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const VehicleCharacteristicsList& p, Document::AllocatorType& allocator);



/*
*   ISO14823Attribute - Type CHOICE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ISO14823Attribute& p, Document::AllocatorType& allocator);



/*
*   CoopAwareness - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const CoopAwareness& p, Document::AllocatorType& allocator);



/*
*   DENM - Type SEQUENCE
*   From DENM-PDU-Descriptions - File EN302637-3v131-DENM.asn
*/

Value to_json(const DENM& p, Document::AllocatorType& allocator);



/*
*   AreaPolygon - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const AreaPolygon& p, Document::AllocatorType& allocator);



/*
*   PerceptionRegionContainer - Type SEQUENCE OF
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const PerceptionRegionContainer& p, Document::AllocatorType& allocator);



/*
*   SensorInformationContainer - Type SEQUENCE OF
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const SensorInformationContainer& p, Document::AllocatorType& allocator);



/*
*   SREM - Type SEQUENCE
*   From SREM-PDU-Descriptions - File TS103301v211-SREM.asn
*/

Value to_json(const SREM& p, Document::AllocatorType& allocator);



/*
*   ItsChargingSpots - Type SEQUENCE OF
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const ItsChargingSpots& p, Document::AllocatorType& allocator);



/*
*   EV-RSR - Type SEQUENCE
*   From EV-RechargingSpotReservation-PDU-Descriptions - File EV-RSR-PDU-Descriptions.asn
*/

Value to_json(const EV_RSR& p, Document::AllocatorType& allocator);



/*
*   InterferenceManagementZoneShape - Type CHOICE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const InterferenceManagementZoneShape& p, Document::AllocatorType& allocator);



/*
*   TyreSetVariant - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TyreSetVariant& p, Document::AllocatorType& allocator);



/*
*   Wgs84Trajectory - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const Wgs84Trajectory& p, Document::AllocatorType& allocator);



/*
*   Submaneuvers - Type SEQUENCE OF
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const Submaneuvers& p, Document::AllocatorType& allocator);



/*
*   InterferenceManagementZone - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_InterferenceManagementZone_t& p, Document::AllocatorType& allocator);



/*
*   InterferenceManagementZones - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ETSI_ITS_CDD_InterferenceManagementZones_t& p, Document::AllocatorType& allocator);



/*
*   ObjectClassDescription - Type SEQUENCE OF
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const ObjectClassDescription_t& p, Document::AllocatorType& allocator);



/*
*   PerceivedObject - Type SEQUENCE
*   From ETSI-ITS-CDD - File CDD-Release2.asn
*/

Value to_json(const PerceivedObject_t& p, Document::AllocatorType& allocator);



/*
*   SPAT - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SPAT& p, Document::AllocatorType& allocator);



/*
*   SignalStatusMessage - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const SignalStatusMessage& p, Document::AllocatorType& allocator);



/*
*   IntersectionGeometry - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const IntersectionGeometry& p, Document::AllocatorType& allocator);



/*
*   IntersectionGeometryList - Type SEQUENCE OF
*   From DSRC - File DSRC.asn
*/

Value to_json(const IntersectionGeometryList& p, Document::AllocatorType& allocator);



/*
*   GddAttribute - Type CHOICE
*   From GDD - File ISO14823.asn
*/

Value to_json(const GddAttribute& p, Document::AllocatorType& allocator);



/*
*   GeographicLocationContainer - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const GeographicLocationContainer& p, Document::AllocatorType& allocator);



/*
*   RccPart - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const RccPart& p, Document::AllocatorType& allocator);



/*
*   TcPart - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const TcPart& p, Document::AllocatorType& allocator);



/*
*   ISO14823Attributes - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ISO14823Attributes& p, Document::AllocatorType& allocator);



/*
*   AnyCatalogue - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const AnyCatalogue& p, Document::AllocatorType& allocator);



/*
*   ISO14823Code - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const ISO14823Code& p, Document::AllocatorType& allocator);



/*
*   RSCode::RSCode__code - Type CHOICE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const RSCode::RSCode__code& p, Document::AllocatorType& allocator);



/*
*   RSCode - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const RSCode& p, Document::AllocatorType& allocator);



/*
*   CAM - Type SEQUENCE
*   From CAM-PDU-Descriptions - File EN302637-2v141-CAM.asn
*/

Value to_json(const CAM& p, Document::AllocatorType& allocator);



/*
*   ClusterBoundingBoxShape - Type CHOICE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const ClusterBoundingBoxShape& p, Document::AllocatorType& allocator);



/*
*   PerceivedObjects - Type SEQUENCE OF
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const PerceivedObjects& p, Document::AllocatorType& allocator);



/*
*   SPATEM - Type SEQUENCE
*   From SPATEM-PDU-Descriptions - File TS103301v211-SPATEM.asn
*/

Value to_json(const SPATEM& p, Document::AllocatorType& allocator);



/*
*   SSEM - Type SEQUENCE
*   From SSEM-PDU-Descriptions - File TS103301v211-SSEM.asn
*/

Value to_json(const SSEM& p, Document::AllocatorType& allocator);



/*
*   ItsChargingStationData - Type SEQUENCE
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const ItsChargingStationData& p, Document::AllocatorType& allocator);



/*
*   ImzmContainer - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const ImzmContainer& p, Document::AllocatorType& allocator);



/*
*   InterferenceManagementZones - Type SEQUENCE OF
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const IMZM_PDU_Descriptions_InterferenceManagementZones_t& p, Document::AllocatorType& allocator);



/*
*   ZoneDefinition - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const ZoneDefinition& p, Document::AllocatorType& allocator);



/*
*   PlacardTable - Type SEQUENCE OF
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const PlacardTable& p, Document::AllocatorType& allocator);



/*
*   VehiclemaneuverContainer::VehiclemaneuverContainer__vehicleTrajectory - Type CHOICE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const VehiclemaneuverContainer::VehiclemaneuverContainer__vehicleTrajectory& p, Document::AllocatorType& allocator);



/*
*   Maneuver - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const Maneuver& p, Document::AllocatorType& allocator);



/*
*   MapData - Type SEQUENCE
*   From DSRC - File DSRC.asn
*/

Value to_json(const MapData_t& p, Document::AllocatorType& allocator);



/*
*   GddAttributes - Type SEQUENCE OF
*   From GDD - File ISO14823.asn
*/

Value to_json(const GddAttributes& p, Document::AllocatorType& allocator);



/*
*   RoadConfigurationContainer - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const RoadConfigurationContainer& p, Document::AllocatorType& allocator);



/*
*   TextContainer - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const TextContainer& p, Document::AllocatorType& allocator);



/*
*   RoadSignCodes - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const RoadSignCodes& p, Document::AllocatorType& allocator);



/*
*   AutomatedVehicleRule - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const AutomatedVehicleRule& p, Document::AllocatorType& allocator);



/*
*   PlatooningRule - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const PlatooningRule& p, Document::AllocatorType& allocator);



/*
*   VruClusterInformationContainer - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VruClusterInformationContainer& p, Document::AllocatorType& allocator);



/*
*   PerceivedObjectContainer - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const PerceivedObjectContainer_t& p, Document::AllocatorType& allocator);



/*
*   MAPEM - Type SEQUENCE
*   From MAPEM-PDU-Descriptions - File TS103301v211-MAPEM.asn
*/

Value to_json(const MAPEM& p, Document::AllocatorType& allocator);



/*
*   ItsEVCSNData::ItsEVCSNData__chargingStationsData - Type SEQUENCE OF
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const ItsEVCSNData::ItsEVCSNData__chargingStationsData& p, Document::AllocatorType& allocator);



/*
*   ItsEVCSNData - Type SEQUENCE
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const ItsEVCSNData& p, Document::AllocatorType& allocator);



/*
*   ImzmParameters - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const ImzmParameters& p, Document::AllocatorType& allocator);



/*
*   InterferenceManagementZone - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const IMZM_PDU_Descriptions_InterferenceManagementZone_t& p, Document::AllocatorType& allocator);



/*
*   TisTpgVDPM - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgVDPM& p, Document::AllocatorType& allocator);



/*
*   Maneuvers - Type SEQUENCE OF
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const Maneuvers& p, Document::AllocatorType& allocator);



/*
*   GddStructure - Type SEQUENCE
*   From GDD - File ISO14823.asn
*/

Value to_json(const GddStructure& p, Document::AllocatorType& allocator);



/*
*   GicPart - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const GicPart& p, Document::AllocatorType& allocator);



/*
*   AutomatedVehicleRules - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const AutomatedVehicleRules& p, Document::AllocatorType& allocator);



/*
*   PlatooningRules - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const PlatooningRules& p, Document::AllocatorType& allocator);



/*
*   VamParameters - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VamParameters& p, Document::AllocatorType& allocator);



/*
*   WrappedCpmContainer::WrappedCpmContainer__containerData - Type CHOICE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const WrappedCpmContainer::WrappedCpmContainer__containerData& p, Document::AllocatorType& allocator);



/*
*   WrappedCpmContainer - Type SEQUENCE
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const WrappedCpmContainer& p, Document::AllocatorType& allocator);



/*
*   WrappedCpmContainers - Type SEQUENCE OF
*   From CPM-PDU-Descriptions - File CPM-PDU-Descriptions.asn
*/

Value to_json(const WrappedCpmContainers& p, Document::AllocatorType& allocator);



/*
*   EVChargingSpotNotificationPOIMessage - Type SEQUENCE
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const EVChargingSpotNotificationPOIMessage& p, Document::AllocatorType& allocator);



/*
*   InterferenceManagementZoneMessage - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const InterferenceManagementZoneMessage& p, Document::AllocatorType& allocator);



/*
*   TisTpgTransaction - Type CHOICE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTransaction& p, Document::AllocatorType& allocator);



/*
*   ManeuverAdviceContainer - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const ManeuverAdviceContainer& p, Document::AllocatorType& allocator);



/*
*   GeneralIviContainer - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const GeneralIviContainer& p, Document::AllocatorType& allocator);



/*
*   AvcPart - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const AvcPart& p, Document::AllocatorType& allocator);



/*
*   VruAwareness - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VruAwareness& p, Document::AllocatorType& allocator);



/*
*   EvcsnPdu - Type SEQUENCE
*   From EVCSN-PDU-Descriptions - File EVCSN-PDU-Descriptions.asn
*/

Value to_json(const EvcsnPdu& p, Document::AllocatorType& allocator);



/*
*   IMZM - Type SEQUENCE
*   From IMZM-PDU-Descriptions - File IMZM-PDU-Descriptions.asn
*/

Value to_json(const IMZM& p, Document::AllocatorType& allocator);



/*
*   TisTpgTransactionsPdu - Type SEQUENCE
*   From TIS-TPG-Transactions-Descriptions - File TIS-TPG-Transactions-Descriptions.asn
*/

Value to_json(const TisTpgTransactionsPdu& p, Document::AllocatorType& allocator);



/*
*   VehiclemaneuverContainer - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const VehiclemaneuverContainer& p, Document::AllocatorType& allocator);



/*
*   IviContainer - Type CHOICE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const IviContainer& p, Document::AllocatorType& allocator);



/*
*   AutomatedVehicleContainer - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const AutomatedVehicleContainer& p, Document::AllocatorType& allocator);



/*
*   VAM - Type SEQUENCE
*   From VAM-PDU-Descriptions - File TS103300-3v211-VAM.asn
*/

Value to_json(const VAM& p, Document::AllocatorType& allocator);



/*
*   McmContainer - Type CHOICE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const McmContainer& p, Document::AllocatorType& allocator);



/*
*   IviContainers - Type SEQUENCE OF
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const IviContainers& p, Document::AllocatorType& allocator);



/*
*   WrappedMcmInformationBlocks - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const WrappedMcmInformationBlocks& p, Document::AllocatorType& allocator);



/*
*   IviStructure - Type SEQUENCE
*   From IVI - File ISO19321IVIv2.asn
*/

Value to_json(const IviStructure& p, Document::AllocatorType& allocator);



/*
*   IVIM - Type SEQUENCE
*   From IVIM-PDU-Descriptions - File TS103301v211-IVIM.asn
*/

Value to_json(const IVIM& p, Document::AllocatorType& allocator);



/*
*   MCM - Type SEQUENCE
*   From MCM-PDU-Descriptions - File MCM-PDU-Descriptions.asn
*/

Value to_json(const MCM& p, Document::AllocatorType& allocator);



#endif
