/* Standard Library Includes */
#include <iostream>
#include <fstream>
#include <string>
#include <filesystem>

/* Boost Includes */
#include <boost/optional.hpp>

/* PcapPlusPlus Includes */
#include <pcap.h>

/* Vanetza Includes */
#include "vanetza/asn1/packet_visitor.hpp"
#include "vanetza/asn1/denm.hpp"
#include "vanetza/asn1/cam.hpp"
#include "vanetza/asn1/spatem.hpp"
#include "vanetza/asn1/mapem.hpp"
#include "vanetza/asn1/ssem.hpp"
#include "vanetza/asn1/srem.hpp"

#include "vanetza/geonet/basic_header.hpp"
#include "vanetza/geonet/secured_pdu.hpp"
#include "vanetza/geonet/common_header.hpp"
#include "vanetza/geonet/header_variant.hpp"
#include "vanetza/btp/header.hpp"
#include "vanetza/geonet/parser.hpp"
#include "vanetza/net/osi_layer.hpp"

/* Local Includes */
#include "subparsers.hpp"
#include "indicator.h"
#include "message_id.h"

#include "build_json.h"


boost::optional<std::string> parse_msg(
    const uint8_t* pBuffer,
    size_t bufferLen,
    double time_reception
) {

    json_context_t context {
            .time_reception = time_reception,
    };


    IEEE_QoS_t* pIeeeQos;

    parse_radiotap(nullptr, &pBuffer, &bufferLen, pBuffer, bufferLen);
    parse_ieee_qos(&pIeeeQos, &pBuffer, &bufferLen, pBuffer, bufferLen);
    parse_logical_link_control(nullptr, &pBuffer, &bufferLen, pBuffer, bufferLen);

    vanetza::ByteBuffer byteBuffer(pBuffer, pBuffer + bufferLen);

    auto packet = std::make_unique<vanetza::geonet::UpPacket>(vanetza::CohesivePacket { std::move(byteBuffer), vanetza::OsiLayer::Network });
    
    std::initializer_list<uint8_t> sender = {
            pIeeeQos->source_mac[0],
            pIeeeQos->source_mac[1],
            pIeeeQos->source_mac[2],
            pIeeeQos->source_mac[3],
            pIeeeQos->source_mac[4],
            pIeeeQos->source_mac[5],
    };
    std::initializer_list<uint8_t> destination = {
            pIeeeQos->destination_mac[0],
            pIeeeQos->destination_mac[1],
            pIeeeQos->destination_mac[2],
            pIeeeQos->destination_mac[3],
            pIeeeQos->destination_mac[4],
            pIeeeQos->destination_mac[5],
    };

    auto finishedPacketPtr = indicate(std::move(packet), sender, destination);

    if (finishedPacketPtr == nullptr) {
        return boost::none;
    }

    uint8_t message_id;

    if (auto cohesive = boost::get<vanetza::CohesivePacket>(finishedPacketPtr.get())) {
        // message id is always on the second byte :)
        message_id =  (*cohesive)[vanetza::OsiLayer::Session][1];
    } else {
        return boost::none;
    }

    auto finishedPacket = *finishedPacketPtr;

    Document json_document;


#define VISIT(name, vanetza_struct, content_name, id) {\
    vanetza::asn1::PacketVisitor<vanetza::asn1::name> visitor;\
    std::shared_ptr<const vanetza::asn1::name> visited = boost::apply_visitor(visitor, *finishedPacketPtr);\
    if (visited != nullptr && (*visited)->header.messageID == id) {\
        vanetza_struct cStruct = {(*visited)->header, (*visited)->content_name};\
        json_document = buildJSON(cStruct, context);\
        found = true;\
    }\
}
    bool found = false;
    switch(message_id) {
        case MESSAGE_ID_DENM:
            VISIT(Denm, DENM_t, denm, MESSAGE_ID_DENM);
            break;
        case MESSAGE_ID_CAM:
            VISIT(Cam, CAM_t, cam, MESSAGE_ID_CAM);
            break;
        case MESSAGE_ID_POIM:
            break;
        case MESSAGE_ID_SPATEM:
            VISIT(Spatem, SPATEM_t, spat, MESSAGE_ID_SPATEM);
            break;
        case MESSAGE_ID_MAPEM:
            VISIT(Mapem, MAPEM_t, map, MESSAGE_ID_MAPEM);
            break;
        case MESSAGE_ID_IVIM:
        case MESSAGE_ID_RFU1:
        case MESSAGE_ID_RFU2:
            break;
        case MESSAGE_ID_SREM:
            VISIT(Srem, SREM_t, srm, MESSAGE_ID_SREM);
            break;
        case MESSAGE_ID_SSEM:
            VISIT(Ssem, SSEM_t, ssm, MESSAGE_ID_SSEM);
            break;
        case MESSAGE_ID_EVCSN:
        case MESSAGE_ID_SAEM:
        case MESSAGE_ID_RTCMEM:
        case MESSAGE_ID_CPM:
        case MESSAGE_ID_IMZM:
        case MESSAGE_ID_VAM:
        case MESSAGE_ID_DSM:
        case MESSAGE_ID_PCIM:
        case MESSAGE_ID_PCVM:
        case MESSAGE_ID_MCM:
        case MESSAGE_ID_PAM:
        default:
            break;
    }

    if (!found) {
        return boost::none;
    }


    // This is just conversion to string to simplify my life.
    StringBuffer strbuf;
    strbuf.Clear();

    Writer<StringBuffer> writer(strbuf);
    json_document.Accept(writer);

    std::string out = strbuf.GetString();

    return out;
}

typedef struct user_data {
    std::ofstream* output_stream;
    size_t message_counter;
} user_data_t;

void pcap_loop_callback(u_char *pUserData, const struct pcap_pkthdr *pkthdr, const unsigned char *packet) {
    auto userData = (user_data_t *) (pUserData);

    double timestamp = (double)(pkthdr->ts.tv_sec) + (double)(pkthdr->ts.tv_usec) / 1e6;
    auto res = parse_msg(packet, pkthdr->len, timestamp);

    if ( res.has_value() ) {
        if (userData->message_counter > 0) {
            *userData->output_stream << ",";
        }
        *userData->output_stream << res.value();
        userData->message_counter += 1;
    }

}

// etsi-its-parser [input file] [output file]
int main(int argc, char **argv) {

    if (argc != 3) {
        std::cerr
                << "ERROR: Not enough arguments provided, provide them in format etsi-its-parser [input file] [output file]."
                << std::endl;

        return EXIT_FAILURE;
    }

    {
        std::filesystem::path inputFilePath(argv[1]);

        if (!std::filesystem::exists(inputFilePath)) {
            std::cerr << "ERROR: Input file does not exist." << std::endl;

            return EXIT_FAILURE;
        }
    }

    std::ofstream outputFile(argv[2]);
    if (!outputFile.is_open()) {
        std::cerr << "ERROR: Could not open output file." << std::endl;
    }

    user_data_t userData {
        .output_stream = &outputFile,
        .message_counter = 0,
    };

    char pcap_errbuf[PCAP_ERRBUF_SIZE];
    auto pcap_fp = pcap_open_offline(argv[1], pcap_errbuf);

    if (pcap_fp == nullptr) {
        std::cerr << "ERROR: Failure to open pcap file: " << pcap_errbuf << std::endl;

        return EXIT_FAILURE;
    }

    outputFile << "[";
    if (pcap_loop(pcap_fp, 0, pcap_loop_callback, (u_char *) &userData) < 0) {
        std::cerr << "ERROR: There was error in pcap_loop: " << pcap_errbuf << std::endl;
    }
    outputFile << "]";

    std::cout << "Parsed " << userData.message_counter << " messages." << std::endl;
    
    return EXIT_SUCCESS;
}