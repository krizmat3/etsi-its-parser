#include "subparsers.hpp"

int parse_radiotap(
    radiotap_header_t** ppRadiotapHeader,

    const uint8_t** ppBuffer_new,
    size_t* pBufLen_new,

    const uint8_t* pBuffer,
    size_t bufLen) {
    if (bufLen < sizeof(radiotap_header_t)) {
        return EXIT_FAILURE;
    }

    auto radiotap = *(radiotap_header_t*)pBuffer;

    if (bufLen < sizeof(radiotap_header_t) + radiotap.vendor_data_length) {
        return EXIT_FAILURE;
    }

    if (ppRadiotapHeader) {
        *ppRadiotapHeader = (radiotap_header_t*)pBuffer;
    }

    if (ppBuffer_new) {
        *ppBuffer_new = &pBuffer[sizeof(radiotap_header_t) + radiotap.vendor_data_length];
    }

    if (pBufLen_new) {
        *pBufLen_new = bufLen - sizeof(radiotap_header_t) - radiotap.vendor_data_length;
    }

    return EXIT_SUCCESS;
}

int parse_ieee_qos(
    IEEE_QoS_t** ppIeeeQos,

    const uint8_t** ppBuffer_new,
    size_t* pBufLen_new,

    const uint8_t* pBuffer,
    size_t bufLen) {
    if (bufLen < sizeof(IEEE_QoS_t)) {
        return EXIT_FAILURE;
    }

    if (ppIeeeQos) {
        *ppIeeeQos = (IEEE_QoS_t*)pBuffer;
    }

    if (ppBuffer_new) {
        *ppBuffer_new = &pBuffer[sizeof(IEEE_QoS_t)];
    }

    if (pBufLen_new) {
        *pBufLen_new = bufLen - sizeof(IEEE_QoS_t);
    }

    return EXIT_SUCCESS;
}

int parse_logical_link_control(
    logical_link_control_t** ppLogicalLinkControl,

    const uint8_t** ppBuffer_new,
    size_t* pBufLen_new,

    const uint8_t* pBuffer,
    size_t bufLen) {
    if (bufLen < sizeof(logical_link_control_t)) {
        return EXIT_FAILURE;
    }

    if (ppLogicalLinkControl) {
        *ppLogicalLinkControl = (logical_link_control_t*)pBuffer;
    }

    if (ppBuffer_new) {
        *ppBuffer_new = &pBuffer[sizeof(logical_link_control_t)];
    }

    if (pBufLen_new) {
        *pBufLen_new = bufLen - sizeof(logical_link_control_t);
    }

    return EXIT_SUCCESS;
}